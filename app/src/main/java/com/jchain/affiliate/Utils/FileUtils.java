package com.jchain.affiliate.Utils;

import android.content.Context;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public final class FileUtils {
    private static final String TAG = "FileUtils";

    private static FileUtils sInstance = null;
    public static FileUtils getInstance(final Context context) {
        if (sInstance == null) {
            sInstance = new FileUtils(context);
        }
        return sInstance;
    }

    private final File mCacheDir;
    private final File mFilesDir;
    private FileUtils(final Context context) {
        mCacheDir = context.getCacheDir();
        mFilesDir = context.getFilesDir();
    }

    public File buildFilePath(final String relatedPath) {
        final int relatedDirEndPos = relatedPath.lastIndexOf(File.separator);
        boolean mkdirRet = true;
        if (relatedDirEndPos > 0) {
            final File relatedDir = new File(mFilesDir.getAbsolutePath()+File.separator+relatedPath.substring(0, relatedDirEndPos));
            if (!relatedDir.exists()) {
                mkdirRet = relatedDir.mkdirs();
            }
        }
        if (mkdirRet) {
            return new File(mFilesDir.getAbsolutePath() + File.separator + relatedPath);
        } else {
            Log.e(TAG, "Failed to create relatedPath: "+relatedPath);
            return null;
        }
    }

    private boolean saveFileInternal(final File file, final byte[] fileData) {
        if (file == null) {
            return false;
        }
        BufferedOutputStream bos = null;
        try {
            bos = new BufferedOutputStream(new FileOutputStream(file));
            bos.write(fileData);
            return true;
        } catch (Exception e) {
            Log.e(TAG, "Failed to save File "+file.getAbsolutePath(), e);
            return false;
        } finally {
            try {
                if (bos != null) {
                    bos.close();
                }
            } catch (IOException e) {
                Log.w(TAG, "Failed to close File "+file.getAbsolutePath()+" for writing", e);
            }
        }
    }

    private static final int DEFAULT_BUFSIZE = 1024;
    private String readFileInternal(final File file) {
        if (file == null) {
            return null;
        }
        BufferedInputStream bis = null;
        try {
            final StringBuilder fileData = new StringBuilder();
            bis = new BufferedInputStream(new FileInputStream(file));
            byte[] buffer = new byte[DEFAULT_BUFSIZE];
            int readCount = 0;
            while((readCount = bis.read(buffer)) >= 0) {
                if (readCount == 0) {
                    continue;
                }
                String data = new String(buffer, 0, readCount, "UTF-8");
                fileData.append(data);
            }
            return fileData.toString();
        } catch (Exception e) {
            Log.e(TAG, "Failed to read File "+file.getAbsolutePath(), e);
            return null;
        } finally {
            try {
                if (bis != null) {
                    bis.close();
                }
            } catch (IOException e) {
                Log.w(TAG, "Failed to close File "+file.getAbsolutePath()+" for reading", e);
            }
        }
    }

    public boolean saveFile(final String relatedPath, final byte[] fileData) {
        final File file = buildFilePath(relatedPath);
        return saveFileInternal(file, fileData);
    }

    public String readFile(final String relatedPath) {
        final File file = buildFilePath(relatedPath);
        return readFileInternal(file);
    }

    public boolean deleteFile(final String relatedPath) {
        final File file = buildFilePath(relatedPath);
        return file.delete();
    }
}
