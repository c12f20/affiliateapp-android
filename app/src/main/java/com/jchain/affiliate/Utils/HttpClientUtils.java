package com.jchain.affiliate.Utils;

import android.content.Context;
import androidx.annotation.NonNull;
import android.util.Log;

import com.jchain.affiliate.R;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

public final class HttpClientUtils {
    private static final String TAG = "HttpClientUtils";
    private final OkHttpClient mHttpClient;

    public HttpClientUtils(final Context context) {
        mHttpClient = buildTrustClient(context);
    }

    public OkHttpClient getOkHttpClient() {
        return mHttpClient;
    }

    private OkHttpClient buildTrustClient(final Context context) {
        final InputStream[] certificateInputs = new InputStream[] {
            context.getResources().openRawResource(R.raw.jchaintech),
        };
        try {
            final TrustManager[] trustManagers = buildFakeTrustManagers();
            final SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, trustManagers, new SecureRandom());
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
            return new OkHttpClient.Builder()
                    .sslSocketFactory(sslSocketFactory, (X509TrustManager) trustManagers[0])
                    .build();
        } catch (GeneralSecurityException e) {
            Log.e(TAG, "Failed to build trust client, exception: ", e);
            return buildGeneralClient();
        }
    }

    private OkHttpClient buildGeneralClient() {
        return new OkHttpClient.Builder()
                .build();
    }

    private TrustManager[] buildFakeTrustManagers() {
        X509TrustManager trustManager = new X509TrustManager() {
            @Override
            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            }

            @Override
            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            }

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }
        };
        return new TrustManager[]{trustManager};
    }

    private TrustManager[] buildTrustManagers(InputStream... inputStreams) throws GeneralSecurityException {
        final CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
        // Build a KeyStore with input certificate files
        final KeyStore keyStore = newEmptyKeyStore();
        if (keyStore == null) {
            return null;
        }
        int index = 0;
        for (InputStream is : inputStreams) {
            final Certificate certificate = certificateFactory.generateCertificate(is);
            if (certificate == null) {
                Log.w(TAG, "Failed to generate one certificate");
                continue;
            }
            final String certificateAlias = Integer.toString(index++);
            keyStore.setCertificateEntry(certificateAlias, certificate);
        }
        if (index == 0) { // No certificate is set in KeyStore
            Log.e(TAG, "No certificate is set in KeyStore, can't build TrustManager");
            return null;
        }
        // Build TrustManager
        final TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        trustManagerFactory.init(keyStore);
        final TrustManager[] trustManagers = trustManagerFactory.getTrustManagers();
        if (trustManagers.length != 1 || !(trustManagers[0] instanceof X509TrustManager)) {
            Log.e(TAG, "Generate an invalid TrustManager");
            return null;
        }
        return trustManagers;
    }

    private KeyStore newEmptyKeyStore() throws GeneralSecurityException {
        KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
        try {
            keyStore.load(null);
            return keyStore;
        } catch(IOException e) {
            Log.e(TAG, "Failed to create Key Store", e);
        }
        return null;
    }

    private static final MediaType MEDIA_TYPE_JSON = MediaType.parse("application/json; charset=utf-8");

    public interface ResponseListener {
        void onResponseData(final String response);
    }

    public void cancelCall(final Call call) {
        if (call != null && !call.isCanceled()) {
            call.cancel();
        }
    }

    private Call buildCall(final Request request, final ResponseListener listener) {
        final Call call = mHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                Log.e(TAG, "Failed to get response, meet exception:", e);
                if (!call.isCanceled() && listener != null) {
                    listener.onResponseData(null);
                }
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                String responseData = null;
                ResponseBody body = response.body();
                if (body != null) {
                    responseData = body.string();
                }
                Log.d(TAG, "Got response: " + responseData);
                if (!call.isCanceled() && listener != null) {
                    listener.onResponseData(responseData);
                }
            }
        });
        return call;
    }

    private Request.Builder createBasicRequestBuilder(final String bearerToken, final String[] headNames, final String[] headValues) {
        final Request.Builder builder = new Request.Builder();
        if (bearerToken != null) {
            builder.addHeader("Authorization", "Bearer "+bearerToken);
        }
        if (headNames != null && headValues != null) {
            for (int i = 0; i < headNames.length; i++) {
                builder.addHeader(headNames[i], headValues[i]);
            }
        }
        return builder;
    }

    private RequestBody createMultipartBody(final String name, final String fileName,
                                            final File file, final MediaType mediaType) {
        final MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        builder.addFormDataPart(name, fileName, RequestBody.create(mediaType, file));
        return builder.build();
    }

    public Call postJSONData(final String url, final String json, final ResponseListener listener) {
        return postJSONData(url, null, null, json, listener);
    }

    public Call postJSONData(final String url, final String token, final String json, final ResponseListener listener) {
        return postJSONData(url, token, null, null, json, listener);
    }

    public Call postJSONData(final String url, final String[] headNames, final String[] headValues, final String json, final ResponseListener listener) {
        return postJSONData(url, null, headNames, headValues, json, listener);
    }

    private Call postJSONData(final String url, final String token,
                             final String[] headNames, final String[] headValues,
                             final String json, final ResponseListener listener) {
        final Request.Builder builder = createBasicRequestBuilder(token, headNames, headValues);
        final RequestBody requestBody = RequestBody.create(MEDIA_TYPE_JSON, json);
        builder.url(url).post(requestBody);
        final Request request = builder.build();
        Log.d(TAG, "POST JSON Data: " + json + " to URL: " + url);
        return buildCall(request, listener);
    }

    public Call postFileData(final String url, final String token, final String name, final String fileName,
                             final MediaType mediaType, final File file, final ResponseListener listener) {
        final Request.Builder builder = createBasicRequestBuilder(token, null, null);
        final RequestBody requestBody = createMultipartBody(name, fileName, file, mediaType);
        builder.url(url).post(requestBody);
        final Request request = builder.build();
        Log.d(TAG, "POST File: " + file.getAbsolutePath() + ", size: " + file.length() + " to URL: " + url);
        return buildCall(request, listener);
    }

    public Call getJSONData(final String url, final ResponseListener listener) {
        return getJSONData(url, null, null, listener);
    }

    public Call getJSONData(final String url, final String token, final ResponseListener listener) {
        return getJSONData(url, token, null, null, listener);
    }

    public Call getJSONData(final String url, final String[] headNames, final String[] headValues, final ResponseListener listener) {
        return getJSONData(url, null, headNames, headValues, listener);
    }

    private Call getJSONData(final String url, final String token,
                            final String[] headNames, final String[] headValues,
                            final ResponseListener listener) {
        final Request.Builder builder = createBasicRequestBuilder(token, headNames, headValues);
        builder.url(url).get();
        final Request request = builder.build();
        Log.d(TAG, "GET JSON Data to URL: " + url);
        return buildCall(request, listener);
    }

    public Call putJSONData(final String url, final String json, final ResponseListener listener) {
        return putJSONData(url, null, null, json, listener);
    }

    public Call putJSONData(final String url, final String token, final String json, final ResponseListener listener) {
        return putJSONData(url, token, null, null, json, listener);
    }

    public Call putJSONData(final String url, final String[] headNames, final String[] headValues, final String json, final ResponseListener listener) {
        return putJSONData(url, null, headNames, headValues, json, listener);
    }

    private Call putJSONData(final String url, final String token,
                            final String[] headNames, final String[] headValues,
                            final String json, final ResponseListener listener) {
        final Request.Builder builder = createBasicRequestBuilder(token, headNames, headValues);
        final RequestBody requestBody = RequestBody.create(MEDIA_TYPE_JSON, json);
        builder.url(url).put(requestBody);
        final Request request = builder.build();
        Log.d(TAG, "PUT JSON Data: "+json + " to URL: "+url);
        return buildCall(request, listener);
    }
}
