package com.jchain.affiliate;

import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationManagerCompat;

import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.security.ProviderInstaller;
import com.jchain.affiliate.Category.CategoryFragment;
import com.jchain.affiliate.Home.HomeFragment;
import com.jchain.affiliate.Product.ProductInfoManager;
import com.jchain.affiliate.Product.ProductListFragment;
import com.jchain.affiliate.Tracker.TrackerCheckService;
import com.jchain.affiliate.Tracker.TrackerListFragment;
import com.jchain.affiliate.UI.Fragments.BasicWebViewFragment;
import com.jchain.affiliate.Settings.SettingsActivity;
import com.jchain.affiliate.UI.Activities.BaseActivityWithDrawer;
import com.jchain.affiliate.UI.Fragments.BaseFragmentWithDrawer;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.Nameable;

import java.util.ArrayList;

public final class MainActivity extends BaseActivityWithDrawer {
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView(savedInstanceState);

        checkNotificationEnabled();

        updateSSLProvider();

        processIntent();
    }

    @Override
    protected void initView(final Bundle savedInstanceState) {
        super.initView(savedInstanceState);
        initFragment();
    }

    public static final String PARAM_NAME_SHOW_TRACKS_DIRECTLY = "Param.ShowTracksDirectly";
    private void processIntent() {
        final Intent intent = getIntent();
        final boolean showTracksDirectly = intent.getBooleanExtra(PARAM_NAME_SHOW_TRACKS_DIRECTLY, false);
        if (showTracksDirectly) {
            switchToTrackerListFragment();
        }
    }

    public static final long DRAWER_ITEM_ID_HOME = 1;
    public static final long DRAWER_ITEM_ID_CATEGORIES = 2;
    public static final long DRAWER_ITEM_ID_POPULAR_PRODUCTS = 3;
    public static final long DRAWER_ITEM_ID_FAVORITES = 4;
    public static final long DRAWER_ITEM_ID_RECOMMEND_PRODUCTS = 5;
    public static final long DRAWER_ITEM_ID_TRACK_PRODUCTS = 6;
    public static final long DRAWER_ITEM_ID_SETTINGS = 7;
    public static final long DRAWER_ITEM_ID_PRIVACY_POLICY = 8;
    public static final long DRAWER_ITEM_ID_RATE_US = 9;
    public static final long DRAWER_ITEM_ID_ABOUT_US = 10;

    @Override
    protected void initDrawer(final Bundle savedInstanceState) {
        mDrawerHeader = buildProfileDrawerHeader(savedInstanceState);

        final ArrayList<IDrawerItem> drawerItems = new ArrayList<IDrawerItem>();
        drawerItems.add(new PrimaryDrawerItem().withIdentifier(DRAWER_ITEM_ID_HOME)
                .withName(R.string.ID_DRAWER_OPTION_HOME).withIcon(GoogleMaterial.Icon.gmd_home));
        drawerItems.add(new PrimaryDrawerItem().withIdentifier(DRAWER_ITEM_ID_CATEGORIES)
                .withName(R.string.ID_DRAWER_OPTION_CATEGORIES).withIcon(GoogleMaterial.Icon.gmd_menu));
        drawerItems.add(new PrimaryDrawerItem().withIdentifier(DRAWER_ITEM_ID_POPULAR_PRODUCTS)
                .withName(R.string.ID_DRAWER_OPTION_POPULAR_PRODUCTS).withIcon(GoogleMaterial.Icon.gmd_party_mode));
        drawerItems.add(new PrimaryDrawerItem().withIdentifier(DRAWER_ITEM_ID_FAVORITES)
                .withName(R.string.ID_DRAWER_OPTION_FAVORITES).withIcon(GoogleMaterial.Icon.gmd_favorite));
        if (mAccountMng.hasAccountLogin()) {
            drawerItems.add(new PrimaryDrawerItem().withIdentifier(DRAWER_ITEM_ID_RECOMMEND_PRODUCTS)
                    .withName(R.string.ID_DRAWER_OPTION_RECOMMEND_PRODUCTS).withIcon(GoogleMaterial.Icon.gmd_flash_on));
            drawerItems.add(new PrimaryDrawerItem().withIdentifier(DRAWER_ITEM_ID_TRACK_PRODUCTS)
                    .withName(R.string.ID_DRAWER_OPTION_TRACK_PRODUCTS).withIcon(GoogleMaterial.Icon.gmd_trending_down));
        }
        drawerItems.add(new DividerDrawerItem());
        drawerItems.add(new SecondaryDrawerItem().withIdentifier(DRAWER_ITEM_ID_SETTINGS)
                .withName(R.string.ID_DRAWER_OPTION_SETTINGS).withIcon(GoogleMaterial.Icon.gmd_settings));
        drawerItems.add(new SecondaryDrawerItem().withIdentifier(DRAWER_ITEM_ID_PRIVACY_POLICY)
                .withName(R.string.ID_DRAWER_OPTION_PRIVACY_POLICY).withIcon(GoogleMaterial.Icon.gmd_security));
        drawerItems.add(new SecondaryDrawerItem().withIdentifier(DRAWER_ITEM_ID_RATE_US)
                .withName(R.string.ID_DRAWER_OPTION_RATE_US).withIcon(GoogleMaterial.Icon.gmd_rate_review));
        drawerItems.add(new SecondaryDrawerItem().withIdentifier(DRAWER_ITEM_ID_ABOUT_US)
                .withName(R.string.ID_DRAWER_OPTION_ABOUT_US).withIcon(GoogleMaterial.Icon.gmd_info));
        final IDrawerItem[] drawerItemsArray = new IDrawerItem[drawerItems.size()];
        drawerItems.toArray(drawerItemsArray);

        mDrawer = new DrawerBuilder()
                .withActivity(this)
                .withSavedInstance(savedInstanceState)
                .withDrawerLayout(R.layout.material_drawer_fits_not)
                .withAccountHeader(mDrawerHeader)
                .addDrawerItems(drawerItemsArray)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        if (drawerItem instanceof Nameable) {
                            final long drawerItemId = drawerItem.getIdentifier();
                            if (mSelectedItemId == drawerItemId) {
                                return false;
                            }
                            if (drawerItemId == DRAWER_ITEM_ID_HOME) {
                                switchToHomeFragment();
                            } else if (drawerItemId == DRAWER_ITEM_ID_CATEGORIES) {
                                switchToCategoryFragment();
                            } else if (drawerItemId == DRAWER_ITEM_ID_POPULAR_PRODUCTS) {
                                switchToProductListFragment(ProductListFragment.LIST_FROM_HOT_PRODUCTS);
                            } else if (drawerItemId == DRAWER_ITEM_ID_FAVORITES) {
                                switchToProductListFragment(ProductListFragment.LIST_FROM_BEST_DEALS);
                            } else if (drawerItemId == DRAWER_ITEM_ID_RECOMMEND_PRODUCTS) {
                                switchToProductListFragment(ProductListFragment.LIST_FROM_RECOMMEND_PRODUCTS);
                            } else if (drawerItemId == DRAWER_ITEM_ID_TRACK_PRODUCTS) {
                                switchToTrackerListFragment();
                            } else if (drawerItemId == DRAWER_ITEM_ID_SETTINGS) {
                                gotoSettings();
                                return false;
                            } else if (drawerItemId == DRAWER_ITEM_ID_PRIVACY_POLICY) {
                                switchToWebViewFragment(getString(R.string.ID_TITLE_PRIVACY_POLICY), R.raw.privacy_policy);
                            } else if (drawerItemId == DRAWER_ITEM_ID_RATE_US) {
                                gotoRateUs();
                                return false;
                            } else if (drawerItemId == DRAWER_ITEM_ID_ABOUT_US) {
                                switchToWebViewFragment(getString(R.string.ID_TITLE_ABOUT_US), R.raw.about_us);
                            } else {
                                Toast.makeText(getBaseContext(), ((Nameable) drawerItem).getName().getText(getBaseContext()), Toast.LENGTH_SHORT).show();
                            }
                            mSelectedItemId = drawerItemId;
                        }
                        return false;
                    }
                })
                .withOnDrawerListener(new Drawer.OnDrawerListener() {
                    @Override
                    public void onDrawerOpened(View drawerView) {
                        onDrawerStatusChanged(true);
                    }

                    @Override
                    public void onDrawerClosed(View drawerView) {
                        onDrawerStatusChanged(false);
                    }

                    @Override
                    public void onDrawerSlide(View drawerView, float slideOffset) {

                    }
                })
                .withTranslucentStatusBar(false)
                .build();
    }

    private void onDrawerStatusChanged(final boolean isOpened) {
        if (mCurFragment instanceof BaseFragmentWithDrawer) {
            final BaseFragmentWithDrawer fragmentWithDrawer = (BaseFragmentWithDrawer) mCurFragment;
            fragmentWithDrawer.onDrawerStatusChanged(isOpened);
        }
    }

    // Fragments controlled by Drawer
    private void initFragment() {
        final HomeFragment homeFragment = HomeFragment.newInstance(false);
        showFragment(homeFragment, false);
    }

    private void switchToHomeFragment() {
        final HomeFragment homeFragment = HomeFragment.newInstance(true);
        showFragment(homeFragment, true);
    }

    private void switchToCategoryFragment() {
        final CategoryFragment categoryFragment = CategoryFragment.newInstance();
        showFragment(categoryFragment, true);
    }

    private void switchToProductListFragment(final @ProductListFragment.ListFrom int listFrom) {
        ProductInfoManager.getInstance(getBaseContext()).resetProductList(); // always reset product list when entering product list
        final ProductListFragment productListFragment = ProductListFragment.newInstance(listFrom);
        showFragment(productListFragment, true);
    }

    private void switchToTrackerListFragment() {
        final TrackerListFragment trackerListFragment = TrackerListFragment.newInstance();
        showFragment(trackerListFragment, true);
    }

    private void switchToWebViewFragment(final String title, final int dataResId) {
        final BasicWebViewFragment webViewFragment = BasicWebViewFragment.newInstance(title, dataResId);
        showFragment(webViewFragment, true);
    }

    private void gotoSettings() {
        final Intent intent = new Intent(getBaseContext(), SettingsActivity.class);
        startActivity(intent);
        mDrawer.setSelection(mSelectedItemId);
    }

    private void gotoRateUs() {
        final Uri uri = Uri.parse("market://details?id=" + getPackageName());
        final Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                            Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                            Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        } else {
            goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        }
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName())));
        }
        mDrawer.setSelection(mSelectedItemId);
    }

    // Check notification related methods
    private void checkNotificationEnabled() {
        if (! isNotificationEnabled() || !isNotificationChannelEnabled()) {
            showEnableNotificationDialog();
        }
    }

    private void showEnableNotificationDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle(R.string.ID_TITLE_ENABLE_NOTIFICATION)
                .setMessage(R.string.ID_MSG_ENABLE_NOTIFICATION)
                .setPositiveButton(R.string.ID_CAPTION_OK, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (!isNotificationEnabled()) {
                            openNotificationSettings();
                        } else if (!isNotificationChannelEnabled()) {
                            openChannelSettings();
                        } else {
                            openNotificationSettings();
                        }
                    }
                })
                .setNegativeButton(R.string.ID_CAPTION_CANCEL, null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private boolean isNotificationEnabled() {
        return NotificationManagerCompat.from(getBaseContext()).areNotificationsEnabled();
    }

    private void openNotificationSettings() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Intent intent = new Intent(Settings.ACTION_APP_NOTIFICATION_SETTINGS);
            intent.putExtra(Settings.EXTRA_APP_PACKAGE, getPackageName());
            startActivity(intent);
        } else {
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            intent.setData(Uri.parse("package:" + getPackageName()));
            startActivity(intent);
        }
    }

    private boolean isNotificationChannelEnabled(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel channel = manager.getNotificationChannel(TrackerCheckService.CHANNEL_ID);
            return channel.getImportance() != NotificationManager.IMPORTANCE_NONE;
        }
        return true;
    }

    private void openChannelSettings() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Intent intent = new Intent(Settings.ACTION_CHANNEL_NOTIFICATION_SETTINGS);
            intent.putExtra(Settings.EXTRA_APP_PACKAGE, getPackageName());
            intent.putExtra(Settings.EXTRA_CHANNEL_ID, TrackerCheckService.CHANNEL_ID);
            startActivity(intent);
        }
    }

    // Update SSL Provider method
    private int ERROR_DIALOG_REQUEST_CODE = 0x100;
    private boolean mNeedRetryProviderInstall = false;

    private ProviderInstaller.ProviderInstallListener mProviderInstallListener = new ProviderInstaller.ProviderInstallListener() {
        @Override
        public void onProviderInstalled() {
            Log.d(TAG, "Update SSL successfully");
        }

        @Override
        public void onProviderInstallFailed(int errorCode, Intent recoveryIntent) {
            GoogleApiAvailability availability = GoogleApiAvailability.getInstance();
            if (availability.isUserResolvableError(errorCode)) {
                // Recoverable error. Show a dialog prompting the user to
                // install/update/enable Google Play services.
                availability.showErrorDialogFragment(
                        MainActivity.this,
                        errorCode,
                        ERROR_DIALOG_REQUEST_CODE,
                        new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                // The user chose not to take the recovery action
                                Log.e(TAG, "User cancelled the update SSL request");
                            }
                        });
            } else {
                // Google Play services is not available.
                Log.e(TAG, "Request SSL update failed, Google Play service isn't available");
            }
        }
    };

    private void updateSSLProvider() {
        ProviderInstaller.installIfNeededAsync(getBaseContext(), mProviderInstallListener);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ERROR_DIALOG_REQUEST_CODE) {
            // Adding a fragment via GoogleApiAvailability.showErrorDialogFragment
            // before the instance state is restored throws an error. So instead,
            // set a flag here, which will cause the fragment to delay until
            // onPostResume.
            mNeedRetryProviderInstall = true;
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (mNeedRetryProviderInstall) {
            // We can now safely retry installation.
            updateSSLProvider();
            mNeedRetryProviderInstall = false;
        }
    }
}
