package com.jchain.affiliate.WebAPI;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;

import com.jchain.affiliate.R;
import com.jchain.affiliate.Utils.HttpClientUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.Call;

public class BaseWebAPI {
    private static final String TAG = "BaseWebAPI";

    private static final String PARAM_NAME_APPID = "appid";
    protected final String SERVER_URL;
    private final String APP_ID;

    protected final Handler mHandler;
    protected final HttpClientUtils mHttpClientUtils;
    protected BaseWebAPI(final Context context) {
        SERVER_URL = context.getString(R.string.jchain_server_base_url);
        APP_ID = context.getString(R.string.jchain_server_app_id);
        mHandler = new Handler(context.getMainLooper());
        mHttpClientUtils = new HttpClientUtils(context);
    }

    protected String buildRequestParamData(final String[] paramNames, final String[] values) {
        final StringBuilder sb = new StringBuilder();
        try {
            // Always put app id as one of parameters
            String paramEntry = PARAM_NAME_APPID+"="+URLEncoder.encode(APP_ID, "UTF-8");
            sb.append(paramEntry).append("&");
            if (paramNames != null && values != null) {
                for (int i = 0; i < paramNames.length; i++) {
                    paramEntry = paramNames[i] + "=" + URLEncoder.encode(values[i], "UTF-8");
                    sb.append(paramEntry).append("&");
                }
            }
            if (sb.length() > 0) { // remove last "&"
                sb.setLength(sb.length()-1);
            }
            return sb.toString();
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, "Failed to encode request params: "+sb.toString(), e);
            return sb.toString();
        }
    }

    protected String buildJSONBody(final String[] paramNames, final Object[] values, final boolean ignoreNullValue) {
        JSONObject object = new JSONObject();
        try {
            // always put app id into JSON Body
            object.put(PARAM_NAME_APPID, APP_ID);
            if (paramNames == null || values == null) {
                return object.toString();
            }
            for (int i = 0; i < paramNames.length; i++) {
                Object value = values[i];
                if (value == null) {
                    if (ignoreNullValue) {
                        continue;
                    } else {
                        value = JSONObject.NULL;
                    }
                }
                object.put(paramNames[i], value);
            }
            return object.toString(2);
        } catch (JSONException e) {
            Log.e(TAG, "Failed to build up JSON data, meet exception", e);
            return null;
        }
    }

    protected String buildJSONBody(final String[] paramNames, final Object[] values) {
        return buildJSONBody(paramNames, values, false);
    }

    protected static class Result {
        public boolean success = false;
        public String errorCode = null;
        public String errorMessage = null;
        public String detailMessage = null;

        private JSONObject jsonData = null;
        public String getDataValue(final String paramName) {
            if (jsonData == null) {
                return null;
            }
            try {
                return jsonData.getString(paramName);
            } catch (JSONException e) {
                Log.e(TAG, "Failed to get value of "+paramName+", meet exception:", e);
                return null;
            }
        }

        public JSONObject getJsonData() {
            return jsonData;
        }
    }

    protected Result parseJsonDataResponse(final String response) {
        try {
            if (response == null || response.isEmpty()) {
                return null;
            }
            Result result = new Result();
            JSONObject object = new JSONObject(response);
            result.success = object.getBoolean("success");
            try {
                result.jsonData = object.getJSONObject("data");
            } catch (JSONException e) {
                Log.w(TAG, "Failed to parse jsonData part of the response: "+response+", meet exception:", e);
                result.jsonData = null;
            }
            if (object.has("message")) {
                Object messageObj = object.get("message");
                if (messageObj instanceof JSONObject) {
                    JSONObject objMessage = (JSONObject)messageObj;
                    result.errorCode = objMessage.getString("errorCode");
                    result.errorMessage = objMessage.getString("message");
                    result.detailMessage = objMessage.getString("detail");
                }
            }
            return result;
        } catch (JSONException e) {
            Log.e(TAG, "Failed to parse Json response: "+response+", meet exception:", e);
            return null;
        }
    }

    protected String safeGetValueFromJson(final JSONObject object, final String paramName) throws JSONException {
        if (!object.has(paramName) || object.isNull(paramName)) {
            return null;
        }
        return object.getString(paramName);
    }

    protected int safeGetIntValueFromJson(final JSONObject object, final String paramName) throws JSONException {
        if (!object.has(paramName) || object.isNull(paramName)) {
            return -1;
        }
        return object.getInt(paramName);
    }

    protected Date safeGetDateValueFromJson(final JSONObject object, final String paramName, final SimpleDateFormat dateFormat) throws JSONException {
        final String dateData = safeGetValueFromJson(object, paramName);
        if (TextUtils.isEmpty(dateData)) {
            return null;
        }
        try {
            return dateFormat.parse(dateData);
        } catch(ParseException e) {
            Log.w(TAG, "safeGetDateValueFromJson, invalid date: "+dateData);
            return null;
        }
    }

    // Common methods
    protected void stopCall(final Call call) {
        if (call != null) {
            mHttpClientUtils.cancelCall(call);
        }
    }
}
