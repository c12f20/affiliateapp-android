package com.jchain.affiliate.WebAPI;

import android.content.Context;
import androidx.annotation.IntDef;
import android.text.TextUtils;
import android.util.Log;

import com.jchain.affiliate.Account.AccountInfo;
import com.jchain.affiliate.Category.CategoryItemInfo;
import com.jchain.affiliate.Product.CouponItemInfo;
import com.jchain.affiliate.Tracker.PriceTrackerItemInfo;
import com.jchain.affiliate.Product.ProductItemInfo;
import com.jchain.affiliate.Product.PromotionItemInfo;
import com.jchain.affiliate.R;
import com.jchain.affiliate.System.SystemInfoManager;
import com.jchain.affiliate.Tracker.TrackerInfoManager;
import com.jchain.affiliate.Utils.FileUtils;
import com.jchain.affiliate.Utils.HttpClientUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.Call;

public final class WebProductAPI extends BaseWebAPI {
    private static final String TAG = "WebProductAPI";

    private static WebProductAPI sInstance = null;
    public static WebProductAPI getInstance(final Context context) {
        if (sInstance == null) {
            sInstance = new WebProductAPI(context);
        }
        return sInstance;
    }

    private final String DEFAULT_PROVIDER_NAME;
    private final FileUtils mFileUtils;
    private final TrackerInfoManager mTrackerInfoManager;
    private WebProductAPI(final Context context) {
        super(context);
        DEFAULT_PROVIDER_NAME = context.getString(R.string.ID_DEFAULT_VENDOR_NAME);
        mFileUtils = FileUtils.getInstance(context);
        mTrackerInfoManager = TrackerInfoManager.getInstance(context);
    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({PRODUCT_ERROR_NONE, PRODUCT_ERROR_GENERAL, PRODUCT_ERROR_NETWORK, PRODUCT_ERROR_PARSE})
    public @interface ProductError {}
    public static final int PRODUCT_ERROR_NONE = 0;
    public static final int PRODUCT_ERROR_GENERAL = 1;
    public static final int PRODUCT_ERROR_NETWORK = 2;
    public static final int PRODUCT_ERROR_PARSE = 3;

    // Categories related methods
    public interface CategoryDataListener {
        void onGotCategoriesList(final @ProductError int errorCode, final ArrayList<CategoryItemInfo> list);
    }

    private CategoryDataListener mCategoryDataListener = null;
    private void notifyGotCategoriesList(final @ProductError int errorCode, final ArrayList<CategoryItemInfo> list) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mCategoryDataListener != null) {
                    mCategoryDataListener.onGotCategoriesList(errorCode, list);
                }
            }
        });
    }

    private static final String LIST_CATEGORY_API_PATH = "/products/listcategory";
    public Call queryAllCategories(final CategoryDataListener listener) {
        mCategoryDataListener = listener;
        final String requestUrl = SERVER_URL + LIST_CATEGORY_API_PATH
                + "?" + buildRequestParamData(null, null);
        return mHttpClientUtils.getJSONData(requestUrl, new HttpClientUtils.ResponseListener() {
            @Override
            public void onResponseData(String response) {
                int errorCode = PRODUCT_ERROR_NONE;
                final Result result = parseJsonDataResponse(response);
                if (response == null) {
                    errorCode = PRODUCT_ERROR_NETWORK;
                } else if (result == null) {
                    errorCode = PRODUCT_ERROR_PARSE;
                } else if (!result.success) {
                    errorCode = PRODUCT_ERROR_GENERAL;
                }
                String categoryData = null;
                if (errorCode != PRODUCT_ERROR_NONE) {
                    categoryData = restoreCategoriesData();
                } else {
                    categoryData = result.getDataValue("category");
                }
                if (categoryData == null || categoryData.isEmpty()) {
                    notifyGotCategoriesList(errorCode, null);
                    return;
                }
                final ArrayList<CategoryItemInfo> categoriesList = parseCategoriesInfoJsonData(categoryData);
                backupCategoriesData(categoryData);
                notifyGotCategoriesList(errorCode, categoriesList);
            }
        });
    }

    public void cancelQueryAllCategories(final Call categoryQueryCall) {
        mCategoryDataListener = null;
        if (categoryQueryCall != null) {
            stopCall(categoryQueryCall);
        }
    }

    private static final String CATEGORY_DATA_FILE_PATH = "categories"+ File.separator+"data.json";
    private void backupCategoriesData(final String data) {
        try {
            final byte[] fileData = data.getBytes("UTF8");
            mFileUtils.saveFile(CATEGORY_DATA_FILE_PATH, fileData);
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, "Failed to backupCategoriesData, exception: ", e);
        }
    }

    private String restoreCategoriesData() {
        return mFileUtils.readFile(CATEGORY_DATA_FILE_PATH);
    }

    private ArrayList<CategoryItemInfo> parseCategoriesInfoJsonData(final String data) {
        try {
            if (data == null || data.isEmpty()) {
                return null;
            }
            final ArrayList<CategoryItemInfo> categoriesList = new ArrayList<CategoryItemInfo>();
            JSONArray array = new JSONArray(data);
            for (int i=0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                final String subCategoriesData = safeGetValueFromJson(object,"sub");
                ArrayList<CategoryItemInfo> subCategories = null;
                if (subCategoriesData != null) {
                    subCategories = parseCategoriesInfoJsonData(subCategoriesData);
                }

                final CategoryItemInfo item = new CategoryItemInfo(safeGetValueFromJson(object,"category_id"),
                        safeGetValueFromJson(object,"image_url"),
                        safeGetValueFromJson(object,"name"),
                        safeGetValueFromJson(object,"description"),
                        subCategories);
                categoriesList.add(item);
            }
            return categoriesList;
        } catch (JSONException e) {
            Log.e(TAG, "Failed to parse Json data: "+data+", meet exception:", e);
            return null;
        }
    }

    // Products related methods
    public interface ProductDataListener {
        void onGotProductsList(final @ProductError int errorCode, final int totalCount, final int totalPagesCount, final ArrayList<ProductItemInfo> list);
    }

    private ProductDataListener mProductDataListener = null;
    private void notifyGotProductsList(final @ProductError int errorCode, final int totalCount, final int totalPagesCount, final ArrayList<ProductItemInfo> list) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mProductDataListener != null) {
                    mProductDataListener.onGotProductsList(errorCode, totalCount, totalPagesCount, list);
                }
            }
        });
    }

    private void notifyGotProductsListFailure(final @ProductError int errorCode) {
        notifyGotProductsList(errorCode, -1, -1, null);
    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({SORTBY_PRODUCT_ID, SORTBY_CATEGORY_ID, SORTBY_PRODUCT_NAME,
            SORTBY_ORIGINAL_PRICE, SORTBY_SALE_PRICE, SORTBY_COMMISSION_RATE,
            SORTBY_OUTOFSTOCK_DATE, SORTBY_DISCOUNT})
    public @interface SortBy {}
    public static final int SORTBY_PRODUCT_ID = 0;
    public static final int SORTBY_CATEGORY_ID = 1;
    public static final int SORTBY_PRODUCT_NAME = 2;
    public static final int SORTBY_ORIGINAL_PRICE = 3;
    public static final int SORTBY_SALE_PRICE = 4;
    public static final int SORTBY_COMMISSION_RATE = 5;
    public static final int SORTBY_OUTOFSTOCK_DATE = 6;
    public static final int SORTBY_DISCOUNT = 7;

    private String getSortName(final @SortBy int id) {
        String name = "id";
        switch (id) {
            case SORTBY_CATEGORY_ID:
                name = "category_id";
                break;
            case SORTBY_PRODUCT_NAME:
                name = "product_name";
                break;
            case SORTBY_ORIGINAL_PRICE:
                name = "original_price";
                break;
            case SORTBY_SALE_PRICE:
                name = "sale_price";
                break;
            case SORTBY_COMMISSION_RATE:
                name = "commission_rate";
                break;
            case SORTBY_OUTOFSTOCK_DATE:
                name = "out_of_stock_date";
                break;
            case SORTBY_DISCOUNT:
                name = "discount";
                break;
            case SORTBY_PRODUCT_ID:
            default:
                break;
        }
        return name;
    }

    private static final String LIST_PRODUCTS_API_PATH = "/products/list";
    private static final String PARAM_NAME_CATEGORY = "category";
    private static final String PARAM_NAME_FILTER = "Filter";
    private static final String PARAM_NAME_SORTBY = "sortBy";
    private static final String PARAM_NAME_SORTORDER = "sortOrder";
    private static final String PARAM_NAME_GROUPBY = "groupBy";
    private static final String PARAM_NAME_PAGE_IDX = "pageIdx";
    private static final String PARAM_NAME_PAGE_SIZE = "pageSize";
    public Call queryProducts(final String categoryId,
                              final String filter,
                              final @SortBy int orderBy,
                              final boolean orderAsc,
                              final int pageIdx,
                              final int pageSize,
                              final ProductDataListener listener) {
        mProductDataListener = listener;

        final String requestUrl = SERVER_URL + LIST_PRODUCTS_API_PATH;
        final String[] paramNames = new String[]{PARAM_NAME_CATEGORY,
                PARAM_NAME_FILTER, PARAM_NAME_SORTBY, PARAM_NAME_SORTORDER, PARAM_NAME_PAGE_IDX, PARAM_NAME_PAGE_SIZE};
        final Object[] paramValues = new Object[]{categoryId,
                filter, getSortName(orderBy), orderAsc ? "asc" : "desc", pageIdx+1, pageSize};
        final String postJsonBody = buildJSONBody(paramNames, paramValues, true);
        return mHttpClientUtils.postJSONData(requestUrl, postJsonBody, new HttpClientUtils.ResponseListener() {
            @Override
            public void onResponseData(String response) {
                final Result result = parseJsonDataResponse(response);
                if (response == null) {
                    notifyGotProductsListFailure(PRODUCT_ERROR_NETWORK);
                    return;
                } else if (result == null) {
                    notifyGotProductsListFailure(PRODUCT_ERROR_PARSE);
                    return;
                }
                if (!result.success) {
                    notifyGotProductsListFailure(PRODUCT_ERROR_GENERAL);
                    return;
                }
                try {
                    final JSONObject resultJson = result.getJsonData();
                    final int productsCount = safeGetIntValueFromJson(resultJson, "count");
                    final int pagesCount = safeGetIntValueFromJson(resultJson, "totalPage");
                    final ArrayList<ProductItemInfo> productsList = parseProductsInfoJsonData(
                            result.getDataValue("product"),
                            false);
                    notifyGotProductsList(PRODUCT_ERROR_NONE, productsCount, pagesCount, productsList);
                } catch (JSONException e) {
                    Log.e(TAG, "Failed to parse products list response", e);
                    notifyGotProductsListFailure(PRODUCT_ERROR_PARSE);
                }
            }
        });
    }

    public Call queryProductsByCategoryId(final String categoryId,
                                          final @SortBy int orderBy,
                                          final boolean orderAsc,
                                          final int pageIdx,
                                          final int pageSize,
                                          final ProductDataListener listener) {
        return queryProducts(categoryId, "", orderBy, orderAsc, pageIdx, pageSize, listener);
    }

    public void cancelQueryProducts(final Call queryProductsCall) {
        mProductDataListener = null;
        if (queryProductsCall != null) {
            stopCall(queryProductsCall);
        }
    }

    // Best Deals related methods
    private ProductDataListener mBestDealDataListener = null;
    private void notifyGotBestDealsList(final @ProductError int errorCode, final ArrayList<ProductItemInfo> list) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mBestDealDataListener != null) {
                    mBestDealDataListener.onGotProductsList(errorCode, 20, 1, list);
                }
            }
        });
    }

    private static final String LIST_BESTDEALS_API_PATH = "/products/list";
    public Call queryBestDeals(final ProductDataListener listener) {
        mBestDealDataListener = listener;

        final String requestUrl = SERVER_URL + LIST_BESTDEALS_API_PATH;
        final String[] paramNames = new String[]{PARAM_NAME_FILTER,
                PARAM_NAME_SORTBY, PARAM_NAME_SORTORDER, PARAM_NAME_GROUPBY, PARAM_NAME_PAGE_IDX, PARAM_NAME_PAGE_SIZE};
        final Object[] paramValues = new Object[]{"",
                getSortName(SORTBY_DISCOUNT), "desc", "discount,sale_price,original_price", 1, 20};
        final String postJsonBody = buildJSONBody(paramNames, paramValues);
        return mHttpClientUtils.postJSONData(requestUrl, postJsonBody, new HttpClientUtils.ResponseListener() {
            @Override
            public void onResponseData(String response) {
                final Result result = parseJsonDataResponse(response);
                int errorCode = PRODUCT_ERROR_NONE;
                if (response == null) {
                    errorCode = PRODUCT_ERROR_NETWORK;
                } else if (result == null) {
                    errorCode = PRODUCT_ERROR_PARSE;
                } else if (!result.success) {
                    errorCode = PRODUCT_ERROR_GENERAL;
                }
                String bestDealsData = null;
                if (errorCode != PRODUCT_ERROR_NONE) {
                    bestDealsData = restoreBestDealsData();
                } else {
                    bestDealsData = result.getDataValue("product");
                }
                if (bestDealsData == null || bestDealsData.isEmpty()) {
                    notifyGotBestDealsList(errorCode, null);
                    return;
                }
                final ArrayList<ProductItemInfo> productsList = parseProductsInfoJsonData(bestDealsData, false);
                backupBestDealsData(bestDealsData);
                notifyGotBestDealsList(PRODUCT_ERROR_NONE, productsList);
            }
        });
    }

    public void cancelQueryBestDeals(final Call queryBestDealsCall) {
        mBestDealDataListener = null;
        if (queryBestDealsCall != null) {
            stopCall(queryBestDealsCall);
        }
    }

    private static final String BESTDEALS_DATA_FILE_PATH = "bestdeals"+File.separator+"data.json";
    private void backupBestDealsData(final String data) {
        try {
            final byte[] fileData = data.getBytes("UTF8");
            mFileUtils.saveFile(BESTDEALS_DATA_FILE_PATH, fileData);
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, "Failed to backupBestDealsData, exception: ", e);
        }
    }

    private String restoreBestDealsData() {
        return mFileUtils.readFile(BESTDEALS_DATA_FILE_PATH);
    }

    // Hot Products related methods
    private ProductDataListener mHotProductsDataListener = null;
    private void notifyGotHotProductsList(final @ProductError int errorCode, final int totalCount, final int totalPagesCount, final ArrayList<ProductItemInfo> list) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mHotProductsDataListener != null) {
                    mHotProductsDataListener.onGotProductsList(errorCode, totalCount, totalPagesCount, list);
                }
            }
        });
    }

    private void notifyGotHotProductsListFailure(final @ProductError int errorCode) {
        notifyGotHotProductsList(errorCode, -1, -1, null);
    }

    private static final String LIST_HOTPRODUCTS_API_PATH = "/products/hot";
    public Call queryHotProducts(final String filter, final int pageIdx, final int pageSize, final ProductDataListener listener) {
        mHotProductsDataListener = listener;

        final String requestUrl = SERVER_URL + LIST_HOTPRODUCTS_API_PATH;
        final String[] paramNames = new String[]{PARAM_NAME_FILTER, PARAM_NAME_PAGE_IDX, PARAM_NAME_PAGE_SIZE};
        final Object[] paramValues = new Object[]{filter, pageIdx+1, pageSize};
        final String postJsonBody = buildJSONBody(paramNames, paramValues, true);
        return mHttpClientUtils.postJSONData(requestUrl, postJsonBody, new HttpClientUtils.ResponseListener() {
            @Override
            public void onResponseData(String response) {
                final Result result = parseJsonDataResponse(response);
                int errorCode = PRODUCT_ERROR_NONE;
                if (response == null) {
                    errorCode = PRODUCT_ERROR_NETWORK;
                } else if (result == null) {
                    errorCode = PRODUCT_ERROR_PARSE;
                } else if (!result.success) {
                    errorCode = PRODUCT_ERROR_GENERAL;
                }
                String hotProductsData = null;
                if (errorCode != PRODUCT_ERROR_NONE) {
                    hotProductsData = restoreHotProductsData();
                } else {
                    hotProductsData = result.getDataValue("product");
                }
                if (hotProductsData == null || hotProductsData.isEmpty()) {
                    notifyGotHotProductsListFailure(errorCode);
                    return;
                }
                try {
                    final ArrayList<ProductItemInfo> productsList = parseProductsInfoJsonData(hotProductsData, true);
                    final int productsCount;
                    final int pagesCount;
                    if (result != null) {
                        final JSONObject resultJson = result.getJsonData();
                        productsCount = safeGetIntValueFromJson(resultJson, "count");
                        pagesCount = safeGetIntValueFromJson(resultJson, "totalPage");
                    } else {
                        productsCount = productsList.size();
                        pagesCount = (productsCount / pageSize) + (productsCount % pageSize == 0 ? 0 : 1);
                    }

                    backupHotProductsData(hotProductsData);
                    notifyGotHotProductsList(PRODUCT_ERROR_NONE, productsCount, pagesCount, productsList);
                } catch (JSONException e) {
                    Log.e(TAG, "Failed to parse products list response", e);
                    notifyGotHotProductsListFailure(PRODUCT_ERROR_PARSE);
                }
            }
        });
    }

    public void cancelQueryHotProducts(final Call queryHotProductsCall) {
        mHotProductsDataListener = null;
        if (queryHotProductsCall != null) {
            stopCall(queryHotProductsCall);
        }
    }

    private static final String HOTPRODUCTS_DATA_FILE_PATH = "hotproducts"+File.separator+"data.json";
    private void backupHotProductsData(final String data) {
        try {
            final byte[] fileData = data.getBytes("UTF8");
            mFileUtils.saveFile(HOTPRODUCTS_DATA_FILE_PATH, fileData);
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, "Failed to backupHotProductsData, exception: ", e);
        }
    }

    private String restoreHotProductsData() {
        return mFileUtils.readFile(HOTPRODUCTS_DATA_FILE_PATH);
    }

    // Query Vehicle related products methods
    private ProductDataListener mVehicleRelatedProductsDataListener = null;
    private void notifyGotVehicleRelatedProductsList(final @ProductError int errorCode, final ArrayList<ProductItemInfo> list) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mVehicleRelatedProductsDataListener != null) {
                    mVehicleRelatedProductsDataListener.onGotProductsList(errorCode, -1, 1, list);
                }
            }
        });
    }

    private static final String LIST_VEHICLE_PRODUCTS_API_PATH = "/products/recommend";
    private static final String PARAM_NAME_CATEGORY_LIST = "category";
    private static final String PARAM_NAME_CATEGORY_LIMIT_COUNT = "limit";
    public Call queryVehicleRelatedProducts(final String accessToken,
                                            final ArrayList<CategoryItemInfo> categories,
                                            final int eachCatLimitCount,
                                            final ProductDataListener listener) {
        mVehicleRelatedProductsDataListener = listener;

        if (TextUtils.isEmpty(accessToken)) {
            Log.e(TAG, "Failed to queryVehicleRelatedProducts for invalid access token");
            notifyGotVehicleRelatedProductsList(PRODUCT_ERROR_GENERAL, null);
            return null;
        }

        // Build categories value
        JSONArray categoriesJSONValue = null;
        if (categories != null && !categories.isEmpty()) {
            categoriesJSONValue = new JSONArray();
            for(CategoryItemInfo category: categories) {
                try {
                    final int categoryId = Integer.parseInt(category.id);
                    categoriesJSONValue.put(categoryId);
                } catch (NumberFormatException e) {
                    Log.w(TAG, "Ignore invalid category id: "+category.id);
                }
            }
        }
        final String requestUrl = SERVER_URL + LIST_VEHICLE_PRODUCTS_API_PATH;
        final String postJsonBody = buildJSONBody(new String[]{PARAM_NAME_CATEGORY_LIST, PARAM_NAME_CATEGORY_LIMIT_COUNT},
                new Object[]{categoriesJSONValue, eachCatLimitCount}, true);
        return mHttpClientUtils.postJSONData(requestUrl, accessToken, postJsonBody, new HttpClientUtils.ResponseListener() {
            @Override
            public void onResponseData(String response) {
                final Result result = parseJsonDataResponse(response);
                int errorCode = PRODUCT_ERROR_NONE;
                if (response == null) {
                    errorCode = PRODUCT_ERROR_NETWORK;
                } else if (result == null) {
                    errorCode = PRODUCT_ERROR_PARSE;
                } else if (!result.success) {
                    errorCode = PRODUCT_ERROR_GENERAL;
                }
                String resultData = null;
                if (errorCode == PRODUCT_ERROR_NONE) {
                    resultData = result.getDataValue("product");
                }
                if (TextUtils.isEmpty(resultData)) {
                    notifyGotVehicleRelatedProductsList(errorCode, null);
                    return;
                }
                final ArrayList<ProductItemInfo> productsList = parseProductsInfoJsonData(resultData, false);
                notifyGotVehicleRelatedProductsList(PRODUCT_ERROR_NONE, productsList);
            }
        });
    }

    public void cancelQueryVehicleRelatedProducts(final Call queryVehicleRelatedProductsCall) {
        mVehicleRelatedProductsDataListener = null;
        if (queryVehicleRelatedProductsCall != null) {
            stopCall(queryVehicleRelatedProductsCall);
        }
    }

    // Products promotions related methods
    public interface PromotionDataListener {
        void onGotPromotionsList(final @ProductError int errorCode, final ArrayList<PromotionItemInfo> list);
    }
    private PromotionDataListener mPromotionDataListener = null;
    private void notifyGotPromotionsList(final @ProductError int errorCode, final ArrayList<PromotionItemInfo> list) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mPromotionDataListener != null) {
                    mPromotionDataListener.onGotPromotionsList(errorCode, list);
                }
            }
        });
    }

    private static final String LIST_PROMOTIONS_API_PATH = "/products/promotion";
    public Call queryPromotions(final PromotionDataListener listener) {
        mPromotionDataListener = listener;

        final String requestUrl = SERVER_URL + LIST_PROMOTIONS_API_PATH;
        final String postJsonBody = buildJSONBody(null, null);
        return mHttpClientUtils.postJSONData(requestUrl, postJsonBody, new HttpClientUtils.ResponseListener() {
            @Override
            public void onResponseData(String response) {
                final Result result = parseJsonDataResponse(response);
                int errorCode = PRODUCT_ERROR_NONE;
                if (response == null) {
                    errorCode = PRODUCT_ERROR_NETWORK;
                } else if (result == null) {
                    errorCode = PRODUCT_ERROR_PARSE;
                } else if (!result.success) {
                    errorCode = PRODUCT_ERROR_GENERAL;
                }
                String promotionsData = null;
                if (errorCode != PRODUCT_ERROR_NONE) {
                    promotionsData = restorePromotionsData();
                } else {
                    promotionsData = result.getDataValue("vendor");
                }
                if (promotionsData == null || promotionsData.isEmpty()) {
                    notifyGotPromotionsList(errorCode, null);
                    return;
                }
                final ArrayList<PromotionItemInfo> promotionsList = parsePromotionsInfoJsonData(promotionsData);
                backupPromotionsData(promotionsData);
                notifyGotPromotionsList(PRODUCT_ERROR_NONE, promotionsList);
            }
        });
    }

    public void cancelQueryPromotions(final Call queryPromotionsCall) {
        mPromotionDataListener = null;
        if (queryPromotionsCall != null) {
            stopCall(queryPromotionsCall);
        }
    }

    private static final String PROMOTIONS_DATA_FILE_PATH = "promotions"+File.separator+"data.json";
    private void backupPromotionsData(final String data) {
        try {
            final byte[] fileData = data.getBytes("UTF8");
            mFileUtils.saveFile(PROMOTIONS_DATA_FILE_PATH, fileData);
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, "Failed to backupHotProductsData, exception: ", e);
        }
    }

    private String restorePromotionsData() {
        return mFileUtils.readFile(PROMOTIONS_DATA_FILE_PATH);
    }

    // Products clicks count related methods
    private static final String RECORD_CLICK_API_PATH = "/products/click";
    private static final String PARAM_NAME_DEVICE_ID = "device_id";
    private static final String PARAM_NAME_CATEGORY_ID = "category_id";
    private static final String PARAM_NAME_CATEGORY_NAME = "category_name";
    private static final String PARAM_NAME_PRODUCT_ID = "product_id";
    private static final String PARAM_NAME_PRODUCT_NAME = "product_name";
    public void reportProductClicked(final Context context, final ProductItemInfo product) {
        final String requestUrl = SERVER_URL + RECORD_CLICK_API_PATH;
        final String deviceId = SystemInfoManager.getInstance().getDeviceUUID(context);
        final String[] paramNames = new String[]{PARAM_NAME_DEVICE_ID,
                PARAM_NAME_CATEGORY_ID, PARAM_NAME_CATEGORY_NAME, PARAM_NAME_PRODUCT_ID, PARAM_NAME_PRODUCT_NAME};
        final Object[] paramValues = new Object[]{deviceId,
                product.categoryId, product.categoryName, product.id, product.name};

        final String postJsonBody = buildJSONBody(paramNames, paramValues, true);
        mHttpClientUtils.postJSONData(requestUrl, postJsonBody, new HttpClientUtils.ResponseListener() {
            @Override
            public void onResponseData(String response) {
                final Result result = parseJsonDataResponse(response);
                if (response == null) {
                    Log.e(TAG, "Failed to reportProductClicked, response is null");
                    return;
                } else if (result == null) {
                    Log.e(TAG, "Failed to parse response of reportProductClicked");
                    return;
                }
                if (!result.success) {
                    Log.e(TAG, "Failed to reportProductClicked, result is failed");
                    return;
                }
                Log.d(TAG, "Succeeded to reportProductClicked");
            }
        });
    }

    // Coupons related methods
    public interface CouponListener {
        void onGotCoupons(final @ProductError int errorCode, final List<CouponItemInfo> coupons);
    }
    private CouponListener mCouponListener = null;
    private void notifyGotCoupons(final @ProductError int errorCode, final List<CouponItemInfo> coupons) {
        mHandler.post(new Runnable() {
           @Override
           public void run() {
               if (mCouponListener != null) {
                   mCouponListener.onGotCoupons(errorCode, coupons);
               }
           }
        });
    }
    private static final String LIST_COUPONS_API_PATH = "/products/coupon";
    public Call queryCoupons(final CouponListener listener) {
        mCouponListener = listener;

        final String requestUrl = SERVER_URL + LIST_COUPONS_API_PATH;
        final String postJsonBody = buildJSONBody(null, null);
        return mHttpClientUtils.postJSONData(requestUrl, postJsonBody, new HttpClientUtils.ResponseListener() {
            @Override
            public void onResponseData(String response) {
                final Result result = parseJsonDataResponse(response);
                if (response == null) {
                    notifyGotCoupons(PRODUCT_ERROR_NETWORK, null);
                    return;
                } else if (result == null) {
                    notifyGotCoupons(PRODUCT_ERROR_PARSE, null);
                    return;
                }
                if (!result.success) {
                    notifyGotCoupons(PRODUCT_ERROR_GENERAL, null);
                    return;
                }
                final List<CouponItemInfo> coupons = parseCouponsJsonData(result.getDataValue("coupons"));
                notifyGotCoupons(PRODUCT_ERROR_NONE, coupons);
            }
        });
    }

    public void cancelQueryCoupons(final Call queryCouponCall) {
        mCouponListener = null;
        if (queryCouponCall != null) {
            stopCall(queryCouponCall);
        }
    }

    // Price Tracker related methods
    public interface AddToTrackListener {
        void onResult(final @ProductError int errorCode, final PriceTrackerItemInfo trackItem);
    }
    private AddToTrackListener mAddToTrackListener = null;
    private void notifyAddToTrackResult(final @ProductError int errorCode, final PriceTrackerItemInfo trackItem) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mAddToTrackListener != null) {
                    mAddToTrackListener.onResult(errorCode, trackItem);
                }
            }
        });
    }

    private static final String ADDTRACK_API_PATH = "/price/addToTrack";
    private static final String PARAM_NAME_VENDOR_ID = "vendor";
    private static final String PARAM_NAME_VENDOR_NAME = "vendor_name";
    private static final String PARAM_NAME_PRODUCT_DISPLAY_NAME = "name";
    private static final String PARAM_NAME_CLICK_URL = "click_url";
    private static final String PARAM_NAME_IMAGE_URL = "img_url";
    private static final String PARAM_NAME_PRODUCT_PRICE = "price";
    private static final String PARAM_NAME_CONDITION = "condition";

    public void addProductToTrack(final AccountInfo userInfo, final ProductItemInfo product, final AddToTrackListener listener) {
        mAddToTrackListener = listener;

        final String requestUrl = SERVER_URL + ADDTRACK_API_PATH;
        final String[] paramNames = new String[]{PARAM_NAME_VENDOR_ID,
                PARAM_NAME_VENDOR_NAME, PARAM_NAME_PRODUCT_DISPLAY_NAME,
                PARAM_NAME_CLICK_URL, PARAM_NAME_IMAGE_URL, PARAM_NAME_PRODUCT_PRICE, PARAM_NAME_CONDITION};
        final Object[] paramValues = new Object[]{product.providerId,
                product.providerName, product.name, product.productURL, product.productMainImageURL,
                String.valueOf(product.price), String.valueOf(product.id)};

        final String postJsonBody = buildJSONBody(paramNames, paramValues, true);
        mHttpClientUtils.postJSONData(requestUrl, userInfo.accessToken, postJsonBody, new HttpClientUtils.ResponseListener() {
            @Override
            public void onResponseData(String response) {
                final Result result = parseJsonDataResponse(response);
                if (response == null) {
                    notifyAddToTrackResult(PRODUCT_ERROR_NETWORK, null);
                    return;
                } else if (result == null) {
                    notifyAddToTrackResult(PRODUCT_ERROR_PARSE, null);
                    return;
                }
                if (!result.success) {
                    notifyAddToTrackResult(PRODUCT_ERROR_GENERAL, null);
                    return;
                }
                final PriceTrackerItemInfo trackItem = parsePriceTrackInfoJsonData(
                        result.getDataValue("track"));
                notifyAddToTrackResult(PRODUCT_ERROR_NONE, trackItem);
            }
        });
    }

    public interface StopTrackListener {
        void onResult(final @ProductError int errorCode);
    }
    private StopTrackListener mStopTrackListener = null;
    private void notifyStopTrackResult(final @ProductError int errorCode) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mStopTrackListener != null) {
                    mStopTrackListener.onResult(errorCode);
                }
            }
        });
    }

    private static final String STOPTRACK_API_PATH = "/price/stopTrack";
    private static final String PARAM_NAME_TRACK_ID = "id";
    public void stopTrack(final AccountInfo userInfo, final PriceTrackerItemInfo track, final StopTrackListener listener) {
        mStopTrackListener = listener;

        final String requestUrl = SERVER_URL + STOPTRACK_API_PATH;
        final String[] paramNames = new String[]{PARAM_NAME_TRACK_ID};
        final Object[] paramValues = new Object[]{track.id};

        final String postJsonBody = buildJSONBody(paramNames, paramValues, true);
        mHttpClientUtils.postJSONData(requestUrl, userInfo.accessToken, postJsonBody, new HttpClientUtils.ResponseListener() {
            @Override
            public void onResponseData(String response) {
                final Result result = parseJsonDataResponse(response);
                if (response == null) {
                    notifyStopTrackResult(PRODUCT_ERROR_NETWORK);
                    return;
                } else if (result == null) {
                    notifyStopTrackResult(PRODUCT_ERROR_PARSE);
                    return;
                }
                if (!result.success) {
                    notifyStopTrackResult(PRODUCT_ERROR_GENERAL);
                    return;
                }
                notifyStopTrackResult(PRODUCT_ERROR_NONE);
            }
        });
    }

    public interface TrackDataListener {
        void onGotTracksList(final @ProductError int errorCode, final ArrayList<PriceTrackerItemInfo> tracksList);
    }
    private TrackDataListener mQueryTracksListener = null;
    private void notifyGotTracksList(final @ProductError int errorCode, final ArrayList<PriceTrackerItemInfo> tracksList) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mQueryTracksListener != null) {
                    mQueryTracksListener.onGotTracksList(errorCode, tracksList);
                }
            }
        });
    }

    private static final String LIST_TRACKS_API_PATH = "/price/listAllTracks";
    public Call queryTracks(final AccountInfo userInfo, final TrackDataListener listener) {
        mQueryTracksListener = listener;

        final String requestUrl = SERVER_URL + LIST_TRACKS_API_PATH;
        final String[] paramNames = new String[]{PARAM_NAME_SORTBY,
                PARAM_NAME_SORTORDER};
        final Object[] paramValues = new Object[]{"id", "asc"};

        final String postJsonBody = buildJSONBody(paramNames, paramValues, true);
        return mHttpClientUtils.postJSONData(requestUrl, userInfo.accessToken, postJsonBody, new HttpClientUtils.ResponseListener() {
            @Override
            public void onResponseData(String response) {
                final Result result = parseJsonDataResponse(response);
                if (response == null) {
                    notifyGotTracksList(PRODUCT_ERROR_NETWORK, null);
                    return;
                } else if (result == null) {
                    notifyGotTracksList(PRODUCT_ERROR_PARSE, null);
                    return;
                }
                if (!result.success) {
                    notifyGotTracksList(PRODUCT_ERROR_GENERAL, null);
                    return;
                }
                final ArrayList<PriceTrackerItemInfo> tracksList = parsePriceTrackListInfoJsonData(
                        result.getDataValue("tracks"));
                notifyGotTracksList(PRODUCT_ERROR_NONE, tracksList);
            }
        });
    }

    public void cancelQueryTracks(final Call queryTracksCall) {
        mQueryTracksListener = null;
        if (queryTracksCall != null) {
            stopCall(queryTracksCall);
        }
    }

    private TrackDataListener mQueryUpdatedTracksListener = null;
    private void notifyGotUpdatedTracksList(final @ProductError int errorCode, final ArrayList<PriceTrackerItemInfo> tracksList) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mQueryUpdatedTracksListener != null) {
                    mQueryUpdatedTracksListener.onGotTracksList(errorCode, tracksList);
                }
            }
        });
    }

    private static final String LIST_UPDATED_TRACKS_API_PATH = "/price/getUpdatedTracks";
    public Call queryUpdatedTracks(final AccountInfo userInfo, final TrackDataListener listener) {
        mQueryUpdatedTracksListener = listener;

        final String requestUrl = SERVER_URL + LIST_UPDATED_TRACKS_API_PATH;
        final String[] paramNames = new String[]{PARAM_NAME_SORTBY,
                PARAM_NAME_SORTORDER};
        final Object[] paramValues = new Object[]{"id", "asc"};

        final String postJsonBody = buildJSONBody(paramNames, paramValues, true);
        return mHttpClientUtils.postJSONData(requestUrl, userInfo.accessToken, postJsonBody, new HttpClientUtils.ResponseListener() {
            @Override
            public void onResponseData(String response) {
                final Result result = parseJsonDataResponse(response);
                if (response == null) {
                    notifyGotUpdatedTracksList(PRODUCT_ERROR_NETWORK, null);
                    return;
                } else if (result == null) {
                    notifyGotUpdatedTracksList(PRODUCT_ERROR_PARSE, null);
                    return;
                }
                if (!result.success) {
                    notifyGotUpdatedTracksList(PRODUCT_ERROR_GENERAL, null);
                    return;
                }
                final ArrayList<PriceTrackerItemInfo> tracksList = parsePriceTrackListInfoJsonData(
                        result.getDataValue("tracks"));
                notifyGotUpdatedTracksList(PRODUCT_ERROR_NONE, tracksList);
            }
        });
    }

    public void cancelQueryUpdatedTracks(final Call queryUpdatedTracksCall) {
        mQueryUpdatedTracksListener = null;
        if (queryUpdatedTracksCall != null) {
            stopCall(queryUpdatedTracksCall);
        }
    }

    // Internal methods
    private ArrayList<ProductItemInfo> parseProductsInfoJsonData(final String data, final boolean isHotProduct) {
        try {
            if (data == null || data.isEmpty()) {
                return null;
            }
            final ArrayList<ProductItemInfo> productsList = new ArrayList<ProductItemInfo>();
            final JSONArray array = new JSONArray(data);
            for (int i=0; i < array.length(); i++) {
                final JSONObject object = array.getJSONObject(i);
                final String productId = safeGetValueFromJson(object, "product_id");
                final String productName = safeGetValueFromJson(object, "product_name");
                final String categoryId = safeGetValueFromJson(object, "category_id");
                final String categoryName = safeGetValueFromJson(object, "category_name");
                final String productUrl = safeGetValueFromJson(object, "click_url");
                final String productMainImageUrl = safeGetValueFromJson(object, "product_image_url");
                final String productProviderName = safeGetValueFromJson(object, "vendor_name");
                final int productProviderId = safeGetIntValueFromJson(object, "vendor_id");
                if (productUrl == null || productUrl.isEmpty()) {
                    Log.w(TAG, "Skip Product without url: "+productName);
                    continue;
                }

                final String productImageUrlsData = safeGetValueFromJson(object, "all_image_urls");
                String[] productImageUrls = null;
                if (productImageUrlsData != null) {
                    productImageUrls = productImageUrlsData.split(",");
                }
                final String original_price_tag = "original_price";
                float productOrigPrice = -1, productPrice = -1;
                if (object.has(original_price_tag) && !object.isNull(original_price_tag)) {
                    productOrigPrice = productPrice = (float)object.getDouble(original_price_tag);
                }
                final String sale_price_tag = "sale_price";
                if (object.has(sale_price_tag) && !object.isNull(sale_price_tag)) {
                    productPrice = (float)object.getDouble(sale_price_tag);
                    if (productOrigPrice < 0) {
                        productOrigPrice = productPrice;
                    }
                }

                final ProductItemInfo item = new ProductItemInfo(
                        productId, productName,
                        categoryId, categoryName,
                        "", productProviderName == null ? DEFAULT_PROVIDER_NAME : productProviderName,
                        productProviderId,
                        productMainImageUrl, productImageUrls,
                        productUrl,
                        productPrice, productOrigPrice,
                        -1, -1);
                item.isHotProduct = isHotProduct;
                productsList.add(item);
            }
            return productsList;
        } catch (JSONException e) {
            Log.e(TAG, "Failed to parse Json data: "+data+", meet exception:", e);
            return null;
        }
    }

    private List<CouponItemInfo> parseCouponsJsonData(final String data) {
        try {
            if (TextUtils.isEmpty(data)) {
                return null;
            }
            final ArrayList<CouponItemInfo> couponsList = new ArrayList<CouponItemInfo>();
            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            final JSONArray array = new JSONArray(data);
            for (int i=0; i < array.length(); i++) {
                final JSONObject object = array.getJSONObject(i);
                final String code = safeGetValueFromJson(object, "code");
                final String description = safeGetValueFromJson(object, "description");
                final String couponURL = safeGetValueFromJson(object, "click_url");
                final String vendorName = safeGetValueFromJson(object, "vendor_name");
                final String vendorLogoURL = safeGetValueFromJson(object, "vendor_logo");
                final Date startTime = safeGetDateValueFromJson(object, "begin", sdf);
                final Date endTime = safeGetDateValueFromJson(object, "end", sdf);
                final CouponItemInfo coupon = new CouponItemInfo(code, description,
                        vendorName, vendorLogoURL, couponURL, startTime, endTime);
                couponsList.add(coupon);
            }
            return couponsList;
        } catch (JSONException e) {
            Log.e(TAG, "Failed to parse Json data: "+data+", meet exception:", e);
            return null;
        }
    }

    private ArrayList<PromotionItemInfo> parsePromotionsInfoJsonData(final String data) {
        try {
            if (data == null || data.isEmpty()) {
                return null;
            }
            final Pattern pattern = Pattern.compile("<a.*href=\"([^\"]+)\"[^>]*>\\s*<img[^>]*src=\"([^\"]+)\"[^>]*>.*</a>");
            final ArrayList<PromotionItemInfo> promotionsList = new ArrayList<PromotionItemInfo>();
            final JSONArray array = new JSONArray(data);
            for (int i=0; i < array.length(); i++) {
                final JSONObject object = array.getJSONObject(i);
                final String promotionData = object.getString("code").replaceAll("[\r\n]*", "");
                final Matcher matcher = pattern.matcher(promotionData);
                if (!matcher.find()) {
                    Log.d(TAG, "Ignore invalid promotion data: "+promotionData);
                    continue;
                }
                String clickURL = matcher.group(1).trim();
                if (!clickURL.startsWith("http")) {
                    clickURL = "https:"+clickURL;
                }
                String imageURL = matcher.group(2).trim();
                if (!imageURL.startsWith("http")) {
                    imageURL = "https:"+imageURL;
                }

                Log.d(TAG, "Got promotion, click: "+clickURL+", image: "+imageURL);
                final String providerName = safeGetValueFromJson(object, "vendor_name");
                final PromotionItemInfo item = new PromotionItemInfo(imageURL, clickURL,
                        providerName == null ? DEFAULT_PROVIDER_NAME : providerName);
                promotionsList.add(item);
            }
            return promotionsList;
        } catch (JSONException e) {
            Log.e(TAG, "Failed to parse Json data: "+data+", meet exception:", e);
            return null;
        }
    }

    private PriceTrackerItemInfo parsePriceTrackInfoJsonData(final String data) {
        try {
            if (data == null || data.isEmpty()) {
                return null;
            }

            final JSONObject object = new JSONObject(data);
            final int id = object.getInt("id");
            final String name = safeGetValueFromJson(object,"name");
            final int providerId = object.getInt("type");
            final String providerName = safeGetValueFromJson(object, "vendor_name");
            final String clickURL = object.getString("click_url");
            String imageURL = safeGetValueFromJson(object,"img_url");
            if (imageURL == null) {
                imageURL = safeGetValueFromJson(object, "vendor_logo");
            }
            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date createTime = safeGetDateValueFromJson(object, "created_at", sdf);
            if (createTime == null) {
                createTime = safeGetDateValueFromJson(object, "creation_time", sdf);
            }
            final float origPrice = Float.parseFloat(object.getString("price"));
            final String latestPriceData = safeGetValueFromJson(object, "last_price");
            final float latestPrice = latestPriceData != null ? Float.parseFloat(latestPriceData) : origPrice;
            final float notifyPrice = mTrackerInfoManager.getTrackerPrice(id);
            final PriceTrackerItemInfo item = new PriceTrackerItemInfo(id, name, "", providerId, providerName,
                    clickURL, imageURL, createTime, origPrice, latestPrice, notifyPrice);
            return item;
        } catch (Exception e) {
            Log.e(TAG, "Failed to parse Json data: "+data+", meet exception:", e);
            return null;
        }
    }

    private ArrayList<PriceTrackerItemInfo> parsePriceTrackListInfoJsonData(final String data) {
        try {
            if (data == null || data.isEmpty()) {
                return null;
            }

            final JSONArray array = new JSONArray(data);
            final ArrayList<PriceTrackerItemInfo> tracksList = new ArrayList<PriceTrackerItemInfo>();
            for (int i=0; i < array.length(); i++) {
                final JSONObject object = array.getJSONObject(i);
                final PriceTrackerItemInfo item = parsePriceTrackInfoJsonData(object.toString());
                if (item != null) {
                    tracksList.add(item);
                }
            }
            return tracksList;
        } catch (JSONException e) {
            Log.e(TAG, "Failed to parse Json data: "+data+", meet exception:", e);
            return null;
        }
    }
}
