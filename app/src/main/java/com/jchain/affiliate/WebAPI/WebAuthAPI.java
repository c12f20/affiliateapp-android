package com.jchain.affiliate.WebAPI;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import androidx.annotation.IntDef;
import android.util.Log;

import com.jchain.affiliate.Account.AccountInfo;
import com.jchain.affiliate.Account.OAuth.OAuthInfo;
import com.jchain.affiliate.Utils.FileUtils;
import com.jchain.affiliate.Utils.HttpClientUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import okhttp3.MediaType;

public final class WebAuthAPI extends BaseWebAPI {
    private static final String TAG = "WebAuthAPI";

    private static WebAuthAPI sInstance = null;

    public static WebAuthAPI getInstance(final Context context) {
        if (sInstance == null) {
            sInstance = new WebAuthAPI(context);
        }
        return sInstance;
    }

    private final FileUtils mFileUtils;
    private WebAuthAPI(final Context context) {
        super(context);
        mFileUtils = FileUtils.getInstance(context);
    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({AUTH_ERROR_NO_ACTIVATAION, AUTH_ERROR_NONE, AUTH_ERROR_GENERAL, AUTH_ERROR_NETWORK, AUTH_ERROR_PARSE,
            AUTH_ERROR_DUPLICATED_NAME, AUTH_ERROR_DUPLICATED_EMAIL, AUTH_ERROR_DUPLICATED_PHONE,
            AUTH_ERROR_BANNED})
    public @interface AuthError {}
    public static final int AUTH_ERROR_NO_ACTIVATAION = 1;
    public static final int AUTH_ERROR_NONE = 0;
    public static final int AUTH_ERROR_GENERAL = -1;
    public static final int AUTH_ERROR_NETWORK = -2;
    public static final int AUTH_ERROR_PARSE = -3;
    public static final int AUTH_ERROR_DUPLICATED_NAME = -4;
    public static final int AUTH_ERROR_DUPLICATED_EMAIL = -5;
    public static final int AUTH_ERROR_DUPLICATED_PHONE = -6;
    public static final int AUTH_ERROR_BANNED = -7;

    // Register methods
    public interface RegisterListener {
        void onRegisterResult(final @AuthError int errorCode);
    }
    private RegisterListener mRegisterListener = null;
    private void notifyRegisterResult(final @AuthError int errorCode) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mRegisterListener != null) {
                    mRegisterListener.onRegisterResult(errorCode);
                }
            }
        });
    }

    private static final String REGISTER_API_PATH = "/register";

    private static final String PARAM_NAME_ACCOUNT_TYPE = "type";
    private static final String PARAM_NAME_ACCOUNT_NAME = "name";
    private static final String PARAM_NAME_ACCOUNT_EMAIL = "email";
    private static final String PARAM_NAME_ACCOUNT_PHONE = "phone";
    private static final String PARAM_NAME_ACCOUNT_PASSWORD = "password";
    private static final String PARAM_NAME_ACCOUNT_FIRSTNAME = "firstName";
    private static final String PARAM_NAME_ACCOUNT_LASTNAME = "lastName";
    private static final String PARAM_NAME_ACCOUNT_AVATAR_URL = "avatar";
    private static final String PARAM_NAME_ACCOUNT_GENDER = "gender";
    private static final String PARAM_NAME_ACCOUNT_BIRTHDAY = "birthday";

    private static final String ERROR_CODE_RESOURCE_EXISTED = "E00000002";
    private static final String ERROR_CODE_BANNED = "E00000006";
    private static final String ERROR_CODE_NOT_ACTIVED = "E00000007";
    private static final String ERROR_MSG_NAME_EXISTED = "user name has been used";
    private static final String ERROR_MSG_EMAIL_EXISTED = "email has been used";
    private static final String ERROR_MSG_PHONE_EXISTED = "phone has been used";

    private int getAccountTypeValue(final @AccountInfo.AccountType int accountType) {
        int accountTypeValue = 1;
        switch(accountType) {
            case AccountInfo.ACCOUNT_TYPE_FACEBOOK_OAUTH:
                accountTypeValue = 2;
                break;
            case AccountInfo.ACCOUNT_TYPE_GOOGLE_OAUTH:
                accountTypeValue = 3;
                break;
            case AccountInfo.ACCOUNT_TYPE_TWITTER_OAUTH:
            case AccountInfo.ACCOUNT_TYPE_GENERAL:
            default:
                break;
        }
        return accountTypeValue;
    }

    private int getAccountGenderValue(final @AccountInfo.AccountGender int gender) {
        int accountGenderValue = -1;
        switch(gender) {
            case AccountInfo.ACCOUNT_GENDER_MALE:
                accountGenderValue = 1;
                break;
            case AccountInfo.ACCOUNT_GENDER_FEMALE:
                accountGenderValue = 2;
                break;
            case AccountInfo.ACCOUNT_GENDER_OTHER:
                accountGenderValue = 3;
                break;
            case AccountInfo.ACCOUNT_GENDER_UNKNOWN:
            default:
                break;
        }
        return accountGenderValue;
    }

    private @AccountInfo.AccountType int getOAuthAccountType(final @OAuthInfo.InfoType int authInfoType) {
        int accountType = AccountInfo.ACCOUNT_TYPE_GENERAL;
        switch (authInfoType) {
            case OAuthInfo.TYPE_FACEBOOK:
                accountType = AccountInfo.ACCOUNT_TYPE_FACEBOOK_OAUTH;
                break;
            case OAuthInfo.TYPE_GOOGLE:
                accountType = AccountInfo.ACCOUNT_TYPE_GOOGLE_OAUTH;
                break;
            case OAuthInfo.TYPE_TWITTER:
                accountType = AccountInfo.ACCOUNT_TYPE_TWITTER_OAUTH;
                break;
            default:
                break;
        }
        return accountType;
    }

    private String getOAuthAccountName(final @OAuthInfo.InfoType int authInfoType, final String authId) {
        String accountName = authId;
        switch (authInfoType) {
            case OAuthInfo.TYPE_FACEBOOK:
                accountName = "f_"+authId;
                break;
            case OAuthInfo.TYPE_GOOGLE:
                accountName = "g_"+authId;
                break;
            case OAuthInfo.TYPE_TWITTER:
                accountName = "t_"+authId;
                break;
            default:
                break;
        }
        return accountName;
    }

    public void registerAccountWithOAuthInfo(final OAuthInfo info, final RegisterListener listener) {
        registerAccount(getOAuthAccountType(info.type),
                        getOAuthAccountName(info.type, info.id),
                        info.name, // We use OAuth account name info as password of our account system
                        info.email,
                        null,
                        info.name,
                        null,
                        AccountInfo.ACCOUNT_GENDER_UNKNOWN,
                        null,
                        null,
                        listener);
    }

    public void registerAccount(final @AccountInfo.AccountType int accountType,
                                final String name,
                                final String password,
                                final String email,
                                final String phone,
                                final String firstName,
                                final String lastName,
                                final @AccountInfo.AccountGender int gender,
                                final Date birthday,
                                final String avatarURL,
                                final RegisterListener listener) {
        mRegisterListener = listener;
        final Thread registerThrd = new Thread() {
            @Override
            public void run() {
                final String requestUrl = SERVER_URL + REGISTER_API_PATH;
                // Build up request parameters and related values
                final int accountTypeValue = getAccountTypeValue(accountType);
                final ArrayList<String> paramNames = new ArrayList<String>();
                paramNames.add(PARAM_NAME_ACCOUNT_TYPE);
                paramNames.add(PARAM_NAME_ACCOUNT_NAME);
                paramNames.add(PARAM_NAME_ACCOUNT_PASSWORD);
                final ArrayList<Object> paramValues = new ArrayList<Object>();
                paramValues.add(accountTypeValue);
                paramValues.add(name);
                paramValues.add(password);
                if (email != null) {
                    paramNames.add(PARAM_NAME_ACCOUNT_EMAIL);
                    paramValues.add(email);
                }
                if (phone != null) {
                    paramNames.add(PARAM_NAME_ACCOUNT_PHONE);
                    paramValues.add(phone);
                }
                if (firstName != null && !firstName.isEmpty()) {
                    paramNames.add(PARAM_NAME_ACCOUNT_FIRSTNAME);
                    paramValues.add(firstName);
                }
                if (lastName != null && !lastName.isEmpty()) {
                    paramNames.add(PARAM_NAME_ACCOUNT_LASTNAME);
                    paramValues.add(lastName);
                }
                final int genderValue = getAccountGenderValue(gender);
                if (genderValue > 0) {
                    paramNames.add(PARAM_NAME_ACCOUNT_GENDER);
                    paramValues.add(genderValue);
                }
                final String birthdayValue = getAccountBirthdayValue(birthday);
                if (birthdayValue != null) {
                    paramNames.add(PARAM_NAME_ACCOUNT_BIRTHDAY);
                    paramValues.add(birthdayValue);
                }
                if (avatarURL != null && !avatarURL.isEmpty()) {
                    paramNames.add(PARAM_NAME_ACCOUNT_AVATAR_URL);
                    paramValues.add(avatarURL);
                }
                String[] paramNamesArray = new String[paramNames.size()];
                final String postJsonBody = buildJSONBody(paramNames.toArray(paramNamesArray), paramValues.toArray());
                if (postJsonBody == null) { // Failed to build JSON data, notify failure
                    notifyRegisterResult(AUTH_ERROR_GENERAL);
                    return;
                }
                mHttpClientUtils.postJSONData(requestUrl, postJsonBody, new HttpClientUtils.ResponseListener() {
                    @Override
                    public void onResponseData(String response) {
                        final Result result = parseJsonDataResponse(response);
                        if (response == null) {
                            notifyRegisterResult(AUTH_ERROR_NETWORK);
                            return;
                        } else if (result == null) {
                            notifyRegisterResult(AUTH_ERROR_PARSE);
                            return;
                        }
                        if (!result.success) {
                            if (result.errorCode.equals(ERROR_CODE_RESOURCE_EXISTED)) {
                                if (result.errorMessage == null || result.errorMessage.equals(ERROR_MSG_NAME_EXISTED)) {
                                    notifyRegisterResult(AUTH_ERROR_DUPLICATED_NAME);
                                } else if (result.errorMessage.equals(ERROR_MSG_EMAIL_EXISTED)) {
                                    notifyRegisterResult(AUTH_ERROR_DUPLICATED_EMAIL);
                                } else if (result.errorMessage.equals(ERROR_MSG_PHONE_EXISTED)) {
                                    notifyRegisterResult(AUTH_ERROR_DUPLICATED_PHONE);
                                }
                            } else {
                                notifyRegisterResult(AUTH_ERROR_GENERAL);
                            }
                            return;
                        }
                        notifyRegisterResult(AUTH_ERROR_NONE);
                    }
                });

            }
        };
        registerThrd.setName("RegisterThrd");
        registerThrd.start();
    }
    // Login method
    public interface LoginListener {
        void onLoginResult(final @AuthError int errorCode, final AccountInfo userInfo);
    }

    private LoginListener mLoginListener = null;
    private void notifyLoginResult(final @AuthError int errorCode) {
        notifyLoginResult(errorCode, null);
    }

    private void notifyLoginResult(final @AuthError int errorCode, final AccountInfo userInfo) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mLoginListener != null) {
                    mLoginListener.onLoginResult(errorCode, userInfo);
                }
            }
        });
    }

    public void loginAccountWithOAuthInfo(final OAuthInfo info, final LoginListener listener) {
        // We use OAuth account name info as password of our account system
        loginAccount(getOAuthAccountName(info.type, info.id), info.name, listener);
    }

    private static final String LOGIN_API_PATH = "/login";
    private static final String PARAM_NAME_ACCOUNT_USERNAME = "username";
    public void loginAccount(final String name,
                             final String password,
                             final LoginListener listener) {
        mLoginListener = listener;
        final Thread loginThrd = new Thread() {
            @Override
            public void run() {
                final String requestUrl = SERVER_URL + LOGIN_API_PATH;
                final String[] paramNames = new String[]{PARAM_NAME_ACCOUNT_USERNAME, PARAM_NAME_ACCOUNT_PASSWORD};
                final Object[] paramValues = new Object[]{name, password};
                final String postJsonBody = buildJSONBody(paramNames, paramValues);
                if (postJsonBody == null) { // Failed to build JSON data, notify failure
                    notifyLoginResult(AUTH_ERROR_GENERAL);
                    return;
                }
                mHttpClientUtils.postJSONData(requestUrl, postJsonBody, new HttpClientUtils.ResponseListener() {
                    @Override
                    public void onResponseData(String response) {
                        final Result result = parseJsonDataResponse(response);
                        if (response == null) {
                            notifyLoginResult(AUTH_ERROR_NETWORK);
                            return;
                        } else if (result == null) {
                            notifyLoginResult(AUTH_ERROR_PARSE);
                            return;
                        }
                        if (!result.success) {
                            if (result.errorCode.equals(ERROR_CODE_NOT_ACTIVED)) {
                                notifyLoginResult(AUTH_ERROR_NO_ACTIVATAION);
                            } else if (result.errorCode.equals(ERROR_CODE_BANNED)) {
                                notifyLoginResult(AUTH_ERROR_BANNED);
                            } else {
                                notifyLoginResult(AUTH_ERROR_GENERAL);
                            }
                            return;
                        }

                        final AccountInfo userInfo = parseUserInfoJsonData(result.getDataValue("user"));
                        if (userInfo != null) {
                            userInfo.accessToken = result.getDataValue("api_token");
                        }
                        notifyLoginResult(AUTH_ERROR_NONE, userInfo);
                    }
                });
            }
        };
        loginThrd.setName("LoginThrd");
        loginThrd.start();
    }

    // Logout Account methods
    private final String LOGOUT_API_PATH = "/logout";
    public void logoutAccount(final AccountInfo userInfo) {
        final Thread logoutAccountThrd = new Thread() {
            @Override
            public void run() {
                if (userInfo.accessToken == null || userInfo.accessToken.isEmpty()) {
                    Log.e(TAG, "Failed to logout account for invalid access token");
                    return;
                }
                final String requestUrl = SERVER_URL + LOGOUT_API_PATH
                        + "?" + buildRequestParamData(null, null);
                mHttpClientUtils.getJSONData(requestUrl, userInfo.accessToken, new HttpClientUtils.ResponseListener() {
                    @Override
                    public void onResponseData(String response) {
                        final Result result = parseJsonDataResponse(response);
                        if (result == null || !result.success) {
                            Log.w(TAG, "Failed to logout account, response: "+response);
                            return;
                        }
                        Log.d(TAG, "Succeed to logout account");
                    }
                });
            }
        };
        logoutAccountThrd.setName("UpdateAccountThrd");
        logoutAccountThrd.start();
    }

    // Update Account methods
    public interface UpdateAccountListener {
        void onUpdateResult(final @AuthError int errorCode, final AccountInfo userInfo);
    }

    private UpdateAccountListener mUpdateAccountListener = null;
    private void notifyUpdateAccountResult(final @AuthError int errorCode) {
        notifyUpdateAccountResult(errorCode, null);
    }

    private void notifyUpdateAccountResult(final @AuthError int errorCode, final AccountInfo userInfo) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mUpdateAccountListener != null) {
                    mUpdateAccountListener.onUpdateResult(errorCode, userInfo);
                }
            }
        });
    }

    private final String ME_API_PATH = "/me";
    public void updateAccount(final String password, final AccountInfo userInfo, final UpdateAccountListener listener) {
        mUpdateAccountListener = listener;
        final Thread updateAccountThrd = new Thread() {
            @Override
            public void run() {
                if (userInfo.accessToken == null || userInfo.accessToken.isEmpty()) {
                    Log.e(TAG, "Failed to update account for invalid access token");
                    notifyUpdateAccountResult(AUTH_ERROR_GENERAL);
                    return;
                }
                // It's avatar update, call updateAvatar instead
                if (userInfo.avatarURL != null) {
                    if (!userInfo.avatarURL.isEmpty()) {
                        Log.d(TAG, "Update Avatar as File: " + userInfo.avatarURL);
                        final File avatarFile = new File(userInfo.avatarURL);
                        updateAccountAvatar(avatarFile, userInfo, listener);
                    } else {
                        Log.d(TAG, "Clear Avatar with empty URL");
                        notifyUpdateAccountResult(AUTH_ERROR_NONE);
                    }
                    return;
                }

                final String requestUrl = SERVER_URL + ME_API_PATH;

                final ArrayList<String> paramNames = new ArrayList<String>();
                final ArrayList<Object> paramValues = new ArrayList<Object>();
                if (password != null && !password.isEmpty()) {
                    paramNames.add(PARAM_NAME_ACCOUNT_PASSWORD);
                    paramValues.add(password);
                }

                if (userInfo.email != null) {
                    paramNames.add(PARAM_NAME_ACCOUNT_EMAIL);
                    paramValues.add(userInfo.email);
                }
                if (userInfo.phone != null) {
                    paramNames.add(PARAM_NAME_ACCOUNT_PHONE);
                    paramValues.add(userInfo.phone);
                }
                if (userInfo.firstName != null) {
                    paramNames.add(PARAM_NAME_ACCOUNT_FIRSTNAME);
                    paramValues.add(userInfo.firstName);
                }
                if (userInfo.lastName != null) {
                    paramNames.add(PARAM_NAME_ACCOUNT_LASTNAME);
                    paramValues.add(userInfo.lastName);
                }
                if (userInfo.gender >= 0) {
                    final int genderValue = getAccountGenderValue(userInfo.gender);
                    paramNames.add(PARAM_NAME_ACCOUNT_GENDER);
                    paramValues.add(genderValue);
                }
                if (userInfo.birthday != null) {
                    final String birthdayValue = getAccountBirthdayValue(userInfo.birthday);
                    paramNames.add(PARAM_NAME_ACCOUNT_BIRTHDAY);
                    paramValues.add(birthdayValue);
                }

                String[] paramNamesArray = new String[paramNames.size()];
                final String putJsonBody = buildJSONBody(paramNames.toArray(paramNamesArray), paramValues.toArray());
                if (putJsonBody == null) { // Failed to build JSON data, notify failure
                    notifyUpdateAccountResult(AUTH_ERROR_GENERAL);
                    return;
                }
                mHttpClientUtils.putJSONData(requestUrl, userInfo.accessToken, putJsonBody, new HttpClientUtils.ResponseListener() {
                    @Override
                    public void onResponseData(String response) {
                        final Result result = parseJsonDataResponse(response);
                        if (response == null) {
                            notifyUpdateAccountResult(AUTH_ERROR_NETWORK);
                            return;
                        } else if (result == null) {
                            notifyUpdateAccountResult(AUTH_ERROR_PARSE);
                            return;
                        }
                        if (!result.success) {
                            if (result.errorCode.equals(ERROR_CODE_RESOURCE_EXISTED)) {
                                if (result.errorMessage == null || result.errorMessage.equals(ERROR_MSG_NAME_EXISTED)) {
                                    notifyUpdateAccountResult(AUTH_ERROR_DUPLICATED_NAME);
                                } else if (result.errorMessage.equals(ERROR_MSG_EMAIL_EXISTED)) {
                                    notifyUpdateAccountResult(AUTH_ERROR_DUPLICATED_EMAIL);
                                } else if (result.errorMessage.equals(ERROR_MSG_PHONE_EXISTED)) {
                                    notifyUpdateAccountResult(AUTH_ERROR_DUPLICATED_PHONE);
                                }
                            } else {
                                notifyUpdateAccountResult(AUTH_ERROR_GENERAL);
                            }
                            return;
                        }
                        final AccountInfo userInfo = parseUserInfoJsonData(result.getDataValue("user"));
                        notifyUpdateAccountResult(AUTH_ERROR_NONE, userInfo);
                    }
                });
            }
        };
        updateAccountThrd.setName("UpdateAccountThrd");
        updateAccountThrd.start();
    }

    private static final String DEFAULT_AVATAR_COMPRESSED_FILENAME = "avatar_compress.jpg";
    private static final int AVATAR_MAX_WIDTH = 300;
    private static final int AVATAR_MAX_HEIGHT = 300;
    private File compressAvatarFile(final File origFile) {
        if (!origFile.exists()) {
            Log.e(TAG, "Failed to compress Avatar file as orig file doesn't exist");
            return null;
        }
        final String origFilePath = origFile.getAbsolutePath();
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        final Bitmap origBmp = BitmapFactory.decodeFile(origFilePath, options);
        final int width = options.outWidth;
        final int height = options.outHeight;
        int inSampleSize = 1;
        while (width/inSampleSize > AVATAR_MAX_WIDTH || height/inSampleSize > AVATAR_MAX_HEIGHT) {
            inSampleSize *= 2;
        }
        options.inSampleSize = inSampleSize;
        options.inJustDecodeBounds = false;
        final Bitmap compressedBmp = BitmapFactory.decodeFile(origFilePath, options);
        final File compressedFile = mFileUtils.buildFilePath(DEFAULT_AVATAR_COMPRESSED_FILENAME);
        if (compressedFile == null) {
            Log.e(TAG, "Can't locate compressed Avatar File");
            return null;
        }
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(compressedFile);
            if (!compressedBmp.compress(Bitmap.CompressFormat.JPEG, 100, fos)) {
                Log.e(TAG, "Failed to compress bitmap of Avatar File");
                return null;
            }
            return compressedFile;
        } catch (FileNotFoundException e) {
            Log.e(TAG, "Can't open compressed Avatar File, exception: ", e);
            return null;
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException e) {
                Log.w(TAG, "Failed to close compressed Avatar file, exception: ", e);
            }
        }
    }

    private static final String UPDATE_AVATAR_API_PATH = "/avatar";
    private static final String AVATAR_DATA_NAME = "file_data";
    private static final MediaType AVATAR_MEDIA_TYPE = MediaType.parse("image/jpeg");
    public void updateAccountAvatar(final File avatarFile, final AccountInfo userInfo, final UpdateAccountListener listener) {
        mUpdateAccountListener = listener;
        final Thread updateAvatarThrd = new Thread() {
            @Override
            public void run() {
                if (userInfo.accessToken == null || userInfo.accessToken.isEmpty()) {
                    Log.e(TAG, "Failed to update account avatar for invalid access token");
                    notifyUpdateAccountResult(AUTH_ERROR_GENERAL);
                    return;
                }

                final File compressAvatarFile = compressAvatarFile(avatarFile);
                if (compressAvatarFile == null) {
                    notifyUpdateAccountResult(AUTH_ERROR_GENERAL);
                    return;
                }
                final String requestUrl = SERVER_URL + UPDATE_AVATAR_API_PATH;
                mHttpClientUtils.postFileData(requestUrl, userInfo.accessToken,
                        AVATAR_DATA_NAME, avatarFile.getName(), AVATAR_MEDIA_TYPE, compressAvatarFile,
                        new HttpClientUtils.ResponseListener() {
                            @Override
                            public void onResponseData(String response) {
                                final Result result = parseJsonDataResponse(response);
                                if (response == null) {
                                    notifyUpdateAccountResult(AUTH_ERROR_NETWORK);
                                    return;
                                } else if (result == null) {
                                    notifyUpdateAccountResult(AUTH_ERROR_PARSE);
                                    return;
                                }
                                if (!result.success) {
                                    notifyUpdateAccountResult(AUTH_ERROR_GENERAL);
                                    return;
                                }
                                final AccountInfo userInfo = parseUserInfoJsonData(result.getDataValue("user"));
                                notifyUpdateAccountResult(AUTH_ERROR_NONE, userInfo);
                            }
                        });
            }
        };
        updateAvatarThrd.setName("UpdateAvatarThrd");
        updateAvatarThrd.start();
    }

    // Verify Account methods
    public interface VerifyAccountListener {
        void onVerifyResult(final @AuthError int errorCode, final AccountInfo userInfo);
    }

    private VerifyAccountListener mVerifyAccountListener = null;
    private void notifyVerifyAccountResult(final @AuthError int errorCode) {
        notifyVerifyAccountResult(errorCode, null);
    }

    private void notifyVerifyAccountResult(final @AuthError int errorCode, final AccountInfo userInfo) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mVerifyAccountListener != null) {
                    mVerifyAccountListener.onVerifyResult(errorCode, userInfo);
                }
            }
        });
    }
    public void verifyAccount(final String accessToken, final VerifyAccountListener listener) {
        mVerifyAccountListener = listener;
        final Thread verifyAccountThrd = new Thread() {
            @Override
            public void run() {
                if (accessToken == null || accessToken.isEmpty()) {
                    Log.e(TAG, "Failed to verify account for invalid access token");
                    notifyVerifyAccountResult(AUTH_ERROR_GENERAL);
                    return;
                }
                final String requestUrl = SERVER_URL + ME_API_PATH
                        + "?" + buildRequestParamData(null, null);
                mHttpClientUtils.getJSONData(requestUrl, accessToken, new HttpClientUtils.ResponseListener() {
                    @Override
                    public void onResponseData(String response) {
                        final Result result = parseJsonDataResponse(response);
                        if (response == null) {
                            notifyVerifyAccountResult(AUTH_ERROR_NETWORK);
                            return;
                        } else if (result == null) {
                            notifyVerifyAccountResult(AUTH_ERROR_PARSE);
                            return;
                        }
                        if (!result.success) {
                            if (result.errorCode.equals(ERROR_CODE_NOT_ACTIVED)) {
                                notifyVerifyAccountResult(AUTH_ERROR_NO_ACTIVATAION);
                            } else if (result.errorCode.equals(ERROR_CODE_BANNED)) {
                                notifyVerifyAccountResult(AUTH_ERROR_BANNED);
                            } else {
                                notifyVerifyAccountResult(AUTH_ERROR_GENERAL);
                            }
                            return;
                        }
                        final AccountInfo userInfo = parseUserInfoJsonData(result.getDataValue("user"));
                        if (userInfo != null) {
                            userInfo.accessToken = accessToken;
                        }
                        notifyVerifyAccountResult(AUTH_ERROR_NONE, userInfo);
                    }
                });
            }
        };
        verifyAccountThrd.setName("VerifyAccountThrd");
        verifyAccountThrd.start();
    }

    // Utils methods
    private static final String BIRTHDAY_DATE_PATTERN = "yyyy-MM-dd";
    private String getAccountBirthdayValue(final Date birthday) {
        if (birthday == null) {
            return null;
        }
        final SimpleDateFormat sdf = new SimpleDateFormat(BIRTHDAY_DATE_PATTERN, Locale.US);
        final String birthdayValue = sdf.format(birthday);
        return birthdayValue;
    }

    private Date parseUserBirthdayData(final String data) {
        Date birth = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(BIRTHDAY_DATE_PATTERN, Locale.US);
            birth = sdf.parse(data);
        } catch (ParseException e) {
            Log.w(TAG, "Failed to parse user birthday: "+data);
        }
        return birth;
    }

    private AccountInfo parseUserInfoJsonData(final String data) {
        try {
            if (data == null || data.isEmpty()) {
                return null;
            }
            JSONObject object = new JSONObject(data);
            AccountInfo accountInfo = new AccountInfo();
            accountInfo.type = object.getInt("type");
            accountInfo.name = object.getString("name");
            accountInfo.avatarURL = safeGetValueFromJson(object, "avatar");
            accountInfo.email = safeGetValueFromJson(object,"email");
            accountInfo.phone = safeGetValueFromJson(object, "phone");
            accountInfo.lastName = safeGetValueFromJson(object,"lastName");
            accountInfo.firstName = safeGetValueFromJson(object,"firstName");
            final String birthdayValue = safeGetValueFromJson(object,"birthday");
            if (birthdayValue != null && !birthdayValue.isEmpty()) {
                accountInfo.birthday = parseUserBirthdayData(birthdayValue);
            }
            accountInfo.gender = object.getInt("gender");
            return accountInfo;
        } catch (JSONException e) {
            Log.e(TAG, "Failed to parse Json data: "+data+", meet exception:", e);
            return null;
        }
    }
}
