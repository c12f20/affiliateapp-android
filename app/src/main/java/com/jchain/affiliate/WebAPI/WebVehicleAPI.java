package com.jchain.affiliate.WebAPI;

import android.content.Context;
import androidx.annotation.IntDef;
import android.text.TextUtils;
import android.util.Log;

import com.jchain.affiliate.Account.AccountInfo;
import com.jchain.affiliate.Settings.VehicleProfile.VehicleInfo;
import com.jchain.affiliate.Utils.HttpClientUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;

public final class WebVehicleAPI extends BaseWebAPI {
    private static final String TAG = "WebVehicleAPI";

    private static WebVehicleAPI gInstance = null;
    public static WebVehicleAPI getInstance(final Context context) {
        if (gInstance == null) {
            gInstance = new WebVehicleAPI(context);
        }
        return gInstance;
    }

    private WebVehicleAPI(final Context context) {
        super(context);
    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({VEHICLE_ERROR_NONE, VEHICLE_ERROR_GENERAL, VEHICLE_ERROR_NETWORK, VEHICLE_ERROR_PARSE,
            VEHICLE_ERROR_TOKEN, VEHICLE_ERROR_INVALID_VINCODE})
    public @interface VehicleError {}
    public static final int VEHICLE_ERROR_NONE = 0;
    public static final int VEHICLE_ERROR_GENERAL = -1;
    public static final int VEHICLE_ERROR_NETWORK = -2;
    public static final int VEHICLE_ERROR_PARSE = -3;
    public static final int VEHICLE_ERROR_TOKEN = -4;
    public static final int VEHICLE_ERROR_INVALID_VINCODE = -5;

    // Attach vehicle method
    public interface AttachVehicleListener {
        void onVehicleAttached(final @VehicleError int errorCode, final VehicleInfo vehicleInfo);
    }
    private AttachVehicleListener mAttachVehicleListener = null;
    private void notifyAttachVehicleResult(final @VehicleError int errorCode, final VehicleInfo vehicleInfo) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mAttachVehicleListener != null) {
                    mAttachVehicleListener.onVehicleAttached(errorCode, vehicleInfo);
                }
            }
        });
    }

    private static final String ATTACH_API_PATH = "/addvehicle";

    private static final String PARAM_NAME_VINCODE = "vin";
    private static final int VALID_VINCODE_LENGTH = 17;
    public void attachVehicleToAccount(final AccountInfo userInfo, final String vinCode, final AttachVehicleListener listener) {
        mAttachVehicleListener = listener;

        final Thread attachVehicleThrd = new Thread() {
            @Override
            public void run() {
                if (TextUtils.isEmpty(userInfo.accessToken)) {
                    Log.e(TAG, "Failed to attach vehicle for invalid access token");
                    notifyAttachVehicleResult(VEHICLE_ERROR_TOKEN, null);
                    return;
                }
                if (TextUtils.isEmpty(vinCode) || vinCode.length() != VALID_VINCODE_LENGTH) {
                    Log.e(TAG, "Failed to attach vehicle with null/empty/invalid vin code");
                    notifyAttachVehicleResult(VEHICLE_ERROR_INVALID_VINCODE, null);
                    return;
                }
                final String requestUrl = SERVER_URL + ATTACH_API_PATH;
                final String[] paramNames = new String[]{PARAM_NAME_VINCODE};
                final Object[] paramValues = new Object[]{vinCode};
                final String postJsonBody = buildJSONBody(paramNames, paramValues);
                mHttpClientUtils.postJSONData(requestUrl, userInfo.accessToken, postJsonBody, new HttpClientUtils.ResponseListener() {
                    @Override
                    public void onResponseData(String response) {
                        final Result result = parseJsonDataResponse(response);
                        if (response == null) {
                            notifyAttachVehicleResult(VEHICLE_ERROR_NETWORK, null);
                            return;
                        } else if (result == null) {
                            notifyAttachVehicleResult(VEHICLE_ERROR_PARSE, null);
                            return;
                        }
                        if (!result.success) {
                            notifyAttachVehicleResult(VEHICLE_ERROR_GENERAL, null);
                            return;
                        }
                        final VehicleInfo vehicleInfo = parseVehicleInfoJsonData(result.getDataValue("vehicle"));
                        notifyAttachVehicleResult(VEHICLE_ERROR_NONE, vehicleInfo);
                    }
                });
            }
        };
        attachVehicleThrd.setName("AttachVehicleThrd");
        attachVehicleThrd.start();
    }

    // Remove Vehicle methods
    public interface RemoveVehicleListener {
        void onVehicleRemoved(final @VehicleError int errorCode);
    }
    private RemoveVehicleListener mRemoveVehicleListener = null;
    private void notifyRemoveVehicleResult(final @VehicleError int errorCode) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mRemoveVehicleListener != null) {
                    mRemoveVehicleListener.onVehicleRemoved(errorCode);
                }
            }
        });
    }

    private static final String REMOVE_API_PATH = "/removevehicle";

    public void removeVehicleFromAccount(final AccountInfo userInfo, final int vehicleDbId, final RemoveVehicleListener listener) {
        mRemoveVehicleListener = listener;

        final Thread removeVehicleThrd = new Thread() {
            @Override
            public void run() {
                if (TextUtils.isEmpty(userInfo.accessToken)) {
                    Log.e(TAG, "Failed to remove vehicle for invalid access token");
                    notifyRemoveVehicleResult(VEHICLE_ERROR_TOKEN);
                    return;
                }
                final String requestUrl = SERVER_URL + REMOVE_API_PATH;
                final String[] paramNames = new String[]{PARAM_NAME_DB_ID};
                final Object[] paramValues = new Object[]{vehicleDbId};
                final String postJsonBody = buildJSONBody(paramNames, paramValues);
                mHttpClientUtils.postJSONData(requestUrl, userInfo.accessToken, postJsonBody, new HttpClientUtils.ResponseListener() {
                    @Override
                    public void onResponseData(String response) {
                        final Result result = parseJsonDataResponse(response);
                        if (response == null) {
                            notifyRemoveVehicleResult(VEHICLE_ERROR_NETWORK);
                            return;
                        } else if (result == null) {
                            notifyRemoveVehicleResult(VEHICLE_ERROR_PARSE);
                            return;
                        }
                        if (!result.success) {
                            notifyRemoveVehicleResult(VEHICLE_ERROR_GENERAL);
                            return;
                        }
                        notifyRemoveVehicleResult(VEHICLE_ERROR_NONE);
                    }
                });
            }
        };
        removeVehicleThrd.setName("RemoveVehicleThrd");
        removeVehicleThrd.start();
    }

    // Query Vehicle method
    public interface VehicleDataListener {
        void onGotVehiclesList(final @VehicleError int errorCode, final List<VehicleInfo> list);
    }
    private VehicleDataListener mVehicleDataListener = null;
    private void notifyGotVehicleResult(final @VehicleError int errorCode, final List<VehicleInfo> list) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mVehicleDataListener != null) {
                    mVehicleDataListener.onGotVehiclesList(errorCode, list);
                }
            }
        });
    }

    private static final String QUERY_API_PATH = "/getvehicles";
    public void queryVehiclesByAccount(final AccountInfo userInfo, final VehicleDataListener listener) {
        mVehicleDataListener = listener;

        final Thread queryVehicleThrd = new Thread() {
            @Override
            public void run() {
                if (TextUtils.isEmpty(userInfo.accessToken)) {
                    Log.e(TAG, "Failed to query vehicles for invalid access token");
                    notifyGotVehicleResult(VEHICLE_ERROR_TOKEN, null);
                    return;
                }
                final String requestUrl = SERVER_URL + QUERY_API_PATH;
                final String postJsonBody = buildJSONBody(null, null);
                mHttpClientUtils.postJSONData(requestUrl, userInfo.accessToken, postJsonBody, new HttpClientUtils.ResponseListener() {
                    @Override
                    public void onResponseData(String response) {
                        final Result result = parseJsonDataResponse(response);
                        if (response == null) {
                            notifyGotVehicleResult(VEHICLE_ERROR_NETWORK, null);
                            return;
                        } else if (result == null) {
                            notifyGotVehicleResult(VEHICLE_ERROR_PARSE, null);
                            return;
                        }
                        if (!result.success) {
                            notifyGotVehicleResult(VEHICLE_ERROR_GENERAL, null);
                            return;
                        }
                        final List<VehicleInfo> vehicles = parseVehiclesListInfoJsonData(result.getDataValue("vehicle"));
                        notifyGotVehicleResult(VEHICLE_ERROR_NONE, vehicles);
                    }
                });
            }
        };
        queryVehicleThrd.setName("QueryVehicleThrd");
        queryVehicleThrd.start();
    }

    // Update Vehicle Info method
    public interface UpdateVehicleListener {
        void onVehicleUpdated(final @VehicleError int errorCode, final VehicleInfo vehicleInfo);
    }
    private UpdateVehicleListener mUpdateVehicleListener = null;
    private void notifyVehicleUpdated(final @VehicleError int errorCode, final VehicleInfo vehicleInfo) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mUpdateVehicleListener != null) {
                    mUpdateVehicleListener.onVehicleUpdated(errorCode, vehicleInfo);
                }
            }
        });
    }

    private static final String UPDATE_API_PATH = "/updatevehicle";
    private static final String PARAM_NAME_DB_ID = "id";
    private static final String PARAM_NAME_MANUFACTURER = "maker";
    private static final String PARAM_NAME_MODEL_NAME = "model";
    private static final String PARAM_NAME_MODEL_YEAR = "year";
    private static final String PARAM_NAME_TRIM = "trim";
    private static final String PARAM_NAME_VEHICLE_TYPE = "type";
    private static final String PARAM_NAME_DOORS_COUNT = "doors";
    public void updateVehicleOfAccount(final AccountInfo userInfo, final VehicleInfo vehicleInfo, final UpdateVehicleListener listener) {
        mUpdateVehicleListener = listener;

        final Thread updateVehicleThrd = new Thread() {
            @Override
            public void run() {
                if (TextUtils.isEmpty(userInfo.accessToken)) {
                    Log.e(TAG, "Failed to update vehicle for invalid access token");
                    notifyVehicleUpdated(VEHICLE_ERROR_TOKEN, null);
                    return;
                }
                final String requestUrl = SERVER_URL + UPDATE_API_PATH;
                final ArrayList<String> paramNames = new ArrayList<String>();
                final ArrayList<Object> paramValues = new ArrayList<Object>();
                if (vehicleInfo.dbId < 0) {
                    Log.e(TAG, "Failed to update vehicle with invalid vehicle DB id: "+vehicleInfo.dbId);
                    notifyVehicleUpdated(VEHICLE_ERROR_GENERAL, null);
                    return;
                }
                paramNames.add(PARAM_NAME_DB_ID);
                paramValues.add(vehicleInfo.dbId);
                if (vehicleInfo.manufacturer != null) {
                    paramNames.add(PARAM_NAME_MANUFACTURER);
                    paramValues.add(vehicleInfo.manufacturer);
                }
                if (vehicleInfo.modelName != null) {
                    paramNames.add(PARAM_NAME_MODEL_NAME);
                    paramValues.add(vehicleInfo.modelName);
                }
                if (vehicleInfo.modelYear > 0) {
                    paramNames.add(PARAM_NAME_MODEL_YEAR);
                    paramValues.add(vehicleInfo.modelYear);
                }
                if (vehicleInfo.trim != null) {
                    paramNames.add(PARAM_NAME_TRIM);
                    paramValues.add(vehicleInfo.trim);
                }
                if (vehicleInfo.vehicleType != null) {
                    paramNames.add(PARAM_NAME_VEHICLE_TYPE);
                    paramValues.add(vehicleInfo.vehicleType);
                }
                if (vehicleInfo.doorsCount >= 0) {
                    paramNames.add(PARAM_NAME_DOORS_COUNT);
                    paramValues.add(vehicleInfo.doorsCount);
                }
                String[] paramNamesArray = new String[paramNames.size()];
                final String postJsonBody = buildJSONBody(paramNames.toArray(paramNamesArray), paramValues.toArray());
                if (postJsonBody == null) { // Failed to build JSON data, notify failure
                    notifyVehicleUpdated(VEHICLE_ERROR_GENERAL, null);
                    return;
                }
                mHttpClientUtils.postJSONData(requestUrl, userInfo.accessToken, postJsonBody, new HttpClientUtils.ResponseListener() {
                    @Override
                    public void onResponseData(String response) {
                        final Result result = parseJsonDataResponse(response);
                        if (response == null) {
                            notifyVehicleUpdated(VEHICLE_ERROR_NETWORK, null);
                            return;
                        } else if (result == null) {
                            notifyVehicleUpdated(VEHICLE_ERROR_PARSE, null);
                            return;
                        }
                        if (!result.success) {
                            notifyVehicleUpdated(VEHICLE_ERROR_GENERAL, null);
                            return;
                        }
                        final VehicleInfo vehicleInfo = parseVehicleInfoJsonData(result.getDataValue("vehicle"));
                        notifyVehicleUpdated(VEHICLE_ERROR_NONE, vehicleInfo);
                    }
                });

            }
        };
        updateVehicleThrd.setName("UpdateVehicleThrd");
        updateVehicleThrd.start();
    }

    // Utility methods
    private List<VehicleInfo> parseVehiclesListInfoJsonData(final String data) {
        if (data == null || data.isEmpty()) {
            return null;
        }
        try {
            final List<VehicleInfo> vehicles = new ArrayList<VehicleInfo>();
            final JSONArray array = new JSONArray(data);
            for (int i=0; i < array.length(); i++) {
                final JSONObject object = array.getJSONObject(i);
                final VehicleInfo vehicleInfo = parseVehicleInfoJsonData(object);
                vehicles.add(vehicleInfo);
            }
            return vehicles;
        } catch (JSONException e) {
            Log.e(TAG, "Failed to parse Json data: "+data+", meet exception:", e);
            return null;
        }
    }

    private VehicleInfo parseVehicleInfoJsonData(final String data) {
        if (data == null || data.isEmpty()) {
            return null;
        }

        try {
            final JSONObject object = new JSONObject(data);
            return parseVehicleInfoJsonData(object);
        } catch (JSONException e) {
            Log.e(TAG, "Failed to parse Json data: "+data+", meet exception:", e);
            return null;
        }
    }

    private VehicleInfo parseVehicleInfoJsonData(final JSONObject object) throws JSONException {
        final VehicleInfo vehicleInfo = new VehicleInfo();
        vehicleInfo.dbId = object.getInt("id");
        vehicleInfo.vinCode = safeGetValueFromJson(object, "vin");
        vehicleInfo.manufacturer = safeGetValueFromJson(object, "maker");
        vehicleInfo.modelName = safeGetValueFromJson(object, "model");
        vehicleInfo.modelYear = safeGetIntValueFromJson(object, "year");
        vehicleInfo.trim = safeGetValueFromJson(object, "trim");
        vehicleInfo.vehicleType = safeGetValueFromJson(object, "type");
        vehicleInfo.doorsCount = safeGetIntValueFromJson(object, "doors");
        return vehicleInfo;
    }
}
