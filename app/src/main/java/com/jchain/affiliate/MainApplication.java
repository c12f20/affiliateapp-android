package com.jchain.affiliate;

import androidx.multidex.MultiDexApplication;

import com.jchain.affiliate.Advertisement.AdsManager;

public final class MainApplication extends MultiDexApplication {
    @Override
    public void onCreate() {
        super.onCreate();
        AdsManager.getInstance().init(getApplicationContext());
    }
}
