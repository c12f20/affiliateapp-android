package com.jchain.affiliate.Home;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jchain.affiliate.Product.ProductItemInfo;
import com.jchain.affiliate.R;
import com.jchain.affiliate.UI.Widgets.OnlineImageViewUtils;

import java.util.ArrayList;
import java.util.Locale;

public final class ProductRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Context mContext;
    public ProductRecyclerAdapter(final Context context) {
        mContext = context;
    }

    public interface OnItemClickListener {
        void onItemClicked(final int position, final ProductItemInfo item);
    }

    private OnItemClickListener mOnItemClickListener = null;
    public void setOnItemClickListener(final OnItemClickListener listener) {
        mOnItemClickListener = listener;
    }

    private void notifyItemClicked(final int position, final ProductItemInfo item) {
        if (mOnItemClickListener != null) {
            mOnItemClickListener.onItemClicked(position, item);
        }
    }

    public interface OnBtnMoreClickListener {
        void onClicked();
    }
    private OnBtnMoreClickListener mOnBtnMoreClickListener = null;
    public void setOnBtnMoreClickListener(final OnBtnMoreClickListener listener) {
        mOnBtnMoreClickListener = listener;
    }

    private void notifyBtnMoreClicked() {
        if (mOnBtnMoreClickListener != null) {
            mOnBtnMoreClickListener.onClicked();
        }
    }

    private boolean hasBtnMore() {
        return mOnBtnMoreClickListener != null;
    }

    private ArrayList<ProductItemInfo> mProductsList = new ArrayList<ProductItemInfo>();
    public void updateProductList(final ArrayList<ProductItemInfo> list) {
        if (list == null) {
            return;
        }
        mProductsList.clear();
        mProductsList.addAll(list);
        notifyDataSetChanged();
    }

    private static class ProductViewHolder extends RecyclerView.ViewHolder {
        private final View mRootView;
        private final ImageView mProductIcon;
        private final TextView mTextProductPrice;
        private final TextView mTextProductDiscountRate;
        private final OnlineImageViewUtils mOnlineImageViewUtils;

        private ProductViewHolder(final Context context, @NonNull View itemView) {
            super(itemView);
            mRootView = itemView;
            mProductIcon = itemView.findViewById(R.id.imageView);
            mTextProductPrice = itemView.findViewById(R.id.textProductPrice);
            mTextProductDiscountRate = itemView.findViewById(R.id.textProductDiscountRate);

            mOnlineImageViewUtils = new OnlineImageViewUtils(context);
        }

        private View getItemView() {
            return mRootView;
        }

        public void updateItemView(final ProductItemInfo itemInfo) {
            if (itemInfo.productMainImageURL != null) {
                mOnlineImageViewUtils.loadImage(mProductIcon, itemInfo.productMainImageURL);
            }
            mTextProductPrice.setText(String.format(Locale.US, "$%.2f", itemInfo.price));
            if (itemInfo.discountRate < 100) {
                mTextProductDiscountRate.setVisibility(View.VISIBLE);
                mTextProductDiscountRate.setText(String.format(Locale.US, "-%d%%", Math.round(itemInfo.discountRate)));
            } else {
                mTextProductDiscountRate.setVisibility(View.GONE);
            }
        }
    }

    private static class MoreBtnViewHolder extends RecyclerView.ViewHolder {
        private final View mRootView;

        private MoreBtnViewHolder(@NonNull View itemView) {
            super(itemView);
            mRootView = itemView;
            final ImageView iconMoreBtn = itemView.findViewById(R.id.imageView);
            iconMoreBtn.setImageResource(R.drawable.ic_more);
        }

        private View getItemView() {
            return mRootView;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(mContext);
        if (viewType == VIEW_TYPE_PRODUCT) {
            final View view = inflater.inflate(R.layout.view_product_recycler_item, viewGroup, false);
            return new ProductViewHolder(mContext, view);
        } else {
            final View view = inflater.inflate(R.layout.view_product_recycler_btn, viewGroup, false);
            return new MoreBtnViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof ProductViewHolder) {
            ProductItemInfo item = mProductsList.get(position);
            final ProductViewHolder productViewHolder = (ProductViewHolder) viewHolder;
            productViewHolder.updateItemView(item);
            productViewHolder.getItemView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final int index = productViewHolder.getAdapterPosition();
                    final ProductItemInfo item = mProductsList.get(index);
                    notifyItemClicked(index, item);
                }
            });
        } else if (viewHolder instanceof MoreBtnViewHolder) {
            final MoreBtnViewHolder moreBtnViewHolder = (MoreBtnViewHolder) viewHolder;
            moreBtnViewHolder.getItemView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    notifyBtnMoreClicked();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if (hasBtnMore()) {
            return mProductsList.size()+1;
        }
        return mProductsList.size();
    }

    private static final int VIEW_TYPE_MORE = 0;
    private static final int VIEW_TYPE_PRODUCT = 1;
    @Override
    public int getItemViewType(int position) {
        if (hasBtnMore() && position >= mProductsList.size()) {
            return VIEW_TYPE_MORE;
        }
        return VIEW_TYPE_PRODUCT;
    }
}
