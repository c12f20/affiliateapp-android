package com.jchain.affiliate.Home;

import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.jchain.affiliate.Account.AccountInfo;
import com.jchain.affiliate.Account.AccountManagement;
import com.jchain.affiliate.Advertisement.AdsManager;
import com.jchain.affiliate.Category.CategoryItemFragment;
import com.jchain.affiliate.Category.CategoryItemInfo;
import com.jchain.affiliate.MainActivity;
import com.jchain.affiliate.Product.CouponsListFragment;
import com.jchain.affiliate.Product.ProductDetailInfoActivity;
import com.jchain.affiliate.Product.ProductInfoManager;
import com.jchain.affiliate.Product.ProductItemInfo;
import com.jchain.affiliate.Product.ProductListFragment;
import com.jchain.affiliate.Product.PromotionItemInfo;
import com.jchain.affiliate.R;
import com.jchain.affiliate.System.SystemInfoManager;
import com.jchain.affiliate.UI.Fragments.BaseFragmentWithDrawer;
import com.jchain.affiliate.UI.Widgets.ImageRecyclerAdapter;
import com.jchain.affiliate.UI.Widgets.ImageViewWithAdsPagerAdapter;
import com.jchain.affiliate.WebAPI.WebProductAPI;
import com.tmall.ultraviewpager.UltraViewPager;
import com.tmall.ultraviewpager.UltraViewPagerAdapter;

import java.util.ArrayList;

public final class HomeFragment extends BaseFragmentWithDrawer {
    private static final String TAG = "HomeFragment";

    private static final String ARG_PARAM_NEED_BACK = "param.needBack";
    public static HomeFragment newInstance(final boolean needBack) {
        Bundle args = new Bundle();
        args.putBoolean(ARG_PARAM_NEED_BACK, needBack);

        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private Handler mHandler = null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHandler = new Handler();
        if (getArguments() != null) {
            mNeedBack = getArguments().getBoolean(ARG_PARAM_NEED_BACK);
        }
    }

    private AccountManagement mAccountManager = null;
    private ProductInfoManager mProductInfoManager = null;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mAccountManager = AccountManagement.getInstance(getContext());
        mProductInfoManager = ProductInfoManager.getInstance(getContext());

        final View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        initView(rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        cancelLoadPromotions();
        cancelLoadCategoriesList();
        cancelLoadBestDealsList();
        cancelLoadHotProducts();
        cancelLoadRecommendProducts();
        super.onDestroyView();
    }

    private void initView(final View rootView) {
        initActionBar(getString(R.string.ID_TITLE_HOME));
        initDrawer(MainActivity.DRAWER_ITEM_ID_HOME);
        initReloadBar(rootView);
        initPromotionsViewPager(rootView);
        initCouponsBar(rootView);
        initCategoriesList(rootView);
        initBestDealList(rootView);
        initHotProductsList(rootView);
        initRecommendProductsList(rootView);
    }

    @Override
    public void onStart() {
        super.onStart();
        loadRecommendProductsList();
    }

    @Override
    public void onResume() {
        super.onResume();
        startUpdatePromotions();
    }

    @Override
    public void onPause() {
        stopUpdatePromotions();
        super.onPause();
    }

    // Promotions ViewPager
    private UltraViewPager mViewPagerPromotions = null;
    private void initPromotionsViewPager(final View rootView) {
        AdsManager.getInstance().init(getContext());

        mViewPagerPromotions = rootView.findViewById(R.id.viewPageAdvertisement);

        loadPromotions();
    }

    private void loadPromotions() {
        mProductInfoManager.loadPromotions(new WebProductAPI.PromotionDataListener() {
            @Override
            public void onGotPromotionsList(int errorCode, ArrayList<PromotionItemInfo> list) {
                if (list != null && !list.isEmpty()) {
                    mProductInfoManager.setPromotionsList(list);

                    fillPromotionsViewPager();
                    startUpdatePromotions();
                } else {
                    Log.e(TAG, "Failed to load promotions, error code: "+errorCode);
                }
            }
        });
    }

    private void cancelLoadPromotions() {
        mProductInfoManager.cancelLoadPromotions();
    }

    private ImageViewWithAdsPagerAdapter mImageViewPagerAdapter = null;
    private UltraViewPagerAdapter mUltraViewPagerAdapter = null;
    private static final int DEFAULT_AUTO_SCROLL_INTERVAL = 5000; // 5 seconds
    private void fillPromotionsViewPager() {
        mImageViewPagerAdapter = new ImageViewWithAdsPagerAdapter(getContext(), R.layout.view_promotion_item, true);
        mImageViewPagerAdapter.setOnImageItemClickListener(new ImageViewWithAdsPagerAdapter.OnImageItemClickListener() {
            @Override
            public void onItemClicked(Bundle imageInfo) {
                final ProductItemInfo product = new ProductItemInfo();
                product.productURL = imageInfo.getString(IMAGE_INFOKEY_URL);
                showProduct(product, true);
            }
        });

        updatePromotionsViewPager();

        mViewPagerPromotions.setScrollMode(UltraViewPager.ScrollMode.HORIZONTAL);
        mUltraViewPagerAdapter = new UltraViewPagerAdapter(mImageViewPagerAdapter);
        mViewPagerPromotions.setAdapter(mUltraViewPagerAdapter);

        final Resources res = getResources();
        mViewPagerPromotions.initIndicator();
        mViewPagerPromotions.getIndicator()
                .setOrientation(UltraViewPager.Orientation.HORIZONTAL)
                .setFocusColor(Color.LTGRAY)
                .setNormalColor(Color.WHITE)
                .setStrokeColor(Color.LTGRAY)
                .setStrokeWidth(res.getDimensionPixelOffset(R.dimen.viewpager_indicator_stoke_width))
                .setRadius(res.getDimensionPixelOffset(R.dimen.viewpager_indicator_radius))
                .setMargin(0, 0, 0, res.getDimensionPixelOffset(R.dimen.viewpager_indicator_margin_bottom))
                .setIndicatorPadding(res.getDimensionPixelOffset(R.dimen.viewpager_indicator_padding));
        mViewPagerPromotions.getIndicator()
                .setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM);
        mViewPagerPromotions.getIndicator().build();
        mViewPagerPromotions.setInfiniteLoop(true);
        mViewPagerPromotions.setAutoScroll(DEFAULT_AUTO_SCROLL_INTERVAL);
    }

    @Override
    public void onDrawerStatusChanged(boolean opened) {
        if (opened) {
            if (mUltraViewPagerAdapter != null) {
                mViewPagerPromotions.disableAutoScroll();
            }
        } else {
            if (mUltraViewPagerAdapter != null) { // resume auto scroll until PromotionsViewPager ready
                mViewPagerPromotions.setAutoScroll(DEFAULT_AUTO_SCROLL_INTERVAL);
            }
        }
    }

    private void updatePromotionsViewPager() {
        final ArrayList<PromotionItemInfo> promotionsList = mProductInfoManager.getNextPromotions();
        if (promotionsList == null) {
            Log.w(TAG, "Failed to load promotions list");
            return;
        }

        final ImageViewWithAdsPagerAdapter.ImageItem[] imageItems = buildImageItems(promotionsList);
        mImageViewPagerAdapter.setImages(imageItems);

        if (mUltraViewPagerAdapter != null) {
            mUltraViewPagerAdapter.notifyDataSetChanged();
        }
    }

    private static final String IMAGE_INFOKEY_URL = "image.url";
    private ImageViewWithAdsPagerAdapter.ImageItem[] buildImageItems(final ArrayList<PromotionItemInfo> promotionsList) {
        int itemCount = 1;
        if (promotionsList != null) {
            itemCount += promotionsList.size();
        }
        final ImageViewWithAdsPagerAdapter.ImageItem[] imageItems = new ImageViewWithAdsPagerAdapter.ImageItem[itemCount];
        //imageItems[itemCount-2] = ImageViewWithAdsPagerAdapter.ImageItem.createAdsItem(AdsManager.ADSTYPE_GOOGLE_ADMOB);
        imageItems[itemCount-1] = ImageViewWithAdsPagerAdapter.ImageItem.createAdsItem(AdsManager.ADSTYPE_FACEBOOK_AUDIENCE);
        if (promotionsList == null) {
            return imageItems;
        }
        for (int i=0; i < promotionsList.size(); i++) {
            final PromotionItemInfo promotionItem = promotionsList.get(i);
            final Bundle info = new Bundle();
            info.putString(IMAGE_INFOKEY_URL, promotionItem.clickURL);
            imageItems[i] = new ImageViewWithAdsPagerAdapter.ImageItem(promotionItem.imageURL, info);
        }
        return imageItems;
    }

    private static final int LOAD_PROMOTION_INTERVAL = 60 * 1000; // 60 seconds
    private Runnable mUpdatePromotionsRunnable = null;
    private void startUpdatePromotions() {
        if (mUltraViewPagerAdapter == null) {
            Log.d(TAG, "Can't startUpdatePromotions as PromotionsViewPager isn't ready yet");
            return;
        }
        if (mUpdatePromotionsRunnable == null) {
            Log.d(TAG, "startUpdatePromotions");
            mUpdatePromotionsRunnable = new Runnable() {
                @Override
                public void run() {
                    updatePromotionsViewPager();
                    if (mUpdatePromotionsRunnable != null) {
                        mHandler.postDelayed(mUpdatePromotionsRunnable, LOAD_PROMOTION_INTERVAL);
                    }
                }
            };
            mHandler.postDelayed(mUpdatePromotionsRunnable, LOAD_PROMOTION_INTERVAL);
        }
    }

    private void stopUpdatePromotions() {
        if (mUpdatePromotionsRunnable != null) {
            Log.d(TAG, "stopUpdatePromotions");
            mHandler.removeCallbacks(mUpdatePromotionsRunnable);
            mUpdatePromotionsRunnable = null;
        }
    }

    // Coupons Bar
    private void initCouponsBar(final View rootView) {
        final View viewCouponsBar = rootView.findViewById(R.id.viewBarCoupons);
        viewCouponsBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCouponsPage();
            }
        });
    }

    private void showCouponsPage() {
        SystemInfoManager.getInstance().checkInternetAvailable(getContext(), new SystemInfoManager.InternetAvailableListener() {
            @Override
            public void onInternetAvailableResult(boolean available) {
                if (!available) {
                    showInvalidError(getString(R.string.ID_MSG_ERROR_NETWORK_FAILURE));
                    return;
                }
                final CouponsListFragment fragment = CouponsListFragment.newInstance();
                showFragment(fragment, true);
            }
        });
    }

    // Categories RecycleView
    private static final String IMAGE_INFOKEY_CATEGORY_INDEX = "image.category_idx";
    private RecyclerView mRecyclerViewCategories = null;
    private ImageRecyclerAdapter mCategoryRecyclerAdapter = null;
    private TextView mTextMsgCategories = null;
    private void initCategoriesList(final View rootView) {
        mTextMsgCategories = rootView.findViewById(R.id.textMsgCategories);

        mRecyclerViewCategories = rootView.findViewById(R.id.listViewCategories);
        mRecyclerViewCategories.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        mCategoryRecyclerAdapter = new ImageRecyclerAdapter(getContext(), R.layout.view_category_image_item);
        mCategoryRecyclerAdapter.setOnItemClickListener(new ImageRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClicked(int position, Bundle info) {
                showProductList(info.getInt(IMAGE_INFOKEY_CATEGORY_INDEX));
            }
        });
        mRecyclerViewCategories.setAdapter(mCategoryRecyclerAdapter);

        loadCategoriesList();
    }

    private void loadCategoriesList() {
        showLoadingCategories();

        mProductInfoManager.loadCategoryList(new WebProductAPI.CategoryDataListener() {
            @Override
            public void onGotCategoriesList(final @WebProductAPI.ProductError int errorCode, final ArrayList<CategoryItemInfo> list) {
                if (list != null && !list.isEmpty()) {
                    if (errorCode == WebProductAPI.PRODUCT_ERROR_NONE) { // only set it as global categories list when there is no error
                        mProductInfoManager.setCategoryList(list);
                    }
                    updateCategoriesList(list);
                } else {
                    showNoCategories();
                }
                int errorMsgResId = R.string.ID_MSG_ERROR_LOAD_CATEGORIES_FAILURE;
                switch(errorCode) {
                    case WebProductAPI.PRODUCT_ERROR_NONE:
                        return;
                    case WebProductAPI.PRODUCT_ERROR_NETWORK:
                        errorMsgResId = R.string.ID_MSG_ERROR_NETWORK_FAILURE;
                        break;
                    case WebProductAPI.PRODUCT_ERROR_PARSE:
                        errorMsgResId = R.string.ID_MSG_ERROR_PARSE_FAILURE;
                        break;
                    case WebProductAPI.PRODUCT_ERROR_GENERAL:
                    default:
                        break;
                }

                showInvalidError(getString(errorMsgResId));
            }
        });
    }

    private void cancelLoadCategoriesList() {
        mProductInfoManager.cancelLoadCategoryList();
    }

    private void showLoadingCategories() {
        mRecyclerViewCategories.setVisibility(View.GONE);
        mTextMsgCategories.setVisibility(View.VISIBLE);
        mTextMsgCategories.setText(R.string.ID_MSG_LOADING_DATA);
    }

    private void hideMsgCategories() {
        mRecyclerViewCategories.setVisibility(View.VISIBLE);
        mTextMsgCategories.setVisibility(View.GONE);
    }

    private void showNoCategories() {
        mRecyclerViewCategories.setVisibility(View.GONE);
        mTextMsgCategories.setVisibility(View.VISIBLE);
        mTextMsgCategories.setText(R.string.ID_MSG_NO_DATA);
    }

    private void updateCategoriesList(final ArrayList<CategoryItemInfo> categories) {
        // Reset visible status
        hideMsgCategories();

        final ImageRecyclerAdapter.ImageItem[] images = new ImageRecyclerAdapter.ImageItem[categories.size()];
        for (int i=0; i < categories.size(); i++) {
            final CategoryItemInfo categoryItem = categories.get(i);
            final Bundle info = new Bundle();
            info.putInt(IMAGE_INFOKEY_CATEGORY_INDEX, i);
            if (categoryItem.iconURL != null) {
                images[i] = new ImageRecyclerAdapter.ImageItem(categoryItem.iconURL, categoryItem.name, info);
            } else {
                images[i] = new ImageRecyclerAdapter.ImageItem(categoryItem.getDefaultIconResId(getContext()), categoryItem.name, info);
            }
        }
        mCategoryRecyclerAdapter.setImages(images);
    }

    // Best Deals RecycleView
    private TextView mTextMsgBestDeals = null;
    private RecyclerView mRecyclerViewBestDeals = null;
    private ProductRecyclerAdapter mBestDealRecyclerAdapter = null;
    private void initBestDealList(final View rootView) {
        mTextMsgBestDeals = rootView.findViewById(R.id.textMsgBestDeal);

        mRecyclerViewBestDeals = rootView.findViewById(R.id.listViewBestDeal);
        mRecyclerViewBestDeals.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        mBestDealRecyclerAdapter = new ProductRecyclerAdapter(getContext());
        mBestDealRecyclerAdapter.setOnItemClickListener(new ProductRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClicked(int position, ProductItemInfo item) {
                if (item.productURL != null) {
                    showProduct(item, false);
                }
            }
        });
        mRecyclerViewBestDeals.setAdapter(mBestDealRecyclerAdapter);

        loadBestDealsList();
    }

    private void loadBestDealsList() {
        showLoadingBestDeals();

        mProductInfoManager.loadBestDeals(new WebProductAPI.ProductDataListener() {
            @Override
            public void onGotProductsList(final @WebProductAPI.ProductError int errorCode, final int totalCount, final int totalPagesCount, final ArrayList<ProductItemInfo> list) {
                if (list != null && !list.isEmpty()) {
                    updateBestDealList(list);
                } else {
                    showNoBestDeals();
                }
                int errorMsgResId = R.string.ID_MSG_ERROR_LOAD_BEST_DEALS_FAILURE;
                switch(errorCode) {
                    case WebProductAPI.PRODUCT_ERROR_NONE:
                        return;
                    case WebProductAPI.PRODUCT_ERROR_NETWORK:
                        errorMsgResId = R.string.ID_MSG_ERROR_NETWORK_FAILURE;
                        break;
                    case WebProductAPI.PRODUCT_ERROR_PARSE:
                        errorMsgResId = R.string.ID_MSG_ERROR_PARSE_FAILURE;
                        break;
                    case WebProductAPI.PRODUCT_ERROR_GENERAL:
                    default:
                        break;
                }
                showInvalidError(getString(errorMsgResId));
            }
        });
    }

    private void cancelLoadBestDealsList() {
        mProductInfoManager.cancelLoadBestDeals();
    }

    private void showLoadingBestDeals() {
        mRecyclerViewBestDeals.setVisibility(View.GONE);
        mTextMsgBestDeals.setVisibility(View.VISIBLE);
        mTextMsgBestDeals.setText(R.string.ID_MSG_LOADING_DATA);
    }

    private void hideMsgBestDeals() {
        mRecyclerViewBestDeals.setVisibility(View.VISIBLE);
        mTextMsgBestDeals.setVisibility(View.GONE);
    }

    private void showNoBestDeals() {
        mRecyclerViewBestDeals.setVisibility(View.GONE);
        mTextMsgBestDeals.setVisibility(View.VISIBLE);
        mTextMsgBestDeals.setText(R.string.ID_MSG_NO_DATA);
    }

    private void updateBestDealList(final ArrayList<ProductItemInfo> bestDeals) {
        if (bestDeals == null || bestDeals.isEmpty()) {
            Log.w(TAG, "Failed to load best deals list");
            return;
        }
        // Reset visible status
        hideMsgBestDeals();

        mBestDealRecyclerAdapter.updateProductList(bestDeals);
    }

    // Hot Products RecycleView
    private TextView mTextMsgHotProducts = null;
    private RecyclerView mRecyclerViewHotProducts = null;
    private ProductRecyclerAdapter mHotProductsRecyclerAdapter = null;
    private void initHotProductsList(final View rootView) {
        mTextMsgHotProducts = rootView.findViewById(R.id.textMsgHotProduct);

        mRecyclerViewHotProducts = rootView.findViewById(R.id.listViewHotProducts);
        mRecyclerViewHotProducts.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        mHotProductsRecyclerAdapter = new ProductRecyclerAdapter(getContext());
        mHotProductsRecyclerAdapter.setOnItemClickListener(new ProductRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClicked(int position, ProductItemInfo item) {
                showProduct(item, false);
            }
        });
        mHotProductsRecyclerAdapter.setOnBtnMoreClickListener(new ProductRecyclerAdapter.OnBtnMoreClickListener() {
            @Override
            public void onClicked() {
                mRecyclerViewHotProducts.scrollToPosition(0);
                mProductInfoManager.resetProductList(); // always reset product list when entering product list
                final ProductListFragment productListFragment = ProductListFragment.newInstance(ProductListFragment.LIST_FROM_HOT_PRODUCTS);
                showFragment(productListFragment, true);
            }
        });
        mRecyclerViewHotProducts.setAdapter(mHotProductsRecyclerAdapter);

        loadHotProductsList();
    }

    private void loadHotProductsList() {
        showLoadingHotProducts();

        mProductInfoManager.loadHotProducts(null, 0, new WebProductAPI.ProductDataListener() {
            @Override
            public void onGotProductsList(int errorCode, int totalCount, int totalPagesCount, ArrayList<ProductItemInfo> list) {
                if (list != null && !list.isEmpty()) {
                    updateHotProductsList(list);
                } else {
                    showNoHotProducts();
                }
                int errorMsgResId = R.string.ID_MSG_ERROR_LOAD_HOT_PRODUCTS_FAILURE;
                switch(errorCode) {
                    case WebProductAPI.PRODUCT_ERROR_NONE:
                        return;
                    case WebProductAPI.PRODUCT_ERROR_NETWORK:
                        errorMsgResId = R.string.ID_MSG_ERROR_NETWORK_FAILURE;
                        break;
                    case WebProductAPI.PRODUCT_ERROR_PARSE:
                        errorMsgResId = R.string.ID_MSG_ERROR_PARSE_FAILURE;
                        break;
                    case WebProductAPI.PRODUCT_ERROR_GENERAL:
                    default:
                        break;
                }
                showInvalidError(getString(errorMsgResId));
            }
        });
    }

    private void cancelLoadHotProducts() {
        mProductInfoManager.cancelLoadHotProducts();
    }

    private void showLoadingHotProducts() {
        mRecyclerViewHotProducts.setVisibility(View.GONE);
        mTextMsgHotProducts.setVisibility(View.VISIBLE);
        mTextMsgHotProducts.setText(R.string.ID_MSG_LOADING_DATA);
    }

    private void hideMsgHotProducts() {
        mRecyclerViewHotProducts.setVisibility(View.VISIBLE);
        mTextMsgHotProducts.setVisibility(View.GONE);
    }

    private void showNoHotProducts() {
        mRecyclerViewHotProducts.setVisibility(View.GONE);
        mTextMsgHotProducts.setVisibility(View.VISIBLE);
        mTextMsgHotProducts.setText(R.string.ID_MSG_NO_DATA);
    }

    private void updateHotProductsList(final ArrayList<ProductItemInfo> hotProducts) {
        if (hotProducts == null || hotProducts.isEmpty()) {
            Log.w(TAG, "Failed to load hot products list");
            return;
        }
        hideMsgHotProducts();
        mHotProductsRecyclerAdapter.updateProductList(hotProducts);
    }
    // Recommend by vehicle RecycleView
    private View mAreaRecommendProducts = null;
    private ProductRecyclerAdapter mRecommendProductsRecyclerAdapter = null;
    private void initRecommendProductsList(final View rootView) {
        mAreaRecommendProducts = rootView.findViewById(R.id.areaRecommendProduct);

        final RecyclerView recyclerViewRecommendProducts = rootView.findViewById(R.id.listViewRecommendProduct);
        recyclerViewRecommendProducts.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        mRecommendProductsRecyclerAdapter = new ProductRecyclerAdapter(getContext());
        mRecommendProductsRecyclerAdapter.setOnItemClickListener(new ProductRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClicked(int position, ProductItemInfo item) {
                showProduct(item, false);
            }
        });
        recyclerViewRecommendProducts.setAdapter(mRecommendProductsRecyclerAdapter);
    }

    private void loadRecommendProductsList() {
        // hide the area when loading
        mAreaRecommendProducts.setVisibility(View.GONE);

        final AccountInfo curUser = mAccountManager.getCurAccountInfo();
        if (curUser == null) {
            Log.d(TAG, "No user login, ignore load recommend products");
            return;
        }
        mProductInfoManager.loadVehicleRelatedProducts(curUser.accessToken, new WebProductAPI.ProductDataListener() {
            @Override
            public void onGotProductsList(int errorCode, int totalCount, int totalPagesCount, ArrayList<ProductItemInfo> list) {
                Log.d(TAG, "Load recommend products result code: "+errorCode);
                if (list != null && !list.isEmpty()) {
                    updateRecommendProductsList(list);
                }
            }
        });
    }

    private void cancelLoadRecommendProducts() {
        mProductInfoManager.cancelLoadVehicleRelatedProducts();
    }


    private void updateRecommendProductsList(final ArrayList<ProductItemInfo> recommendProducts) {
        if (recommendProducts == null || recommendProducts.isEmpty()) {
            Log.w(TAG, "Failed to load recommend products list");
            return;
        }
        mAreaRecommendProducts.setVisibility(View.VISIBLE);
        mRecommendProductsRecyclerAdapter.updateProductList(recommendProducts);
    }
    // Error process methods
    private void showInvalidError(final String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
        showReloadBar();
    }

    private View mReloadBtn = null;
    private void initReloadBar(final View rootView) {
        mReloadBtn = rootView.findViewById(R.id.btnReloadData);
        mReloadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideReloadBar();
                loadPromotions();
                loadCategoriesList();
                loadBestDealsList();
                loadHotProductsList();
            }
        });
        hideReloadBar();
    }

    private void showReloadBar() {
        mReloadBtn.setVisibility(View.VISIBLE);
    }

    private void hideReloadBar() {
        mReloadBtn.setVisibility(View.GONE);
    }

    // Action methods
    private void showProduct(final ProductItemInfo product, final boolean isPromotion) {
        if (product == null || product.productURL == null || product.productURL.isEmpty()) {
            Log.w(TAG, "No valid product URL to show product");
            return;
        }

        SystemInfoManager.getInstance().checkInternetAvailable(getContext(), new SystemInfoManager.InternetAvailableListener() {
            @Override
            public void onInternetAvailableResult(boolean available) {
                if (!available) {
                    showInvalidError(getString(R.string.ID_MSG_ERROR_NETWORK_FAILURE));
                    return;
                }
                ProductDetailInfoActivity.showProduct(getContext(), product, isPromotion);
            }
        });
    }

    private void showProductList(final int categoryIdx) {
        final ArrayList<CategoryItemInfo> categoriesList = mProductInfoManager.getCategoryList();
        if (categoriesList == null || categoryIdx > categoriesList.size()-1) {
            Log.w(TAG, "No categories list or index is invalid "+categoryIdx);
            return;
        }

        mProductInfoManager.resetProductList();
        mProductInfoManager.setCurCategoryIndex(categoryIdx);
        final CategoryItemFragment fragment = CategoryItemFragment.newInstance(categoryIdx, -1);
        showFragment(fragment, true);
    }
}
