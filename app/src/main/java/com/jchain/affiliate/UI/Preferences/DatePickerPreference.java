package com.jchain.affiliate.UI.Preferences;

import android.content.Context;
import android.content.res.TypedArray;
import androidx.annotation.Nullable;
import androidx.preference.Preference;
import android.util.AttributeSet;
import android.util.Log;

import com.jchain.affiliate.R;
import com.tsongkha.spinnerdatepicker.DatePicker;
import com.tsongkha.spinnerdatepicker.DatePickerDialog;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public final class DatePickerPreference extends Preference {
    private static final String TAG = "DatePickerPref";

    public DatePickerPreference(Context context) {
        this(context, null, 0);
    }

    public DatePickerPreference(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DatePickerPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public DatePickerPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        initAttributes(attrs, defStyleAttr, defStyleRes);

        setOnPreferenceClickListener(new OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                showDatePickerDialog();
                return false;
            }
        });
    }

    private String mDateFormat = null;
    private String mMinDateValue = null;
    private String mMaxDateValue = null;
    private void initAttributes(AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        final Context context = getContext();
        if (attrs != null) {
            final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.DatePickerPreference, defStyleAttr, defStyleRes);
            mMinDateValue = a.getString(R.styleable.DatePickerPreference_minDate);
            mMaxDateValue = a.getString(R.styleable.DatePickerPreference_maxDate);
            mDateFormat = a.getString(R.styleable.DatePickerPreference_dateFormat);
            a.recycle();
        }
        if (mDateFormat == null || mDateFormat.isEmpty()) {
            mDateFormat = context.getString(R.string.ID_DEFAULT_DATE_FORMAT);
        }
        if (mMinDateValue == null || mMinDateValue.isEmpty()) {
            mMinDateValue = context.getString(R.string.ID_DEFAULT_DATE_VALUE_MIN);
        }
        if (mMaxDateValue == null || mMaxDateValue.isEmpty()) {
            final Calendar cal = Calendar.getInstance();
            final SimpleDateFormat sdf = new SimpleDateFormat(mDateFormat, Locale.US);
            mMaxDateValue = sdf.format(cal.getTime());
        }
    }

    private String mDefaultDateValue = null;
    private String mCurDateValue = null;
    @Override
    protected void onSetInitialValue(@Nullable Object defaultValue) {
        mDefaultDateValue = getContext().getString(R.string.ID_DEFAULT_DATE_VALUE_MIN);

        if (defaultValue == null) {
            mCurDateValue = getPersistedString(mDefaultDateValue);
        } else {
            mCurDateValue = (String) defaultValue;
            persistString(mCurDateValue);
        }
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return a.getString(index);
    }

    private int[] getYMDFromDateValue(final String dateValue) {
        final Calendar cal = Calendar.getInstance();
        final SimpleDateFormat sdf = new SimpleDateFormat(mDateFormat, Locale.US);
        int[] ymdData = new int[]{0, 0, 0};
        try {
            final Date date = sdf.parse(dateValue);
            cal.setTime(date);
            ymdData[0] = cal.get(Calendar.YEAR);
            ymdData[1] = cal.get(Calendar.MONTH);
            ymdData[2] = cal.get(Calendar.DAY_OF_MONTH);
        } catch (ParseException e) {
            Log.e(TAG, "Failed to parse date value: dateValue", e);
        }
        return ymdData;
    }

    private void showDatePickerDialog() {
        final String curDateValue = getPersistedString(mDefaultDateValue);
        final int[] curDateYMD = getYMDFromDateValue(curDateValue);
        final int[] maxDateYMD = getYMDFromDateValue(mMaxDateValue);
        final int[] minDateYMD = getYMDFromDateValue(mMinDateValue);

        final SpinnerDatePickerDialogBuilder builder = new SpinnerDatePickerDialogBuilder();
        builder.context(getContext())
                .dialogTheme(R.style.CommonDialogStyle)
                .spinnerTheme(R.style.NumberPickerStyle)
                .showTitle(true)
                .showDaySpinner(true)
                .defaultDate(curDateYMD[0], curDateYMD[1], curDateYMD[2])
                .maxDate(maxDateYMD[0], maxDateYMD[1], maxDateYMD[2])
                .minDate(minDateYMD[0], minDateYMD[1], minDateYMD[2])
                .callback(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        final Calendar cal = Calendar.getInstance();
                        final SimpleDateFormat sdf = new SimpleDateFormat(mDateFormat, Locale.US);
                        cal.set(year, monthOfYear, dayOfMonth, 0, 0, 0);
                        final Date newDate = cal.getTime();
                        mCurDateValue = sdf.format(newDate);
                        persistString(mCurDateValue);
                        notifyChanged();
                    }
                });
        final DatePickerDialog dialog = builder.build();
        dialog.show();
    }

    public String getDateValue() {
        return mCurDateValue;
    }
}
