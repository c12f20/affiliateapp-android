package com.jchain.affiliate.UI.Activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.jchain.affiliate.Authorization.LoginActivity;
import com.jchain.affiliate.R;

public abstract class BaseActivityWithActionBar extends AppCompatActivity {
    protected void initView(final Bundle savedInstanceState) {
        initActionBar();
    }

    private View.OnClickListener mOnActionBarBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final int viewId = v.getId();
            switch(viewId) {
                case R.id.back:
                    onBackPressed();
                    break;
                default:
                    break;
            }
        }
    };

    private View mBtnBack = null;
    private TextView mTextTitle = null;
    protected void initActionBar() {
        final Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final View btnDrawer = toolbar.findViewById(R.id.drawer);
        btnDrawer.setVisibility(View.GONE);

        mBtnBack = toolbar.findViewById(R.id.back);
        mBtnBack.setOnClickListener(mOnActionBarBtnClickListener);

        mTextTitle = toolbar.findViewById(R.id.title);

        final ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayShowTitleEnabled(false);
    }

    public void updateActionBar(final String title, final boolean needBack) {
        if (mTextTitle != null) {
            mTextTitle.setText(title);
        }
        mBtnBack.setVisibility(needBack ? View.VISIBLE : View.GONE);
    }

    // Common UI Switch methods
    public void clearToLoginActivity() {
        finishAffinity();

        final Intent intent = new Intent(getBaseContext(), LoginActivity.class);
        startActivity(intent);
    }

    // Fragment Controller
    protected Fragment mCurFragment = null;
    public void showFragment(final Fragment fragment, final boolean needBack) {
        final FragmentManager fm = getSupportFragmentManager();
        final FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.containerMain, fragment);
        if (needBack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
        mCurFragment = fragment;
    }
}
