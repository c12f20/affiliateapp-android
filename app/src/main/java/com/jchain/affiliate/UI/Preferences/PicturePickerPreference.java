package com.jchain.affiliate.UI.Preferences;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.preference.Preference;
import androidx.preference.PreferenceViewHolder;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;

import com.jchain.affiliate.R;
import com.jchain.affiliate.UI.Widgets.OnlineImageViewUtils;

import java.io.File;

public final class PicturePickerPreference extends Preference {
    private static final String TAG = "PicturePickerPref";

    public PicturePickerPreference(Context context) {
        this(context, null, 0);
    }

    public PicturePickerPreference(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PicturePickerPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    private Handler mHandler = null;
    public PicturePickerPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        mHandler = new Handler(context.getMainLooper());

        initAttributes(attrs, defStyleAttr, defStyleRes);

        initOnlineImageViewUtils(context);
    }

    private Drawable mDefaultPicDrawable = null;
    private void initAttributes(AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        final Context context = getContext();
        if (attrs != null) {
            final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.PicturePickerPreference, defStyleAttr, defStyleRes);
            mDefaultPicDrawable = a.getDrawable(R.styleable.PicturePickerPreference_defaultPicture);
            a.recycle();
        }
        if (mDefaultPicDrawable == null) {
            mDefaultPicDrawable = ContextCompat.getDrawable(context, R.drawable.ic_user_profile);
        }
    }

    private OnlineImageViewUtils mOnlineImageViewUtils = null;
    private void initOnlineImageViewUtils(final Context context) {
        final Resources res = context.getResources();
        mOnlineImageViewUtils = new OnlineImageViewUtils(context);
        mOnlineImageViewUtils.setPlaceholdImage(mDefaultPicDrawable);
        mOnlineImageViewUtils.setErrorImage(mDefaultPicDrawable);
        mOnlineImageViewUtils.setImageSize(res.getDimensionPixelOffset(R.dimen.settings_preference_pic_size),
                res.getDimensionPixelOffset(R.dimen.settings_preference_pic_size));
    }

    private ImageView mIconPic = null;
    @Override
    public void onBindViewHolder(PreferenceViewHolder holder) {
        super.onBindViewHolder(holder);
        mIconPic = (ImageView) holder.findViewById(R.id.iconPic);
        updatePicIcon();
    }

    private String mCurPicURL = null;
    @Override
    protected void onSetInitialValue(@Nullable Object defaultValue) {
        if (defaultValue == null) {
            mCurPicURL = getPersistedString("");
        } else {
            mCurPicURL = (String) defaultValue;
            persistString(mCurPicURL);
        }
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return a.getString(index);
    }

    public void updatePicIcon() {
        if (mIconPic == null) {
            return;
        }
        mCurPicURL = getPersistedString("");
        if (!TextUtils.isEmpty(mCurPicURL)) {
            if (mCurPicURL.startsWith("http")) {
                mOnlineImageViewUtils.loadImage(mIconPic, mCurPicURL);
                Log.d(TAG, "Load Image: "+mCurPicURL);
            } else {
                final File imageFile = new File(mCurPicURL);

                Log.d(TAG, "Load Image: " + mCurPicURL + " existed: " + imageFile.exists());
                mOnlineImageViewUtils.loadImage(mIconPic, imageFile);
            }
        } else {
            Log.d(TAG, "Load default Image");
            mIconPic.setImageDrawable(mDefaultPicDrawable);
        }
    }

    public void onActivityResult(final int resultCode, final Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        final Uri uri_data = data.getData();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Get the path from the Uri
                String path = getPathFromURI(uri_data);
                Log.d(TAG, "Will change to picture: "+path);
                if (path != null) {
                    persistString(path);
                    notifyChanged();
                }
            }
        }, 500);
    }

    @Nullable
    private String getPathFromURI(Uri uri) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT
        && DocumentsContract.isDocumentUri(getContext(), uri)) {
            return getPathFromDocumentURI(uri);
        } else if (uri.getScheme().equals("content")){
            return getPathFromContentURI(uri);
        } else if (uri.getScheme().equals("file")) {
            return uri.getPath();
        } else {
            Log.w(TAG, "Unknow Uri: "+uri);
            return null;
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private String getPathFromDocumentURI(final Uri documentUri) {
        final String authority = documentUri.getAuthority();
        if (authority.equals("com.android.providers.downloads.documents")) {
            final String id = DocumentsContract.getDocumentId(documentUri);
            final Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            return getPathFromContentURI(contentUri);
        } else if (authority.equals("com.android.providers.media.documents")) {
            final String docId = DocumentsContract.getDocumentId(documentUri);
            final String[] split = docId.split(":");
            final String type = split[0];
            Uri contentUri = null;
            if (type.equals("image")) {
                contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
            } else if (type.equals("video")) {
                contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
            } else if (type.equals("audio")) {
                contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            }
            final String selection = "_id=?";
            final String[] selectionArgs = new String[]{ split[1] };
            return getPathFromContentURI(contentUri, selection, selectionArgs);
        } else {
            Log.w(TAG, "Authority: "+authority+" isn't supported yet");
            return null;
        }
    }

    private String getPathFromContentURI(final Uri contentUri) {
        return getPathFromContentURI(contentUri, null, null);
    }

    private String getPathFromContentURI(final Uri contentUri, final String selection, final String[] selectionArgs) {
        Cursor cursor = null;
        try {
            final String[] projection = {MediaStore.Images.Media.DATA};
            cursor = getContext().getContentResolver().query(contentUri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                return cursor.getString(column_index);
            } else {
                Log.e(TAG, "Failed to find data of Uri: "+contentUri);
                return null;
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
}
