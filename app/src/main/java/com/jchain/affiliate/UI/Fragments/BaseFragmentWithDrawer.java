package com.jchain.affiliate.UI.Fragments;

import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.jchain.affiliate.R;
import com.jchain.affiliate.Search.SearchActivity;
import com.jchain.affiliate.UI.Activities.BaseActivityWithDrawer;

public abstract class BaseFragmentWithDrawer extends Fragment {
    private static final String TAG = "BaseFragmentWithDrawer";

    private final boolean mNeedSearch;
    protected BaseFragmentWithDrawer() {
        this(true);
    }

    protected BaseFragmentWithDrawer(final boolean needSearch) {
        mNeedSearch = needSearch;
    }

    public void onDrawerStatusChanged(final boolean opened) {
    }

    protected boolean mNeedBack = true;
    protected void initActionBar(final String title) {
        final BaseActivityWithDrawer activity = (BaseActivityWithDrawer) getActivity();
        activity.updateActionBar(title, mNeedBack);

        setHasOptionsMenu(true);
    }

    protected void initDrawer(final long selectedItemId) {
        final BaseActivityWithDrawer activity = (BaseActivityWithDrawer) getActivity();
        activity.updateDrawer(selectedItemId);
    }

    @Override
    public void onDestroyView() {
        clearBackButtonMonitor();
        super.onDestroyView();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (mNeedSearch) {
            inflater.inflate(R.menu.menu_actionbar_search, menu);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int itemId = item.getItemId();
        switch(itemId) {
            case R.id.menu_item_search:
                Log.d(TAG, "Go to Search Activity");
                showSearchActivityForResult();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    // Action methods
    protected boolean mIsHotProductPage = false;
    private void showSearchActivityForResult() {
        SearchActivity.showSearchActivity(getContext(), mIsHotProductPage);
    }

    // Fragment Controller
    protected void showFragment(final Fragment fragment, final boolean needBack) {
        if(getActivity() instanceof BaseActivityWithDrawer) {
            final BaseActivityWithDrawer activity = (BaseActivityWithDrawer) getActivity();
            activity.showFragment(fragment, needBack);
        }
    }

    // Back button listeners
    protected boolean onBackPressed() {
        return false;
    }

    private BaseActivityWithDrawer.BackButtonListener mBackButtonListener = null;
    protected void enableBackButtonMonitor() {
        if (getActivity() instanceof BaseActivityWithDrawer) {
            mBackButtonListener = new BaseActivityWithDrawer.BackButtonListener() {
                @Override
                public boolean onBackPressed() {
                    return BaseFragmentWithDrawer.this.onBackPressed();
                }
            };
            final BaseActivityWithDrawer activity = (BaseActivityWithDrawer) getActivity();
            activity.addBackButtonListener(mBackButtonListener);
        }
    }

    private void clearBackButtonMonitor() {
        if (mBackButtonListener == null) {
            return;
        }
        if (getActivity() instanceof BaseActivityWithDrawer) {
            final BaseActivityWithDrawer activity = (BaseActivityWithDrawer) getActivity();
            activity.removeBackButtonListener(mBackButtonListener);
            mBackButtonListener = null;
        }
    }
}
