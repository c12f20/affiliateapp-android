package com.jchain.affiliate.UI.Activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.jchain.affiliate.Account.AccountInfo;
import com.jchain.affiliate.Account.AccountManagement;
import com.jchain.affiliate.Authorization.LoginActivity;
import com.jchain.affiliate.Settings.UserProfile.UserProfileActivity;
import com.jchain.affiliate.R;
import com.jchain.affiliate.UI.Widgets.OnlineImageViewUtils;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.mikepenz.materialdrawer.util.DrawerImageLoader;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseActivityWithDrawer extends AppCompatActivity {
    private static final String TAG = "BaseActivityWithDrawer";

    protected AccountManagement mAccountMng = null;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAccountMng = AccountManagement.getInstance(getBaseContext());
    }

    protected void initView(final Bundle savedInstanceState) {
        initActionBar();
        initDrawer(savedInstanceState);
    }

    // Action Bar
    private View.OnClickListener mOnActionBarBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final int viewId = v.getId();
            switch(viewId) {
                case R.id.back:
                    onBackPressed();
                    break;
                case R.id.drawer:
                    mDrawer.openDrawer();
                    break;
                default:
                    break;
            }
        }
    };

    private View mBtnBack = null;
    private TextView mTextTitle = null;
    protected void initActionBar() {
        final Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final View btnDrawer = toolbar.findViewById(R.id.drawer);
        btnDrawer.setOnClickListener(mOnActionBarBtnClickListener);

        mBtnBack = toolbar.findViewById(R.id.back);
        mBtnBack.setOnClickListener(mOnActionBarBtnClickListener);

        mTextTitle = toolbar.findViewById(R.id.title);

        final ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayShowTitleEnabled(false);
        }
    }

    public void updateActionBar(final String title, final boolean needBack) {
        if (mTextTitle != null) {
            mTextTitle.setText(title);
        }
        mBtnBack.setVisibility(needBack ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        if (mDrawer != null) {
            outState = mDrawer.saveInstanceState(outState);
        }
        if (mDrawerHeader != null) {
            outState = mDrawerHeader.saveInstanceState(outState);
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        if (mDrawer.isDrawerOpen()) {
            mDrawer.closeDrawer();
        } else {
            if (notifyBackButtonPressed()) {
                return;
            }
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_EDIT_PROFILE) {
            updateProfile();
        }
    }

    // Drawer Header
    protected AccountHeader mDrawerHeader = null;
    protected AccountHeader buildProfileDrawerHeader(final Bundle savedInstanceState) {
        DrawerImageLoader.init(new DrawerImageLoader.IDrawerImageLoader() {
            private final OnlineImageViewUtils mImageViewUtils = new OnlineImageViewUtils(getBaseContext());
            @Override
            public void set(ImageView imageView, Uri uri, Drawable placeholder) {
                set(imageView, uri, placeholder, "");
            }

            @Override
            public void set(ImageView imageView, Uri uri, Drawable placeholder, String tag) {
                mImageViewUtils.setPlaceholdImage(placeholder);
                mImageViewUtils.loadImage(imageView, uri.toString());
            }

            @Override
            public void cancel(ImageView imageView) {
                mImageViewUtils.cancelLoadImage(imageView);
            }

            @Override
            public Drawable placeholder(Context ctx) {
                return placeholder(ctx, "");
            }

            @Override
            public Drawable placeholder(Context ctx, String tag) {
                return ctx.getResources().getDrawable(R.drawable.ic_user_profile);
            }
        });

        final ProfileDrawerItem profile = buildUserProfile();
        final AccountHeaderBuilder builder = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.drawer_header_bg)
                .addProfiles(profile)
                .withSavedInstance(savedInstanceState)
                .withSelectionListEnabled(false)
                .withOnAccountHeaderProfileImageListener(new AccountHeader.OnAccountHeaderProfileImageListener() {
                    @Override
                    public boolean onProfileImageClick(View view, IProfile profile, boolean current) {
                        if (mAccountMng.hasAccountLogin()) {
                            enterEditProfileActivity();
                        } else {
                            enterLoginActivity();
                        }
                        return false;
                    }

                    @Override
                    public boolean onProfileImageLongClick(View view, IProfile profile, boolean current) {
                        return false;
                    }
                });
        return builder.build();
    }

    private void updateProfile() {
        if (mDrawerHeader == null) {
            Log.e(TAG, "updateProfile, DrawerHeader isn't ready");
            return;
        }
        final ProfileDrawerItem profile = buildUserProfile();
        mDrawerHeader.removeProfile(0);
        mDrawerHeader.addProfile(profile, 0);
    }

    private ProfileDrawerItem buildUserProfile() {
        final AccountInfo curAccount = mAccountMng.getCurAccountInfo();
        final ProfileDrawerItem userProfileItem;
        if (curAccount != null) {
            userProfileItem = new ProfileDrawerItem()
                    .withName(curAccount.getDisplayName())
                    .withEmail(curAccount.email);

            if (curAccount.avatarURL != null && !curAccount.avatarURL.isEmpty()) {
                userProfileItem.withIcon(curAccount.avatarURL);
            } else {
                userProfileItem.withIcon(R.drawable.ic_user_profile);
            }
        } else {
            userProfileItem = new ProfileDrawerItem()
                    .withName(R.string.app_name)
                    .withEmail(R.string.ID_CAPTION_NOT_SIGN_IN)
                    .withIcon(R.drawable.ic_user_profile);
        }
        return userProfileItem;
    }

    private void enterLoginActivity() {
        final Intent intent = new Intent(getBaseContext(), LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private static final int REQUEST_CODE_EDIT_PROFILE = 1;
    private void enterEditProfileActivity() {
        final Intent intent = new Intent(getBaseContext(), UserProfileActivity.class);
        startActivityForResult(intent, REQUEST_CODE_EDIT_PROFILE);
    }

    // Drawer
    public static final long DRAWER_ITEM_ID_NONE = 0;
    protected Drawer mDrawer = null;
    protected long mSelectedItemId = DRAWER_ITEM_ID_NONE;

    protected abstract void initDrawer(final Bundle savedInstanceState);

    public void updateDrawer(final long selectedItemId) {
        mDrawer.setSelection(selectedItemId, false);
        mSelectedItemId = selectedItemId;
    }

    // Fragment Controller
    protected Fragment mCurFragment = null;
    public void showFragment(final Fragment fragment, final boolean needBack) {
        final FragmentManager fm = getSupportFragmentManager();
        final FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.containerMain, fragment);
        if (needBack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
        mCurFragment = fragment;
    }

    // Back Button monitor
    public interface BackButtonListener {
        boolean onBackPressed(); // return value indicates whether listener has processed the back action
    }
    private List<BackButtonListener> mBackButtonListeners = new ArrayList<BackButtonListener>();
    public void addBackButtonListener(final BackButtonListener listener) {
        if (listener != null) {
            mBackButtonListeners.add(listener);
        }
    }
    public void removeBackButtonListener(final BackButtonListener listener) {
        if (listener != null) {
            mBackButtonListeners.remove(listener);
        }
    }

    private boolean notifyBackButtonPressed() {
        boolean ret = false;
        for (final BackButtonListener listener : mBackButtonListeners) {
            ret |= listener.onBackPressed();
        }
        return ret;
    }
}
