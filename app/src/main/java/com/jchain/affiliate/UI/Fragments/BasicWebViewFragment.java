package com.jchain.affiliate.UI.Fragments;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.jchain.affiliate.R;

import java.io.IOException;
import java.io.InputStream;

public class BasicWebViewFragment extends BaseFragmentWithDrawer {
    private static final String TAG = "BasicWebViewFragment";

    protected static final String ARG_PARAM_TITLE = "param.title";
    protected static final String ARG_PARAM_DATA_RES_ID = "param.dataResId";
    protected static final String ARG_PARAM_WEBVIEW_URL = "param.webViewURL";
    protected static final String ARG_PARAM_BACK_ENABLED = "param.backEnabled";

    public static BasicWebViewFragment newInstance(final String title, final int dataResId) {
        Bundle args = new Bundle();
        args.putString(ARG_PARAM_TITLE, title);
        args.putInt(ARG_PARAM_DATA_RES_ID, dataResId);

        BasicWebViewFragment fragment = new BasicWebViewFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static BasicWebViewFragment newInstance(final String title, final String url, final boolean backEnabled) {
        Bundle args = new Bundle();
        args.putString(ARG_PARAM_TITLE, title);
        args.putString(ARG_PARAM_WEBVIEW_URL, url);
        args.putBoolean(ARG_PARAM_BACK_ENABLED, backEnabled);

        BasicWebViewFragment fragment = new BasicWebViewFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public BasicWebViewFragment() {
        super(false);
    }

    private String mTitle = null;
    private String mWebViewURL = null;
    private int mDataResId = -1;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Bundle args = getArguments();
        if (args != null) {
            mTitle = args.getString(ARG_PARAM_TITLE);
            if (args.containsKey(ARG_PARAM_WEBVIEW_URL)) {
                mWebViewURL = getArguments().getString(ARG_PARAM_WEBVIEW_URL);
            } else if (args.containsKey(ARG_PARAM_DATA_RES_ID)) {
                mDataResId = args.getInt(ARG_PARAM_DATA_RES_ID);
            }
            if (args.containsKey(ARG_PARAM_BACK_ENABLED) && args.getBoolean(ARG_PARAM_BACK_ENABLED)) {
                enableBackButtonMonitor();
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_webview, container, false);
        initData();
        initView(rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mWebView != null) {
            mWebView.resumeTimers();
            mWebView.destroy();
            mWebView = null;
        }
    }

    @Override
    public void onPause() {
        if (mWebView != null) {
            mWebView.onPause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mWebView != null) {
            mWebView.onResume();
        }
    }

    @Override
    public void onStop() {
        if (mWebView != null) {
            mWebView.pauseTimers();
        }
        super.onStop();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mWebView != null) {
            mWebView.resumeTimers();
        }
    }

    @Override
    protected boolean onBackPressed() {
        Log.d(TAG, "onBackPressed");
        if (mWebView != null && mWebView.canGoBack()) {
            mWebView.goBack();
            return true;
        }
        return false;
    }

    private static final int DEFAULT_BUFFER_SIZE = 1024;
    private String mHTMLData = null;
    private void initData() {
        if (mDataResId < 0) {
            return;
        }
        final Resources res = getResources();
        final InputStream is = res.openRawResource(mDataResId);
        try {
            byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
            int readCount = 0;
            StringBuilder sb = new StringBuilder();
            while ((readCount = is.read(buffer)) >= 0) {
                if (readCount == 0) {
                    continue;
                }
                String data = new String(buffer, 0, readCount, "UTF-8");
                sb.append(data);
            }
            mHTMLData = sb.toString();
        } catch (IOException e) {
            Log.e(TAG, "Failed to read file, exception: ", e);
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                Log.w(TAG, "Failed to close file, exception: ", e);
            }
        }
    }

    protected void initView(final View rootView) {
        initActionBar(mTitle);
        initProgressBar(rootView);
        initWebView(rootView);
    }

    private ProgressBar mProgressBar = null;
    private void initProgressBar(final View rootView) {
        mProgressBar = rootView.findViewById(R.id.progressBar);
        hideProgressBar();
    }

    private void showProgressBar() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar() {
        mProgressBar.setVisibility(View.GONE);
    }

    private WebView mWebView = null;
    private void initWebView(final View rootView) {
        mWebView = rootView.findViewById(R.id.webview);

        final WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);

        if (mHTMLData != null) {
            mWebView.loadDataWithBaseURL(null, mHTMLData, "text/html", "UTF-8", null);
        } else if (mWebViewURL != null) {
            mWebView.loadUrl(mWebViewURL);
            mWebView.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    super.onPageStarted(view, url, favicon);
                    Log.d(TAG, "Basic Webview onPageStarted: "+url);
                    BasicWebViewFragment.this.onPageStarted(view, url);
                    showProgressBar();
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    Log.d(TAG, "Basic Webview onPageFinished: "+url);
                    BasicWebViewFragment.this.onPageFinished(view, url, true);
                    hideProgressBar();
                }

                @Override
                public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                    super.onReceivedError(view, request, error);
                    Log.d(TAG, "Basic Webview onReceivedError: "+error);
                    BasicWebViewFragment.this.onPageFinished(view, view.getUrl(), false);
                    hideProgressBar();
                }
            });
        }
    }

    protected WebView getWebView() {
        return mWebView;
    }

    protected String getBaseURL() {
        return mWebViewURL;
    }

    protected void onPageStarted(final WebView webView, final String url) {
    }

    protected void onPageFinished(final WebView webView, final String url, final boolean success) {
    }
}
