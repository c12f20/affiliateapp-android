package com.jchain.affiliate.UI.Widgets;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.jchain.affiliate.Advertisement.AdsManager;
import com.jchain.affiliate.R;

import java.util.ArrayList;
import java.util.Arrays;

public final class ImageViewWithAdsPagerAdapter extends PagerAdapter {
    private static final String TAG = "ImageAdsPagerAdapter";

    private final Context mContext;
    private final int mItemLayoutResId;
    private final boolean mWithAds;
    private final OnlineImageViewUtils mOnlineImageViewUtils;
    private final AdsManager mAdsManager;

    public ImageViewWithAdsPagerAdapter(final Context context, final int itemLayoutResId) {
        this(context, itemLayoutResId, false);
    }

    public ImageViewWithAdsPagerAdapter(final Context context, final int itemLayoutResId, final boolean withAds) {
        mContext = context;
        mItemLayoutResId = itemLayoutResId;
        mWithAds = withAds;
        mAdsManager = AdsManager.getInstance();
        mOnlineImageViewUtils = new OnlineImageViewUtils(context);
    }

    public void setImageSize(final int width, final int height) {
        mOnlineImageViewUtils.setImageSize(width, height);
    }

    public interface OnImageItemClickListener {
        void onItemClicked(final Bundle imageInfo);
    }

    private OnImageItemClickListener mOnImageItemClickListener = null;

    public void setOnImageItemClickListener(final OnImageItemClickListener listener) {
        mOnImageItemClickListener = listener;
    }

    private void notifyOnImageItemClicked(final ImageItem image) {
        if (mOnImageItemClickListener != null) {
            mOnImageItemClickListener.onItemClicked(image.imageInfo);
        }
    }

    public static class ImageItem {
        private final int imageResId;
        private final String imageURL;
        private final Bundle imageInfo;
        private final @AdsManager.AdsType int imageAdsType;
        private View attachedView = null;

        public ImageItem(final String url, final Bundle info) {
            this(url, -1, AdsManager.ADSTYPE_NONE, info);
        }

        public ImageItem(final int resId, final Bundle info) {
            this(null, resId, AdsManager.ADSTYPE_NONE, info);
        }

        private ImageItem(final int adsType) {
            this(null, -1, adsType, null);
        }

        public static ImageItem createAdsItem(final int adsType) {
            return new ImageItem(adsType);
        }

        public ImageItem(final String url, final int resId, final int adsType, final Bundle info) {
            imageURL = url;
            imageResId = resId;
            imageAdsType = adsType;
            imageInfo = info;
        }
    }

    private ArrayList<ImageItem> mImagesList = new ArrayList<ImageItem>();
    public void setImages(ImageItem[] images) {
        if (images == null) {
            return;
        }
        mImagesList.clear();
        mImagesList.addAll(Arrays.asList(images));
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mImagesList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        if (!(object instanceof ImageItem)) {
            return false;
        }
        final ImageItem imageItem = (ImageItem)object;
        return view == imageItem.attachedView;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        for (int i=0; i < mImagesList.size(); i++) {
            final ImageItem imageItem = mImagesList.get(i);
            if (imageItem == object) {
                return i;
            }
        }
        return PagerAdapter.POSITION_NONE;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        if (position < 0 || position >= mImagesList.size()) {
            Log.e(TAG, "Invalid position: "+position);
            return super.instantiateItem(container, position);
        }
        final ImageItem imageItem = mImagesList.get(position);
        if (mWithAds && imageItem.imageURL == null && imageItem.imageResId < 0) {
            if (imageItem.imageAdsType == AdsManager.ADSTYPE_FACEBOOK_AUDIENCE) {
                initFacebookAdsItem(imageItem, container);
            } else { // default is Google Ads
                //initGoogleAdsItem(imageItem);
                initFacebookAdsItem(imageItem, container);
            }
        } else {
            initImgItem(imageItem, container);
        }
        if (imageItem.attachedView.getParent() == null) {
            container.addView(imageItem.attachedView);
        }
        return imageItem;
    }

    private static final int USE_RECT_ADS_MIN_HEIGHT = 250; // min height 250dp for rect ads
    /*private PublisherAdView mGoogleAdView = null;
    private void initGoogleAdsItem(final ImageItem imageItem) {
        if (mGoogleAdView == null) {
            final Resources res = mContext.getResources();
            final int itemHeightDp = Math.round(res.getDimensionPixelOffset(R.dimen.home_advertise_height)
                    / res.getDisplayMetrics().density);
            if (itemHeightDp < USE_RECT_ADS_MIN_HEIGHT) {
                mGoogleAdView = mAdsManager.createGoogleAdView(mContext);
            } else {
                mGoogleAdView = mAdsManager.createGoogleRectAdView(mContext);
            }
        } else {
            mGoogleAdView.resume();
        }
        imageItem.attachedView = mGoogleAdView;
    }*/

    private void initFacebookAdsItem(final ImageItem imageItem, final ViewGroup container) {
        final RelativeLayout adsContainer = (RelativeLayout) LayoutInflater.from(mContext).inflate(R.layout.view_advertisement_item, container, false);
        final Resources res = mContext.getResources();
        final int itemHeightDp = Math.round(res.getDimensionPixelOffset(R.dimen.home_advertise_height)
                / res.getDisplayMetrics().density);
        final View adsView;
        if (itemHeightDp < USE_RECT_ADS_MIN_HEIGHT) {
            adsView = mAdsManager.createFacebookAdView(mContext);
        } else {
            adsView = mAdsManager.createFacebookRectAdView(mContext);
        }
        adsContainer.addView(adsView, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        imageItem.attachedView = adsContainer;
    }

    private void initImgItem(final ImageItem imageItem, final ViewGroup container) {
        final View newView = LayoutInflater.from(mContext).inflate(mItemLayoutResId, container, false);
        imageItem.attachedView = newView;
        final ImageView imageView = newView.findViewById(R.id.imageView);
        if (imageItem.imageURL != null) {
            mOnlineImageViewUtils.loadImage(imageView, imageItem.imageURL);
        } else if (imageItem.imageResId >= 0) {
            imageView.setImageResource(imageItem.imageResId);
        }

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notifyOnImageItemClicked(imageItem);
            }
        });
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        if (!(object instanceof ImageItem)) {
            return;
        }
        final ImageItem imageItem = (ImageItem) object;
        /*if (imageItem.attachedView instanceof com.google.android.gms.ads.AdView) {
            final com.google.android.gms.ads.AdView adView = (com.google.android.gms.ads.AdView) imageItem.attachedView;
            adView.pause();
        }*/
        container.removeView(imageItem.attachedView);
    }
}
