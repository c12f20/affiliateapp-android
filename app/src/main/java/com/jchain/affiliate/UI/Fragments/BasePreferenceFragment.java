package com.jchain.affiliate.UI.Fragments;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceGroup;
import androidx.preference.PreferenceScreen;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jchain.affiliate.UI.Activities.BaseActivityWithActionBar;
import com.jchain.affiliate.UI.Activities.BaseSettingsActivity;
import com.jchain.affiliate.UI.Preferences.DatePickerPreference;
import com.jchain.affiliate.UI.Preferences.PicturePickerPreference;
import com.takisoft.fix.support.v7.preference.EditTextPreference;
import com.takisoft.fix.support.v7.preference.PreferenceFragmentCompat;

public abstract class BasePreferenceFragment extends PreferenceFragmentCompat
    implements SharedPreferences.OnSharedPreferenceChangeListener {
    private static final String TAG = "BasePreferenceFragment";

    protected void initSummary(final Preference pref) {
        if (pref instanceof PreferenceGroup) {
            PreferenceGroup group = (PreferenceGroup) pref;
            for (int i=0; i < group.getPreferenceCount(); i++) {
                initSummary(group.getPreference(i));
            }
        } else {
            updateSummary(pref);
        }
    }

    protected void updateSummary(final Preference pref) {
        CharSequence summary = null;
        if (pref instanceof ListPreference) {
            ListPreference listPref = (ListPreference) pref;
            summary = listPref.getEntry();
        } else if (pref instanceof EditTextPreference) {
            EditTextPreference editTextPref = (EditTextPreference) pref;
            summary = editTextPref.getText();
        } else if (pref instanceof DatePickerPreference) {
            DatePickerPreference datePickerPref = (DatePickerPreference) pref;
            summary = datePickerPref.getDateValue();
        } else if (pref instanceof PicturePickerPreference) {
            PicturePickerPreference picturePickerPref = (PicturePickerPreference) pref;
            picturePickerPref.updatePicIcon();
            return;
        }
        if (summary == null || summary.length() == 0) { // Ensure summary to be not empty in order to keep the UI layout
            summary = " ";
        }
        if (pref != null) {
            pref.setSummary(summary);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        final PreferenceScreen preferenceScreen = getPreferenceScreen();
        if (preferenceScreen != null) {
            preferenceScreen.getSharedPreferences()
                    .registerOnSharedPreferenceChangeListener(this);
        }
    }

    @Override
    public void onPause() {
        final PreferenceScreen preferenceScreen = getPreferenceScreen();
        if (preferenceScreen != null) {
            preferenceScreen.getSharedPreferences()
                    .unregisterOnSharedPreferenceChangeListener(this);
        }
        super.onPause();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        final Preference pref = findPreference(key);
        if (pref != null) {
            if (pref instanceof EditTextPreference) {
                final EditTextPreference editTextPref = (EditTextPreference) pref;
                editTextPref.setText(sharedPreferences.getString(key, ""));
            }
            updateSummary(pref);
        } else {
            Log.w(TAG, "Don't updateSummary as can't find pref with key: "+key);
        }
    }

    // Activity related methods
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        addCustomView();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        removeCustomView();
        super.onDestroyView();
    }

    protected boolean mNeedBack = true;
    protected void initActionBar(final String title) {
        final Activity activity = getActivity();
        if (activity instanceof BaseActivityWithActionBar) {
            final BaseActivityWithActionBar baseActivityWithActionBar = (BaseActivityWithActionBar) activity;
            baseActivityWithActionBar.updateActionBar(title, mNeedBack);
        } else {
            Log.e(TAG, "Failed to initActionBar as parent activity doesn't support");
        }
    }

    protected void clearToLoginActivity() {
        final Activity activity = getActivity();
        if (activity instanceof BaseActivityWithActionBar) {
            final BaseActivityWithActionBar baseActivityWithActionBar = (BaseActivityWithActionBar) activity;
            baseActivityWithActionBar.clearToLoginActivity();
        } else {
            Log.e(TAG, "Failed to clearToLoginActivity as parent activity doesn't support");
        }
    }

    protected void showFragment(final Fragment fragment, final boolean needBack) {
        final Activity activity = getActivity();
        if (activity instanceof BaseActivityWithActionBar) {
            final BaseActivityWithActionBar baseActivityWithActionBar = (BaseActivityWithActionBar) activity;
            baseActivityWithActionBar.showFragment(fragment, needBack);
        } else {
            Log.e(TAG, "Failed to showFragment as parent activity doesn't support");
        }
    }
    // Custom Bottom View
    private View mCustomView = null;
    protected void setCustomView(final View view) {
        mCustomView = view;
    }

    private void addCustomView() {
        if (mCustomView == null) {
            return;
        }
        final Activity activity = getActivity();
        if (activity instanceof BaseSettingsActivity) {
            final BaseSettingsActivity baseSettingsActivity = (BaseSettingsActivity) activity;
            baseSettingsActivity.setCustomBottomView(mCustomView);
        } else {
            Log.e(TAG, "Failed to addCustomView as parent activity doesn't support");
        }
    }

    private void removeCustomView() {
        if (mCustomView == null) {
            return;
        }
        mCustomView = null;
        final Activity activity = getActivity();
        if (activity instanceof BaseSettingsActivity) {
            final BaseSettingsActivity baseSettingsActivity = (BaseSettingsActivity) activity;
            baseSettingsActivity.setCustomBottomView(null);
        } else {
            Log.e(TAG, "Failed to removeCustomView as parent activity doesn't support");
        }
    }
    // Loading view
    protected void showLoading() {
        final Activity activity = getActivity();
        if (activity instanceof BaseSettingsActivity) {
            final BaseSettingsActivity baseSettingsActivity = (BaseSettingsActivity) activity;
            baseSettingsActivity.showLoading();
        }
    }

    protected void hideLoading() {
        final Activity activity = getActivity();
        if (activity instanceof BaseSettingsActivity) {
            final BaseSettingsActivity baseSettingsActivity = (BaseSettingsActivity) activity;
            baseSettingsActivity.hideLoading();
        }
    }
}
