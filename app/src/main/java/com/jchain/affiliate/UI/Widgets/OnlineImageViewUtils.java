package com.jchain.affiliate.UI.Widgets;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.request.RequestOptions;

import java.io.File;

public final class OnlineImageViewUtils {
    private static final String TAG = "OnlineImageViewUtils";

    private final Context mContext;
    public OnlineImageViewUtils(final Context context) {
        mContext = context;
    }

    private int mPlaceholdImageResId = -1;
    public OnlineImageViewUtils setPlaceholdImage(final int placeholdImageResId) {
        mPlaceholdImageResId = placeholdImageResId;
        return this;
    }

    private Drawable mPlaceholdDrawable = null;
    public OnlineImageViewUtils setPlaceholdImage(final Drawable placeholdDrawable) {
        mPlaceholdDrawable = placeholdDrawable;
        return this;
    }

    private int mErrorImageResId = -1;
    public OnlineImageViewUtils setErrorImage(final int errorImageResId) {
        mErrorImageResId = errorImageResId;
        return this;
    }

    private Drawable mErrorImageDrawable = null;
    public OnlineImageViewUtils setErrorImage(final Drawable errorImageDrawable) {
        mErrorImageDrawable = errorImageDrawable;
        return this;
    }

    private int mImageWidth = -1;
    private int mImageHeight = -1;
    public OnlineImageViewUtils setImageSize(final int imageWidth, final int imageHeight) {
        mImageWidth = imageWidth;
        mImageHeight = imageHeight;
        return this;
    }

    private void setupParameters(final RequestBuilder builder) {
        final RequestOptions options = new RequestOptions();
        if (mPlaceholdImageResId >= 0) {
            options.placeholder(mPlaceholdImageResId);
        }
        if (mPlaceholdDrawable != null) {
            options.placeholder(mPlaceholdDrawable);
        }
        if (mErrorImageResId >= 0) {
            options.error(mErrorImageResId);
        }
        if (mErrorImageDrawable != null) {
            options.error(mErrorImageDrawable);
        }
        if (mImageWidth > 0 && mImageHeight > 0) {
            options.centerCrop();
            options.override(mImageWidth, mImageHeight);
        }
        builder.apply(options);
    }

    public ImageView loadImage(final ImageView view, final File imageFile) {
        final String imageURL = Uri.fromFile(imageFile).toString();
        return loadImage(view, imageURL);
    }

    public ImageView loadImage(final ImageView view, final String imageURL) {
        cancelLoadImage(view);

        final RequestBuilder builder;
        if (imageURL.endsWith(".gif")) {
            builder = Glide.with(mContext).asGif().load(imageURL);
        } else {
            builder = Glide.with(mContext).load(imageURL);
        }
        setupParameters(builder);
        builder.into(view);
        return view;
    }

    public void cancelLoadImage(final ImageView view) {
        if (view == null) {
            Log.w(TAG, "Failed to cancel load image as Image view is invalid");
            return;
        }
        Glide.with(mContext).clear(view);
    }
}
