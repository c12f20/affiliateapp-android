package com.jchain.affiliate.UI.Widgets;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jchain.affiliate.R;

import java.util.ArrayList;
import java.util.Arrays;

public final class ImageRecyclerAdapter extends RecyclerView.Adapter<ImageRecyclerAdapter.ImageViewHolder> {
    private final Context mContext;
    private final int mItemLayoutResId;
    public ImageRecyclerAdapter(final Context context, final int itemLayoutResId) {
        mContext = context;
        mItemLayoutResId = itemLayoutResId;
    }

    public interface OnItemClickListener {
        void onItemClicked(final int position, final Bundle info);
    }

    private OnItemClickListener mOnItemClickListener = null;
    public void setOnItemClickListener(final OnItemClickListener listener) {
        mOnItemClickListener = listener;
    }

    private void notifyItemClicked(final int position, final Bundle info) {
        if (mOnItemClickListener != null) {
            mOnItemClickListener.onItemClicked(position, info);
        }
    }

    public static class ImageItem {
        private final int imageResId;
        private final String imageCaption;
        private final String imageURL;
        private final Bundle imageInfo;

        public ImageItem(final String url, final Bundle info) {
            this(url, null, info);
        }

        public ImageItem(final int resId, final Bundle info) {
            this( resId, null, info);
        }


        public ImageItem(final String url, final String caption, final Bundle info) {
            this(url, -1, caption, info);
        }

        public ImageItem(final int resId, final String caption, final Bundle info) {
            this(null, resId, caption, info);
        }

        public ImageItem(final String url, final int resId, final String caption, final Bundle info) {
            imageURL = url;
            imageResId = resId;
            imageCaption = caption;
            imageInfo = info;
        }
    }

    private ArrayList<ImageItem> mImagesList = new ArrayList<ImageItem>();
    public void setImages(final ImageItem[] images) {
        if (images == null) {
            return;
        }
        mImagesList.clear();
        mImagesList.addAll(Arrays.asList(images));
        notifyDataSetChanged();
    }

    protected static class ImageViewHolder extends RecyclerView.ViewHolder {
        private final View mRootView;
        private final ImageView mImageView;
        private final TextView mTextCaption;

        private final OnlineImageViewUtils mOnlineImageViewUtils;

        private ImageViewHolder(final Context context, @NonNull View itemView) {
            super(itemView);
            mRootView = itemView;
            mImageView = itemView.findViewById(R.id.imageView);
            mTextCaption = itemView.findViewById(R.id.caption);

            mOnlineImageViewUtils = new OnlineImageViewUtils(context);
        }

        private View getItemView() {
            return mRootView;
        }

        private void setImageURL(final String url) {
            if (url == null || url.isEmpty()) {
                return;
            }
            mOnlineImageViewUtils.loadImage(mImageView, url);
        }

        private void setImageResId(final int resId) {
            mImageView.setImageResource(resId);
        }

        private void setCaption(final String caption) {
            if (caption == null || mTextCaption == null) {
                return;
            }
            mTextCaption.setText(caption);
        }
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View rootView = LayoutInflater.from(mContext).inflate(mItemLayoutResId, parent, false);
        return new ImageViewHolder(mContext, rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ImageViewHolder holder, int position) {
        if (position < 0 || position >= mImagesList.size()) {
            return;
        }
        final ImageItem item = mImagesList.get(position);
        if (item.imageCaption != null) {
            holder.setCaption(item.imageCaption);
        }
        if (item.imageURL != null) {
            holder.setImageURL(item.imageURL);
        } else if (item.imageResId >= 0) {
            holder.setImageResId(item.imageResId);
        }
        holder.getItemView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notifyItemClicked(holder.getAdapterPosition(), item.imageInfo);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mImagesList.size();
    }
}
