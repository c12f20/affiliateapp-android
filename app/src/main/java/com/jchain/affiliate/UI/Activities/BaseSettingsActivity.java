package com.jchain.affiliate.UI.Activities;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.jchain.affiliate.R;

public class BaseSettingsActivity extends BaseActivityWithActionBar {

    private View mLoadingArea = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_settings);

        mLoadingArea = findViewById(R.id.loadingArea);
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);
    }

    public void setCustomBottomView(final View view) {
        final FrameLayout customBottomArea = findViewById(R.id.customBottomArea);
        customBottomArea.removeAllViews();
        if (view != null) {
            final FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            customBottomArea.addView(view, layoutParams);
            customBottomArea.setVisibility(View.VISIBLE);
        }
    }

    public void showLoading() {
        mLoadingArea.setVisibility(View.VISIBLE);
    }

    public void hideLoading() {
        mLoadingArea.setVisibility(View.GONE);
    }
}
