package com.jchain.affiliate.UI.Widgets;

import android.content.Context;
import android.content.res.TypedArray;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jchain.affiliate.R;

public final class FilterExpandableView extends LinearLayout {
    public FilterExpandableView(Context context) {
        this(context, null);
    }

    public FilterExpandableView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FilterExpandableView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAttributes(attrs, defStyleAttr);
        initView(context);
    }

    private static final int DEFAULT_SPAN_COUNT = 2;
    private String mTitleValue = "";
    private int mSpanCount = DEFAULT_SPAN_COUNT;
    private boolean mIsFilterExpanded = true;
    private void initAttributes(AttributeSet attrs, int defStyleAttr) {
        final Context context = getContext();
        if (attrs != null) {
            final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.FilterExpandableView, defStyleAttr, 0);
            mTitleValue = a.getString(R.styleable.FilterExpandableView_title);
            mSpanCount = a.getInteger(R.styleable.FilterExpandableView_spanCount, 0);
            mIsFilterExpanded = a.getBoolean(R.styleable.FilterExpandableView_expanded, mIsFilterExpanded);
            a.recycle();
        }
    }

    private TextView mTextFilterValue = null;
    private RecyclerView mRecyclerViewFilters = null;
    private ImageView mTitleFilterExpandIcon = null;
    private void initView(final Context context) {
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View rootView = inflater.inflate(R.layout.view_expandable_filter, this);
        final View mAreaFilterTitle = rootView.findViewById(R.id.areaFilterTitle);
        mAreaFilterTitle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                switchExpandedStatus();
            }
        });
        final TextView mTextFilterTitle = rootView.findViewById(R.id.textFilterTitle);
        mTextFilterTitle.setText(mTitleValue);
        mTextFilterValue = rootView.findViewById(R.id.textFilterValue);
        mTitleFilterExpandIcon = rootView.findViewById(R.id.iconExpand);

        mRecyclerViewFilters = rootView.findViewById(R.id.listViewFilter);
        mRecyclerViewFilters.setLayoutManager(new GridLayoutManager(getContext(), mSpanCount));

        setExpandedStatus(mIsFilterExpanded);
    }

    private void switchExpandedStatus() {
        mIsFilterExpanded = !mIsFilterExpanded;
        setExpandedStatus(mIsFilterExpanded);
    }

    public void setExpandedStatus(final boolean expanded) {
        mIsFilterExpanded = expanded;
        if (mIsFilterExpanded) {
            mTitleFilterExpandIcon.setImageResource(R.drawable.ic_arrow_down);
            mRecyclerViewFilters.setVisibility(View.VISIBLE);
        } else {
            mTitleFilterExpandIcon.setImageResource(R.drawable.ic_arrow_right);
            mRecyclerViewFilters.setVisibility(View.GONE);
        }
    }

    public void setAdapter(final RecyclerView.Adapter<RecyclerView.ViewHolder> adapter) {
        mRecyclerViewFilters.setAdapter(adapter);
    }

    public void setFilterValue(final String value) {
        mTextFilterValue.setText(value);
    }
}
