package com.jchain.affiliate.Search;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.PersistableBundle;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.jchain.affiliate.Account.AccountInfo;
import com.jchain.affiliate.Account.AccountManagement;
import com.jchain.affiliate.Category.CategoryItemInfo;
import com.jchain.affiliate.Tracker.PriceTrackerItemInfo;
import com.jchain.affiliate.Product.ProductDetailInfoActivity;
import com.jchain.affiliate.Product.ProductInfoManager;
import com.jchain.affiliate.Product.ProductItemInfo;
import com.jchain.affiliate.Product.ProductItemRecyclerAdapter;
import com.jchain.affiliate.R;
import com.jchain.affiliate.Settings.VehicleProfile.VehicleInfo;
import com.jchain.affiliate.System.SystemInfoManager;
import com.jchain.affiliate.UI.Widgets.FilterExpandableView;
import com.jchain.affiliate.WebAPI.WebProductAPI;
import com.jchain.affiliate.WebAPI.WebVehicleAPI;

import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends AppCompatActivity {
    private static final String TAG = "SearchActivity";

    private Handler mHandler = null;
    private ProductInfoManager mProductInfoManager = null;
    private AccountManagement mAccountManager = null;
    private WebVehicleAPI mWebVehicleAPI = null;

    private static final String PARAM_NAME_IS_HOT_PRODUCTS = "param.is_hot";

    public static void showSearchActivity(final Context context, final boolean isHot) {
        final Intent intent = new Intent(context, SearchActivity.class);
        intent.putExtra(PARAM_NAME_IS_HOT_PRODUCTS, isHot);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        mHandler = new Handler();
        mProductInfoManager = ProductInfoManager.getInstance(getBaseContext());
        mAccountManager = AccountManagement.getInstance(getBaseContext());
        mWebVehicleAPI = WebVehicleAPI.getInstance(getBaseContext());

        processIntent();
        initData();
        initView();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            mIsHot = savedInstanceState.getBoolean(PARAM_NAME_IS_HOT_PRODUCTS, false);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        outState.putBoolean(PARAM_NAME_IS_HOT_PRODUCTS, mIsHot);
        super.onSaveInstanceState(outState, outPersistentState);
    }

    @Override
    protected void onDestroy() {
        cancelSearch();
        super.onDestroy();
    }

    // Data
    private boolean mIsHot = false;
    private void processIntent() {
        final Intent intent = getIntent();
        mIsHot = intent.getBooleanExtra(PARAM_NAME_IS_HOT_PRODUCTS, false);
    }

    private ArrayList<CategoryItemInfo> mCategoryFilterList = null;
    private void initData() {
        if (!mIsHot) {
            mCategoryFilterList = mProductInfoManager.getCategoryList();
            initVehicleInfo();
        }
    }

    private AccountInfo mCurAccount = null;
    private ArrayList<VehicleInfo> mVehicleFilterList = new ArrayList<VehicleInfo>();
    private void initVehicleInfo() {
        mCurAccount = mAccountManager.getCurAccountInfo();
        if (mCurAccount == null) {
            return;
        }
        final List<VehicleInfo> vehiclesList = mCurAccount.getVehicles();
        if (mCurAccount.hasSyncedVehicles()) {
            mVehicleFilterList.addAll(vehiclesList);
            return;
        }
        mWebVehicleAPI.queryVehiclesByAccount(mCurAccount, new WebVehicleAPI.VehicleDataListener() {
            @Override
            public void onGotVehiclesList(int errorCode, List<VehicleInfo> list) {
                if (errorCode == WebVehicleAPI.VEHICLE_ERROR_NONE && list != null) {
                    mCurAccount.setVehicles(list);
                    mVehicleFilterList.addAll(list);
                    updateVehicleFilter();
                }
            }
        });
    }

    private String getVehicleKeyword() {
        if (mCurAccount == null || mCurAccount.getVehicleIndex() < 0
        || mVehicleFilterList.isEmpty()) {
            return "";
        }
        final VehicleInfo vehicle = mVehicleFilterList.get(mCurAccount.getVehicleIndex());
        return " " + vehicle.manufacturer + " " + vehicle.modelName;
    }
    // UI
    private void initView() {
        final View btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        initSearchView();
        initCategoryFiltersList();
        initVehicleFiltersList();
        initSearchResultList();
    }

    private SearchView mSearchViewKeyword = null;
    private void initSearchView() {
        mSearchViewKeyword = findViewById(R.id.searchViewKeyword);
        mSearchViewKeyword.onActionViewExpanded();
        mSearchViewKeyword.setQueryHint(getString(R.string.ID_HINT_SEARCH_KEYWORD));
        mSearchViewKeyword.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String text) {
                doSearch(text);
                foldFilters(null);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String text) {
                return false;
            }
        });
    }

    // Category Filter
    private FilterExpandableView mFilterCategory = null;
    private CategoryFilterItemRecyclerAdapter mCategoryFilterItemAdapter = null;
    private void initCategoryFiltersList() {
        // Got list, init the filter
        mFilterCategory = findViewById(R.id.filterCategory);
        if (mCategoryFilterList == null || mCategoryFilterList.isEmpty()) {
            Log.w(TAG, "No categories filter found");
            mFilterCategory.setVisibility(View.GONE);
            return;
        }
        mCategoryFilterItemAdapter = new CategoryFilterItemRecyclerAdapter(getBaseContext());
        mCategoryFilterItemAdapter.setOnItemClickListener(new CategoryFilterItemRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClicked(int position, CategoryItemInfo filter) {
                final int curSelectedIndex = mCategoryFilterItemAdapter.getSelectedFilterIndex();
                int newSelectedIndex = -1; // always cancel selected when click a selected item
                if (curSelectedIndex != position) {
                    newSelectedIndex = position;
                }
                mCategoryFilterItemAdapter.updateCategoryFilters(mCategoryFilterList, newSelectedIndex);
                updateCategoryFilterValue();
                // When keyword isn't empty, update search result list
                final String keyword = mSearchViewKeyword.getQuery().toString();
                if (!keyword.isEmpty()) {
                    doSearch(keyword);
                    foldFilters(mFilterCategory);
                }
            }
        });
        mFilterCategory.setAdapter(mCategoryFilterItemAdapter);
        mCategoryFilterItemAdapter.updateCategoryFilters(mCategoryFilterList, mProductInfoManager.getCurCategoryIndex());

        updateCategoryFilterValue();
    }

    private void updateCategoryFilterValue() {
        final int curSelectedCategoryIdx = mCategoryFilterItemAdapter.getSelectedFilterIndex();
        if (curSelectedCategoryIdx < 0) {
            mFilterCategory.setFilterValue(getString(R.string.ID_CAPTION_CATEGORY_FILTER_ALL));
        } else {
            final CategoryItemInfo category = mCategoryFilterList.get(curSelectedCategoryIdx);
            mFilterCategory.setFilterValue(category.name);
        }
    }

    // Vehicle Filter
    private FilterExpandableView mFilterVehicle = null;
    private VehicleFilterItemRecyclerAdapter mVehicleFilterItemAdapter = null;
    private void initVehicleFiltersList() {
        mFilterVehicle = findViewById(R.id.filterVehicles);
        mVehicleFilterItemAdapter = new VehicleFilterItemRecyclerAdapter(getBaseContext());
        mVehicleFilterItemAdapter.setOnItemClickListener(new VehicleFilterItemRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClicked(int position, VehicleInfo filter) {
                final int curSelectedIndex = mVehicleFilterItemAdapter.getSelectedFilterIndex();
                int newSelectedIndex = -1; // always cancel selected when click a selected item
                if (curSelectedIndex != position) {
                    newSelectedIndex = position;
                }
                mCurAccount.setVehicleIndex(newSelectedIndex);
                updateVehicleFilter();
                // When keyword isn't empty, update search result list
                final String keyword = mSearchViewKeyword.getQuery().toString();
                if (!keyword.isEmpty()) {
                    doSearch(keyword);
                    foldFilters(mFilterVehicle);
                }
            }
        });
        mFilterVehicle.setAdapter(mVehicleFilterItemAdapter);

        updateVehicleFilter();
    }

    private void updateVehicleFilter() {
        if (mCurAccount == null || mVehicleFilterList.isEmpty()) {
            Log.d(TAG, "No vehicles filter found");
            mFilterVehicle.setVisibility(View.GONE);
            return;
        }
        final int selectedVehicleIdx = mCurAccount.getVehicleIndex();
        mVehicleFilterItemAdapter.updateVehicleFilters(mVehicleFilterList, selectedVehicleIdx);
        mFilterVehicle.setVisibility(View.VISIBLE);
        // Update filter value
        if (selectedVehicleIdx < 0) {
            mFilterVehicle.setFilterValue(getString(R.string.ID_CAPTION_VEHICLE_FILTER_NONE));
        } else {
            final VehicleInfo vehicle = mVehicleFilterList.get(selectedVehicleIdx);
            mFilterVehicle.setFilterValue(vehicle.getDisplayName());
        }
    }

    private void foldFilters(final View focusFilter) {
        if (mFilterCategory != focusFilter) {
            mFilterCategory.setExpandedStatus(false);
        }
        if (mFilterCategory != focusFilter) {
            mFilterVehicle.setExpandedStatus(false);
        }
    }

    // Search Result List
    private TextView mTextMsgSearch = null;
    private RecyclerView mRecycleViewSearchResult = null;
    private ProductItemRecyclerAdapter mResultItemRecyclerAdapter = null;
    private void initSearchResultList() {
        mTextMsgSearch = findViewById(R.id.textMsgSearch);

        mRecycleViewSearchResult = findViewById(R.id.listSearchResult);
        mRecycleViewSearchResult.setLayoutManager(new LinearLayoutManager(getBaseContext(), LinearLayoutManager.VERTICAL, false));
        mResultItemRecyclerAdapter = new ProductItemRecyclerAdapter(getBaseContext(), mRecycleViewSearchResult);
        mResultItemRecyclerAdapter.setOnItemClickListener(new ProductItemRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClicked(int position) {
                if (mSearchResultList == null) {
                    return;
                }
                if (position >= 0 && position < mSearchResultList.size()) {
                    showProduct(position);
                }
            }
        });
        if (mAccountManager.hasAccountLogin()) {
            mResultItemRecyclerAdapter.setOnItemTrackClickListener(new ProductItemRecyclerAdapter.OnItemTrackClickListener() {
                @Override
                public void onItemTrackClicked(int position) {
                    Log.d(TAG, "onItemTrackClicked, position: " + position);
                    if (position >= 0 && position < mSearchResultList.size()) {
                        trackProduct(position);
                    }
                }
            });
        }
        mResultItemRecyclerAdapter.setLoadMoreListener(new ProductItemRecyclerAdapter.LoadMoreListener() {
            @Override
            public void onNeedLoadMore() {
                if (mSearchResultList.size() < mSearchResultTotalCount) {
                    showLoadingMoreResults();
                    mHandler.postDelayed(new Runnable(){
                        @Override
                        public void run() {
                            loadSearchResultWithPage(mSearchResultPageIdx+1);
                        }
                    }, 1000);

                }
            }
        });
        mRecycleViewSearchResult.setAdapter(mResultItemRecyclerAdapter);
    }

    private void showLoadingMoreResults() {
        mResultItemRecyclerAdapter.showLoadingProductList();
    }

    private void hideLoadingMoreResults() {
        mResultItemRecyclerAdapter.hideLoadingProductList();
        mResultItemRecyclerAdapter.setLoaded();
    }

    private void showSearching() {
        mRecycleViewSearchResult.setVisibility(View.GONE);
        mTextMsgSearch.setVisibility(View.VISIBLE);
        mTextMsgSearch.setText(R.string.ID_MSG_SEARCHING);
    }

    private void hideMsgSearch() {
        mRecycleViewSearchResult.setVisibility(View.VISIBLE);
        mTextMsgSearch.setVisibility(View.GONE);
    }

    private void showNoData() {
        mRecycleViewSearchResult.setVisibility(View.GONE);
        mTextMsgSearch.setVisibility(View.VISIBLE);
        mTextMsgSearch.setText(R.string.ID_MSG_NO_DATA);
    }

    private void processSearchError(final @WebProductAPI.ProductError int errorCode) {
        int errorMsgResId = R.string.ID_MSG_ERROR_SEARCH_FAILURE;
        switch(errorCode) {
            case WebProductAPI.PRODUCT_ERROR_NONE:
                return;
            case WebProductAPI.PRODUCT_ERROR_NETWORK:
                errorMsgResId = R.string.ID_MSG_ERROR_NETWORK_FAILURE;
                break;
            case WebProductAPI.PRODUCT_ERROR_PARSE:
                errorMsgResId = R.string.ID_MSG_ERROR_PARSE_FAILURE;
                break;
            case WebProductAPI.PRODUCT_ERROR_GENERAL:
            default:
                break;
        }
        showInvalidError(getString(errorMsgResId));
    }

    // Search methods
    private CategoryItemInfo getCategoryFilter() {
        if (mCategoryFilterItemAdapter == null) {
            return null;
        }
        final int categoryFilterIndex = mCategoryFilterItemAdapter.getSelectedFilterIndex();
        if (mCategoryFilterList == null || mCategoryFilterList.isEmpty()) {
            return null;
        }
        if (categoryFilterIndex < 0 || categoryFilterIndex > mCategoryFilterList.size()-1) {
            return null;
        }
        return mCategoryFilterList.get(categoryFilterIndex);
    }

    private WebProductAPI.ProductDataListener mProductDataListener = new WebProductAPI.ProductDataListener() {
        @Override
        public void onGotProductsList(int errorCode, int totalCount, int totalPagesCount, ArrayList<ProductItemInfo> list) {
            hideLoadingMoreResults();
            if (errorCode == WebProductAPI.PRODUCT_ERROR_NONE && list != null) {
                mProductInfoManager.appendSearchResultList(list);
                updateSearchResultCount(totalCount);
                updateSearchResult(mProductInfoManager.getSearchResultList());
                return;
            }
            final ArrayList<ProductItemInfo> latestSearchResult = mProductInfoManager.getSearchResultList();
            if (latestSearchResult == null || latestSearchResult.isEmpty()) {
                showNoData();
            }
            processSearchError(errorCode);
        }
    };

    private void hideSoftKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(mSearchViewKeyword.getWindowToken(), 0);
        }
    }

    private String mSearchKeyword = null;
    private CategoryItemInfo mSearchCategory = null;
    private void doSearch(final String keyword) {
        mSearchKeyword = keyword.trim() + getVehicleKeyword();
        mSearchCategory = getCategoryFilter();
        mSearchResultTotalCount = -1;
        mSearchResultPageIdx = -1;
        mProductInfoManager.resetSearchResultList();
        loadSearchResultWithPage(0);
        hideSoftKeyboard();
        showSearching();
    }

    private void cancelSearch() {
        mProductInfoManager.cancelLoadProductList();
    }

    private int mSearchResultPageIdx = -1;
    private void loadSearchResultWithPage(final int pageIdx) {
        mSearchResultPageIdx = pageIdx;
        if (mIsHot) {
            mProductInfoManager.loadHotProducts(mSearchKeyword, pageIdx, mProductDataListener);
        } else {
            mProductInfoManager.queryProductList(mSearchCategory, mSearchKeyword,
                    WebProductAPI.SORTBY_PRODUCT_ID, true, pageIdx, mProductDataListener);
        }
    }

    private int mSearchResultTotalCount = -1;
    private void updateSearchResultCount(final int count) {
        if (mSearchResultTotalCount < 0) {
            mSearchResultTotalCount = count;
        }
    }

    private ArrayList<ProductItemInfo> mSearchResultList = null;
    private void updateSearchResult(final ArrayList<ProductItemInfo> list) {
        hideMsgSearch();
        mSearchResultList = list;
        mResultItemRecyclerAdapter.updateProductList(list);
    }

    // Error process methods
    private void showInvalidError(final String message) {
        Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT).show();
    }

    // Action methods
    private void showProduct(final int index) {
        SystemInfoManager.getInstance().checkInternetAvailable(getBaseContext(), new SystemInfoManager.InternetAvailableListener() {
            @Override
            public void onInternetAvailableResult(boolean available) {
                if (!available) {
                    showInvalidError(getString(R.string.ID_MSG_ERROR_NETWORK_FAILURE));
                    return;
                }
                final ProductItemInfo item = mSearchResultList.get(index);
                ProductDetailInfoActivity.showProduct(getBaseContext(), item);
            }
        });
    }

    private void trackProduct(final int index) {
        final ProductItemInfo item = mSearchResultList.get(index);
        mProductInfoManager.addTrack(item, new WebProductAPI.AddToTrackListener() {
            @Override
            public void onResult(int errorCode, PriceTrackerItemInfo trackItem) {
                Log.d(TAG, "AddTrack result: "+errorCode);
                final String resultMsg;
                if (errorCode == WebProductAPI.PRODUCT_ERROR_NONE) {
                    resultMsg = getString(R.string.ID_MSG_SUCCESS_ADD_TRACK);
                } else {
                    resultMsg = getString(R.string.ID_MSG_ERROR_ADD_TRACK);
                }
                Toast.makeText(getBaseContext(), resultMsg, Toast.LENGTH_LONG).show();
            }
        });
    }
}
