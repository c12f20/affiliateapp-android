package com.jchain.affiliate.Search;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jchain.affiliate.Category.CategoryItemInfo;
import com.jchain.affiliate.R;

import java.util.ArrayList;

public final class CategoryFilterItemRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;
    public CategoryFilterItemRecyclerAdapter(final Context context) {
        mContext = context;
    }

    public interface OnItemClickListener {
        void onItemClicked(final int position, final CategoryItemInfo filter);
    }

    private OnItemClickListener mOnItemClickListener = null;
    public void setOnItemClickListener(final OnItemClickListener listener) {
        mOnItemClickListener = listener;
    }

    private void notifyItemClicked(final int position, final CategoryItemInfo filter) {
        if (mOnItemClickListener != null) {
            mOnItemClickListener.onItemClicked(position, filter);
        }
    }

    private ArrayList<CategoryItemInfo> mCategoryFilterList = null;
    private int mSelectedFilterIndex = -1;
    public void updateCategoryFilters(final ArrayList<CategoryItemInfo> filters, final int selectedFilterIndex) {
        mCategoryFilterList = filters;
        mSelectedFilterIndex = selectedFilterIndex;
        notifyDataSetChanged();
    }

    public int getSelectedFilterIndex() {
        return mSelectedFilterIndex;
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {
        private final View mRootView;
        private final TextView mTextCategoryName;
        private ViewHolder(@NonNull View itemView) {
            super(itemView);
            mRootView = itemView;
            mTextCategoryName = itemView.findViewById(R.id.textFilterName);
        }

        private View getItemView() {
            return mRootView;
        }

        private void updateItem(final CategoryItemInfo itemInfo, final boolean selected) {
            mTextCategoryName.setText(itemInfo.name);
            mTextCategoryName.setActivated(selected);
        }
    }

    @NonNull
    @Override
    public CategoryFilterItemRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View rootView = LayoutInflater.from(mContext).inflate(R.layout.view_filter_item, parent, false);
        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        if (mCategoryFilterList == null) {
            return;
        }
        if (position < 0 || position >= mCategoryFilterList.size()) {
            return;
        }
        final ViewHolder viewHolder = (ViewHolder)holder;
        final CategoryItemInfo categoryFilter = mCategoryFilterList.get(position);
        viewHolder.updateItem(categoryFilter, position == mSelectedFilterIndex);
        viewHolder.getItemView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notifyItemClicked(holder.getAdapterPosition(), categoryFilter);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mCategoryFilterList == null) {
            return 0;
        }
        return mCategoryFilterList.size();
    }
}
