package com.jchain.affiliate.Search;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jchain.affiliate.R;
import com.jchain.affiliate.Settings.VehicleProfile.VehicleInfo;

import java.util.ArrayList;

public final class VehicleFilterItemRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;
    public VehicleFilterItemRecyclerAdapter(final Context context) {
        mContext = context;
    }

    public interface OnItemClickListener {
        void onItemClicked(final int position, final VehicleInfo filter);
    }

    private OnItemClickListener mOnItemClickListener = null;
    public void setOnItemClickListener(final OnItemClickListener listener) {
        mOnItemClickListener = listener;
    }

    private void notifyItemClicked(final int position, final VehicleInfo filter) {
        if (mOnItemClickListener != null) {
            mOnItemClickListener.onItemClicked(position, filter);
        }
    }

    private ArrayList<VehicleInfo> mVehicleFilterList = null;
    private int mSelectedFilterIndex = -1;
    public void updateVehicleFilters(final ArrayList<VehicleInfo> filters, final int selectedFilterIndex) {
        mVehicleFilterList = filters;
        mSelectedFilterIndex = selectedFilterIndex;
        notifyDataSetChanged();
    }

    public int getSelectedFilterIndex() {
        return mSelectedFilterIndex;
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {
        private final View mRootView;
        private final TextView mTextVehicleName;
        private ViewHolder(@NonNull View itemView) {
            super(itemView);
            mRootView = itemView;
            mTextVehicleName = itemView.findViewById(R.id.textFilterName);
        }

        private View getItemView() {
            return mRootView;
        }

        private void updateItem(final VehicleInfo itemInfo, final boolean selected) {
            mTextVehicleName.setText(itemInfo.getDisplayName());
            mTextVehicleName.setActivated(selected);
        }
    }

    @NonNull
    @Override
    public VehicleFilterItemRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View rootView = LayoutInflater.from(mContext).inflate(R.layout.view_filter_item, parent, false);
        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        if (mVehicleFilterList == null) {
            return;
        }
        if (position < 0 || position >= mVehicleFilterList.size()) {
            return;
        }
        final ViewHolder viewHolder = (ViewHolder)holder;
        final VehicleInfo vehicleFilter = mVehicleFilterList.get(position);
        viewHolder.updateItem(vehicleFilter, position == mSelectedFilterIndex);
        viewHolder.getItemView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notifyItemClicked(holder.getAdapterPosition(), vehicleFilter);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mVehicleFilterList == null) {
            return 0;
        }
        return mVehicleFilterList.size();
    }
}
