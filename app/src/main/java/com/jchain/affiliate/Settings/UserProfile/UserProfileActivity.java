package com.jchain.affiliate.Settings.UserProfile;

import android.os.Bundle;

import com.jchain.affiliate.UI.Activities.BaseSettingsActivity;

public final class UserProfileActivity extends BaseSettingsActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initView(savedInstanceState);
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);
        initFragment();
    }

    private void initFragment() {
        final UserProfilePrefFragment fragment = UserProfilePrefFragment.newInstance();
        showFragment(fragment, false);
    }
}
