package com.jchain.affiliate.Settings.UserProfile;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.preference.Preference;
import androidx.preference.PreferenceGroup;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.jchain.affiliate.Account.AccountInfo;
import com.jchain.affiliate.Account.AccountManagement;
import com.jchain.affiliate.R;
import com.jchain.affiliate.Settings.VehicleProfile.VehicleProfilesPrefFragment;
import com.jchain.affiliate.UI.Fragments.BasePreferenceFragment;
import com.jchain.affiliate.UI.Preferences.PicturePickerPreference;
import com.jchain.affiliate.WebAPI.WebAuthAPI;
import com.takisoft.fix.support.v7.preference.EditTextPreference;

public final class UserProfilePrefFragment extends BasePreferenceFragment {
    private static final String TAG = "UserProfilePrefFragment";

    public static UserProfilePrefFragment newInstance() {
        UserProfilePrefFragment fragment = new UserProfilePrefFragment();
        return fragment;
    }

    private AccountManagement mAccountMng = null;
    private WebAuthAPI mWebAuthAPI = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        mAccountMng = AccountManagement.getInstance(getContext());
        mWebAuthAPI = WebAuthAPI.getInstance(getContext());
        initData();
        super.onCreate(savedInstanceState);
    }

    private AccountInfo mCurAccount = null;
    private void initData() {
        mCurAccount = mAccountMng.getCurAccountInfo();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        initActionBar(getString(R.string.ID_TITLE_USER_PROFILE));
        setCustomView(buildSignOutBtn());
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setDivider(new ColorDrawable(Color.TRANSPARENT));
        setDividerHeight(0);
    }

    private View buildSignOutBtn() {
        final LayoutInflater inflater = LayoutInflater.from(getContext());
        final View view = inflater.inflate(R.layout.view_settings_bottom_btn, null);
        final Button btnSignOut = view.findViewById(R.id.btnSettingsBottom);
        btnSignOut.setText(R.string.ID_CAPTION_SIGNOUT_BTN);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mWebAuthAPI.logoutAccount(mCurAccount);
                mAccountMng.setCurAccountInfo(null);
                clearToLoginActivity();
            }
        });
        return view;
    }

    @Override
    public void onCreatePreferencesFix(@Nullable Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.preferences_user_profile);
        initPreferences(getPreferenceScreen());
    }

    private void initPreferences(final PreferenceGroup group) {
        initSummary(group);

        if (mCurAccount != null) {
            final Preference prefName = group.findPreference(mAccountMng.mPrefKeyProfileName);
            prefName.setSummary(mCurAccount.name);
        }
        final EditTextPreference prefPhone = (EditTextPreference) group.findPreference(mAccountMng.mPrefKeyProfilePhone);
        final EditText editTextPhone = prefPhone.getEditText();
        editTextPhone.setSingleLine(true);
        editTextPhone.setInputType(InputType.TYPE_CLASS_PHONE);

        final EditTextPreference prefEmail = (EditTextPreference) group.findPreference(mAccountMng.mPrefKeyProfileEmail);
        final EditText editTextEmail = prefEmail.getEditText();
        editTextEmail.setSingleLine(true);
        editTextEmail.setInputType(InputType.TYPE_CLASS_TEXT
                | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);

        final PicturePickerPreference prefAvatar = (PicturePickerPreference) group
                .findPreference(mAccountMng.mPrefKeyProfileAvatar);
        prefAvatar.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                openSelectPictureUI();
                return false;
            }
        });

        final EditTextPreference prefFirstName = (EditTextPreference) group.findPreference(mAccountMng.mPrefKeyProfileFirstName);
        final EditText editTextFirstName = prefFirstName.getEditText();
        editTextFirstName.setSingleLine(true);
        editTextFirstName.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PERSON_NAME);
        final EditTextPreference prefLastName = (EditTextPreference) group.findPreference(mAccountMng.mPrefKeyProfileLastName);
        final EditText editTextLastName = prefLastName.getEditText();
        editTextLastName.setSingleLine(true);
        editTextLastName.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PERSON_NAME);

        final Preference prefVehicles = group.findPreference(mAccountMng.mPrefKeyProfileVehicles);
        prefVehicles.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                showVehiclesProfileUI();
                return false;
            }
        });
    }

    // Picture Picker UI related methods
    private static final int REQUEST_CODE_PICK_IMAGE = 1;
    private void openSelectPictureUI() {
        if (!checkReadExternalStoragePermission()) {
            return;
        }

        Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
        getIntent.addCategory(Intent.CATEGORY_OPENABLE);
        getIntent.setType("image/*");

        Intent chooserIntent = Intent.createChooser(getIntent, getString(R.string.ID_TITLE_SELECT_AVATAR));

        startActivityForResult(chooserIntent, REQUEST_CODE_PICK_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_PICK_IMAGE) {
            final PicturePickerPreference prefAvatar = (PicturePickerPreference) getPreferenceScreen()
                    .findPreference(getString(R.string.PREFKEY_PROFILE_AVATAR));
            prefAvatar.onActivityResult(resultCode, data);
        }
    }

    private static final int REQUEST_CODE_GRANT_READ_PERMISSION = 2;
    private boolean checkReadExternalStoragePermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {
            showRequestReadPermissionDialog();
            return false;
        } else {
            requestPermissions(new String[]{ Manifest.permission.READ_EXTERNAL_STORAGE },
                    REQUEST_CODE_GRANT_READ_PERMISSION);
            return false;
        }
    }

    private void showRequestReadPermissionDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.ID_TITLE_REQUEST_PERMISSION)
               .setMessage(R.string.ID_MSG_REQUEST_PERMISSION_FOR_CHOOSE_AVATAR)
               .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                   @Override
                   public void onClick(DialogInterface dialog, int which) {
                       requestPermissions(new String[]{ Manifest.permission.READ_EXTERNAL_STORAGE },
                               REQUEST_CODE_GRANT_READ_PERMISSION);
                   }
               });
        final AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CODE_GRANT_READ_PERMISSION) {
            Log.d(TAG, "onRequestPermissionsResult for REQUEST_CODE_GRANT_READ_PERMISSION");
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openSelectPictureUI();
            } else {
                Log.d(TAG, "User reject our request to grant READ_PERMISSION");
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    // Vehicle Profile UI related methods
    private void showVehiclesProfileUI() {
        final VehicleProfilesPrefFragment fragment = VehicleProfilesPrefFragment.newInstance();
        showFragment(fragment, true);
    }

    @Override
    public void onSharedPreferenceChanged(final SharedPreferences sharedPreferences, final String key) {
        Log.d(TAG, "onSharedPreferenceChanged key: "+key);
        final AccountInfo updateAccountInfo = mAccountMng.buildUpdateInfoWithPrefKey(key);
        if (updateAccountInfo == null) {
            super.onSharedPreferenceChanged(sharedPreferences, key);
            return;
        }
        showLoading();
        mWebAuthAPI.updateAccount(null, updateAccountInfo, new WebAuthAPI.UpdateAccountListener() {
            @Override
            public void onUpdateResult(int errorCode, AccountInfo userInfo) {
                hideLoading();
                if (errorCode == WebAuthAPI.AUTH_ERROR_NONE) {
                    // to avoid to triggering onSharedPreferenceChanged, only update values in mCurAccount
                    mAccountMng.updateCurAccountInfo(userInfo);
                    mCurAccount = mAccountMng.getCurAccountInfo();
                    UserProfilePrefFragment.super.onSharedPreferenceChanged(sharedPreferences, key);
                    return;
                }
                // reset account info when failed to update account
                mAccountMng.setCurAccountInfo(mCurAccount);
                processUpdateAccountFailure(errorCode);
            }
        });
    }

    private void processUpdateAccountFailure(final @WebAuthAPI.AuthError int errorCode) {
        final Context context = getContext();
        if (context == null) {
            Log.w(TAG, "Fragment has destroyed, ignore invalid error Code: "+errorCode);
            return;
        }
        int errorMsgResId = R.string.ID_MSG_ERROR_UPDATE_PROFILE_FAILURE;
        switch (errorCode) {
            case WebAuthAPI.AUTH_ERROR_NETWORK:
                errorMsgResId = R.string.ID_MSG_ERROR_UPDATE_PROFILE_NETWORK_FAILURE;
                break;
            case WebAuthAPI.AUTH_ERROR_PARSE:
                errorMsgResId = R.string.ID_MSG_ERROR_PARSE_FAILURE;
                break;
            case WebAuthAPI.AUTH_ERROR_DUPLICATED_NAME:
                errorMsgResId = R.string.ID_MSG_ERROR_DUPLICATED_NAME;
                break;
            case WebAuthAPI.AUTH_ERROR_DUPLICATED_EMAIL:
                errorMsgResId = R.string.ID_MSG_ERROR_DUPLICATED_EMAIL;
                break;
            case WebAuthAPI.AUTH_ERROR_DUPLICATED_PHONE:
                errorMsgResId = R.string.ID_MSG_ERROR_DUPLICATED_PHONE;
                break;
            case WebAuthAPI.AUTH_ERROR_GENERAL:
            default:
                break;
        }
        showInvalidError(getString(errorMsgResId));
    }

    private void showInvalidError(final String message) {
        final Context context = getContext();
        if (context != null) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }
    }
}
