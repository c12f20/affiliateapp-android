package com.jchain.affiliate.Settings.VehicleProfile;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;

import com.jchain.affiliate.R;

public final class VehicleManagement {
    private static final String TAG = "VehicleManagement";

    private static VehicleManagement gInstance = null;

    public static VehicleManagement getInstance(final Context context) {
        if (gInstance == null) {
            gInstance = new VehicleManagement(context);
        }
        return gInstance;
    }

    private final SharedPreferences mSP;
    public final String mPrefKeyVinCode;
    public final String mPrefKeyManufacturer;
    public final String mPrefKeyModelName;
    public final String mPrefKeyModelYear;
    public final String mPrefKeyTrim;
    public final String mPrefKeyVehicleType;
    public final String mPrefKeyDoorsCount;

    public VehicleManagement(final Context context) {
        mSP = PreferenceManager.getDefaultSharedPreferences(context);

        mPrefKeyVinCode = context.getString(R.string.PREFKEY_VEHICLE_VINCODE);
        mPrefKeyManufacturer = context.getString(R.string.PREFKEY_VEHICLE_MANUFACTURER);
        mPrefKeyModelName = context.getString(R.string.PREFKEY_VEHICLE_MODEL_NAME);
        mPrefKeyModelYear = context.getString(R.string.PREFKEY_VEHICLE_MODEL_YEAR);
        mPrefKeyTrim = context.getString(R.string.PREFKEY_VEHICLE_TRIM);
        mPrefKeyVehicleType = context.getString(R.string.PREFKEY_VEHICLE_TYPE);
        mPrefKeyDoorsCount = context.getString(R.string.PREFKEY_VEHICLE_DOORS_COUNT);
    }

    private VehicleInfo mCurVehicle = null;
    public void setCurVehicle(final VehicleInfo vehicle) {
        mCurVehicle = vehicle;
        updateSPData();
    }

    public VehicleInfo getCurVehicle() {
        return mCurVehicle;
    }

    private void clearSPData() {
        final SharedPreferences.Editor editor = mSP.edit();
        editor.remove(mPrefKeyVinCode);
        editor.remove(mPrefKeyManufacturer);
        editor.remove(mPrefKeyModelName);
        editor.remove(mPrefKeyModelYear);
        editor.remove(mPrefKeyTrim);
        editor.remove(mPrefKeyVehicleType);
        editor.remove(mPrefKeyDoorsCount);
        editor.apply();
    }

    private void updateSPData() {
        if (mCurVehicle == null) {
            clearSPData();
            return;
        }
        final SharedPreferences.Editor editor = mSP.edit();
        editor.putString(mPrefKeyVinCode, mCurVehicle.vinCode);
        editor.putString(mPrefKeyManufacturer, mCurVehicle.manufacturer);
        editor.putString(mPrefKeyModelName, mCurVehicle.modelName);
        if (mCurVehicle.modelYear > 0) {
            editor.putString(mPrefKeyModelYear, String.valueOf(mCurVehicle.modelYear));
        } else {
            editor.remove(mPrefKeyModelYear);
        }
        editor.putString(mPrefKeyTrim, mCurVehicle.trim);
        editor.putString(mPrefKeyVehicleType, mCurVehicle.vehicleType);
        if (mCurVehicle.doorsCount > 0) {
            editor.putString(mPrefKeyDoorsCount, String.valueOf(mCurVehicle.doorsCount));
        } else {
            editor.remove(mPrefKeyDoorsCount);
        }
        editor.apply();
    }

    public VehicleInfo buildUpdateInfoWithPrefKey(final String key) {
        final VehicleInfo newVehicleInfo = new VehicleInfo();
        if (mCurVehicle == null) {
            if (key.equals(mPrefKeyVinCode)) {
                newVehicleInfo.vinCode = mSP.getString(key, "");
                if (!TextUtils.isEmpty(newVehicleInfo.vinCode)) {
                    return newVehicleInfo;
                }
            }
            return null;
        }
        boolean isChanged = false;
        newVehicleInfo.dbId = mCurVehicle.dbId;
        if (key.equals(mPrefKeyVinCode)) {
            newVehicleInfo.vinCode = mSP.getString(key, "");
            isChanged = (mCurVehicle.vinCode == null || !mCurVehicle.vinCode.equals(newVehicleInfo.vinCode));
        } else if (key.equals(mPrefKeyManufacturer)) {
            newVehicleInfo.manufacturer = mSP.getString(key, "");
            isChanged = (mCurVehicle.manufacturer == null || !mCurVehicle.manufacturer.equals(newVehicleInfo.manufacturer));
        } else if (key.equals(mPrefKeyModelName)) {
            newVehicleInfo.modelName = mSP.getString(key, "");
            isChanged = (mCurVehicle.modelName == null || !mCurVehicle.modelName.equals(newVehicleInfo.modelName));
        } else if (key.equals(mPrefKeyModelYear)) {
            newVehicleInfo.modelYear = getIntPrefValue(key);
            isChanged = (mCurVehicle.modelYear != newVehicleInfo.modelYear);
        } else if (key.equals(mPrefKeyTrim)) {
            newVehicleInfo.trim = mSP.getString(key, "");
            isChanged = (mCurVehicle.trim == null || !mCurVehicle.trim.equals(newVehicleInfo.trim));
        } else if (key.equals(mPrefKeyVehicleType)) {
            newVehicleInfo.vehicleType = mSP.getString(key, "");
            isChanged = (mCurVehicle.vehicleType == null || !mCurVehicle.vehicleType.equals(newVehicleInfo.vehicleType));
        } else if (key.equals(mPrefKeyDoorsCount)) {
            newVehicleInfo.doorsCount = getIntPrefValue(key);
            isChanged = (mCurVehicle.doorsCount != newVehicleInfo.doorsCount);
        }
        if (!isChanged) {
            return null;
        }
        return newVehicleInfo;
    }

    public void updateCurVehicleInfo(final VehicleInfo newVehicle) {
        if (mCurVehicle == null) {
            mCurVehicle = newVehicle;
            return;
        }
        mCurVehicle.dbId = newVehicle.dbId;
        if (newVehicle.vinCode != null) {
            mCurVehicle.vinCode = newVehicle.vinCode;
        }
        if (newVehicle.manufacturer != null) {
            mCurVehicle.manufacturer = newVehicle.manufacturer;
        }
        if (newVehicle.modelName != null) {
            mCurVehicle.modelName = newVehicle.modelName;
        }
        if (newVehicle.modelYear >= 0) {
            mCurVehicle.modelYear = newVehicle.modelYear;
        }
        if (newVehicle.trim != null) {
            mCurVehicle.trim = newVehicle.trim;
        }
        if (newVehicle.vehicleType != null) {
            mCurVehicle.vehicleType = newVehicle.vehicleType;
        }
        if (newVehicle.doorsCount >= 0) {
            mCurVehicle.doorsCount = newVehicle.doorsCount;
        }
    }

    private int getIntPrefValue(final String key) {
        final String valueData = mSP.getString(key, "");
        if (TextUtils.isEmpty(valueData)) {
            return 0;
        }
        try {
            return Integer.parseInt(valueData);
        } catch (NumberFormatException e) {
            Log.e(TAG, "Failed to getIntPrefValue, exception: ", e);
            return 0;
        }
    }
}
