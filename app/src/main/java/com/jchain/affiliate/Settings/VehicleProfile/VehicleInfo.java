package com.jchain.affiliate.Settings.VehicleProfile;

import android.text.TextUtils;

public final class VehicleInfo {
    public int dbId = -1;
    public String vinCode = null;
    public String manufacturer = null;
    public String modelName = null;
    public int modelYear = -1;
    public String trim = null;
    public String vehicleType = null;
    public int doorsCount = -1;

    public String getDisplayName() {
        final StringBuilder sb = new StringBuilder();
        if (!TextUtils.isEmpty(manufacturer)) {
            sb.append(manufacturer+" ");
        }
        if (!TextUtils.isEmpty(modelName)) {
            sb.append(modelName+" ");
        }
        if (modelYear > 0) {
            sb.append(modelYear+" ");
        }
        if (!TextUtils.isEmpty(trim)) {
            sb.append(trim);
        }
        return sb.toString().trim();
    }
}
