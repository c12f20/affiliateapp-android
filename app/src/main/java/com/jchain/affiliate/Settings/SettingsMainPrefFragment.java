package com.jchain.affiliate.Settings;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.preference.Preference;
import androidx.preference.PreferenceGroup;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jchain.affiliate.Account.AccountManagement;
import com.jchain.affiliate.R;
import com.jchain.affiliate.Settings.UserProfile.UserProfilePrefFragment;
import com.jchain.affiliate.System.SystemInfoManager;
import com.jchain.affiliate.UI.Fragments.BasePreferenceFragment;

public final class SettingsMainPrefFragment extends BasePreferenceFragment {
    private static final String TAG = "SettingsMainPref";

    public static SettingsMainPrefFragment newInstance() {
        SettingsMainPrefFragment fragment = new SettingsMainPrefFragment();
        return fragment;
    }

    private AccountManagement mAccountMng = null;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        mAccountMng = AccountManagement.getInstance(getContext());
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        initActionBar(getString(R.string.ID_TITLE_SETTINGS));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreatePreferencesFix(@Nullable Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.preferences_empty);
        initPreferences(getPreferenceScreen());
    }

    private static final int ORDER_USER_PROFILE = 0;
    private static final int ORDER_ABOUT = 1;
    private void initPreferences(final PreferenceGroup group) {
        // Build click listener
        final Preference.OnPreferenceClickListener clickListener = new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                final int order = preference.getOrder();
                switch (order) {
                    case ORDER_USER_PROFILE:
                        showUserProfileUI();
                        break;
                    case ORDER_ABOUT:
                        showAboutUI();
                        break;
                    default:
                        Log.w(TAG, "Unknown order: "+order);
                        break;
                }
                return false;
            }
        };
        // User Profile
        int titleUserProfileResId = R.string.ID_TITLE_USER_PROFILE;
        String summaryUserProfile = null;
        if (!mAccountMng.hasAccountLogin()) {
            titleUserProfileResId = R.string.ID_TITLE_NOT_SIGN_IN;
            summaryUserProfile = getString(R.string.ID_MSG_CLICK_SIGN_IN);
        }
        final Preference prefUserProfile = buildBasicItemPreference(ORDER_USER_PROFILE,
                getString(titleUserProfileResId), summaryUserProfile);
        prefUserProfile.setOnPreferenceClickListener(clickListener);
        group.addPreference(prefUserProfile);
        // About
        final String versionValue = "V"+ SystemInfoManager.getInstance().getCurAppVersion(getContext());
        final Preference prefAbout = buildBasicItemPreference(ORDER_ABOUT,
                getString(R.string.ID_TITLE_SETTINGS_ABOUT), versionValue);
        prefAbout.setOnPreferenceClickListener(clickListener);
        group.addPreference(prefAbout);
    }

    private Preference buildBasicItemPreference(final int order, final String title, final String summary) {
        final Preference pref = new Preference(getContext());
        pref.setPersistent(false);
        pref.setLayoutResource(R.layout.preference_popup);
        pref.setOrder(order);
        pref.setTitle(title);
        if (TextUtils.isEmpty(summary)) {
            pref.setSummary(" "); // We need add empty string to extends summary area
        } else {
            pref.setSummary(summary);
        }
        return pref;
    }

    // action methods
    private void showUserProfileUI() {
        if (mAccountMng.hasAccountLogin()) {
            showFragment(UserProfilePrefFragment.newInstance(), true);
        } else {
            clearToLoginActivity();
        }
    }

    private void showAboutUI() {
        showFragment(AboutFragment.newInstance(), true);
    }
}
