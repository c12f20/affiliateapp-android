package com.jchain.affiliate.Settings;

import android.os.Bundle;
import androidx.annotation.Nullable;

import com.jchain.affiliate.UI.Activities.BaseSettingsActivity;

public final class SettingsActivity extends BaseSettingsActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView(savedInstanceState);
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);
        initFragment();
    }

    private void initFragment() {
        showFragment(SettingsMainPrefFragment.newInstance(), false);
    }
}
