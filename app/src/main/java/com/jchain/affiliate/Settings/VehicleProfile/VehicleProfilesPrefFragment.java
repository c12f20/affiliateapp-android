package com.jchain.affiliate.Settings.VehicleProfile;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.preference.Preference;
import androidx.preference.PreferenceGroup;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.jchain.affiliate.Account.AccountInfo;
import com.jchain.affiliate.Account.AccountManagement;
import com.jchain.affiliate.R;
import com.jchain.affiliate.UI.Fragments.BasePreferenceFragment;
import com.jchain.affiliate.WebAPI.WebVehicleAPI;

import java.util.ArrayList;
import java.util.List;

public final class VehicleProfilesPrefFragment extends BasePreferenceFragment {

    private static final String TAG = "VehicleProfilePref";

    public static VehicleProfilesPrefFragment newInstance() {
        VehicleProfilesPrefFragment fragment = new VehicleProfilesPrefFragment();
        return fragment;
    }

    private AccountManagement mAccountMng = null;
    private WebVehicleAPI mWebVehicleAPI = null;
    private AccountInfo mCurAccount = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        mAccountMng = AccountManagement.getInstance(getContext());
        mWebVehicleAPI = WebVehicleAPI.getInstance(getContext());
        initData();
        super.onCreate(savedInstanceState);
    }

    private void initData() {
        mCurAccount = mAccountMng.getCurAccountInfo();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        initActionBar(getString(R.string.ID_TITLE_VEHICLE_PROFILE));
        setCustomView(buildAddVehicleBtn());
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    private View buildAddVehicleBtn() {
        final LayoutInflater inflater = LayoutInflater.from(getContext());
        final View view = inflater.inflate(R.layout.view_settings_bottom_btn, null);
        final Button btnAddVehicle = view.findViewById(R.id.btnSettingsBottom);
        btnAddVehicle.setText(R.string.ID_CAPTION_ADD_VEHICLE);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enterVehicleItemEditUI(-1);
            }
        });
        return view;
    }

    @Override
    public void onCreatePreferencesFix(@Nullable Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.preferences_empty);
    }

    @Override
    public void onStart() {
        super.onStart();
        initPreferences(getPreferenceScreen());
    }

    @Override
    public void onStop() {
        clearVehiclePreferences(getPreferenceScreen());
        super.onStop();
    }

    private void initPreferences(final PreferenceGroup group) {
        group.setOrderingAsAdded(true);

        if (mCurAccount.hasSyncedVehicles()) {
            loadVehiclePreferences(group);
        } else {
            showLoading();
            mWebVehicleAPI.queryVehiclesByAccount(mCurAccount, new WebVehicleAPI.VehicleDataListener() {
                @Override
                public void onGotVehiclesList(final @WebVehicleAPI.VehicleError int errorCode, final List<VehicleInfo> list) {
                    hideLoading();
                    if (errorCode == WebVehicleAPI.VEHICLE_ERROR_NONE && list != null) {
                        mCurAccount.setVehicles(list);
                    } else {
                        processVehicleError(errorCode);
                    }
                    loadVehiclePreferences(group);
                }
            });
        }
    }

    private void clearVehiclePreferences(final PreferenceGroup group) {
        mVehiclesList.clear();
        group.removeAll();
    }

    private List<VehicleInfo> mVehiclesList = new ArrayList<VehicleInfo>();
    private void loadVehiclePreferences(final PreferenceGroup group) {
        final List<VehicleInfo> vehiclesList = mCurAccount.getVehicles();
        Log.d(TAG, "loadVehiclePreferences, vehicles count: "+vehiclesList.size());
        if (vehiclesList != null) {
            mVehiclesList.addAll(vehiclesList);
        }
        if (mVehiclesList.isEmpty()) {
            showNoVehicleUI(group);
            return;
        }
        // Build click listener
        final Preference.OnPreferenceClickListener clickListener = new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                final int order = preference.getOrder();
                if (order >= 0 && order < mVehiclesList.size()) {
                    final VehicleInfo vehicle = mVehiclesList.get(order);
                    Log.d(TAG, "Show Vehicle: "+vehicle.getDisplayName());
                    enterVehicleItemEditUI(order);
                }
                return false;
            }
        };
        // Add preference items
        for (int i=0; i < mVehiclesList.size(); i++) {
            final VehicleInfo vehicle = mVehiclesList.get(i);
            final Preference prefVehicle = buildVehiclePrefItem(vehicle);
            prefVehicle.setOrder(i);
            if (prefVehicle != null) {
                prefVehicle.setOnPreferenceClickListener(clickListener);
                group.addPreference(prefVehicle);
            }
        }
    }

    private Preference buildVehiclePrefItem(final VehicleInfo vehicleInfo) {
        if (vehicleInfo == null) {
            Log.e(TAG, "Failed to build Vehicle Pref item with null vehicle info");
            return null;
        }
        final Preference pref = new Preference(getContext());
        pref.setPersistent(false);
        pref.setLayoutResource(R.layout.preference_popup);
        pref.setTitle(vehicleInfo.getDisplayName());
        pref.setSummary(" "); // We need add empty string to extends summary area
        return pref;
    }

    private void showNoVehicleUI(final PreferenceGroup group) {
        final Preference pref = new Preference(getContext());
        pref.setPersistent(false);
        pref.setLayoutResource(R.layout.preference_message);
        pref.setTitle(R.string.ID_MSG_NO_VEHICLES);
        group.addPreference(pref);
    }

    private void enterVehicleItemEditUI(final int vehicleIndex) {
        VehicleProfileItemPrefFragment fragment = VehicleProfileItemPrefFragment.newInstance(vehicleIndex);
        showFragment(fragment, true);
    }

    // Error process methods
    private void processVehicleError(final @WebVehicleAPI.VehicleError int errorCode) {
        int errorMsgResId = R.string.ID_MSG_ERROR_LOAD_VEHICLE_PROFILES;
        switch(errorCode) {
            case WebVehicleAPI.VEHICLE_ERROR_NONE:
                return;
            case WebVehicleAPI.VEHICLE_ERROR_NETWORK:
                errorMsgResId = R.string.ID_MSG_ERROR_NETWORK_FAILURE;
                break;
            case WebVehicleAPI.VEHICLE_ERROR_PARSE:
                errorMsgResId = R.string.ID_MSG_ERROR_PARSE_FAILURE;
                break;
            case WebVehicleAPI.VEHICLE_ERROR_GENERAL:
            default:
                break;
        }
        showInvalidError(getString(errorMsgResId));
    }

    private void showInvalidError(final String message) {
        final Context context = getContext();
        if (context != null) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        } else {
            Log.w(TAG, "Ignore invalid error: "+message);
        }
    }
}
