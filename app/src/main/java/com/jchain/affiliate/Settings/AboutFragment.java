package com.jchain.affiliate.Settings;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jchain.affiliate.R;
import com.jchain.affiliate.System.SystemInfoManager;
import com.jchain.affiliate.UI.Fragments.BasePreferenceFragment;

public final class AboutFragment extends BasePreferenceFragment {

    public static AboutFragment newInstance() {
        AboutFragment fragment = new AboutFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_about, container, false);
        initView(rootView);
        return rootView;
    }

    @Override
    public void onCreatePreferencesFix(@Nullable Bundle savedInstanceState, String rootKey) {
    }

    private String buildSystemVersionInfo() {
        final SystemInfoManager systemInfoMng = SystemInfoManager.getInstance();
        final String versionInfoTemplate = getString(R.string.ID_MSG_VERSION_INFO_TEMPLATE);
        return String.format(versionInfoTemplate, systemInfoMng.getCurAppVersion(getContext()));
    }

    private void initView(final View rootView) {
        initActionBar(getString(R.string.ID_TITLE_SETTINGS_ABOUT));

        final TextView textVersionInfo = rootView.findViewById(R.id.textVersionInfo);
        textVersionInfo.setText(buildSystemVersionInfo());
    }
}
