package com.jchain.affiliate.Settings.VehicleProfile;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.preference.PreferenceGroup;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.jchain.affiliate.Account.AccountInfo;
import com.jchain.affiliate.Account.AccountManagement;
import com.jchain.affiliate.R;
import com.jchain.affiliate.UI.Fragments.BasePreferenceFragment;
import com.jchain.affiliate.WebAPI.WebVehicleAPI;
import com.takisoft.fix.support.v7.preference.EditTextPreference;

import java.util.List;

public final class VehicleProfileItemPrefFragment extends BasePreferenceFragment {
    private static final String TAG = "VehicleProfileItemPref";

    private static final String ARG_PARAM_VEHICLE_INDEX = "param.VehilceIndex";
    public static VehicleProfileItemPrefFragment newInstance(final int vehicleIndex) {
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM_VEHICLE_INDEX, vehicleIndex);

        VehicleProfileItemPrefFragment fragment = new VehicleProfileItemPrefFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private AccountManagement mAccountMng = null;
    private VehicleManagement mVehicleMng = null;
    private WebVehicleAPI mWebVehicleAPI = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        mAccountMng = AccountManagement.getInstance(getContext());
        mVehicleMng = new VehicleManagement(getContext());
        mWebVehicleAPI = WebVehicleAPI.getInstance(getContext());
        initData();
        super.onCreate(savedInstanceState);
    }

    private AccountInfo mCurAccount = null;
    private VehicleInfo mCurVehicle = null;
    private void initData() {
        mCurAccount = mAccountMng.getCurAccountInfo();
        final List<VehicleInfo> vehicles = mCurAccount.getVehicles();
        if (vehicles == null) {
            return;
        }
        final Bundle args = getArguments();
        final int vehicleIndex = args.getInt(ARG_PARAM_VEHICLE_INDEX, -1);
        if (vehicleIndex >= 0 && vehicleIndex < vehicles.size()) {
            mCurVehicle = vehicles.get(vehicleIndex);
        }
        mVehicleMng.setCurVehicle(mCurVehicle);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setCustomView(buildRemoveVehicleBtn());
        setRemoveVehicleBtnEnabled(mCurVehicle != null);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    private Button mBtnRemoveVehicle = null;
    private View buildRemoveVehicleBtn() {
        final LayoutInflater inflater = LayoutInflater.from(getContext());
        final View view = inflater.inflate(R.layout.view_settings_bottom_btn, null);
        mBtnRemoveVehicle = view.findViewById(R.id.btnSettingsBottom);
        mBtnRemoveVehicle.setText(R.string.ID_CAPTION_REMOVE_VEHICLE);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doRemoveCurVehicle();
            }
        });
        return view;
    }

    private void doRemoveCurVehicle() {
        if (mCurVehicle == null) {
            return;
        }
        showLoading();
        removeVehicle(mCurVehicle, new WebVehicleAPI.RemoveVehicleListener() {
            @Override
            public void onVehicleRemoved(int errorCode) {
                hideLoading();
                if (errorCode != WebVehicleAPI.VEHICLE_ERROR_NONE) {
                    return;
                }
                mCurAccount.removeVehicle(mCurVehicle);
                final Activity activity = getActivity();
                if (activity != null) {
                    activity.onBackPressed();
                }
            }
        });
    }

    private void setRemoveVehicleBtnEnabled(final boolean enabled) {
        if (mBtnRemoveVehicle != null) {
            mBtnRemoveVehicle.setEnabled(enabled);
        }
    }

    @Override
    public void onCreatePreferencesFix(@Nullable Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.preferences_vehicle_profile);
        initPreferences(getPreferenceScreen());
    }

    private void initPreferences(final PreferenceGroup group) {
        initSummary(group);

        final boolean prefsEnabled = mCurVehicle != null;

        final EditTextPreference prefVinCode = (EditTextPreference) group.findPreference(mVehicleMng.mPrefKeyVinCode);
        final EditText editTextVinCode = prefVinCode.getEditText();
        editTextVinCode.setSingleLine(true);

        final EditTextPreference prefManufacturer = (EditTextPreference) group.findPreference(mVehicleMng.mPrefKeyManufacturer);
        prefManufacturer.setEnabled(prefsEnabled);
        final EditText editTextManufacturer = prefManufacturer.getEditText();
        editTextManufacturer.setSingleLine(true);

        final EditTextPreference prefModelName = (EditTextPreference) group.findPreference(mVehicleMng.mPrefKeyModelName);
        prefModelName.setEnabled(prefsEnabled);
        final EditText editTextModelName = prefModelName.getEditText();
        editTextModelName.setSingleLine(true);

        final EditTextPreference prefModelYear = (EditTextPreference) group.findPreference(mVehicleMng.mPrefKeyModelYear);
        prefModelYear.setEnabled(prefsEnabled);
        final EditText editTextModelYear = prefModelYear.getEditText();
        editTextModelYear.setSingleLine(true);
        editTextModelYear.setInputType(InputType.TYPE_CLASS_DATETIME | InputType.TYPE_DATETIME_VARIATION_DATE);

        final EditTextPreference prefTrim = (EditTextPreference) group.findPreference(mVehicleMng.mPrefKeyTrim);
        prefTrim.setEnabled(prefsEnabled);
        final EditText editTextSeries = prefTrim.getEditText();
        editTextSeries.setSingleLine(true);

        final EditTextPreference prefVehicleType = (EditTextPreference) group.findPreference(mVehicleMng.mPrefKeyVehicleType);
        prefVehicleType.setEnabled(prefsEnabled);
        final EditText editTextVehicleType = prefVehicleType.getEditText();
        editTextVehicleType.setSingleLine(true);

        final EditTextPreference prefDoorsCount = (EditTextPreference) group.findPreference(mVehicleMng.mPrefKeyDoorsCount);
        prefDoorsCount.setEnabled(prefsEnabled);
        final EditText editTextDoorsCount = prefDoorsCount.getEditText();
        editTextDoorsCount.setSingleLine(true);
        editTextDoorsCount.setInputType(InputType.TYPE_CLASS_NUMBER);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Log.d(TAG, "onSharedPreferenceChanged key: "+key);
        final VehicleInfo updateVehicleInfo = mVehicleMng.buildUpdateInfoWithPrefKey(key);
        if (updateVehicleInfo == null) {
            super.onSharedPreferenceChanged(sharedPreferences, key);
            return;
        }
        if (updateVehicleInfo.vinCode != null) {
            attachVehicle(updateVehicleInfo.vinCode, sharedPreferences, key);
        } else {
            updateVehicle(updateVehicleInfo, sharedPreferences, key);
        }
    }

    private void updateVehicle(final VehicleInfo updateInfo, final SharedPreferences sharedPreferences, final String key) {
        showLoading();
        mWebVehicleAPI.updateVehicleOfAccount(mCurAccount, updateInfo, new WebVehicleAPI.UpdateVehicleListener() {
            @Override
            public void onVehicleUpdated(int errorCode, VehicleInfo vehicleInfo) {
                hideLoading();
                if (errorCode == WebVehicleAPI.VEHICLE_ERROR_NONE && vehicleInfo != null) {
                    mVehicleMng.updateCurVehicleInfo(vehicleInfo);
                    mCurVehicle = mVehicleMng.getCurVehicle();
                    VehicleProfileItemPrefFragment.super.onSharedPreferenceChanged(sharedPreferences, key);
                    return;
                }
                mVehicleMng.setCurVehicle(mCurVehicle);
                processVehicleError(errorCode, R.string.ID_MSG_ERROR_UPDATE_VEHICLE_PROFILE);
            }
        });
    }

    private void attachVehicle(final String vinCode, final SharedPreferences sharedPreferences, final String key) {
        showLoading();
        mWebVehicleAPI.attachVehicleToAccount(mCurAccount, vinCode, new WebVehicleAPI.AttachVehicleListener() {
            @Override
            public void onVehicleAttached(int errorCode, VehicleInfo vehicleInfo) {
                hideLoading();
                if (errorCode == WebVehicleAPI.VEHICLE_ERROR_NONE && vehicleInfo != null) {
                    if (mCurVehicle != null) {
                        removeVehicle(mCurVehicle, null);
                    }
                    mVehicleMng.setCurVehicle(vehicleInfo);
                    mCurAccount.addVehicle(vehicleInfo);
                    mCurVehicle = vehicleInfo;
                    initPreferences(getPreferenceScreen());
                    setRemoveVehicleBtnEnabled(true);
                    VehicleProfileItemPrefFragment.super.onSharedPreferenceChanged(sharedPreferences, key);
                    return;
                }
                mVehicleMng.setCurVehicle(mCurVehicle);
                processVehicleError(errorCode, R.string.ID_MSG_ERROR_ATTACH_VEHICLE_PROFILE);
            }
        });
    }

    private void removeVehicle(final VehicleInfo vehicle, final WebVehicleAPI.RemoveVehicleListener listener) {
        final String vehicleName = vehicle.getDisplayName();
        mWebVehicleAPI.removeVehicleFromAccount(mCurAccount, vehicle.dbId, new WebVehicleAPI.RemoveVehicleListener() {
            @Override
            public void onVehicleRemoved(int errorCode) {
                if (errorCode != WebVehicleAPI.VEHICLE_ERROR_NONE) {
                    Log.e(TAG, "Failed to remove vehicle, errorCode: "+errorCode);
                    final String errorMsg = String.format(getString(R.string.ID_MSG_ERROR_REMOVE_VEHICLE_PROFILE), vehicleName);
                    showInvalidError(errorMsg);
                }
                if (listener != null) {
                    listener.onVehicleRemoved(errorCode);
                }
            }
        });
    }

    // Error process methods
    private void processVehicleError(final @WebVehicleAPI.VehicleError int errorCode, final int generalErrorMsgResId) {
        final Context context = getContext();
        if (context == null) {
            Log.w(TAG, "Fragment destroyed, ignore invalid error code: "+errorCode);
            return;
        }
        int errorMsgResId = generalErrorMsgResId;
        switch(errorCode) {
            case WebVehicleAPI.VEHICLE_ERROR_NONE:
                return;
            case WebVehicleAPI.VEHICLE_ERROR_NETWORK:
                errorMsgResId = R.string.ID_MSG_ERROR_NETWORK_FAILURE;
                break;
            case WebVehicleAPI.VEHICLE_ERROR_PARSE:
                errorMsgResId = R.string.ID_MSG_ERROR_PARSE_FAILURE;
                break;
            case WebVehicleAPI.VEHICLE_ERROR_INVALID_VINCODE:
                errorMsgResId = R.string.ID_MSG_ERROR_INVALID_VEHICLE_VINCODE;
                break;
            case WebVehicleAPI.VEHICLE_ERROR_GENERAL:
            default:
                break;
        }
        showInvalidError(getString(errorMsgResId));
    }

    private void showInvalidError(final String message) {
        final Context context = getContext();
        if (context != null) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        } else {
            Log.w(TAG, "Fragment destroyed, ignore invalid error: "+message);
        }
    }
}
