package com.jchain.affiliate.System;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.jchain.affiliate.Tracker.TrackerCheckService;

public final class BootCompletedReceiver extends BroadcastReceiver {
    private static final String TAG = "BootCompletedReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();
        Log.d(TAG, "Got action: "+action);
        if (action != null && (action.equals(Intent.ACTION_BOOT_COMPLETED)
        || action.equals("android.intent.action.QUICKBOOT_POWERON"))) {
            final Intent svcIntent = new Intent(context, TrackerCheckService.class);
            context.startService(svcIntent);
        }
    }
}
