package com.jchain.affiliate.System;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.os.Handler;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import com.jchain.affiliate.Utils.FileUtils;

import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.UUID;

public final class SystemInfoManager {
    private static final String TAG = "SystemInfoManager";

    private static SystemInfoManager sInstance = null;

    public static SystemInfoManager getInstance() {
        if (sInstance == null) {
            sInstance = new SystemInfoManager();
        }
        return sInstance;
    }

    private SystemInfoManager() {
    }

    private String mDeviceUUID = null;
    public String getDeviceUUID(final Context context) {
        if (mDeviceUUID != null) {
            return mDeviceUUID;
        }
        mDeviceUUID = loadDeviceUUID(context);
        return mDeviceUUID;
    }

    private static final String DEVICE_UUID_FILENAME = "deviceUUID";
    private String loadDeviceUUID(final Context context) {
        final FileUtils fileUtils = FileUtils.getInstance(context);
        String deviceUUID = fileUtils.readFile(DEVICE_UUID_FILENAME);
        if (deviceUUID != null) {
            return deviceUUID;
        }
        deviceUUID = generateDeviceUUID(context);
        try {
            fileUtils.saveFile(DEVICE_UUID_FILENAME, deviceUUID.getBytes("UTF8"));
        } catch(UnsupportedEncodingException e) {
            Log.e(TAG, "Failed to save deviceUUID file, exception: ", e);
        }
        return deviceUUID;
    }

    private String generateDeviceUUID(final Context context) {
        final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        String tmDevice = null, tmSerial = null, androidId = null;
        try {
            tmDevice = tm.getDeviceId();
        } catch (SecurityException e) {
            Log.w(TAG, "Failed to get tmDeviceID, exception: ", e);
        }
        if (tmDevice == null) {
            tmDevice = "";
        }
        try {
            tmSerial = tm.getSimSerialNumber();
        } catch (SecurityException e) {
            Log.w(TAG, "Failed to get tmSerialID, exception: ", e);
        }
        if (tmSerial == null) {
            tmSerial = "";
        }
        androidId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        if (androidId == null) {
            androidId = "";
        }

        UUID deviceUUID = new UUID(androidId.hashCode(), ((long)tmDevice.hashCode() << 32) | tmSerial.hashCode());
        return deviceUUID.toString();
    }

    public String getCurAppVersion(final Context context) {
        final PackageManager pm = context.getPackageManager();
        try {
            final PackageInfo pkgInfo = pm.getPackageInfo(context.getPackageName(), PackageManager.GET_META_DATA);
            return pkgInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "Failed to query package info of current app", e);
            return "";
        }
    }

    public float convertDPToPixels(final Context context, final int dpValue) {
        final DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return dpValue*displayMetrics.density;
    }

    public Point getScreenSize(final Context context) {
        final WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        final Display display = wm.getDefaultDisplay();
        final Point point = new Point();
        display.getSize(point);
        return point;
    }

    public boolean isNetworkAvailable(final Context context) {
        final ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    public interface InternetAvailableListener {
        void onInternetAvailableResult(final boolean available);
    }

    private void notifyInternetAvailableListener(final Context context, final InternetAvailableListener listener, final boolean available) {
        final Handler handler = new Handler(context.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (listener != null) {
                    listener.onInternetAvailableResult(available);
                }
            }
        });
    }

    private static final String CHECK_SERVER_NAME = "jchaintech.com";
    public void checkInternetAvailable(final Context context, final InternetAvailableListener listener) {
        if (!isNetworkAvailable(context)) {
            notifyInternetAvailableListener(context, listener, false);
            return;
        }
        final Thread checkInternetThrd = new Thread() {
            @Override
            public void run() {
                try {
                    InetAddress ipAddr = InetAddress.getByName(CHECK_SERVER_NAME);
                    notifyInternetAvailableListener(context, listener, ipAddr != null && !ipAddr.toString().isEmpty());
                } catch (UnknownHostException e) {
                    Log.d(TAG, "Internet checking failed, exception: ", e);
                    notifyInternetAvailableListener(context, listener, false);
                }
            }
        };
        checkInternetThrd.setName("CheckInternetThrd");
        checkInternetThrd.start();
    }
}
