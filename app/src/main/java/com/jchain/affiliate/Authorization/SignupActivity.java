package com.jchain.affiliate.Authorization;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.jchain.affiliate.Account.AccountInfo;
import com.jchain.affiliate.Account.AccountManagement;
import com.jchain.affiliate.WebAPI.WebAuthAPI;
import com.jchain.affiliate.R;

public final class SignupActivity extends AppCompatActivity {

    private AccountManagement mAccountMng = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        mAccountMng = AccountManagement.getInstance(getBaseContext());

        initUI();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private EditText mInputName = null;
    private EditText mInputEmail = null;
    private EditText mInputPassword = null;
    private EditText mInputRepeatPassword = null;
    private Button mBtnSignUp = null;
    private View mProgressBarLayer = null;
    private void initUI() {
        mInputName = findViewById(R.id.inputName);
        mInputEmail = findViewById(R.id.inputEmail);
        mInputPassword = findViewById(R.id.inputPassword);
        mInputRepeatPassword = findViewById(R.id.inputRepeatPassword);

        mBtnSignUp = findViewById(R.id.btnSignUp);
        mBtnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkDataValid()) {
                    doSignUp();
                }
            }
        });

        mProgressBarLayer = findViewById(R.id.progressBarLayer);
        mProgressBarLayer.setVisibility(View.GONE);
    }

    // Action Methods
    private static final int PASSWORD_MIN_LENGTH_ALLOWED = 6;
    private boolean checkDataValid() {
        // Check name
        final String name = mInputName.getEditableText().toString();
        if (name.isEmpty() ) {
            mInputName.requestFocus();
            showInvalidError(getString(R.string.ID_MSG_ERROR_EMPTY_ACCOUNT_NAME));
            return false;
        }
        final String email = mInputEmail.getEditableText().toString();
        if (!email.matches(AccountInfo.PATTERN_EMAIL)) {
            mInputEmail.requestFocus();
            showInvalidError(getString(R.string.ID_MSG_ERROR_INVALID_EMAIL));
            return false;
        }
        // Check password
        final String password = mInputPassword.getEditableText().toString();
        final String repeatedPassword = mInputRepeatPassword.getEditableText().toString();
        if (password.length() < PASSWORD_MIN_LENGTH_ALLOWED) {
            mInputPassword.requestFocus();
            showInvalidError(getString(R.string.ID_MSG_ERROR_TOO_SHORT_PASSWORD));
            return false;
        }
        if (!password.equals(repeatedPassword)) {
            mInputRepeatPassword.requestFocus();
            showInvalidError(getString(R.string.ID_MSG_ERROR_INCONSISTENT_PASSWORD));
            return false;
        }
        return true;
    }

    private void showInvalidError(final String message) {
        Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT).show();
    }

    private void doSignUp() {
        final String name = mInputName.getEditableText().toString();
        String email = mInputEmail.getEditableText().toString();
        final String password = mInputPassword.getEditableText().toString();
        final WebAuthAPI webAuthAPI = WebAuthAPI.getInstance(getBaseContext());
        webAuthAPI.registerAccount(AccountInfo.ACCOUNT_TYPE_GENERAL,
                name, password, email, null, null, null,
                AccountInfo.ACCOUNT_GENDER_UNKNOWN, null, null,
                new WebAuthAPI.RegisterListener() {
                    @Override
                    public void onRegisterResult(final @WebAuthAPI.AuthError int errorCode) {
                        if (errorCode == WebAuthAPI.AUTH_ERROR_NONE) { // Register successfully
                            doLogin(name, password);
                            return;
                        }
                        // Stop loading bar
                        hideProgressBar();
                        // Register failed, show error messages
                        showRegisterFailureMessage(errorCode);
                    }
                });
        showProgressBar();
    }

    private void doLogin(final String name, final String password) {
        final WebAuthAPI webAuthAPI = WebAuthAPI.getInstance(getBaseContext());
        webAuthAPI.loginAccount(name, password, new WebAuthAPI.LoginListener() {
            @Override
            public void onLoginResult(final @WebAuthAPI.AuthError int errorCode, final AccountInfo accountInfo) {
                // Stop loading bar
                hideProgressBar();

                if (errorCode == WebAuthAPI.AUTH_ERROR_NONE) {
                    if (accountInfo == null) {
                        showLoginFailureMessage(WebAuthAPI.AUTH_ERROR_GENERAL);
                        processLoginFailure();
                        return;
                    }
                    processSignupSuccess(accountInfo);
                    return;
                }
                // Login failed, show error messages
                showLoginFailureMessage(errorCode);
                processLoginFailure();
            }
        });
    }

    private void showProgressBar() {
        mBtnSignUp.setEnabled(false);
        mProgressBarLayer.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar() {
        mBtnSignUp.setEnabled(true);
        mProgressBarLayer.setVisibility(View.GONE);
    }

    private void showLoginFailureMessage(final @WebAuthAPI.AuthError int errorCode) {
        int errorMsgResId = R.string.ID_MSG_ERROR_LOGIN_FAILURE;
        switch (errorCode) {
            case WebAuthAPI.AUTH_ERROR_NETWORK:
                errorMsgResId = R.string.ID_MSG_ERROR_NETWORK_FAILURE;
                break;
            case WebAuthAPI.AUTH_ERROR_PARSE:
                errorMsgResId = R.string.ID_MSG_ERROR_PARSE_FAILURE;
                break;
            case WebAuthAPI.AUTH_ERROR_BANNED:
                errorMsgResId = R.string.ID_MSG_ERROR_BANNED;
                break;
            case WebAuthAPI.AUTH_ERROR_NO_ACTIVATAION:
                errorMsgResId = R.string.ID_MSG_ERROR_NO_ACTIVATION;
                break;
            case WebAuthAPI.AUTH_ERROR_GENERAL:
            default:
                break;
        }
        showInvalidError(getString(errorMsgResId));
    }

    private void showRegisterFailureMessage(final @WebAuthAPI.AuthError int errorCode) {
        int errorMsgResId = R.string.ID_MSG_ERROR_SIGNUP_FAILURE;
        switch (errorCode) {
            case WebAuthAPI.AUTH_ERROR_NETWORK:
                errorMsgResId = R.string.ID_MSG_ERROR_NETWORK_FAILURE;
                break;
            case WebAuthAPI.AUTH_ERROR_PARSE:
                errorMsgResId = R.string.ID_MSG_ERROR_PARSE_FAILURE;
                break;
            case WebAuthAPI.AUTH_ERROR_DUPLICATED_NAME:
                mInputName.requestFocus();
                errorMsgResId = R.string.ID_MSG_ERROR_DUPLICATED_NAME;
                break;
            case WebAuthAPI.AUTH_ERROR_DUPLICATED_EMAIL:
                mInputEmail.requestFocus();
                errorMsgResId = R.string.ID_MSG_ERROR_DUPLICATED_EMAIL;
                break;
            case WebAuthAPI.AUTH_ERROR_DUPLICATED_PHONE:
                errorMsgResId = R.string.ID_MSG_ERROR_DUPLICATED_PHONE; // this shall never happen now
                break;
            case WebAuthAPI.AUTH_ERROR_GENERAL:
            default:
                break;
        }
        showInvalidError(getString(errorMsgResId));
    }

    private void processSignupSuccess(final AccountInfo newAccount) {
        mAccountMng.setCurAccountInfo(newAccount);
        setResult(RESULT_OK);
        finish();
    }

    private void processLoginFailure() {
        setResult(RESULT_CANCELED);
        finish();
    }
}
