package com.jchain.affiliate.Authorization;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.jchain.affiliate.Account.AccountInfo;
import com.jchain.affiliate.Account.AccountManagement;
import com.jchain.affiliate.Account.OAuth.BaseOAuthHelper;
import com.jchain.affiliate.Account.OAuth.OAuthInfo;
import com.jchain.affiliate.Account.OAuth.OAuthManagement;
import com.jchain.affiliate.Tracker.TrackerCheckService;
import com.jchain.affiliate.WebAPI.WebAuthAPI;
import com.jchain.affiliate.MainActivity;
import com.jchain.affiliate.R;
import com.jchain.affiliate.System.SystemInfoManager;

public final class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";

    private AccountManagement mAccountMng = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAccountMng = AccountManagement.getInstance(getBaseContext());

        initUI();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private static final int REQUEST_CODE_SIGN_UP = 1;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_CODE_SIGN_UP) {
            if (resultCode == RESULT_OK) {
                enterMainActivity();
            }
            return;
        }
        final OAuthManagement oauthMng = OAuthManagement.getInstance(getBaseContext());
        if (oauthMng.onActivityResult(requestCode, resultCode, data)) {
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private EditText mInputName = null;
    private EditText mInputPassword = null;
    private Button mBtnLogin = null;
    private View mProgressBarLayer = null;
    private void initUI() {
        // Login area
        mInputName = findViewById(R.id.inputName);
        mInputPassword = findViewById(R.id.inputPassword);

        mBtnLogin = findViewById(R.id.btnLogin);
        mBtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkDataValid()) {
                    doLogin();
                }
            }
        });
        // 3rd party OAuth part
        final View btnAuthWithFacebook = findViewById(R.id.btnAuthWithFacebook);
        btnAuthWithFacebook.setOnClickListener(mOAuthBtnsListener);
        final View btnAuthWithGoogle = findViewById(R.id.btnAuthWithGoogle);
        btnAuthWithGoogle.setOnClickListener(mOAuthBtnsListener);
        final View btnAuthWithTwitter = findViewById(R.id.btnAuthWithTwitter);
        btnAuthWithTwitter.setOnClickListener(mOAuthBtnsListener);

        // TODO: Disable temporarily, will enable in the future.
        btnAuthWithFacebook.setVisibility(View.GONE);
        btnAuthWithGoogle.setVisibility(View.GONE);
        btnAuthWithTwitter.setVisibility(View.GONE);

        // Other buttons
        final View linkBtnSignUp = findViewById(R.id.btnSignUp);
        linkBtnSignUp.setOnClickListener(mLinkBtnsListener);

        final View linkBtnResetPassword = findViewById(R.id.btnResetPassword);
        linkBtnResetPassword.setOnClickListener(mLinkBtnsListener);
        // TODO: Disable temporarily, will enable in the future.
        linkBtnResetPassword.setVisibility(View.GONE);

        final View linkBtnSkip = findViewById(R.id.btnSkip);
        linkBtnSkip.setOnClickListener(mLinkBtnsListener);

        // Other part
        final String versionInfo = buildVersionInfo();
        final TextView textVersionInfo = findViewById(R.id.textVersionInfo);
        textVersionInfo.setText(versionInfo);

        mProgressBarLayer = findViewById(R.id.progressBarLayer);
    }

    private String buildVersionInfo() {
        final SystemInfoManager systemInfoMng = SystemInfoManager.getInstance();
        final String versionInfoTemplate = getString(R.string.ID_MSG_VERSION_INFO_TEMPLATE);
        return String.format(versionInfoTemplate, systemInfoMng.getCurAppVersion(getApplicationContext()));
    }

    private boolean checkDataValid() {
        final String name = mInputName.getEditableText().toString();
        if (name.isEmpty()) {
            mInputName.requestFocus();
            showInvalidError(getString(R.string.ID_MSG_ERROR_EMPTY_ACCOUNT_NAME));
            return false;
        }
        final String password = mInputPassword.getEditableText().toString();
        if (password.isEmpty()) {
            mInputPassword.requestFocus();
            showInvalidError(getString(R.string.ID_MSG_ERROR_EMPTY_PASSWORD));
            return false;
        }
        return true;
    }

    private void showInvalidError(final String message) {
        Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT).show();
    }

    private void doLogin() {
        showProgressBar();

        final String name = mInputName.getEditableText().toString();
        final String password = mInputPassword.getEditableText().toString();
        final WebAuthAPI webAuthAPI = WebAuthAPI.getInstance(getBaseContext());
        webAuthAPI.loginAccount(name, password, new WebAuthAPI.LoginListener() {
            @Override
            public void onLoginResult(final @WebAuthAPI.AuthError int errorCode, final AccountInfo accountInfo) {
                // Stop loading bar
                hideProgressBar();

                if (errorCode == WebAuthAPI.AUTH_ERROR_NONE && accountInfo != null) { // Login successfully
                    mAccountMng.setCurAccountInfo(accountInfo);
                    enterMainActivity();
                    return;
                }
                // Login failed, show error messages
                processLoginFailure(errorCode);
            }
        });
    }

    private void processLoginFailure(final @WebAuthAPI.AuthError int errorCode) {
        int errorMsgResId = R.string.ID_MSG_ERROR_LOGIN_FAILURE;
        switch (errorCode) {
            case WebAuthAPI.AUTH_ERROR_NETWORK:
                errorMsgResId = R.string.ID_MSG_ERROR_NETWORK_FAILURE;
                break;
            case WebAuthAPI.AUTH_ERROR_PARSE:
                errorMsgResId = R.string.ID_MSG_ERROR_PARSE_FAILURE;
                break;
            case WebAuthAPI.AUTH_ERROR_BANNED:
                errorMsgResId = R.string.ID_MSG_ERROR_BANNED;
                break;
            case WebAuthAPI.AUTH_ERROR_NO_ACTIVATAION:
                errorMsgResId = R.string.ID_MSG_ERROR_NO_ACTIVATION;
                break;
            case WebAuthAPI.AUTH_ERROR_GENERAL:
            default:
                break;
        }
        showInvalidError(getString(errorMsgResId));
    }

    private void showProgressBar() {
        mBtnLogin.setEnabled(false);
        mProgressBarLayer.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar() {
        mBtnLogin.setEnabled(true);
        mProgressBarLayer.setVisibility(View.GONE);
    }
    // OAuth methods
    private final View.OnClickListener mOAuthBtnsListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            final int viewId = view.getId();
            switch (viewId) {
                case R.id.btnAuthWithFacebook:
                    authWithFacebook();
                    break;
                case R.id.btnAuthWithGoogle:
                    authWithGoogle();
                    break;
                case R.id.btnAuthWithTwitter:
                default:
                    showInvalidError(getString(R.string.ID_MSG_ERROR_NOT_SUPPORTED));
                    break;
            }
        }
    };

    private final OAuthManagement.ResultListener mResultListener = new OAuthManagement.ResultListener() {
        @Override
        public void onResult(final @BaseOAuthHelper.ResultValue int result, final OAuthInfo info) {
            if (result == BaseOAuthHelper.RESULT_SUCCESS && info != null) {
                loginOAuthAccount(info, true);
                return;
            }
            // Login failed, hide progress bar at first
            hideProgressBar();
            // Login failed, show error messages
            processOAuthFailure(result);
        }
    };

    private void authWithFacebook() {
        showProgressBar();
        final OAuthManagement oauthMng = OAuthManagement.getInstance(getBaseContext());
        oauthMng.loginFacebook(this, mResultListener);
    }

    private void authWithGoogle() {
        showProgressBar();
        final OAuthManagement oauthMng = OAuthManagement.getInstance(getBaseContext());
        oauthMng.loginGoogle(this, mResultListener);
    }

    private void loginOAuthAccount(final OAuthInfo info, final boolean fallToRegister) {
        final WebAuthAPI webAuthAPI = WebAuthAPI.getInstance(getBaseContext());
        webAuthAPI.loginAccountWithOAuthInfo(info,
                new WebAuthAPI.LoginListener() {
                    @Override
                    public void onLoginResult(final @WebAuthAPI.AuthError int errorCode, final AccountInfo accountInfo) {
                        if (errorCode == WebAuthAPI.AUTH_ERROR_NONE && accountInfo != null) { // Login successfully
                            // Hide progress bar
                            hideProgressBar();

                            processOAuthSuccess(info, accountInfo);
                            return;
                        }
                        if (fallToRegister) { // Login failed, try to register new one
                            registerOAuthAccount(info);
                        } else { // Login failed after registered, show error message
                            // Hide progress bar
                            hideProgressBar();

                            processOAuthFailure(BaseOAuthHelper.RESULT_FAILURE_GENERAL);
                        }
                    }
                });
    }

    private void registerOAuthAccount(final OAuthInfo info) {
        final WebAuthAPI webAuthAPI = WebAuthAPI.getInstance(getBaseContext());
        webAuthAPI.registerAccountWithOAuthInfo(info,
                new WebAuthAPI.RegisterListener() {
                    @Override
                    public void onRegisterResult(final @WebAuthAPI.AuthError int errorCode) {
                        if (errorCode == WebAuthAPI.AUTH_ERROR_NONE) { // Register successfully
                            loginOAuthAccount(info, false);
                            return;
                        }
                        // Register failed, hide progress bar
                        hideProgressBar();

                        processOAuthFailure(BaseOAuthHelper.RESULT_FAILURE_GENERAL);
                    }
                });
    }

    private void processOAuthSuccess(final OAuthInfo info, final AccountInfo accountInfo) {
        if (accountInfo == null) {
            Log.e(TAG, "Original account info is null, it shouldn't happen");
            return;
        }
        accountInfo.firstName = info.name;
        accountInfo.email = info.email;
        mAccountMng.setCurAccountInfo(accountInfo);

        enterMainActivity();
    }

    private void processOAuthFailure(final @BaseOAuthHelper.ResultValue int result) {
        int errorMsgResId = R.string.ID_MSG_ERROR_OAUTH_FAILURE;
        switch (result) {
            case BaseOAuthHelper.RESULT_FAILURE_NETWORK:
                errorMsgResId = R.string.ID_MSG_ERROR_NETWORK_FAILURE;
                break;
            case BaseOAuthHelper.RESULT_FAILURE_REJECT:
            case BaseOAuthHelper.RESULT_FAILURE_GENERAL:
            default:
                break;
        }
        showInvalidError(getString(errorMsgResId));
    }

    // Other button methods
    private final View.OnClickListener mLinkBtnsListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            final int viewId = view.getId();
            switch (viewId) {
                case R.id.btnSignUp:
                    enterSignupActivity();
                    break;
                case R.id.btnSkip:
                    enterMainActivity();
                    break;
                default:
                    break;
            }
        }
    };
    // Action methods
    private void enterMainActivity() {
        final Intent intent = new Intent(getBaseContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);

        // always start Check Tracker service
        final Intent svcIntent = new Intent(getBaseContext(), TrackerCheckService.class);
        startService(svcIntent);
        finish();
    }

    private void enterSignupActivity() {
        final Intent intent = new Intent(getBaseContext(), SignupActivity.class);
        startActivityForResult(intent, REQUEST_CODE_SIGN_UP);
    }
}
