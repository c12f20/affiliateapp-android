package com.jchain.affiliate.Tracker;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import android.util.Log;

import com.jchain.affiliate.Account.AccountInfo;
import com.jchain.affiliate.Account.AccountManagement;
import com.jchain.affiliate.MainActivity;
import com.jchain.affiliate.Product.ProductInfoManager;
import com.jchain.affiliate.R;
import com.jchain.affiliate.WebAPI.WebAuthAPI;
import com.jchain.affiliate.WebAPI.WebProductAPI;

import java.util.ArrayList;
import java.util.List;

public final class TrackerCheckService extends Service {
    private static final String TAG = "TrackerCheckService";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private ProductInfoManager mProductInfoManager = null;
    private AccountManagement mAccountManager = null;
    private Handler mHandler = null;
    @Override
    public void onCreate() {
        super.onCreate();
        mHandler = new Handler();
        mProductInfoManager = ProductInfoManager.getInstance(getBaseContext());
        mAccountManager = AccountManagement.getInstance(getBaseContext());

        createNotificationChannel();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startCheckUpdate();
        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        stopCheckUpdate();
        super.onDestroy();
    }

    // Check Update timer
    private static final long CHECK_UPDATE_INTERVAL = 60*1000L; // one hour
    private static final long CHECK_UPDATE_RETRY_TIMEOUT = 60*1000L; // one minute
    private static final int MAX_RETRY_TIMES = 3;
    private Runnable mCheckUpdateRunnable = null;
    private int mCheckRetryTimes = 0;
    private void startCheckUpdate() {
        stopCheckUpdate();
        if (mCheckUpdateRunnable == null) {
            mCheckUpdateRunnable = new Runnable() {
                @Override
                public void run() {
                    checkLogin(); // always do check login at first
                }
            };
            mHandler.post(mCheckUpdateRunnable);
        }
    }

    private void delayToCheckUpdate(final long delay) {
        if (mCheckUpdateRunnable != null) {
            Log.d(TAG, "Check update again after: "+delay);
            mHandler.postDelayed(mCheckUpdateRunnable, delay);
        }
    }

    private void stopCheckUpdate() {
        if (mCheckUpdateRunnable != null) {
            mHandler.removeCallbacks(mCheckUpdateRunnable);
            mCheckUpdateRunnable = null;
        }
        mProductInfoManager.cancelQueryUpdatedTracks();
    }

    private void checkUpdate() {
        mProductInfoManager.queryUpdatedTracks(new WebProductAPI.TrackDataListener() {
            @Override
            public void onGotTracksList(int errorCode, ArrayList<PriceTrackerItemInfo> tracksList) {
                if (errorCode == WebProductAPI.PRODUCT_ERROR_NONE) {
                    checkUpdateTrackers(tracksList);
                } else {
                    if (mCheckRetryTimes < MAX_RETRY_TIMES) {
                        mCheckRetryTimes++;
                        Log.d(TAG, "Retry check update");
                        delayToCheckUpdate(CHECK_UPDATE_RETRY_TIMEOUT);
                        return;
                    }
                }
                mCheckRetryTimes = 0;
                delayToCheckUpdate(CHECK_UPDATE_INTERVAL);
            }
        });
    }

    private List<PriceTrackerItemInfo> mLatestUpdateTrackerList = new ArrayList<PriceTrackerItemInfo>();
    private void checkUpdateTrackers(ArrayList<PriceTrackerItemInfo> tracksList) {
        for (PriceTrackerItemInfo tracker : tracksList) {
            Log.d(TAG, "Got updated tracker: "+tracker.id+", price: "+tracker.latestPrice);
            final int oldIndex = mLatestUpdateTrackerList.indexOf(tracker);
            if (oldIndex >= 0) { // this is a notified tracker
                final PriceTrackerItemInfo oldTracker = mLatestUpdateTrackerList.get(oldIndex);
                if (oldTracker.latestPrice == tracker.latestPrice) {
                    Log.d(TAG, "It's a old one and no price change, ignore it");
                    continue;
                }
            }
            if (tracker.needNotify()) { // Got one need notify, send and return
                Log.d(TAG, "Notify this tracker");
                sendUpdateNotification();
                break;
            }
        }
        mLatestUpdateTrackerList.clear();
        mLatestUpdateTrackerList.addAll(tracksList);
    }

    // Account Check methods
    private void checkLogin() {
        if (mAccountManager.hasAccountLogin()) {
            mCheckRetryTimes = 0;
            checkUpdate();
            return;
        }
        final String savedToken = mAccountManager.loadCurAccountToken();
        if (savedToken.isEmpty()) {
            Log.d(TAG, "CheckLogin: No saved access token found");
            return;
        }

        final WebAuthAPI webAuthAPI = WebAuthAPI.getInstance(getBaseContext());
        webAuthAPI.verifyAccount(savedToken, new WebAuthAPI.VerifyAccountListener() {
            @Override
            public void onVerifyResult(final int errorCode, final AccountInfo userInfo) {
                if (errorCode == WebAuthAPI.AUTH_ERROR_NONE && userInfo != null) { // Login successfully
                    mAccountManager.setCurAccountInfo(userInfo);
                    mCheckRetryTimes = 0;
                    checkUpdate();
                } else {
                    if (mCheckRetryTimes < MAX_RETRY_TIMES) {
                        mCheckRetryTimes++;
                        Log.d(TAG, "Retry check update as verifyAccount failure");
                        delayToCheckUpdate(CHECK_UPDATE_RETRY_TIMEOUT);
                    } else {
                        mCheckRetryTimes = 0;
                        delayToCheckUpdate(CHECK_UPDATE_INTERVAL);
                    }
                }
            }
        });
    }

    // Notification related methods
    public static final String CHANNEL_ID = "Channel.TrackerUpdate";
    private static final int NOTIFICATION_ID = 0x0001;

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.ID_NOTIFICATION_CHANNEL_NAME);
            String description = getString(R.string.ID_NOTIFICATION_CHANNEL_DESC);
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }
    }

    private void sendUpdateNotification() {
        final Intent intent = new Intent(getBaseContext(), MainActivity.class);
        intent.putExtra(MainActivity.PARAM_NAME_SHOW_TRACKS_DIRECTLY, true);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        final PendingIntent pendingIntent = PendingIntent.getActivity(getBaseContext(), 0, intent, 0);
        final NotificationCompat.Builder builder = new NotificationCompat.Builder(getBaseContext(), CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_logo)
                .setContentTitle(getString(R.string.ID_TITLE_NEW_PRICE))
                .setContentText(getString(R.string.ID_MSG_NEW_PRICE))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getBaseContext());
        notificationManager.notify(NOTIFICATION_ID, builder.build());
    }
}
