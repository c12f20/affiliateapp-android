package com.jchain.affiliate.Tracker;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jchain.affiliate.R;
import com.jchain.affiliate.UI.Widgets.OnlineImageViewUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

public class TrackerItemRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Context mContext;
    public TrackerItemRecyclerAdapter(final Context context) {
        mContext = context;
    }

    public interface OnItemClickListener {
        void onItemClicked(final int position);
    }

    private OnItemClickListener mOnItemClickListener = null;
    public void setOnItemClickListener(final OnItemClickListener listener) {
        mOnItemClickListener = listener;
    }

    private void notifyItemClicked(final int position) {
        if (mOnItemClickListener != null) {
            mOnItemClickListener.onItemClicked(position);
        }
    }

    public interface OnItemFuncBtnClickListener {
        void onItemStopTrackClicked(final int position);
        void onItemSetPriceClicked(final int position);
    }
    private OnItemFuncBtnClickListener mOnItemFuncBtnClickListener = null;
    public void setOnItemFuncBtnClickListener(final OnItemFuncBtnClickListener listener) {
        mOnItemFuncBtnClickListener = listener;
    }

    private ArrayList<PriceTrackerItemInfo> mTrackersList = new ArrayList<PriceTrackerItemInfo>();
    public void updateTrackerList(final ArrayList<PriceTrackerItemInfo> list) {
        if (list == null) {
            return;
        }
        mTrackersList.clear();
        mTrackersList.addAll(list);
        notifyDataSetChanged();
    }

    private static class TrackerViewHolder extends RecyclerView.ViewHolder {
        private final View mRootView;
        private final ImageView mImageProduct;
        private final TextView mTextProductName;
        private final TextView mTextVendorName;
        private final TextView mTextProviderName;
        private final TextView mTextTrackDate;
        private final TextView mTextProductOrigPrice;
        private final TextView mTextProductPrice;
        private final ImageView mBtnStopTrack;
        private final ImageView mBtnSetPrice;

        private final OnItemFuncBtnClickListener mOnItemFuncBtnClickListener;

        private final OnlineImageViewUtils mOnlineImageViewUtils;

        private int mItemIndex = -1;

        private final Context mContext;
        public TrackerViewHolder(final Context context, @NonNull View itemView, OnItemFuncBtnClickListener listener) {
            super(itemView);
            mContext = context;
            mRootView = itemView;
            mImageProduct = itemView.findViewById(R.id.imgProductPhoto);
            mTextProductName = itemView.findViewById(R.id.textProductName);
            mTextVendorName = itemView.findViewById(R.id.textVendorName);
            mTextProviderName = itemView.findViewById(R.id.textDataProviderName);
            mTextTrackDate = itemView.findViewById(R.id.textProductTrackDate);
            mTextProductOrigPrice = itemView.findViewById(R.id.textProductOrigPrice);
            mTextProductPrice = itemView.findViewById(R.id.textProductPrice);

            mOnItemFuncBtnClickListener = listener;

            mBtnStopTrack = itemView.findViewById(R.id.btnStopTrack);
            mBtnStopTrack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    notifyStopTrackClicked(mItemIndex);
                }
            });
            mBtnSetPrice = itemView.findViewById(R.id.btnSetPrice);
            mBtnSetPrice.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    notifySetPriceClicked(mItemIndex);
                }
            });

            mOnlineImageViewUtils = new OnlineImageViewUtils(context);
        }

        private View getItemView() {
            return mRootView;
        }

        private void updateItemView(final int itemIndex, final PriceTrackerItemInfo itemInfo) {
            mItemIndex = itemIndex;
            if (itemInfo.imageURL != null) {
                mOnlineImageViewUtils.loadImage(mImageProduct, itemInfo.imageURL);
            }
            mTextProductName.setText(itemInfo.name);
            mTextVendorName.setText(itemInfo.vendorName);
            mTextProviderName.setText(itemInfo.providerName);
            final SimpleDateFormat sdf = new SimpleDateFormat("MMM d", Locale.US);
            if (itemInfo.createTime != null) {
                mTextTrackDate.setText(sdf.format(itemInfo.createTime));
            } else {
                mTextTrackDate.setVisibility(View.GONE);
            }
            mBtnSetPrice.setActivated(itemInfo.hasNotifyPrice());
            final int priceTextColorId;
            if (itemInfo.needNotify()) {
                priceTextColorId = R.color.colorProductLowerPriceTextColor;
            } else {
                priceTextColorId = R.color.colorProductPriceTextColor;
            }
            mTextProductOrigPrice.setText(String.format(Locale.US, "$%.2f", itemInfo.origPrice));
            mTextProductPrice.setText(String.format(Locale.US, "$%.2f", itemInfo.latestPrice));
            mTextProductPrice.setTextColor(mContext.getResources().getColor(priceTextColorId));
        }

        private void notifyStopTrackClicked(final int position) {
            if (mOnItemFuncBtnClickListener != null) {
                mOnItemFuncBtnClickListener.onItemStopTrackClicked(position);
            }
        }

        private void notifySetPriceClicked(final int position) {
            if (mOnItemFuncBtnClickListener != null) {
                mOnItemFuncBtnClickListener.onItemSetPriceClicked(position);
            }
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        final LayoutInflater inflater = LayoutInflater.from(mContext);
        final View view = inflater.inflate(R.layout.view_tracker_item, viewGroup, false);
        return new TrackerItemRecyclerAdapter.TrackerViewHolder(mContext, view, mOnItemFuncBtnClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof TrackerViewHolder) {
            final int itemIndex = viewHolder.getAdapterPosition();
            PriceTrackerItemInfo item = mTrackersList.get(itemIndex);
            final TrackerViewHolder trackerViewHolder = (TrackerViewHolder) viewHolder;
            trackerViewHolder.updateItemView(itemIndex, item);
            trackerViewHolder.getItemView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    notifyItemClicked(itemIndex);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mTrackersList.size();
    }
}
