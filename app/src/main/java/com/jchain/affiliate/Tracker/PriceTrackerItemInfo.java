package com.jchain.affiliate.Tracker;

import androidx.annotation.IntDef;

import java.util.Date;

public final class PriceTrackerItemInfo {
    public int id;
    public String name;
    public String vendorName;
    public int providerId;
    public String providerName;
    public String clickURL;
    public String imageURL;
    public Date createTime;
    public float origPrice;
    public float latestPrice;
    public float notifyPrice;

    public PriceTrackerItemInfo(int id, String name,
                                String vendorName, int providerId, String providerName,
                                String clickURL, String imageURL,
                                Date createTime, float origPrice,
                                float latestPrice, float notifyPrice) {
        this.id = id;
        this.name = name;
        this.providerId = providerId;
        this.vendorName = vendorName;
        this.providerName = providerName;
        this.clickURL = clickURL;
        this.imageURL = imageURL;
        this.createTime = createTime;
        this.origPrice = origPrice;
        this.latestPrice = latestPrice;
        this.notifyPrice = notifyPrice;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof PriceTrackerItemInfo)) {
            return false;
        }
        PriceTrackerItemInfo other = (PriceTrackerItemInfo)obj;

        return id == other.id;
    }

    public boolean hasNotifyPrice() {
        return notifyPrice >= 0;
    }

    public boolean needNotify() {
        if (notifyPrice < 0) {
            return latestPrice < origPrice;
        } else {
            return latestPrice < notifyPrice;
        }
    }
}
