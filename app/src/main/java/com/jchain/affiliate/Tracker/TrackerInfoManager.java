package com.jchain.affiliate.Tracker;

import android.content.Context;
import android.util.Log;

import com.jchain.affiliate.Utils.FileUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collection;
import java.util.HashMap;

public final class TrackerInfoManager {
    private static final String TAG = "TrackerInfoManager";

    private static TrackerInfoManager sInstance = null;
    public static TrackerInfoManager getInstance(final Context context) {
        if (sInstance == null) {
            sInstance = new TrackerInfoManager(context);
        }
        return sInstance;
    }

    private static final String TRACKER_INFO_FILENAME = "trackerInfo.json";
    private final FileUtils mFileUtils;
    private final HashMap<Integer, Integer> mTrackInfoHash = new HashMap<Integer, Integer>();

    private TrackerInfoManager(final Context context) {
        mFileUtils = FileUtils.getInstance(context);
        loadTrackerInfo();
    }

    private static final String PARAM_NAME_ID = "id";
    private static final String PARAM_NAME_PRICE = "price";
    private void loadTrackerInfo() {
        final String trackerInfoData = mFileUtils.readFile(TRACKER_INFO_FILENAME);
        if (trackerInfoData == null) {
            return;
        }
        try {
            final JSONArray trackerArray = new JSONArray(trackerInfoData);
            for (int i=0; i < trackerArray.length(); i++) {
                final JSONObject trackerObj = trackerArray.getJSONObject(i);
                final int trackerId = trackerObj.getInt(PARAM_NAME_ID);
                final int trackPrice = trackerObj.getInt(PARAM_NAME_PRICE);
                mTrackInfoHash.put(trackerId, trackPrice);
                Log.d(TAG, "Load Tracker "+trackerId+", price: "+trackPrice);
            }
        } catch (JSONException e) {
            Log.e(TAG, "Failed to load tracker info, clear data", e);
            mFileUtils.deleteFile(TRACKER_INFO_FILENAME);
        }
    }

    private boolean saveTrackerInfo() {
        try {
            final JSONArray trackerArray = new JSONArray();
            for (final int trackId : mTrackInfoHash.keySet()) {
                final int trackPrice = mTrackInfoHash.get(trackId);
                final JSONObject trackerObj = new JSONObject();
                trackerObj.put(PARAM_NAME_ID, trackId);
                trackerObj.put(PARAM_NAME_PRICE, trackPrice);
                trackerArray.put(trackerObj);
            }
            final String trackerInfoData = trackerArray.toString();
            return mFileUtils.saveFile(TRACKER_INFO_FILENAME, trackerInfoData.getBytes("UTF8"));
        } catch(Exception e) {
            Log.e(TAG, "Failed to save tracker info", e);
            return false;
        }
    }

    public HashMap<Integer, Integer> getTrackInfoHash() {
        return mTrackInfoHash;
    }

    public boolean setTrackerPrice(final int trackerId, final float price) {
        mTrackInfoHash.put(trackerId, Math.round(price*100));
        return saveTrackerInfo();
    }

    public float getTrackerPrice(final int trackerId) {
        if (!mTrackInfoHash.containsKey(trackerId)) {
            return -1;
        }
        return mTrackInfoHash.get(trackerId)/100.0f;
    }

    public boolean removePriceTracker(final Collection<Integer> trackerIds) {
        if (trackerIds.isEmpty()) {
            return true;
        }
        for (int trackerId : trackerIds) {
            Log.d(TAG, "Remove Tracker: "+trackerId);
            mTrackInfoHash.remove(trackerId);
        }
        return saveTrackerInfo();
    }
}
