package com.jchain.affiliate.Tracker;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.jchain.affiliate.Account.AccountManagement;
import com.jchain.affiliate.MainActivity;
import com.jchain.affiliate.Product.ProductDetailInfoActivity;
import com.jchain.affiliate.Product.ProductInfoManager;
import com.jchain.affiliate.R;
import com.jchain.affiliate.System.SystemInfoManager;
import com.jchain.affiliate.UI.Fragments.BaseFragmentWithDrawer;
import com.jchain.affiliate.WebAPI.WebProductAPI;

import java.util.ArrayList;
import java.util.HashMap;

public final class TrackerListFragment extends BaseFragmentWithDrawer {
    private static final String TAG = "TrackerListFragment";

    public static TrackerListFragment newInstance() {
        Bundle args = new Bundle();
        TrackerListFragment fragment = new TrackerListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private ProductInfoManager mProductInfoManager = null;
    private AccountManagement mAccountManager = null;
    private TrackerInfoManager mTrackerManager = null;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mProductInfoManager = ProductInfoManager.getInstance(getContext());
        mAccountManager = AccountManagement.getInstance(getContext());
        mTrackerManager = TrackerInfoManager.getInstance(getContext());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_tracker_list, container, false);
        initView(rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        cancelLoadTrackersData();
        super.onDestroyView();
    }

    private void initView(final View rootView) {
        initActionBar(getString(R.string.ID_TITLE_TRACK_PRODUCTS));
        initDrawer(MainActivity.DRAWER_ITEM_ID_TRACK_PRODUCTS);
        initReloadBar(rootView);
        initTrackersList(rootView);
    }

    private TextView mTextMsgTrackers = null;
    private RecyclerView mRecycleViewTrackers = null;
    private TrackerItemRecyclerAdapter mTrackerItemRecyclerAdapter = null;
    private void initTrackersList(final View rootView) {
        mTextMsgTrackers = rootView.findViewById(R.id.textMsgTrackers);

        mRecycleViewTrackers = rootView.findViewById(R.id.listTrackers);
        mRecycleViewTrackers.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mTrackerItemRecyclerAdapter = new TrackerItemRecyclerAdapter(getContext());
        mTrackerItemRecyclerAdapter.setOnItemClickListener(new TrackerItemRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClicked(int position) {
                if (mTrackersList == null) {
                    return;
                }
                if (position >= 0 && position < mTrackersList.size()) {
                    showTrackerProduct(position);
                }
            }
        });
        if (mAccountManager.hasAccountLogin()) {
            mTrackerItemRecyclerAdapter.setOnItemFuncBtnClickListener(new TrackerItemRecyclerAdapter.OnItemFuncBtnClickListener() {
                @Override
                public void onItemStopTrackClicked(int position) {
                    Log.d(TAG, "onItemStopTrackClicked, position: " + position);
                    stopTrackProduct(position);
                }

                @Override
                public void onItemSetPriceClicked(int position) {
                    Log.d(TAG, "onItemSetPriceClicked, position: " + position);
                    setProductTrackPrice(position);
                }
            });
        }
        mRecycleViewTrackers.setAdapter(mTrackerItemRecyclerAdapter);

        loadTrackersData();
    }

    // Loading tracker UI methods
    private void showLoadingTrackers() {
        mRecycleViewTrackers.setVisibility(View.GONE);
        mTextMsgTrackers.setVisibility(View.VISIBLE);
        mTextMsgTrackers.setText(R.string.ID_MSG_LOADING_DATA);
    }

    private void hideMsgTrackers() {
        mRecycleViewTrackers.setVisibility(View.VISIBLE);
        mTextMsgTrackers.setVisibility(View.GONE);
    }

    private void showNoData() {
        mRecycleViewTrackers.setVisibility(View.GONE);
        mTextMsgTrackers.setVisibility(View.VISIBLE);
        mTextMsgTrackers.setText(R.string.ID_MSG_NO_DATA);
    }

    private void processError(final @WebProductAPI.ProductError int errorCode) {
        int errorMsgResId = R.string.ID_MSG_ERROR_LOAD_TRACKERS_FAILURE;
        switch(errorCode) {
            case WebProductAPI.PRODUCT_ERROR_NONE:
                return;
            case WebProductAPI.PRODUCT_ERROR_NETWORK:
                errorMsgResId = R.string.ID_MSG_ERROR_NETWORK_FAILURE;
                break;
            case WebProductAPI.PRODUCT_ERROR_PARSE:
                errorMsgResId = R.string.ID_MSG_ERROR_PARSE_FAILURE;
                break;
            case WebProductAPI.PRODUCT_ERROR_GENERAL:
            default:
                break;
        }
        showInvalidError(getString(errorMsgResId));
    }

    //  Error process methods
    private void showInvalidError(final String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
        showReloadBar();
    }

    private View mReloadBtn = null;
    private void initReloadBar(final View rootView) {
        mReloadBtn = rootView.findViewById(R.id.btnReloadData);
        mReloadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideReloadBar();
                loadTrackersData();
            }
        });
        hideReloadBar();
    }

    private void showReloadBar() {
        mReloadBtn.setVisibility(View.VISIBLE);
    }

    private void hideReloadBar() {
        mReloadBtn.setVisibility(View.GONE);
    }

    // Load Trackers method
    private WebProductAPI.TrackDataListener mTrackDataListener = new WebProductAPI.TrackDataListener() {
        @Override
        public void onGotTracksList(int errorCode, ArrayList<PriceTrackerItemInfo> tracksList) {
            if (tracksList == null || tracksList.isEmpty()) {
                showNoData();
            } else {
                updateTrackerList(tracksList);
            }
            if (errorCode == WebProductAPI.PRODUCT_ERROR_NONE) {
                // It's a success result, do clear up.
                clearupTrackerInfo(tracksList);
            } else {
                processError(errorCode);
            }
        }
    };

    private void loadTrackersData() {
        mProductInfoManager.queryTracks(mTrackDataListener);
        showLoadingTrackers();
    }

    private void cancelLoadTrackersData() {
        mProductInfoManager.cancelQueryTracks();
    }

    private ArrayList<PriceTrackerItemInfo> mTrackersList = null;
    private void updateTrackerList(final ArrayList<PriceTrackerItemInfo> list) {
        hideMsgTrackers();
        mTrackersList = list;
        mTrackerItemRecyclerAdapter.updateTrackerList(list);
    }

    private void removeTrackerItem(final int index) {
        final FragmentActivity parent = getActivity();
        if (parent == null) {
            Log.w(TAG, "Can't remove Tracker item as activity has dismissed");
            return;
        }
        parent.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mTrackersList.remove(index);
                if (mTrackersList.isEmpty()) {
                    showNoData();
                } else {
                    mTrackerItemRecyclerAdapter.updateTrackerList(mTrackersList);
                }
            }
        });
    }

    private void showSetPriceDialog(final PriceTrackerItemInfo tracker) {
        final View dialogView = getLayoutInflater().inflate(R.layout.dialog_set_price, null);
        final EditText editPrice = dialogView.findViewById(R.id.editPrice);
        final float initPrice = tracker.notifyPrice > 0 ? tracker.notifyPrice : tracker.origPrice;
        editPrice.setText(String.valueOf(initPrice));
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext())
                .setTitle(R.string.ID_TITLE_SET_PRICE)
                .setView(dialogView)
                .setPositiveButton(R.string.ID_CAPTION_OK, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        actualSetProductPrice(tracker, editPrice.getEditableText().toString());
                    }
                });

        builder.create().show();
    }

    private void showSetPriceFailure() {
        Toast.makeText(getContext(), R.string.ID_MSG_ERROR_SET_PRICE, Toast.LENGTH_LONG).show();
    }

    // Action methods
    private void showTrackerProduct(final int index) {
        SystemInfoManager.getInstance().checkInternetAvailable(getContext(), new SystemInfoManager.InternetAvailableListener() {
            @Override
            public void onInternetAvailableResult(boolean available) {
                if (!available) {
                    showInvalidError(getString(R.string.ID_MSG_ERROR_NETWORK_FAILURE));
                    return;
                }
                final PriceTrackerItemInfo item = mTrackersList.get(index);
                ProductDetailInfoActivity.showTracker(getContext(), item);
            }
        });
    }

    private void stopTrackProduct(final int index) {
        if (index < 0 || index >= mTrackersList.size()) {
            Log.w(TAG, "Can't stop a tracker out of range of the list");
            return;
        }
        final PriceTrackerItemInfo tracker = mTrackersList.get(index);
        mProductInfoManager.stopTrack(tracker, new WebProductAPI.StopTrackListener() {
            @Override
            public void onResult(int errorCode) {
                if (errorCode != WebProductAPI.PRODUCT_ERROR_NONE) {
                    Log.d(TAG, "Stop tracker failed, error code: " + errorCode);
                } else {
                    removeTrackerItem(index);
                }
            }
        });
    }

    private void setProductTrackPrice(final int index) {
        if (index < 0 || index >= mTrackersList.size()) {
            Log.w(TAG, "Can't set price of tracker which is out of range of the list");
            return;
        }
        final PriceTrackerItemInfo tracker = mTrackersList.get(index);
        showSetPriceDialog(tracker);
    }

    private void actualSetProductPrice(final PriceTrackerItemInfo tracker, final String priceData) {
        try {
            float price = Float.parseFloat(priceData);
            if (!mTrackerManager.setTrackerPrice(tracker.id, price)) {
                showSetPriceFailure();
                return;
            }
            Log.d(TAG, "actualSetProductPrice: "+price+" for "+tracker.id);
            tracker.notifyPrice = price;
            updateTrackerList(mTrackersList);
        } catch (NumberFormatException e) {
            showSetPriceFailure();
        }
    }

    private void clearupTrackerInfo(final ArrayList<PriceTrackerItemInfo> list) {
        final HashMap<Integer, Integer> trackerInfoHash = mTrackerManager.getTrackInfoHash();
        final ArrayList<Integer> removedTrackerIds = new ArrayList<Integer>(trackerInfoHash.keySet());
        if (list != null) {
            for (PriceTrackerItemInfo tracker : list) {
                final int foundIndex = removedTrackerIds.indexOf(tracker.id);
                if (foundIndex >= 0) {
                    removedTrackerIds.remove(foundIndex);
                }
            }
        }
        if (!removedTrackerIds.isEmpty()) {
            mTrackerManager.removePriceTracker(removedTrackerIds);
        }
    }
}
