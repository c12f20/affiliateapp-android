package com.jchain.affiliate;

import android.content.Intent;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityOptionsCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.os.Build;
import android.widget.Toast;

import com.jchain.affiliate.Account.AccountInfo;
import com.jchain.affiliate.Account.AccountManagement;
import com.jchain.affiliate.Authorization.LoginActivity;
import com.jchain.affiliate.Tracker.TrackerCheckService;
import com.jchain.affiliate.WebAPI.WebAuthAPI;

public final class SplashActivity extends AppCompatActivity {
    private static final String TAG = "SplashActivity";

    private static final int REQUEST_CODE_LOGIN = 1;

    private Handler mHandler = null;
    private View mRootView = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRootView = LayoutInflater.from(this).inflate(R.layout.activity_splash, null);
        setContentView(mRootView);

        mHandler = new Handler();

        verifySavedAccessToken();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_CODE_LOGIN) {
            finish();
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    // Verify Access Token methods
    private void verifySavedAccessToken() {
        final AccountManagement accountManager = AccountManagement.getInstance(getBaseContext());
        final String savedToken = accountManager.loadCurAccountToken();
        if (savedToken.isEmpty()) {
            Log.d(TAG, "No saved access token found, skip verification");
            enterLoginActivity();
            return;
        }

        final WebAuthAPI webAuthAPI = WebAuthAPI.getInstance(getBaseContext());
        webAuthAPI.verifyAccount(savedToken, new WebAuthAPI.VerifyAccountListener() {
            @Override
            public void onVerifyResult(final int errorCode, final AccountInfo userInfo) {
                if (errorCode == WebAuthAPI.AUTH_ERROR_NONE && userInfo != null) { // Login successfully
                    accountManager.setCurAccountInfo(userInfo);
                    enterMainActivity();
                } else {
                    // Login failed, show error messages
                    processLoginFailure(errorCode);
                    enterLoginActivity();
                }
            }
        });
    }

    private void enterLoginActivity() {
        Animation animation = AnimationUtils.loadAnimation(getBaseContext(), R.anim.alpha);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        final Intent intent = new Intent(getBaseContext(), LoginActivity.class);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            final View firstSharedView = findViewById(R.id.firstSharedView);
                            final Bundle transitionBundle = ActivityOptionsCompat.makeSceneTransitionAnimation(SplashActivity.this,
                                    firstSharedView, getString(R.string.transition_splash_icon)).toBundle();
                            startActivityForResult(intent, REQUEST_CODE_LOGIN, transitionBundle);
                        } else {
                            startActivity(intent);
                            finish();
                        }
                    }
                }, 1000);
            }
        });
        mRootView.startAnimation(animation);
    }

    private void enterMainActivity() {
        final Intent intent = new Intent(getBaseContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);

        // always start Check Tracker service
        final Intent svcIntent = new Intent(getBaseContext(), TrackerCheckService.class);
        startService(svcIntent);

        finish();
    }

    private void processLoginFailure(final @WebAuthAPI.AuthError int errorCode) {
        int errorMsgResId = R.string.ID_MSG_ERROR_LOGIN_FAILURE;
        switch (errorCode) {
            case WebAuthAPI.AUTH_ERROR_NETWORK:
                errorMsgResId = R.string.ID_MSG_ERROR_NETWORK_FAILURE;
                break;
            case WebAuthAPI.AUTH_ERROR_PARSE:
                errorMsgResId = R.string.ID_MSG_ERROR_PARSE_FAILURE;
                break;
            case WebAuthAPI.AUTH_ERROR_BANNED:
                errorMsgResId = R.string.ID_MSG_ERROR_BANNED;
                break;
            case WebAuthAPI.AUTH_ERROR_NO_ACTIVATAION:
                errorMsgResId = R.string.ID_MSG_ERROR_NO_ACTIVATION;
                break;
            case WebAuthAPI.AUTH_ERROR_GENERAL:
            default:
                break;
        }
        showInvalidError(getString(errorMsgResId));
    }

    private void showInvalidError(final String message) {
        Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT).show();
    }
}
