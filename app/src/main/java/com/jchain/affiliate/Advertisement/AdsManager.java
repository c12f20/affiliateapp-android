package com.jchain.affiliate.Advertisement;

import android.content.Context;
import androidx.annotation.IntDef;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.AdSettings;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.facebook.ads.AudienceNetworkAds;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.jchain.affiliate.R;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class AdsManager {
    private static final String TAG = AdsManager.class.getSimpleName();

    private static AdsManager sInstance = null;
    public static AdsManager getInstance() {
        if (sInstance == null) {
            sInstance = new AdsManager();
        }
        return sInstance;
    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({ADSTYPE_NONE, ADSTYPE_GOOGLE_ADMOB, ADSTYPE_FACEBOOK_AUDIENCE})
    public @interface AdsType {}
    public static final int ADSTYPE_NONE = 0;
    public static final int ADSTYPE_GOOGLE_ADMOB = 1;
    public static final int ADSTYPE_FACEBOOK_AUDIENCE = 2;

    public void init(final Context context) {
        //initGoogleAds(context);
        initFacebookAds(context);
    }

    // Google Admob
    /*private void initGoogleAds(final Context context) {
        // Google Admob init
        MobileAds.initialize(context, context.getString(R.string.google_admob_app_id));
    }

    public PublisherAdView createGoogleAdView(final Context context) {
        return createGoogleAdView(context, com.google.android.gms.ads.AdSize.SMART_BANNER);
    }

    public PublisherAdView createGoogleRectAdView(final Context context) {
        return createGoogleAdView(context, com.google.android.gms.ads.AdSize.MEDIUM_RECTANGLE);
    }

    public PublisherAdView createGoogleAdView(final Context context, com.google.android.gms.ads.AdSize adSize) {
        final PublisherAdView adView = new PublisherAdView(context);
        adView.setAdSizes(adSize);
        adView.setAdUnitId(context.getString(R.string.google_admob_ad_unit_id));

        final PublisherAdRequest adRequest = createGoogleAdRequest();
        adView.loadAd(adRequest);
        return adView;
    }

    private PublisherAdRequest createGoogleAdRequest() {
        return new PublisherAdRequest.Builder()
                .addTestDevice("A599605598558222CA77F78927770988")
                .addTestDevice("5E4527B3BEDE3C14B0E4E6EBE968BEE2")
                .build();
    }*/

    // Facebook AudienceNetwork
    private void initFacebookAds(final Context context) {
        // Facebook AudienceNetwork init
        if (!AudienceNetworkAds.isInitialized(context)) {
            AdSettings.turnOnSDKDebugger(context);
            AdSettings.setIntegrationErrorMode(AdSettings.IntegrationErrorMode.INTEGRATION_ERROR_CRASH_DEBUG_MODE);
            AudienceNetworkAds.buildInitSettings(context)
                    .withInitListener(new AudienceNetworkAds.InitListener() {
                        @Override
                        public void onInitialized(AudienceNetworkAds.InitResult initResult) {
                            Log.d(TAG, initResult.getMessage());
                        }
                    })
                    .initialize();
            AudienceNetworkAds.isInAdsProcess(context);
            final Thread addTestThrd = new Thread() {
                @Override
                public void run() {
                    final String testId = getGoogleAdsDeviceId(context);
                    if (!TextUtils.isEmpty(testId)) {
                        Log.d(TAG, "Google Ads Device Id: "+testId);
                        AdSettings.addTestDevice(testId);
                    }
                }
            };
            addTestThrd.start();
        }
    }

    private String getGoogleAdsDeviceId(final Context context) {
        try {
            final AdvertisingIdClient.Info info = AdvertisingIdClient.getAdvertisingIdInfo(context);
            return info.getId();
        } catch (Exception e) {
            Log.e(TAG, "Failed to get Google Ads Id info, exception: ", e);
            return null;
        }
    }

    public View createFacebookAdView(final Context context) {
        return createFacebookAdView(context,
                context.getString(R.string.facebook_ads_bar_placement_id),
                AdSize.BANNER_HEIGHT_50);
    }

    public View createFacebookRectAdView(final Context context) {
        return createFacebookAdView(context,
                context.getString(R.string.facebook_ads_rect_placement_id),
                AdSize.RECTANGLE_HEIGHT_250);
    }

    private View createFacebookAdView(final Context context,
                                                        final String adsPlacementId,
                                                        final com.facebook.ads.AdSize adSize) {
        final com.facebook.ads.AdView adView = new com.facebook.ads.AdView(context,
                adsPlacementId, adSize);
        AdView.AdViewLoadConfigBuilder adViewConfigBuilder = adView.buildLoadAdConfig();
        adViewConfigBuilder.withAdListener(new AdListener() {
            @Override
            public void onError(Ad ad, AdError adError) {
                Log.e(TAG, "Load FB Ads error: "+adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                Log.d(TAG, "onAdLoaded successfully");
            }

            @Override
            public void onAdClicked(Ad ad) {
                Log.d(TAG, "onAdClicked");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                Log.d(TAG, "onLoggingImpression");
            }
        });
        adView.loadAd(adViewConfigBuilder.build());
        return adView;
    }
}
