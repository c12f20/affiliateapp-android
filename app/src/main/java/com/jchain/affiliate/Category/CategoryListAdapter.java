package com.jchain.affiliate.Category;

import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.jchain.affiliate.R;
import com.jchain.affiliate.UI.Widgets.OnlineImageViewUtils;

import java.util.ArrayList;

public final class CategoryListAdapter extends BaseExpandableListAdapter {

    private final Context mContext;
    public CategoryListAdapter(final Context context) {
        mContext = context;
    }

    private ArrayList<CategoryItemInfo> mCategoriesList = new ArrayList<CategoryItemInfo>();
    public void updateCategoryList(final ArrayList<CategoryItemInfo> list) {
        if (list == null) {
            return;
        }
        mCategoriesList = list;
        notifyDataSetChanged();
    }

    @Override
    public int getGroupCount() {
        return mCategoriesList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mCategoriesList.get(groupPosition).subCategories.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mCategoriesList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mCategoriesList.get(groupPosition).subCategories.get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        View view;
        GroupHolder holder;
        if (convertView != null) {
            view = convertView;
            holder = (GroupHolder) convertView.getTag();
        } else {
            view = LayoutInflater.from(mContext).inflate(R.layout.view_category_item, null);
            holder = new GroupHolder(mContext, view);
            view.setTag(holder);
        }
        final CategoryItemInfo groupItem = (CategoryItemInfo) getGroup(groupPosition);
        holder.updateItem(mContext, groupItem, isExpanded);
        return view;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        View view;
        ChildHolder holder;
        if (convertView != null) {
            view = convertView;
            holder = (ChildHolder) convertView.getTag();
        } else {
            view = LayoutInflater.from(mContext).inflate(R.layout.view_subcategory_item, null);
            holder = new ChildHolder(mContext, view);
            view.setTag(holder);
        }
        final CategoryItemInfo childItem = (CategoryItemInfo) getChild(groupPosition, childPosition);
        holder.updateItem(mContext, childItem);
        return view;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    private static class GroupHolder {
        private final ImageView icon;
        private final TextView textName;
        private final TextView textDescription;
        private final ImageView iconIndicator;

        private final OnlineImageViewUtils mOnlineImageViewUtils;

        private GroupHolder(final Context context, @NonNull View itemView) {
            icon = itemView.findViewById(R.id.icon);
            textName = itemView.findViewById(R.id.textViewName);
            textDescription = itemView.findViewById(R.id.textViewDescription);
            iconIndicator = itemView.findViewById(R.id.iconIndicator);

            mOnlineImageViewUtils = new OnlineImageViewUtils(context);
        }

        private void updateItem(final Context context, final CategoryItemInfo groupItem, final boolean isExpanded) {
            if (groupItem.iconURL != null) {
                mOnlineImageViewUtils.loadImage(icon, groupItem.iconURL);
            } else {
                icon.setImageResource(groupItem.getDefaultIconResId(context));
            }
            textName.setText(groupItem.name);
            if (groupItem.description != null && !groupItem.description.isEmpty()) {
                textDescription.setVisibility(View.VISIBLE);
                textDescription.setText(groupItem.description);
            } else {
                textDescription.setVisibility(View.GONE);
            }

            if (groupItem.subCategories.isEmpty()) {
                iconIndicator.setVisibility(View.GONE);
            } else {
                iconIndicator.setImageResource(isExpanded ? R.drawable.ic_arrow_down : R.drawable.ic_arrow_right);
                iconIndicator.setVisibility(View.VISIBLE);
            }
        }
    }

    private static class ChildHolder {
        //private final ImageView icon;
        private final TextView textName;
        private final TextView textDescription;

        //private final OnlineImageViewUtils mOnlineImageViewUtils;

        private ChildHolder(final Context context, @NonNull View itemView) {
            //icon = itemView.findViewById(R.id.icon);
            textName = itemView.findViewById(R.id.textViewName);
            textDescription = itemView.findViewById(R.id.textViewDescription);

            //mOnlineImageViewUtils = new OnlineImageViewUtils(context);
        }

        private void updateItem(final Context context, final CategoryItemInfo childItem) {
            /*if (childItem.iconURL != null) {
                mOnlineImageViewUtils.loadImage(icon, childItem.iconURL);
            } else {
                icon.setImageResource(childItem.getDefaultIconResId(context));
            }*/
            textName.setText(childItem.name);
            if (childItem.description != null && !childItem.description.isEmpty()) {
                textDescription.setVisibility(View.VISIBLE);
                textDescription.setText(childItem.description);
            } else {
                textDescription.setVisibility(View.GONE);
            }
        }
    }

}
