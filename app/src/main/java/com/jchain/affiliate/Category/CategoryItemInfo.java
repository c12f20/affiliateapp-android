package com.jchain.affiliate.Category;

import android.content.Context;
import android.content.res.Resources;

import com.jchain.affiliate.R;

import java.util.ArrayList;
import java.util.Collection;

public final class CategoryItemInfo {
    public final String id;
    public String iconURL;
    public String name;
    public String description;
    public ArrayList<CategoryItemInfo> subCategories = new ArrayList<CategoryItemInfo>();

    public CategoryItemInfo(final String id,
                            final String iconURL,
                            final String name,
                            final String description,
                            final Collection<CategoryItemInfo> subCategories) {
        this.id = id;
        this.iconURL = iconURL;
        this.name = name;
        this.description = description;
        if (subCategories != null) {
            this.subCategories.addAll(subCategories);
        }
    }

    public int getDefaultIconResId(final Context context) {
        final String resName = "ic_category_"+id;
        final Resources res = context.getResources();
        final int resId = res.getIdentifier(resName, "drawable", context.getPackageName());
        return resId <= 0 ? R.drawable.ic_category_default : resId;
    }

    public String getIconId() {
        return (this.id == null || this.id.isEmpty()) ? null : "category_"+this.id;
    }
}
