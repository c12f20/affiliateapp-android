package com.jchain.affiliate.Category;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jchain.affiliate.R;

import java.util.ArrayList;

public final class CategoryItemRecyclerAdapter extends RecyclerView.Adapter<CategoryItemRecyclerAdapter.ViewHolder> {

    private final Context mContext;
    public CategoryItemRecyclerAdapter(final Context context) {
        mContext = context;
    }

    public interface OnItemClickListener {
        void onItemClicked(final int position, final CategoryItemInfo subItem);
    }

    private OnItemClickListener mOnItemClickListener = null;
    public void setOnItemClickListener(final OnItemClickListener listener) {
        mOnItemClickListener = listener;
    }

    private void notifyItemClicked(final int position, final CategoryItemInfo subItem) {
        if (mOnItemClickListener != null) {
            mOnItemClickListener.onItemClicked(position, subItem);
        }
    }

    private CategoryItemInfo mCategoryItem = null;
    private int mSelectedSubItemIndex = -1;
    public void setCaegoryItem(final CategoryItemInfo item, final int selectedSubItemIndex) {
        if (item == null) {
            return;
        }
        mCategoryItem = item;
        mSelectedSubItemIndex = selectedSubItemIndex;
        notifyDataSetChanged();
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {
        private final View mRootView;
        private final TextView mTextName;
        //private final ImageView mIcon;
        //private final OnlineImageViewUtils mOnlineImageViewUtils;

        public ViewHolder(final Context context, @NonNull View itemView) {
            super(itemView);
            mRootView = itemView;
            mTextName = itemView.findViewById(R.id.textName);
            //mIcon = itemView.findViewById(R.id.icon);
            //mOnlineImageViewUtils = new OnlineImageViewUtils(context);
        }

        private View getItemView() {
            return mRootView;
        }

        private void updateItem(final Context context, final CategoryItemInfo itemInfo, final boolean selected) {
            mTextName.setText(itemInfo.name);
            mTextName.setActivated(selected);
            /*if (itemInfo.iconURL != null) {
                mOnlineImageViewUtils.loadImage(mIcon, itemInfo.iconURL);
            } else {
                mIcon.setImageResource(itemInfo.getDefaultIconResId(context));
            }*/
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View rootView = LayoutInflater.from(mContext).inflate(R.layout.view_subcategory_recycler_item, parent, false);
        return new ViewHolder(mContext, rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        if (mCategoryItem == null) {
            return;
        }
        final ArrayList<CategoryItemInfo> subCategories = mCategoryItem.subCategories;
        if (position < 0 || position >= subCategories.size()) {
            return;
        }
        final CategoryItemInfo item = subCategories.get(position);
        holder.updateItem(mContext, item, position == mSelectedSubItemIndex);
        holder.getItemView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notifyItemClicked(holder.getAdapterPosition(), item);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mCategoryItem == null) {
            return 0;
        }
        return mCategoryItem.subCategories.size();
    }
}
