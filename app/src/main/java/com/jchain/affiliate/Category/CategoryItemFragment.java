package com.jchain.affiliate.Category;

import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.jchain.affiliate.Account.AccountManagement;
import com.jchain.affiliate.MainActivity;
import com.jchain.affiliate.Tracker.PriceTrackerItemInfo;
import com.jchain.affiliate.Product.ProductDetailInfoActivity;
import com.jchain.affiliate.Product.ProductInfoManager;
import com.jchain.affiliate.Product.ProductItemInfo;
import com.jchain.affiliate.Product.ProductItemRecyclerAdapter;
import com.jchain.affiliate.R;
import com.jchain.affiliate.System.SystemInfoManager;
import com.jchain.affiliate.UI.Fragments.BaseFragmentWithDrawer;
import com.jchain.affiliate.WebAPI.WebProductAPI;

import java.util.ArrayList;
import java.util.Locale;

public final class CategoryItemFragment extends BaseFragmentWithDrawer {
    private static final String TAG = "CategoryItemFragment";

    private static final String ARG_PARAM_CATEGORY_IDX = "param.CategoryIdx";
    private static final String ARG_PARAM_SUBCATEGORY_IDX = "param.SubCategoryIdx";

    public static CategoryItemFragment newInstance(final int categoryIndex, final int subCategoryIndex) {
        Bundle args = new Bundle();

        args.putInt(ARG_PARAM_CATEGORY_IDX, categoryIndex);
        args.putInt(ARG_PARAM_SUBCATEGORY_IDX, subCategoryIndex);
        CategoryItemFragment fragment = new CategoryItemFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private CategoryItemInfo mCategoryInfo = null;
    private int mSubCategoryIndex = -1;

    private CategoryItemInfo getCurCategory() {
        if (mSubCategoryIndex < 0) {
            return mCategoryInfo;
        }
        return mCategoryInfo.subCategories.get(mSubCategoryIndex);
    }

    private AccountManagement mAccountMng = null;
    private ProductInfoManager mProductInfoManager = null;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mProductInfoManager = ProductInfoManager.getInstance(getContext());
        mAccountMng = AccountManagement.getInstance(getContext());
        if (getArguments() != null) {
            final ArrayList<CategoryItemInfo> categoryList = mProductInfoManager.getCategoryList();
            final int categoryIndex = getArguments().getInt(ARG_PARAM_CATEGORY_IDX);
            mCategoryInfo = categoryList.get(categoryIndex);
            mSubCategoryIndex = getArguments().getInt(ARG_PARAM_SUBCATEGORY_IDX);
            if (mSubCategoryIndex < 0 && !mCategoryInfo.subCategories.isEmpty()) {
                mSubCategoryIndex = 0;
            }
            Log.d(TAG, "CategoryItemFragment, category: "+mCategoryInfo.name+" subcategory index: "+mSubCategoryIndex);
        }
    }

    private Handler mHandler = null;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mHandler = new Handler();

        final View rootView = inflater.inflate(R.layout.fragment_category_item, container, false);
        initView(rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        Log.d(TAG, "onDestroyView");
        cancelLoadProductsData();
        super.onDestroyView();
    }

    private void initView(final View rootView) {
        initActionBar(mCategoryInfo.name);
        initDrawer(MainActivity.DRAWER_ITEM_ID_CATEGORIES);
        initReloadBar(rootView);
        initSubCategoriesList(rootView);
        initProductsTitleBar(rootView);
        initProductsList(rootView);
    }

    private void initSubCategoriesList(final View rootView) {
        final RecyclerView listSubCategories = rootView.findViewById(R.id.listSubCategories);
        if (mSubCategoryIndex < 0) { // No subcategory in this category, hide this list
            listSubCategories.setVisibility(View.GONE);
            return;
        }
        listSubCategories.setVisibility(View.VISIBLE);
        listSubCategories.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        final CategoryItemRecyclerAdapter adapter = new CategoryItemRecyclerAdapter(getContext());
        adapter.setCaegoryItem(mCategoryInfo, mSubCategoryIndex);
        adapter.setOnItemClickListener(new CategoryItemRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClicked(int position, CategoryItemInfo subItem) {
                if (mSubCategoryIndex == position) {
                    Log.d(TAG, "Ignore same subcategory item clicking");
                    return;
                }
                mSubCategoryIndex = position;
                adapter.setCaegoryItem(mCategoryInfo, mSubCategoryIndex);
                // reload products list of new sub
                resetProductsList();
                loadProductsData(getCurCategory());
            }
        });
        listSubCategories.setAdapter(adapter);
        listSubCategories.scrollToPosition(mSubCategoryIndex);
    }

    private TextView mTextProductsCount = null;
    private void initProductsTitleBar(final View rootView) {
        mTextProductsCount = rootView.findViewById(R.id.textProductsCountMsg);
        final View btnHotFilter = rootView.findViewById(R.id.btnHotFilter);
        btnHotFilter.setVisibility(View.GONE); // TODO: temporarily remove Hot Filter Button

        final View btnFilter = rootView.findViewById(R.id.btnFilter);
        btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFilterPopupMenu(v);
            }
        });
    }

    private int mSelectedSortByMenuId = R.id.menu_sortby_none;
    private void showFilterPopupMenu(final View view) {
        PopupMenu menu = new PopupMenu(getContext(), view);
        menu.inflate(R.menu.menu_popup_filter);
        final MenuItem menuItem = menu.getMenu().findItem(mSelectedSortByMenuId);
        menuItem.setChecked(true);
        menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                final int menuId = item.getItemId();
                if (mSelectedSortByMenuId != menuId) {
                    mSelectedSortByMenuId = menuId;
                    sortProductsList(getCurCategory());
                }
                return false;
            }
        });
        menu.show();
    }

    private @WebProductAPI.SortBy int getCurSortByType() {
        int sortByType = WebProductAPI.SORTBY_PRODUCT_ID;
        switch(mSelectedSortByMenuId) {
            case R.id.menu_sortby_price_lh:
            case R.id.menu_sortby_price_hl:
                sortByType = WebProductAPI.SORTBY_SALE_PRICE;
                break;
            case R.id.menu_sortby_discount:
                sortByType = WebProductAPI.SORTBY_DISCOUNT;
                break;
            case R.id.menu_sortby_none:
            default:
                break;
        }
        return sortByType;
    }

    private boolean getCurNeedSortAsc() {
        boolean needSortAsc = true;
        switch(mSelectedSortByMenuId) {
            case R.id.menu_sortby_price_hl:
            case R.id.menu_sortby_discount:
                needSortAsc = false;
                break;
            case R.id.menu_sortby_none:
            case R.id.menu_sortby_price_lh:
            default:
                break;
        }
        return needSortAsc;
    }

    private View mAreaCategoryData = null;
    private TextView mTextMsgCategory = null;
    private RecyclerView mRecycleViewProducts = null;
    private ProductItemRecyclerAdapter mProductItemRecyclerAdapter = null;
    private void initProductsList(final View rootView) {
        mAreaCategoryData = rootView.findViewById(R.id.areaCategoryData);
        mTextMsgCategory = rootView.findViewById(R.id.textMsgCategories);

        mRecycleViewProducts = rootView.findViewById(R.id.listProducts);
        mRecycleViewProducts.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mProductItemRecyclerAdapter = new ProductItemRecyclerAdapter(getContext(), mRecycleViewProducts);
        mProductItemRecyclerAdapter.setOnItemClickListener(new ProductItemRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClicked(int position) {
                if (mProductsList == null) {
                    return;
                }
                if (position >= 0 && position < mProductsList.size()) {
                    showProduct(position);
                }
            }
        });
        if (mAccountMng.hasAccountLogin()) {
            mProductItemRecyclerAdapter.setOnItemTrackClickListener(new ProductItemRecyclerAdapter.OnItemTrackClickListener() {
                @Override
                public void onItemTrackClicked(int position) {
                    Log.d(TAG, "onItemTrackClicked, position: " + position);
                    if (position >= 0 && position < mProductsList.size()) {
                        trackProduct(position);
                    }
                }
            });
        }
        mProductItemRecyclerAdapter.setLoadMoreListener(new ProductItemRecyclerAdapter.LoadMoreListener() {
            @Override
            public void onNeedLoadMore() {
                if (mProductsList.size() < mProductListCount) {
                    showLoadingMoreProducts();
                    mHandler.postDelayed(new Runnable(){
                        @Override
                        public void run() {
                            loadProductsListWithPage(getCurCategory(),mProductPageIdx+1);
                        }
                    }, 1000);

                }
            }
        });
        mRecycleViewProducts.setAdapter(mProductItemRecyclerAdapter);

        loadProductsData(getCurCategory());
    }

    private void showLoadingProducts() {
        mAreaCategoryData.setVisibility(View.GONE);
        mTextMsgCategory.setVisibility(View.VISIBLE);
        mTextMsgCategory.setText(R.string.ID_MSG_LOADING_DATA);
    }

    private void hideMsgProducts() {
        mAreaCategoryData.setVisibility(View.VISIBLE);
        mTextMsgCategory.setVisibility(View.GONE);
    }

    private void showNoData() {
        mAreaCategoryData.setVisibility(View.GONE);
        mTextMsgCategory.setVisibility(View.VISIBLE);
        mTextMsgCategory.setText(R.string.ID_MSG_NO_DATA);
    }

    private void showLoadingMoreProducts() {
        mProductItemRecyclerAdapter.showLoadingProductList();
    }

    private void hideLoadingMoreProducts() {
        mProductItemRecyclerAdapter.hideLoadingProductList();
        mProductItemRecyclerAdapter.setLoaded();
    }

    private void processProductError(final @WebProductAPI.ProductError int errorCode) {
        int errorMsgResId = R.string.ID_MSG_ERROR_LOAD_PRODUCTS_FAILURE;
        switch(errorCode) {
            case WebProductAPI.PRODUCT_ERROR_NONE:
                return;
            case WebProductAPI.PRODUCT_ERROR_NETWORK:
                errorMsgResId = R.string.ID_MSG_ERROR_NETWORK_FAILURE;
                break;
            case WebProductAPI.PRODUCT_ERROR_PARSE:
                errorMsgResId = R.string.ID_MSG_ERROR_PARSE_FAILURE;
                break;
            case WebProductAPI.PRODUCT_ERROR_GENERAL:
            default:
                break;
        }
        showInvalidError(getString(errorMsgResId));
    }

    // Load Products methods
    private WebProductAPI.ProductDataListener mProductDataListener = new WebProductAPI.ProductDataListener() {

        @Override
        public void onGotProductsList(int errorCode, int totalCount, int totalPagesCount, ArrayList<ProductItemInfo> list) {
            hideLoadingMoreProducts();

            if (errorCode == WebProductAPI.PRODUCT_ERROR_NONE && list != null) {
                mProductInfoManager.appendProductList(list);
                updateProductsCount(totalCount);
                updateProductList(mProductInfoManager.getProductList());
                return;
            }
            final ArrayList<ProductItemInfo> latestProductList = mProductInfoManager.getProductList();
            if (latestProductList == null || latestProductList.isEmpty()) {
                showNoData();
            }
            processProductError(errorCode);
        }
    };

    private void loadProductsData(final CategoryItemInfo category) {
        mProductListCount = -1;
        mProductPageIdx = -1;
        loadProductsList();
        showLoadingProducts();
    }

    private void sortProductsList(final CategoryItemInfo category) {
        resetProductsList();
        mProductPageIdx = -1;
        loadProductsListWithPage(category, 0);
        showLoadingProducts();
    }

    private void cancelLoadProductsData() {
        mProductInfoManager.cancelLoadProductList();
    }

    private void loadProductsList() {
        final ArrayList<ProductItemInfo> oldList = mProductInfoManager.getProductList();
        if (!oldList.isEmpty()) {
            updateProductList(oldList);
            final int curProductIdx = mProductInfoManager.getCurProductIndex();
            mProductPageIdx = mProductInfoManager.getCurProductPageIndex();
            mRecycleViewProducts.scrollToPosition(mProductItemRecyclerAdapter.getDisplayIdx(curProductIdx));
        } else {
            loadProductsListWithPage(getCurCategory(),0);
            mRecycleViewProducts.scrollToPosition(0);
        }
    }

    private void resetProductsList() {
        mProductInfoManager.resetProductList();
    }

    private int mProductPageIdx = -1;
    private void loadProductsListWithPage(final CategoryItemInfo category, final int pageIdx) {
        mProductPageIdx = pageIdx;
        mProductInfoManager.loadProductListByCategory(category, getCurSortByType(), getCurNeedSortAsc(), pageIdx, mProductDataListener);
    }

    private int mProductListCount = -1;
    private void updateProductsCount(final int count) {
        if (mProductListCount < 0) {
            mProductListCount = count;
            final String countMsg = String.format(Locale.US, getString(R.string.ID_MSG_PRODUCTS_COUNT_TEMPLATE), count);
            mTextProductsCount.setText(countMsg);
        }
    }

    private ArrayList<ProductItemInfo> mProductsList = null;
    private void updateProductList(final ArrayList<ProductItemInfo> list) {
        hideMsgProducts();
        mProductsList = list;
        mProductItemRecyclerAdapter.updateProductList(list);
    }

    // Error process methods
    private void showInvalidError(final String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
        showReloadBar();
    }

    private View mReloadBtn = null;
    private void initReloadBar(final View rootView) {
        mReloadBtn = rootView.findViewById(R.id.btnReloadData);
        mReloadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideReloadBar();
                loadProductsData(getCurCategory());
            }
        });
        hideReloadBar();
    }

    private void showReloadBar() {
        mReloadBtn.setVisibility(View.VISIBLE);
    }

    private void hideReloadBar() {
        mReloadBtn.setVisibility(View.GONE);
    }

    // Action methods
    private void showProduct(final int index) {
        SystemInfoManager.getInstance().checkInternetAvailable(getContext(), new SystemInfoManager.InternetAvailableListener() {
            @Override
            public void onInternetAvailableResult(boolean available) {
                if (!available) {
                    showInvalidError(getString(R.string.ID_MSG_ERROR_NETWORK_FAILURE));
                    return;
                }
                mProductInfoManager.setCurProductIndex(index);
                final ProductItemInfo item = mProductsList.get(index);
                ProductDetailInfoActivity.showProduct(getContext(), item);
            }
        });
    }

    private void trackProduct(final int index) {
        final ProductItemInfo item = mProductsList.get(index);
        mProductInfoManager.addTrack(item, new WebProductAPI.AddToTrackListener() {
            @Override
            public void onResult(int errorCode, PriceTrackerItemInfo trackItem) {
                Log.d(TAG, "AddTrack result: "+errorCode);
                final String resultMsg;
                if (errorCode == WebProductAPI.PRODUCT_ERROR_NONE) {
                    resultMsg = getString(R.string.ID_MSG_SUCCESS_ADD_TRACK);
                } else {
                    resultMsg = getString(R.string.ID_MSG_ERROR_ADD_TRACK);
                }
                Toast.makeText(getContext(), resultMsg, Toast.LENGTH_LONG).show();
            }
        });
    }
}
