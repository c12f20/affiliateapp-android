package com.jchain.affiliate.Category;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.jchain.affiliate.MainActivity;
import com.jchain.affiliate.Product.ProductInfoManager;
import com.jchain.affiliate.R;
import com.jchain.affiliate.UI.Fragments.BaseFragmentWithDrawer;
import com.jchain.affiliate.WebAPI.WebProductAPI;

import java.util.ArrayList;

public final class CategoryFragment extends BaseFragmentWithDrawer {
    private static final String TAG = "CategoryFragment";

    public static CategoryFragment newInstance() {
        Bundle args = new Bundle();

        CategoryFragment fragment = new CategoryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private ProductInfoManager mProductInfoManager = null;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mProductInfoManager = ProductInfoManager.getInstance(getContext());

        final View rootView = inflater.inflate(R.layout.fragment_category, container, false);
        initView(rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        cancelLoadCategoryInfo();
        super.onDestroyView();
    }

    private void initView(final View rootView) {
        initActionBar(getString(R.string.ID_TITLE_CATEGORIES));
        initDrawer(MainActivity.DRAWER_ITEM_ID_CATEGORIES);
        initReloadBar(rootView);
        initCategoryList(rootView);
    }

    private TextView mTextMsgCategory = null;
    private ExpandableListView mListViewCategory = null;
    private CategoryListAdapter mCategoryListAdapter = null;
    private void initCategoryList(final View rootView) {
        mTextMsgCategory = rootView.findViewById(R.id.textMsgCategories);

        mListViewCategory = rootView.findViewById(R.id.listViewCategories);
        mCategoryListAdapter = new CategoryListAdapter(getContext());
        mListViewCategory.setAdapter(mCategoryListAdapter);
        mListViewCategory.setGroupIndicator(null);
        mListViewCategory.setDivider(null);
        mListViewCategory.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                final CategoryItemInfo categoryItem = mCategoryList.get(groupPosition);
                if (categoryItem.subCategories.isEmpty()) {
                    showSubCategory(groupPosition, -1);
                }
                return false;
            }
        });
        mListViewCategory.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                final CategoryItemInfo categoryItem = mCategoryList.get(groupPosition);
                final CategoryItemInfo subItem = categoryItem.subCategories.get(childPosition);
                Log.d(TAG, "Subcategory Item: "+subItem.name+" of category "+categoryItem.name+" clicked");
                showSubCategory(groupPosition, childPosition);
                return true;
            }
        });

        loadCategoryInfo();
    }

    private ArrayList<CategoryItemInfo> mCategoryList = null;
    private void loadCategoryInfo() {
        loadCategoriesList();
    }

    private void cancelLoadCategoryInfo() {
        mProductInfoManager.cancelLoadCategoryList();
    }

    private void loadCategoriesList() {
        showLoadingCategories();

        mProductInfoManager.loadCategoryList(new WebProductAPI.CategoryDataListener() {
            @Override
            public void onGotCategoriesList(final @WebProductAPI.ProductError int errorCode, final ArrayList<CategoryItemInfo> list) {
                if (list != null && !list.isEmpty()) {
                    if (errorCode == WebProductAPI.PRODUCT_ERROR_NONE) { // only set it as global categories list when there is no error
                        mProductInfoManager.setCategoryList(list);
                    }
                    updateCategoriesList(list);
                } else {
                    showNoCategories();
                }
                int errorMsgResId = R.string.ID_MSG_ERROR_LOAD_CATEGORIES_FAILURE;
                switch(errorCode) {
                    case WebProductAPI.PRODUCT_ERROR_NONE:
                        return;
                    case WebProductAPI.PRODUCT_ERROR_NETWORK:
                        errorMsgResId = R.string.ID_MSG_ERROR_NETWORK_FAILURE;
                        break;
                    case WebProductAPI.PRODUCT_ERROR_PARSE:
                        errorMsgResId = R.string.ID_MSG_ERROR_PARSE_FAILURE;
                        break;
                    case WebProductAPI.PRODUCT_ERROR_GENERAL:
                    default:
                        break;
                }
                showInvalidError(getString(errorMsgResId));
            }
        });
    }

    private void updateCategoriesList(final ArrayList<CategoryItemInfo> list) {
        hideMsgCategories();
        mCategoryList = list;
        mCategoryListAdapter.updateCategoryList(mCategoryList);
    }

    private void showLoadingCategories() {
        mListViewCategory.setVisibility(View.GONE);
        mTextMsgCategory.setVisibility(View.VISIBLE);
        mTextMsgCategory.setText(R.string.ID_MSG_LOADING_DATA);
    }

    private void hideMsgCategories() {
        mListViewCategory.setVisibility(View.VISIBLE);
        mTextMsgCategory.setVisibility(View.GONE);
    }

    private void showNoCategories() {
        mListViewCategory.setVisibility(View.GONE);
        mTextMsgCategory.setVisibility(View.VISIBLE);
        mTextMsgCategory.setText(R.string.ID_MSG_NO_DATA);
    }

    // Error process methods
    private void showInvalidError(final String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
        showReloadBar();
    }

    private View mReloadBtn = null;
    private void initReloadBar(final View rootView) {
        mReloadBtn = rootView.findViewById(R.id.btnReloadData);
        mReloadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideReloadBar();
                loadCategoriesList();
            }
        });
        hideReloadBar();
    }

    private void showReloadBar() {
        mReloadBtn.setVisibility(View.VISIBLE);
    }

    private void hideReloadBar() {
        mReloadBtn.setVisibility(View.GONE);
    }

    // Action methods
    private void showSubCategory(final int categoryIndex, final int subCategoryIndex) {
        final ArrayList<CategoryItemInfo> categoriesList = mProductInfoManager.getCategoryList();
        if (categoriesList == null || categoryIndex > categoriesList.size()-1) {
            Log.w(TAG, "No categories list or index is invalid "+categoryIndex);
            return;
        }
        mProductInfoManager.resetProductList();
        mProductInfoManager.setCurCategoryIndex(categoryIndex);
        final CategoryItemFragment itemFragment = CategoryItemFragment.newInstance(categoryIndex, subCategoryIndex);
        showFragment(itemFragment, true);
    }
}
