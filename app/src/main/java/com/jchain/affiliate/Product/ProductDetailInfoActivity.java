package com.jchain.affiliate.Product;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.jchain.affiliate.Account.AccountManagement;
import com.jchain.affiliate.R;
import com.jchain.affiliate.Tracker.PriceTrackerItemInfo;
import com.jchain.affiliate.UI.Activities.BaseActivityWithActionBar;
import com.jchain.affiliate.WebAPI.WebProductAPI;

public final class ProductDetailInfoActivity extends BaseActivityWithActionBar {
    private static final String TAG = "ProductInfoActivity";

    private static final String ARG_PARAM_PRODUCT_INFO = "param.ProductInfo";
    private static final String ARG_PARAM_IS_TRACKER = "param.IsTracker";
    private static final String ARG_PARAM_IS_PROMOTION = "param.IsPromotion";

    public static void showTracker(final Context context, final PriceTrackerItemInfo tracker) {
        final ProductItemInfo product = new ProductItemInfo(null, tracker.name,
            null, null, tracker.vendorName, tracker.providerName, -1, null, null, tracker.clickURL,
            tracker.latestPrice, tracker.origPrice, 0, 0);
        final Intent intent = new Intent(context, ProductDetailInfoActivity.class);
        intent.putExtra(ARG_PARAM_PRODUCT_INFO, product.buildBundleInfo());
        intent.putExtra(ARG_PARAM_IS_TRACKER, true);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static void showProduct(final Context context, final ProductItemInfo product, final boolean isPromotion) {
        final Intent intent = new Intent(context, ProductDetailInfoActivity.class);
        intent.putExtra(ARG_PARAM_PRODUCT_INFO, product.buildBundleInfo());
        intent.putExtra(ARG_PARAM_IS_PROMOTION, isPromotion);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);

        if (!isPromotion) {
            final ProductInfoManager productMng = ProductInfoManager.getInstance(context);
            productMng.recordProductClick(context, product);
        }
    }

    public static void showProduct(final Context context, final ProductItemInfo product) {
        showProduct(context, product, false);
    }

    private ProductItemInfo mProductInfo = null;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        initData();
        initView(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        if (mWebView != null) {
            mWebView.resumeTimers();
            mWebView.destroy();
            mWebView = null;
        }
        super.onDestroy();
    }

    private void initData() {
        final Bundle args = getIntent().getExtras();
        if (args != null && args.containsKey(ARG_PARAM_PRODUCT_INFO)) {
            final Bundle productBundle = args.getBundle(ARG_PARAM_PRODUCT_INFO);
            if (productBundle != null) {
                mProductInfo = new ProductItemInfo(productBundle);
                Log.d(TAG, "product: " + mProductInfo.name);
            } else {
                Log.e(TAG, "Failed to show product as no info in args");
            }
        } else {
            Log.e(TAG, "Failed to get args from Intent");
        }
    }

    @Override
    public void onPause() {
        if (mWebView != null) {
            mWebView.onPause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mWebView != null) {
            mWebView.onResume();
        }
    }

    @Override
    public void onStop() {
        if (mWebView != null) {
            mWebView.pauseTimers();
        }
        super.onStop();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mWebView != null) {
            mWebView.resumeTimers();
        }
    }

    @Override
    protected void initView(final Bundle savedInstanceState) {
        super.initView(savedInstanceState);
        updateActionBar(getString(R.string.ID_TITLE_PRODUCT), true);
        initProgressBar();
        initWebView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        final Intent intent = getIntent();
        final boolean isTracker = intent.getBooleanExtra(ARG_PARAM_IS_TRACKER, false);
        final boolean isPromotion = intent.getBooleanExtra(ARG_PARAM_IS_PROMOTION, false);
        if (AccountManagement.getInstance(getBaseContext()).hasAccountLogin()
            && !isTracker && !isPromotion) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_product_detail, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int itemId = item.getItemId();
        switch(itemId) {
            case R.id.menu_item_track:
                Log.d(TAG, "Track the product");
                trackProduct();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private ProgressBar mProgressBar = null;
    private void initProgressBar() {
        mProgressBar = findViewById(R.id.progressBar);
        hideProgressBar();
    }

    private void showProgressBar() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar() {
        mProgressBar.setVisibility(View.GONE);
    }

    private static final String ALIEXPRESS_ROOT_PAGE = "https://m.aliexpress.com/";
    private static final String GEARBEST_SCHEMA_NAME = "gearbest";
    private WebView mWebView = null;
    private void initWebView() {
        mWebView = findViewById(R.id.webview);
        mWebView.loadUrl(mProductInfo.productURL);

        final WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);

        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                Log.d(TAG, "Product Webview is going to url: "+url);
                if (url.equals(ALIEXPRESS_ROOT_PAGE)) {
                    onBackPressed();
                    return;
                } else if (url.startsWith(GEARBEST_SCHEMA_NAME)) {
                    goToGearBestSchemaURL(url);
                    onBackPressed();
                    return;
                }
                showProgressBar();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                Log.d(TAG, "Product Webview finished to change to url: "+url);
                hideProgressBar();
            }
        });
    }

    private void goToGearBestSchemaURL(final String url) {
        try {
            final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);
        } catch (Exception e) {
            Log.w(TAG, "Failed to goToGearBestSchemaURL, exception: ", e);
        }
    }

    // action methods
    private void trackProduct() {
        final ProductInfoManager productInfoManager = ProductInfoManager.getInstance(getBaseContext());
        productInfoManager.addTrack(mProductInfo, new WebProductAPI.AddToTrackListener() {
            @Override
            public void onResult(int errorCode, PriceTrackerItemInfo trackItem) {
                Log.d(TAG, "AddTrack result: "+errorCode);
                final String resultMsg;
                if (errorCode == WebProductAPI.PRODUCT_ERROR_NONE) {
                    resultMsg = getString(R.string.ID_MSG_SUCCESS_ADD_TRACK);
                } else {
                    resultMsg = getString(R.string.ID_MSG_ERROR_ADD_TRACK);
                }
                Toast.makeText(getBaseContext(), resultMsg, Toast.LENGTH_LONG).show();
            }
        });
    }
}
