package com.jchain.affiliate.Product;

import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.jchain.affiliate.Account.AccountInfo;
import com.jchain.affiliate.Account.AccountManagement;
import com.jchain.affiliate.MainActivity;
import com.jchain.affiliate.R;
import com.jchain.affiliate.System.SystemInfoManager;
import com.jchain.affiliate.Tracker.PriceTrackerItemInfo;
import com.jchain.affiliate.UI.Fragments.BaseFragmentWithDrawer;
import com.jchain.affiliate.WebAPI.WebProductAPI;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;

public final class ProductListFragment extends BaseFragmentWithDrawer {
    private static final String TAG = "ProductListFragment";

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({LIST_FROM_NONE, LIST_FROM_BEST_DEALS, LIST_FROM_HOT_PRODUCTS,
            LIST_FROM_RECOMMEND_PRODUCTS})
    public @interface ListFrom {}
    public static final int LIST_FROM_NONE = 0;
    public static final int LIST_FROM_BEST_DEALS = 1;
    public static final int LIST_FROM_HOT_PRODUCTS = 2;
    public static final int LIST_FROM_RECOMMEND_PRODUCTS = 3;

    private static final String ARG_PARAM_LIST_FROM = "param.listFrom";
    public static ProductListFragment newInstance(final @ListFrom int listFrom) {
        Bundle args = new Bundle();

        args.putInt(ARG_PARAM_LIST_FROM, listFrom);
        ProductListFragment fragment = new ProductListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private ProductInfoManager mProductInfoManager = null;
    private AccountManagement mAccountManager = null;
    private @ListFrom int mListFrom = LIST_FROM_NONE;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mProductInfoManager = ProductInfoManager.getInstance(getContext());
        mAccountManager = AccountManagement.getInstance(getContext());
        if (getArguments() != null) {
            mListFrom = getArguments().getInt(ARG_PARAM_LIST_FROM);
        }
    }

    private Handler mHandler = null;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mHandler = new Handler();

        final View rootView = inflater.inflate(R.layout.fragment_product_list, container, false);
        initValues();
        initView(rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        cancelLoadProductsData();
        super.onDestroyView();
    }

    private String mTitle = null;
    private long mSelectedDrawerItemId = MainActivity.DRAWER_ITEM_ID_POPULAR_PRODUCTS;
    private void initValues() {
        switch(mListFrom) {
            case LIST_FROM_BEST_DEALS:
                mTitle = getString(R.string.ID_DRAWER_OPTION_FAVORITES);
                mSelectedDrawerItemId = MainActivity.DRAWER_ITEM_ID_FAVORITES;
                break;
            case LIST_FROM_HOT_PRODUCTS:
                mTitle = getString(R.string.ID_DRAWER_OPTION_POPULAR_PRODUCTS);
                mSelectedDrawerItemId = MainActivity.DRAWER_ITEM_ID_POPULAR_PRODUCTS;
                mIsHotProductPage = true;
                break;
            case LIST_FROM_RECOMMEND_PRODUCTS:
                mTitle = getString(R.string.ID_TITLE_RECOMMEND);
                mSelectedDrawerItemId = MainActivity.DRAWER_ITEM_ID_RECOMMEND_PRODUCTS;
                break;
            case LIST_FROM_NONE:
            default:
                break;
        }
    }

    private void initView(final View rootView) {
        initActionBar(mTitle);
        initDrawer(mSelectedDrawerItemId);
        initReloadBar(rootView);
        initProductsList(rootView);
    }

    private TextView mTextMsgProducts = null;
    private RecyclerView mRecycleViewProducts = null;
    private ProductItemRecyclerAdapter mProductItemRecyclerAdapter = null;
    private void initProductsList(final View rootView) {
        mTextMsgProducts = rootView.findViewById(R.id.textMsgProducts);

        mRecycleViewProducts = rootView.findViewById(R.id.listProducts);
        mRecycleViewProducts.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mProductItemRecyclerAdapter = new ProductItemRecyclerAdapter(getContext(), mRecycleViewProducts);
        mProductItemRecyclerAdapter.setOnItemClickListener(new ProductItemRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClicked(int position) {
                if (mProductsList == null) {
                    return;
                }
                if (position >= 0 && position < mProductsList.size()) {
                    showProduct(position);
                }
            }
        });
        mProductItemRecyclerAdapter.setLoadMoreListener(new ProductItemRecyclerAdapter.LoadMoreListener() {
            @Override
            public void onNeedLoadMore() {
                if (mListFrom == LIST_FROM_HOT_PRODUCTS && mProductsList.size() < mHotProductListCount) {
                    showLoadingMoreProducts();
                    mHandler.postDelayed(new Runnable(){
                        @Override
                        public void run() {
                            loadHotProductsListWithPage(mHotProductPageIdx+1);
                        }
                    }, 1000);
                }
            }
        });
        if (mAccountManager.hasAccountLogin()) {
            mProductItemRecyclerAdapter.setOnItemTrackClickListener(new ProductItemRecyclerAdapter.OnItemTrackClickListener() {
                @Override
                public void onItemTrackClicked(int position) {
                    Log.d(TAG, "onItemTrackClicked, position: " + position);
                    if (position >= 0 && position < mProductsList.size()) {
                        trackProduct(position);
                    }
                }
            });
        }
        mRecycleViewProducts.setAdapter(mProductItemRecyclerAdapter);

        loadProductsData();
    }

    private void showLoadingProducts() {
        mRecycleViewProducts.setVisibility(View.GONE);
        mTextMsgProducts.setVisibility(View.VISIBLE);
        mTextMsgProducts.setText(R.string.ID_MSG_LOADING_DATA);
    }

    private void hideMsgProducts() {
        mRecycleViewProducts.setVisibility(View.VISIBLE);
        mTextMsgProducts.setVisibility(View.GONE);
    }

    private void showNoData() {
        mRecycleViewProducts.setVisibility(View.GONE);
        mTextMsgProducts.setVisibility(View.VISIBLE);
        mTextMsgProducts.setText(R.string.ID_MSG_NO_DATA);
    }

    private void processProductError(final @WebProductAPI.ProductError int errorCode) {
        int errorMsgResId = R.string.ID_MSG_ERROR_LOAD_PRODUCTS_FAILURE;
        switch(errorCode) {
            case WebProductAPI.PRODUCT_ERROR_NONE:
                return;
            case WebProductAPI.PRODUCT_ERROR_NETWORK:
                errorMsgResId = R.string.ID_MSG_ERROR_NETWORK_FAILURE;
                break;
            case WebProductAPI.PRODUCT_ERROR_PARSE:
                errorMsgResId = R.string.ID_MSG_ERROR_PARSE_FAILURE;
                break;
            case WebProductAPI.PRODUCT_ERROR_GENERAL:
            default:
                break;
        }
        showInvalidError(getString(errorMsgResId));
    }

    private void showLoadingMoreProducts() {
        mProductItemRecyclerAdapter.showLoadingProductList();
    }

    private void hideLoadingMoreProducts() {
        mProductItemRecyclerAdapter.hideLoadingProductList();
        mProductItemRecyclerAdapter.setLoaded();
    }

    // Load Products methods
    private WebProductAPI.ProductDataListener mProductDataListener = new WebProductAPI.ProductDataListener() {
        @Override
        public void onGotProductsList(int errorCode, int totalCount, int totalPagesCount, ArrayList<ProductItemInfo> list) {
            hideLoadingMoreProducts();
            if (list == null || list.isEmpty()) {
                showNoData();
            } else {
                if (mListFrom == LIST_FROM_HOT_PRODUCTS) {
                    mProductInfoManager.appendProductList(list);
                    updateHotProductsCount(totalCount);
                    updateProductList(mProductInfoManager.getProductList());
                } else {
                    updateProductList(list);
                }
            }
            if (errorCode != WebProductAPI.PRODUCT_ERROR_NONE) {
                processProductError(errorCode);
            }
        }
    };

    private void loadProductsData() {
        switch(mListFrom) {
            case LIST_FROM_BEST_DEALS:
                mProductInfoManager.loadBestDeals(mProductDataListener);
                break;
            case LIST_FROM_HOT_PRODUCTS:
                loadHotProducts();
                break;
            case LIST_FROM_RECOMMEND_PRODUCTS:
                final AccountInfo curUser = mAccountManager.getCurAccountInfo();
                if (curUser != null) {
                    mProductInfoManager.loadVehicleRelatedProducts(curUser.accessToken, mProductDataListener);
                }
                break;
            case LIST_FROM_NONE:
            default:
                showNoData();
                return;
        }
        showLoadingProducts();
    }

    private void cancelLoadProductsData() {
        switch(mListFrom) {
            case LIST_FROM_BEST_DEALS:
                mProductInfoManager.cancelLoadBestDeals();
                break;
            case LIST_FROM_HOT_PRODUCTS:
                mProductInfoManager.cancelLoadHotProducts();
                break;
            case LIST_FROM_RECOMMEND_PRODUCTS:
                mProductInfoManager.cancelLoadVehicleRelatedProducts();
                break;
            case LIST_FROM_NONE:
            default:
                break;
        }
    }

    private ArrayList<ProductItemInfo> mProductsList = null;
    private void updateProductList(final ArrayList<ProductItemInfo> list) {
        hideMsgProducts();
        mProductsList = list;
        mProductItemRecyclerAdapter.updateProductList(list);
    }

    // Error process methods
    private void showInvalidError(final String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
        showReloadBar();
    }

    private View mReloadBtn = null;
    private void initReloadBar(final View rootView) {
        mReloadBtn = rootView.findViewById(R.id.btnReloadData);
        mReloadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideReloadBar();
                loadProductsData();
            }
        });
        hideReloadBar();
    }

    private void showReloadBar() {
        mReloadBtn.setVisibility(View.VISIBLE);
    }

    private void hideReloadBar() {
        mReloadBtn.setVisibility(View.GONE);
    }

    // Hot products loading methods
    private int mHotProductListCount = -1;
    private int mHotProductPageIdx = -1;
    private void loadHotProducts() {
        mHotProductListCount = -1;
        mHotProductPageIdx = -1;
        loadHotProductsList();
    }

    private void updateHotProductsCount(final int count) {
        if (mHotProductListCount < 0) {
            mHotProductListCount = count;
        }
    }

    private void loadHotProductsList() {
        final ArrayList<ProductItemInfo> oldList = mProductInfoManager.getProductList();
        if (!oldList.isEmpty()) {
            updateProductList(oldList);
            final int curProductIdx = mProductInfoManager.getCurProductIndex();
            mHotProductPageIdx = mProductInfoManager.getCurProductPageIndex();
            mRecycleViewProducts.scrollToPosition(mProductItemRecyclerAdapter.getDisplayIdx(curProductIdx));
        } else {
            loadHotProductsListWithPage(0);
        }
    }

    private void loadHotProductsListWithPage(final int pageIdx) {
        mHotProductPageIdx = pageIdx;
        mProductInfoManager.loadHotProducts(null, mHotProductPageIdx, mProductDataListener);
    }

    // Action methods
    private void showProduct(final int index) {
        SystemInfoManager.getInstance().checkInternetAvailable(getContext(), new SystemInfoManager.InternetAvailableListener() {
            @Override
            public void onInternetAvailableResult(boolean available) {
                if (!available) {
                    showInvalidError(getString(R.string.ID_MSG_ERROR_NETWORK_FAILURE));
                    return;
                }
                final ProductItemInfo item = mProductsList.get(index);
                ProductDetailInfoActivity.showProduct(getContext(), item);
            }
        });
    }

    private void trackProduct(final int index) {
        final ProductItemInfo item = mProductsList.get(index);
        mProductInfoManager.addTrack(item, new WebProductAPI.AddToTrackListener() {
            @Override
            public void onResult(int errorCode, PriceTrackerItemInfo trackItem) {
                Log.d(TAG, "AddTrack result: "+errorCode);
                final String resultMsg;
                if (errorCode == WebProductAPI.PRODUCT_ERROR_NONE) {
                    resultMsg = getString(R.string.ID_MSG_SUCCESS_ADD_TRACK);
                } else {
                    resultMsg = getString(R.string.ID_MSG_ERROR_ADD_TRACK);
                }
                Toast.makeText(getContext(), resultMsg, Toast.LENGTH_LONG).show();
            }
        });
    }
}
