package com.jchain.affiliate.Product;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.jchain.affiliate.R;
import com.jchain.affiliate.System.SystemInfoManager;
import com.jchain.affiliate.UI.Fragments.BaseFragmentWithDrawer;
import com.jchain.affiliate.WebAPI.WebProductAPI;

import java.util.List;

public final class CouponsListFragment extends BaseFragmentWithDrawer {
    public static CouponsListFragment newInstance() {
        CouponsListFragment fragment = new CouponsListFragment();
        return fragment;
    }

    private ProductInfoManager mProductInfoManager = null;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mProductInfoManager = ProductInfoManager.getInstance(getContext());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_coupon_list, container, false);
        initView(rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        cancelLoadCoupons();
        super.onDestroyView();
    }

    private void initView(final View rootView) {
        initActionBar(getString(R.string.ID_TITLE_COUPONS));
        initReloadBar(rootView);
        initCouponsList(rootView);
    }

    private TextView mTextMsgCoupons = null;
    private RecyclerView mRecycleViewCoupons = null;
    private CouponItemRecyclerAdapter mCouponItemRecyclerAdapter = null;
    private void initCouponsList(final View rootView) {
        mTextMsgCoupons = rootView.findViewById(R.id.textMsgCoupons);

        mRecycleViewCoupons = rootView.findViewById(R.id.listCoupons);
        mRecycleViewCoupons.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mCouponItemRecyclerAdapter = new CouponItemRecyclerAdapter(getContext(), mRecycleViewCoupons);
        mCouponItemRecyclerAdapter.setOnItemClickListener(new CouponItemRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClicked(int position) {
                if (mCouponsList == null) {
                    return;
                }
                if (position >= 0 && position < mCouponsList.size()) {
                    showCoupon(position);
                }
            }
        });
        mRecycleViewCoupons.setAdapter(mCouponItemRecyclerAdapter);

        loadCouponsData();
    }

    private void showLoadingProducts() {
        mRecycleViewCoupons.setVisibility(View.GONE);
        mTextMsgCoupons.setVisibility(View.VISIBLE);
        mTextMsgCoupons.setText(R.string.ID_MSG_LOADING_DATA);
    }

    private void hideMsgProducts() {
        mRecycleViewCoupons.setVisibility(View.VISIBLE);
        mTextMsgCoupons.setVisibility(View.GONE);
    }

    private void showNoData() {
        mRecycleViewCoupons.setVisibility(View.GONE);
        mTextMsgCoupons.setVisibility(View.VISIBLE);
        mTextMsgCoupons.setText(R.string.ID_MSG_NO_DATA);
    }

    private void processCouponError(final @WebProductAPI.ProductError int errorCode) {
        int errorMsgResId = R.string.ID_MSG_ERROR_LOAD_PRODUCTS_FAILURE;
        switch(errorCode) {
            case WebProductAPI.PRODUCT_ERROR_NONE:
                return;
            case WebProductAPI.PRODUCT_ERROR_NETWORK:
                errorMsgResId = R.string.ID_MSG_ERROR_NETWORK_FAILURE;
                break;
            case WebProductAPI.PRODUCT_ERROR_PARSE:
                errorMsgResId = R.string.ID_MSG_ERROR_PARSE_FAILURE;
                break;
            case WebProductAPI.PRODUCT_ERROR_GENERAL:
            default:
                break;
        }
        showInvalidError(getString(errorMsgResId));
    }

    // Load Products methods
    private WebProductAPI.CouponListener mCouponDataListener = new WebProductAPI.CouponListener() {
        @Override
        public void onGotCoupons(int errorCode, List<CouponItemInfo> list) {
            if (list == null || list.isEmpty()) {
                showNoData();
            } else {
                updateCouponList(list);
            }
            if (errorCode != WebProductAPI.PRODUCT_ERROR_NONE) {
                processCouponError(errorCode);
            }
        }
    };

    private void loadCouponsData() {
        mProductInfoManager.queryCoupons(mCouponDataListener);
        showLoadingProducts();
    }

    private void cancelLoadCoupons() {
        mProductInfoManager.cancelQueryCoupons();
    }

    private List<CouponItemInfo> mCouponsList = null;
    private void updateCouponList(final List<CouponItemInfo> list) {
        hideMsgProducts();
        mCouponsList = list;
        mCouponItemRecyclerAdapter.updateCouponList(list);
    }

    // Error process methods
    private void showInvalidError(final String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
        showReloadBar();
    }

    private View mReloadBtn = null;
    private void initReloadBar(final View rootView) {
        mReloadBtn = rootView.findViewById(R.id.btnReloadData);
        mReloadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideReloadBar();
                loadCouponsData();
            }
        });
        hideReloadBar();
    }

    private void showReloadBar() {
        mReloadBtn.setVisibility(View.VISIBLE);
    }

    private void hideReloadBar() {
        mReloadBtn.setVisibility(View.GONE);
    }

    // Action methods
    private void showCoupon(final int index) {
        SystemInfoManager.getInstance().checkInternetAvailable(getContext(), new SystemInfoManager.InternetAvailableListener() {
            @Override
            public void onInternetAvailableResult(boolean available) {
                if (!available) {
                    showInvalidError(getString(R.string.ID_MSG_ERROR_NETWORK_FAILURE));
                    return;
                }
                final CouponItemInfo item = mCouponsList.get(index);
                final CouponsDetailInfoFragment fragment = CouponsDetailInfoFragment.newInstance(getString(R.string.ID_TITLE_COUPONS), item);
                showFragment(fragment, true);
            }
        });
    }
}
