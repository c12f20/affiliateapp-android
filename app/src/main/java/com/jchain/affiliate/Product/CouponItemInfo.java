package com.jchain.affiliate.Product;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public final class CouponItemInfo {
    private static final String TAG = "CouponItemInfo";

    public final String code;
    public final String description;
    public final String vendorName;
    public final String vendorLogoURL;
    public final String couponURL;
    public Date startTime = null;
    public Date endTime = null;

    public CouponItemInfo(final String code, final String description,
                          final String vendorName, final String vendorLogoURL,
                          final String couponURL,
                          final Date startTime, final Date endTime) {
        this.code = code;
        this.description = description;
        this.vendorName = vendorName;
        this.vendorLogoURL = vendorLogoURL;
        this.couponURL = couponURL;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    private static final String BUNDLE_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    private static final String BUNDLE_KEY_CODE = "code";
    private static final String BUNDLE_KEY_DESCRIPTION = "description";
    private static final String BUNDLE_KEY_VENDOR_NAME = "vendor_name";
    private static final String BUNDLE_KEY_VENDOR_LOGO = "vendor_logo";
    private static final String BUNDLE_KEY_COUPON_URL = "coupon_url";
    private static final String BUNDLE_KEY_START_TIME = "start_time";
    private static final String BUNDLE_KEY_END_TIME = "end_time";
    public CouponItemInfo(final Bundle bundle) {
        this.code = bundle.getString(BUNDLE_KEY_CODE);
        this.description = bundle.getString(BUNDLE_KEY_DESCRIPTION);
        this.vendorName = bundle.getString(BUNDLE_KEY_VENDOR_NAME);
        this.vendorLogoURL = bundle.getString(BUNDLE_KEY_VENDOR_LOGO);
        this.couponURL = bundle.getString(BUNDLE_KEY_COUPON_URL);
        final SimpleDateFormat sdf = new SimpleDateFormat(BUNDLE_DATE_FORMAT, Locale.US);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            final String startTimeData = bundle.getString(BUNDLE_KEY_START_TIME);
            this.startTime = TextUtils.isEmpty(startTimeData) ? null : sdf.parse(startTimeData);
            final String endTimeData = bundle.getString(BUNDLE_KEY_END_TIME);
            this.endTime = TextUtils.isEmpty(endTimeData) ? null : sdf.parse(endTimeData);
        } catch (ParseException e) {
            Log.e(TAG, "Invalid begin/end time", e);
        }
    }

    public Bundle buildBundleInfo() {
        final SimpleDateFormat sdf = new SimpleDateFormat(BUNDLE_DATE_FORMAT, Locale.US);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

        final Bundle info = new Bundle();
        info.putString(BUNDLE_KEY_CODE, this.code);
        info.putString(BUNDLE_KEY_DESCRIPTION, this.description);
        info.putString(BUNDLE_KEY_VENDOR_NAME, this.vendorName);
        info.putString(BUNDLE_KEY_VENDOR_LOGO, this.vendorLogoURL);
        info.putString(BUNDLE_KEY_COUPON_URL, this.couponURL);
        if (this.startTime != null) {
            info.putString(BUNDLE_KEY_START_TIME, sdf.format(this.startTime));
        } else {
            info.putString(BUNDLE_KEY_START_TIME, "");
        }
        if (this.endTime != null) {
            info.putString(BUNDLE_KEY_END_TIME, sdf.format(this.endTime));
        } else {
            info.putString(BUNDLE_KEY_END_TIME, "");
        }
        return info;
    }
}
