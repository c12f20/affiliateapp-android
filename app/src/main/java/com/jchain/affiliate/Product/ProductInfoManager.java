package com.jchain.affiliate.Product;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.jchain.affiliate.Account.AccountInfo;
import com.jchain.affiliate.Account.AccountManagement;
import com.jchain.affiliate.Category.CategoryItemInfo;
import com.jchain.affiliate.Tracker.PriceTrackerItemInfo;
import com.jchain.affiliate.WebAPI.WebProductAPI;

import java.util.ArrayList;

import okhttp3.Call;

import static com.jchain.affiliate.WebAPI.WebProductAPI.PRODUCT_ERROR_GENERAL;

public final class ProductInfoManager {
    private static final String TAG = "ProductInfoManager";

    private static ProductInfoManager sInstance = null;
    public static ProductInfoManager getInstance(final Context context) {
        if (sInstance == null) {
            sInstance = new ProductInfoManager(context);
        }
        return sInstance;
    }

    private final WebProductAPI mWebProductAPI;
    private final AccountManagement mAccountMng;
    private ProductInfoManager(final Context context) {
        mWebProductAPI = WebProductAPI.getInstance(context);
        mAccountMng = AccountManagement.getInstance(context);
    }

    // Query Categories methods
    private Call mLoadCategoryCall = null;
    public void loadCategoryList(final WebProductAPI.CategoryDataListener listener) {
        resetCurCategoryIndex();
        if (mCategoryList != null) { // We've got category list, just return it.
            if (listener != null) {
                listener.onGotCategoriesList(WebProductAPI.PRODUCT_ERROR_NONE, mCategoryList);
            }
            return;
        }
        cancelLoadCategoryList();

        mLoadCategoryCall = mWebProductAPI.queryAllCategories(listener);
    }

    public void cancelLoadCategoryList() {
        if (mLoadCategoryCall != null) {
            mWebProductAPI.cancelQueryAllCategories(mLoadCategoryCall);
            mLoadCategoryCall = null;
        }
    }

    private ArrayList<CategoryItemInfo> mCategoryList = null;
    public void setCategoryList(final ArrayList<CategoryItemInfo> list) {
        mCategoryList = list;
    }

    public ArrayList<CategoryItemInfo> getCategoryList() {
        return mCategoryList;
    }
    private int mCurCategoryIndex = -1;
    public void setCurCategoryIndex(final int index) {
        mCurCategoryIndex = index;
    }
    public int getCurCategoryIndex() {
        return mCurCategoryIndex;
    }
    private void resetCurCategoryIndex() {
        mCurCategoryIndex = -1;
    }

    // Query Products methods
    private String getCategoryId(final CategoryItemInfo category) {
        String category_id = null;
        if (category != null && category.id != null && !category.id.isEmpty()) {
            category_id = category.id;
        }
        return category_id;
    }

    private static final int PRODUCTS_QUERY_COUNT_PER_PAGE = 20;
    private Call mLoadProductCall = null;
    public void loadProductListByCategory(final CategoryItemInfo category,
                                          final @WebProductAPI.SortBy int orderBy,
                                          final boolean orderAsc,
                                          final int pageIdx,
                                          final WebProductAPI.ProductDataListener listener) {
        if (category == null || category.id == null || category.id.isEmpty()) {
            Log.w(TAG, "Invalid category for loadProductList");
            if (listener != null) {
                listener.onGotProductsList(PRODUCT_ERROR_GENERAL, -1, -1,null);
            }
            return;
        }
        cancelLoadProductList();
        mLoadProductCall = mWebProductAPI.queryProductsByCategoryId(category.id, orderBy, orderAsc,
                pageIdx, PRODUCTS_QUERY_COUNT_PER_PAGE, listener);
    }

    public void queryProductList(final CategoryItemInfo category,
                                 final String keyword,
                                 final @WebProductAPI.SortBy int sortBy,
                                 final boolean sortAsc,
                                 final int pageIdx,
                                 final WebProductAPI.ProductDataListener listener) {
        cancelLoadProductList();
        mLoadProductCall = mWebProductAPI.queryProducts(getCategoryId(category),
                keyword, sortBy, sortAsc, pageIdx, PRODUCTS_QUERY_COUNT_PER_PAGE, listener);
    }

    public void cancelLoadProductList() {
        if (mLoadProductCall != null) {
            mWebProductAPI.cancelQueryProducts(mLoadProductCall);
            mLoadProductCall = null;
        }
    }
    // General Product list related methods
    private ArrayList<ProductItemInfo> mProductList = new ArrayList<ProductItemInfo>();
    public void resetProductList() {
        cancelLoadProductList();
        mCurProductIdx = 0;
        mProductList.clear();
    }
    public void appendProductList(final ArrayList<ProductItemInfo> list) {
        if (list == null) {
            return;
        }
        mProductList.addAll(list);
    }
    public ArrayList<ProductItemInfo> getProductList() {
        return mProductList;
    }

    private int mCurProductIdx = 0;
    public void setCurProductIndex(final int index) {
        mCurProductIdx = index;
    }
    public int getCurProductIndex() {
        return mCurProductIdx;
    }
    public int getCurProductPageIndex() {
        return mCurProductIdx / PRODUCTS_QUERY_COUNT_PER_PAGE;
    }

    // Search Result list related methods
    private ArrayList<ProductItemInfo> mSearchResultList = new ArrayList<ProductItemInfo>();
    public void resetSearchResultList() {
        cancelLoadHotProducts();
        cancelLoadProductList();
        mSearchResultList.clear();
    }
    public void appendSearchResultList(final ArrayList<ProductItemInfo> list) {
        if (list == null) {
            return;
        }
        mSearchResultList.addAll(list);
    }
    public ArrayList<ProductItemInfo> getSearchResultList() {
        return mSearchResultList;
    }

    // Promotions methods
    private Call mLoadPromotionsCall = null;
    public void loadPromotions(final WebProductAPI.PromotionDataListener listener) {
        cancelLoadPromotions();
        mLoadPromotionsCall = mWebProductAPI.queryPromotions(listener);
    }

    public void cancelLoadPromotions() {
        if (mLoadPromotionsCall != null) {
            mWebProductAPI.cancelQueryPromotions(mLoadPromotionsCall);
            mLoadPromotionsCall = null;
        }
    }

    private ArrayList<PromotionItemInfo> mPromotionsList = null;
    public void setPromotionsList(final ArrayList<PromotionItemInfo> list) {
        mPromotionsList = list;
    }

    private final int PROMOTIONS_COUNT_PER_PAGE = 5;
    private int mCurPromotionStartIdx = 0;
    public ArrayList<PromotionItemInfo> getNextPromotions() {
        if (mPromotionsList == null) {
            return null;
        }
        final ArrayList<PromotionItemInfo> nextList = new ArrayList<PromotionItemInfo>();
        for (int i=0; i < PROMOTIONS_COUNT_PER_PAGE; i++) {
            final int index = (mCurPromotionStartIdx + i) % mPromotionsList.size();
            nextList.add(mPromotionsList.get(index));
        }
        mCurPromotionStartIdx = (mCurPromotionStartIdx+PROMOTIONS_COUNT_PER_PAGE) % mPromotionsList.size();
        return nextList;
    }
    // Query Best Deals methods
    private Call mLoadBestDealsCall = null;
    public void loadBestDeals(final WebProductAPI.ProductDataListener listener) {
        cancelLoadBestDeals();
        mLoadBestDealsCall = mWebProductAPI.queryBestDeals(listener);
    }

    public void cancelLoadBestDeals() {
        if (mLoadBestDealsCall != null) {
            mWebProductAPI.cancelQueryBestDeals(mLoadBestDealsCall);
            mLoadBestDealsCall = null;
        }
    }

    // Query Hot Products methods
    private Call mLoadHotProductsCall = null;
    public void loadHotProducts(final String filter, final int pageIdx, final WebProductAPI.ProductDataListener listener) {
        cancelLoadHotProducts();
        mLoadHotProductsCall = mWebProductAPI.queryHotProducts(filter, pageIdx, PRODUCTS_QUERY_COUNT_PER_PAGE, listener);
    }

    public void cancelLoadHotProducts() {
        if (mLoadHotProductsCall != null) {
            mWebProductAPI.cancelQueryHotProducts(mLoadHotProductsCall);
            mLoadHotProductsCall = null;
        }
    }

    // Query My Vehicle Related Products method
    private static final int DEFAULT_VEHICLE_PRODUCTS_LIMIT = 10;
    private Call mLoadVehilceRelatedProductsCall = null;
    public void loadVehicleRelatedProducts(final String accessToken, final WebProductAPI.ProductDataListener listener) {
        cancelLoadVehicleRelatedProducts();
        mLoadVehilceRelatedProductsCall = mWebProductAPI.queryVehicleRelatedProducts(accessToken,
                null, DEFAULT_VEHICLE_PRODUCTS_LIMIT, listener);
    }

    public void cancelLoadVehicleRelatedProducts() {
        if (mLoadVehilceRelatedProductsCall != null) {
            mWebProductAPI.cancelQueryVehicleRelatedProducts(mLoadVehilceRelatedProductsCall);
            mLoadVehilceRelatedProductsCall = null;
        }
    }

    // Record Product click count
    public void recordProductClick(final Context context, final ProductItemInfo product) {
        if (product == null) {
            Log.e(TAG, "Ignore to record Product click as null product");
            return;
        }
        if (TextUtils.isEmpty(product.id) || TextUtils.isEmpty(product.name)) {
            Log.e(TAG, "Ignore to record Product click as invalid product, id: "+product.id+", name: "+product.name);
            return;
        }
        mWebProductAPI.reportProductClicked(context, product);
    }

    // Query coupon code
    private Call mQueryCouponsCall = null;
    public void queryCoupons(final WebProductAPI.CouponListener listener) {
        cancelQueryCoupons();
        mQueryCouponsCall = mWebProductAPI.queryCoupons(listener);
    }

    public void cancelQueryCoupons() {
        if (mQueryCouponsCall != null) {
            mWebProductAPI.cancelQueryCoupons(mQueryCouponsCall);
            mQueryCouponsCall = null;
        }
    }

    // Tracker management
    public void addTrack(final ProductItemInfo product, final WebProductAPI.AddToTrackListener listener) {
        if (product == null) {
            Log.e(TAG, "Can't add track to null product");
            return;
        }
        final AccountInfo curAccount = mAccountMng.getCurAccountInfo();
        if (curAccount == null) {
            Log.e(TAG, "Can't add track without user login");
            return;
        }
        mWebProductAPI.addProductToTrack(curAccount, product, listener);
    }

    public void stopTrack(final PriceTrackerItemInfo track, final WebProductAPI.StopTrackListener listener) {
        if (track == null || track.id < 0) {
            Log.e(TAG, "Can't stop an invalid track");
            return;
        }
        final AccountInfo curAccount = mAccountMng.getCurAccountInfo();
        if (curAccount == null) {
            Log.e(TAG, "Can't stop track without user login");
            return;
        }
        mWebProductAPI.stopTrack(curAccount, track, listener);
    }

    private Call mQueryTracksCall = null;
    public void queryTracks(final WebProductAPI.TrackDataListener listener) {
        final AccountInfo curAccount = mAccountMng.getCurAccountInfo();
        if (curAccount == null) {
            Log.e(TAG, "Can't query tracks without user login");
            return;
        }
        cancelQueryTracks();
        mQueryTracksCall = mWebProductAPI.queryTracks(curAccount, listener);
    }

    public void cancelQueryTracks() {
        if (mQueryTracksCall != null) {
            mWebProductAPI.cancelQueryTracks(mQueryTracksCall);
            mQueryTracksCall = null;
        }
    }

    private Call mQueryUpdatedTracksCall = null;
    public void queryUpdatedTracks(final WebProductAPI.TrackDataListener listener) {
        final AccountInfo curAccount = mAccountMng.getCurAccountInfo();
        if (curAccount == null) {
            Log.e(TAG, "Can't query updated tracks without user login");
            return;
        }
        cancelQueryUpdatedTracks();
        mQueryUpdatedTracksCall = mWebProductAPI.queryUpdatedTracks(curAccount, listener);
    }

    public void cancelQueryUpdatedTracks() {
        if (mQueryUpdatedTracksCall != null) {
            mWebProductAPI.cancelQueryTracks(mQueryTracksCall);
            mQueryTracksCall = null;
        }
    }

    // Utility Methods
    /*private String trimDataInfo(final String data) {
        return data.replace("\"", "");
    }

    private ArrayList<ProductItemInfo> parseProductCSV(final Context context, final int csvFileId) throws IOException {
        InputStream is = null;
        String lineData = null;
        try {
            final ArrayList<ProductItemInfo> productList = new ArrayList<ProductItemInfo>();
            is = context.getResources().openRawResource(csvFileId);
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            while((lineData = reader.readLine()) != null) {
                final String[] infos = lineData.split("(?<=[\"|,]),");
                if (infos.length != 13) {
                    Log.w(TAG, "Skip product line: "+lineData+" as its format is invalid");
                    continue;
                }
                final String categoryName = trimDataInfo(infos[0]);
                final String categoryId = trimDataInfo(infos[1]);
                final String productName = trimDataInfo(infos[2]);
                final String productId = trimDataInfo(infos[3]);
                final String productImageUrl = trimDataInfo(infos[4]);
                final String productImagesData = trimDataInfo(infos[5]);
                final String productOrigPriceData = trimDataInfo(infos[7]).replace(",", "");
                final String productSalePriceData = trimDataInfo(infos[8]).replace(",", "");
                final String productUrl = trimDataInfo(infos[12]);
                if (!productUrl.startsWith("http")) { // skip title line
                    continue;
                }
                final String[] productImageUrls = productImagesData.split(",");
                float productPrice = -1, productOrigPrice = -1;
                if (!productOrigPriceData.isEmpty()) {
                    productOrigPrice = productPrice = Float.parseFloat(productOrigPriceData);
                }
                if (!productSalePriceData.isEmpty()) { // When sale price is empty, we just use original price
                    productPrice = Float.parseFloat(productSalePriceData);
                    if (productOrigPrice < 0) {
                        productOrigPrice = productPrice;
                    }
                }
                final ProductItemInfo productInfo = new ProductItemInfo(productId, productName,
                        categoryId, categoryName,
                        "","by aliexpress", -1, productImageUrl, productImageUrls, productUrl,
                        productPrice, productOrigPrice, -1, -1);
                productList.add(productInfo);
            }
            return productList;
        } catch (Exception e) {
            Log.e(TAG, "Failed to parse Product CSV file, data:"+lineData+", exception: ", e);
            return null;
        } finally {
            if (is != null) {
                is.close();
            }
        }
    }*/

}
