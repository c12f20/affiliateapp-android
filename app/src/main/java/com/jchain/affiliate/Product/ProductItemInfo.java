package com.jchain.affiliate.Product;

import android.os.Bundle;

import com.jchain.affiliate.Advertisement.AdsManager;

public final class ProductItemInfo {
    public String id = null;
    public String name = null;
    public String categoryId = null;
    public String categoryName = null;
    public String vendorName = null;
    public String providerName = null;
    public int providerId = -1;
    public String productMainImageURL = null;
    public String[] productImageURLs = null;
    public String productURL = null;
    public float origPrice = 0;
    public float price = 0;
    public float discountRate = 0;
    public float rating = 0;
    public int ratingCount = 0;
    public boolean isHotProduct = false;

    public ProductItemInfo() {
    }

    private static final String BUNDLE_KEY_ID = "id";
    private static final String BUNDLE_KEY_NAME = "name";
    private static final String BUNDLE_KEY_CATEGORY_ID = "category_id";
    private static final String BUNDLE_KEY_CATEGORY_NAME = "category_name";
    private static final String BUNDLE_KEY_PRODUCT_URL = "product_url";
    private static final String BUNDLE_KEY_PRODUCT_IMAGE_URL = "product_image_url";
    private static final String BUNDLE_KEY_PROVIDER_ID = "provider_id";
    private static final String BUNDLE_KEY_PROVIDER_NAME = "provider_name";
    private static final String BUNDLE_KEY_PRICE = "price";

    public ProductItemInfo(final Bundle bundle) {
        this.id = bundle.getString(BUNDLE_KEY_ID);
        this.name = bundle.getString(BUNDLE_KEY_NAME);
        this.categoryId = bundle.getString(BUNDLE_KEY_CATEGORY_ID);
        this.categoryName = bundle.getString(BUNDLE_KEY_CATEGORY_NAME);
        this.productURL = bundle.getString(BUNDLE_KEY_PRODUCT_URL);
        this.productMainImageURL = bundle.getString(BUNDLE_KEY_PRODUCT_IMAGE_URL);
        this.providerId = bundle.getInt(BUNDLE_KEY_PROVIDER_ID);
        this.providerName = bundle.getString(BUNDLE_KEY_PROVIDER_NAME);
        this.price = bundle.getFloat(BUNDLE_KEY_PRICE, price);
    }

    public Bundle buildBundleInfo() {
        final Bundle info = new Bundle();
        info.putString(BUNDLE_KEY_ID, id);
        info.putString(BUNDLE_KEY_NAME, name);
        info.putString(BUNDLE_KEY_CATEGORY_ID, categoryId);
        info.putString(BUNDLE_KEY_CATEGORY_NAME, categoryName);
        info.putString(BUNDLE_KEY_PRODUCT_URL, productURL);
        info.putString(BUNDLE_KEY_PRODUCT_IMAGE_URL, productMainImageURL);
        info.putInt(BUNDLE_KEY_PROVIDER_ID, providerId);
        info.putString(BUNDLE_KEY_PROVIDER_NAME, providerName);
        info.putFloat(BUNDLE_KEY_PRICE, price);
        return info;
    }

    public ProductItemInfo(final String id, final String name,
                           final String categoryId, final String categoryName,
                           final String vendorName, final String providerName, final int providerId,
                           final String mainImageURL, final String[] imageURLs, final String productURL,
                           final float price, final float origPrice, final float rating, final int ratingCount) {
        this.id = id;
        this.name = name;
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.vendorName = vendorName;
        this.providerName = providerName;
        this.providerId = providerId;
        this.productMainImageURL = mainImageURL;
        this.productImageURLs = imageURLs;
        this.productURL = productURL;
        this.price = price;
        this.origPrice = origPrice;
        if (price < origPrice) {
            discountRate = (origPrice-price)*100/origPrice;
        } else {
            discountRate = 100;
        }
        this.rating = rating;
        this.ratingCount = ratingCount;
    }

    private static final String ADS_NAME_GOOGLE = "adsGoogle";
    private static final String ADS_NAME_FACEBOOK = "adsFacebook";
    public static ProductItemInfo createProductAdsItem(final @AdsManager.AdsType int adsType) {
        final ProductItemInfo item = new ProductItemInfo();
        switch(adsType) {
            case AdsManager.ADSTYPE_FACEBOOK_AUDIENCE:
                item.name = ADS_NAME_FACEBOOK;
                break;
            case AdsManager.ADSTYPE_GOOGLE_ADMOB:
                item.name = ADS_NAME_GOOGLE;
            case AdsManager.ADSTYPE_NONE:
            default:
                break;
        }
        return item;
    }

    public @AdsManager.AdsType int getAdsType() {
        if (name.equals(ADS_NAME_GOOGLE)) {
            return AdsManager.ADSTYPE_GOOGLE_ADMOB;
        } else if (name.equals(ADS_NAME_FACEBOOK)) {
            return AdsManager.ADSTYPE_FACEBOOK_AUDIENCE;
        } else {
            return AdsManager.ADSTYPE_NONE;
        }
    }

    public boolean isHotSales() {
        return false;
    }
}
