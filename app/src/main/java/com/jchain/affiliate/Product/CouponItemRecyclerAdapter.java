package com.jchain.affiliate.Product;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jchain.affiliate.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public final class CouponItemRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;
    public CouponItemRecyclerAdapter(final Context context, final RecyclerView recyclerView) {
        mContext = context;
    }

    public interface OnItemClickListener {
        void onItemClicked(final int position);
    }

    private OnItemClickListener mOnItemClickListener = null;

    public void setOnItemClickListener(final OnItemClickListener listener) {
        mOnItemClickListener = listener;
    }

    private void notifyItemClicked(final int position) {
        if (mOnItemClickListener != null) {
            mOnItemClickListener.onItemClicked(position);
        }
    }

    private ArrayList<CouponItemInfo> mCouponsList = new ArrayList<CouponItemInfo>();

    public void updateCouponList(final List<CouponItemInfo> list) {
        if (list == null) {
            return;
        }
        mCouponsList.clear();
        mCouponsList.addAll(list);
        notifyDataSetChanged();
    }

    private static class CouponViewHolder extends RecyclerView.ViewHolder {
        private final Context mContext;
        private final View mRootView;
        private final TextView mTextDescription;
        private final TextView mTextCode;
        private final TextView mTextValidTime;

        private CouponViewHolder(final Context context, @NonNull View itemView) {
            super(itemView);
            mContext = context;
            mRootView = itemView;
            mTextDescription = itemView.findViewById(R.id.textDescription);
            mTextCode = itemView.findViewById(R.id.textCode);
            mTextValidTime = itemView.findViewById(R.id.textValidTime);
        }

        private View getItemView() {
            return mRootView;
        }

        private void updateItemView(final CouponItemInfo itemInfo) {
            mTextDescription.setText(itemInfo.description);
            mTextCode.setText(String.format(
                    mContext.getString(R.string.ID_CAPTION_COUPON_CODE_TEMPLATE),
                    itemInfo.code));
            final SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy HH:mm", Locale.US);
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            final String startTimeData = itemInfo.startTime != null ? sdf.format(itemInfo.startTime) : null;
            final String endTimeData = itemInfo.endTime != null ? sdf.format(itemInfo.endTime) : null;
            mTextValidTime.setVisibility(View.VISIBLE);
            if (!TextUtils.isEmpty(startTimeData) && !TextUtils.isEmpty(endTimeData)) {
                mTextValidTime.setText(String.format(
                        mContext.getString(R.string.ID_CAPTION_COUPON_VALID_TIME_TEMPLATE),
                        startTimeData, endTimeData));
            } else if (!TextUtils.isEmpty(endTimeData)) {
                mTextValidTime.setText(String.format(
                        mContext.getString(R.string.ID_CAPTION_COUPON_VALID_END_TIME_TEMPLATE),
                        endTimeData));
            } else {
                mTextValidTime.setVisibility(View.GONE);
            }
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(mContext);
        final View view = inflater.inflate(R.layout.view_coupon_item, viewGroup, false);
        return new CouponViewHolder(mContext, view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof CouponViewHolder) {
            final CouponItemInfo item = mCouponsList.get(position);
            final CouponViewHolder couponViewHolder = (CouponViewHolder) viewHolder;
            couponViewHolder.updateItemView(item);
            couponViewHolder.getItemView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    notifyItemClicked(couponViewHolder.getAdapterPosition());
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mCouponsList.size();
    }
}
