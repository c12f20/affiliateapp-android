package com.jchain.affiliate.Product;

public final class PromotionItemInfo {
    public String imageURL = null;
    public String clickURL = null;
    public String providerName = null;

    public PromotionItemInfo(final String imageURL, final String clickURL, final String providerName) {
        this.imageURL = imageURL;
        this.clickURL = clickURL;
        this.providerName = providerName;
    }
}
