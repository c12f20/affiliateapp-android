package com.jchain.affiliate.Product;

import android.content.Context;
import android.graphics.Paint;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jchain.affiliate.Advertisement.AdsManager;
import com.jchain.affiliate.R;
import com.jchain.affiliate.UI.Widgets.OnlineImageViewUtils;

import java.util.ArrayList;
import java.util.Locale;

public final class ProductItemRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "ProductItemRecycAdapter";

    private final Context mContext;
    private final Handler mHandler;
    public ProductItemRecyclerAdapter(final Context context, final RecyclerView recyclerView) {
        mContext = context;
        mHandler = new Handler(context.getMainLooper());
        configRecyclerView(recyclerView);
    }

    private static final int VISIBLE_THRESHOLD = 10;
    private boolean mIsLoading = false;
    private void configRecyclerView(final RecyclerView recyclerView) {
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        if (linearLayoutManager == null) {
            Log.w(TAG, "Can't use LoadMore feature of this adapter on a RecyclerView without LinearLayoutManager");
            return;
        }
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                final int totalItemCount = linearLayoutManager.getItemCount();
                final int lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!mIsLoading && totalItemCount <= (lastVisibleItem + VISIBLE_THRESHOLD)) {
                    notifyLoadMore();
                    mIsLoading = true;
                }
            }
        });
    }

    public interface LoadMoreListener {
        void onNeedLoadMore();
    }

    private LoadMoreListener mLoadMoreListener = null;
    public void setLoadMoreListener(LoadMoreListener listener) {
        mLoadMoreListener = listener;
    }
    private void notifyLoadMore() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mLoadMoreListener != null) {
                    mLoadMoreListener.onNeedLoadMore();
                }
            }
        });
    }

    public void setLoaded() {
        mIsLoading = false;
    }

    public interface OnItemClickListener {
        void onItemClicked(final int position);
    }

    private OnItemClickListener mOnItemClickListener = null;
    public void setOnItemClickListener(final OnItemClickListener listener) {
        mOnItemClickListener = listener;
    }

    private void notifyItemClicked(final int position) {
        if (mOnItemClickListener != null) {
            mOnItemClickListener.onItemClicked(position);
        }
    }

    public interface OnItemTrackClickListener {
        void onItemTrackClicked(final int position);
    }
    private OnItemTrackClickListener mOnItemTrackClickListener = null;
    public void setOnItemTrackClickListener(final OnItemTrackClickListener listener) {
        mOnItemTrackClickListener = listener;
    }

    private ArrayList<ProductItemInfo> mProductsList = new ArrayList<ProductItemInfo>();
    public void updateProductList(final ArrayList<ProductItemInfo> list) {
        if (list == null) {
            return;
        }
        mProductsList.clear();
        mProductsList.addAll(list);
        addAdvertismentsInList();
        notifyDataSetChanged();
    }

    private static final int LIST_PRODUCTS_PER_ADVERTISEMENT = 10;
    private int mAdvCount = 0;
    private @AdsManager.AdsType int mAdsTypes[] = new int[]{
            /*AdsManager.ADSTYPE_GOOGLE_ADMOB,*/ AdsManager.ADSTYPE_FACEBOOK_AUDIENCE};
    private int mAdsTypeIdx = 0;
    private void addAdvertismentsInList() {
        int count = mProductsList.size();
        mAdvCount = count/LIST_PRODUCTS_PER_ADVERTISEMENT;
        for (int i=0; i < mAdvCount; i++) {
            final @AdsManager.AdsType int adsType = mAdsTypes[mAdsTypeIdx++];
            mAdsTypeIdx %= mAdsTypes.length;
            final ProductItemInfo advertisementItem = ProductItemInfo.createProductAdsItem(adsType);
            final int advertisementIdx = (LIST_PRODUCTS_PER_ADVERTISEMENT+1)*(i+1)-1;
            if (advertisementIdx < count) {
                mProductsList.add(advertisementIdx, advertisementItem);
                count++;
            } else {
                mAdvCount--; // remove one as it's not inserted
            }
        }
    }

    private int getProductIdx(final int displayIdx) {
        for (int i=mAdvCount; i > 0; i--) {
            final int advertisementIdx = (LIST_PRODUCTS_PER_ADVERTISEMENT+1)*i-1;
            if (displayIdx > advertisementIdx) {
                return displayIdx-i;
            }
        }
        return displayIdx;
    }

    public int getDisplayIdx(final int productIdx) {
        int advCount = productIdx/LIST_PRODUCTS_PER_ADVERTISEMENT;
        if (productIdx % LIST_PRODUCTS_PER_ADVERTISEMENT == 0) { // just before one adv, we shall remove the tail one
            advCount--;
        }
        return productIdx+advCount;
    }

    public void showLoadingProductList() {
        mProductsList.add(null);
        notifyItemInserted(mProductsList.size()-1);
    }

    public void hideLoadingProductList() {
        if (mProductsList.isEmpty()) {
            return;
        }
        if (mProductsList.get(mProductsList.size()-1) == null) {
            mProductsList.remove(mProductsList.size() - 1);
            notifyItemRemoved(mProductsList.size());
        }
    }

    private static class ProductViewHolder extends RecyclerView.ViewHolder {
        private final View mRootView;
        private final ImageView mImageProduct;
        private final ImageView mIconHot;
        private final TextView mTextProductName;
        private final TextView mTextVendorName;
        private final TextView mTextProviderName;
        private final TextView mTextProductPrice;
        private final TextView mTextProductOrigPrice;
        private final TextView mTextProductDiscountRate;
        private final RatingBar mRatingBarProductRating;
        private final TextView mTextRatingCount;
        private final TextView mBtnTrack;
        private final OnlineImageViewUtils mOnlineImageViewUtils;
        private final OnItemTrackClickListener mOnItemTrackClickListener;
        private final boolean mNeedTrackFeature;
        private int mItemIndex = -1;

        private ProductViewHolder(final Context context,
                                  @NonNull View itemView,
                                  OnItemTrackClickListener trackClickListener) {
            super(itemView);
            mRootView = itemView;
            mImageProduct = itemView.findViewById(R.id.imgProductPhoto);
            mIconHot = itemView.findViewById(R.id.iconHot);
            mTextProductName = itemView.findViewById(R.id.textProductName);
            mTextVendorName = itemView.findViewById(R.id.textVendorName);
            mTextProviderName = itemView.findViewById(R.id.textDataProviderName);
            mTextProductPrice = itemView.findViewById(R.id.textProductPrice);
            mTextProductOrigPrice = itemView.findViewById(R.id.textProductOrigPrice);
            mTextProductDiscountRate = itemView.findViewById(R.id.textProductDiscountRate);
            mRatingBarProductRating = itemView.findViewById(R.id.ratingBarProductRate);
            mTextRatingCount = itemView.findViewById(R.id.textRatingCount);

            mOnItemTrackClickListener = trackClickListener;
            mNeedTrackFeature = (mOnItemTrackClickListener != null);

            mBtnTrack = itemView.findViewById(R.id.btnTrack);

            mOnlineImageViewUtils = new OnlineImageViewUtils(context);

            if (mNeedTrackFeature) {
                mBtnTrack.setVisibility(View.VISIBLE);
                mBtnTrack.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        notifyItemTrackClicked(mItemIndex);
                    }
                });
            }
        }

        private View getItemView() {
            return mRootView;
        }

        private void updateItemView(final int itemIndex, final ProductItemInfo itemInfo) {
            mItemIndex = itemIndex;
            mIconHot.setVisibility(itemInfo.isHotSales() ? View.VISIBLE : View.GONE);
            if (itemInfo.productMainImageURL != null) {
                mOnlineImageViewUtils.loadImage(mImageProduct, itemInfo.productMainImageURL);
            }
            mTextProductName.setText(itemInfo.name);
            mTextVendorName.setText(itemInfo.vendorName);
            mTextProviderName.setText(itemInfo.providerName);
            mTextProductPrice.setText(String.format(Locale.US, "$%.2f", itemInfo.price));
            if (itemInfo.discountRate < 100) {
                mTextProductOrigPrice.setVisibility(View.VISIBLE);
                mTextProductOrigPrice.setText(String.format(Locale.US, "[$%.2f]", itemInfo.origPrice));
                mTextProductOrigPrice.setPaintFlags(mTextProductOrigPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                mTextProductDiscountRate.setVisibility(View.VISIBLE);
                mTextProductDiscountRate.setText(String.format(Locale.US, "-%d%%", Math.round(itemInfo.discountRate)));
            } else {
                mTextProductOrigPrice.setVisibility(View.GONE);
                mTextProductDiscountRate.setVisibility(View.GONE);
            }

            if (itemInfo.rating >= 0 && itemInfo.ratingCount > 0) {
                mRatingBarProductRating.setVisibility(View.VISIBLE);
                mRatingBarProductRating.setRating(itemInfo.rating);
                mTextRatingCount.setVisibility(View.VISIBLE);
                mTextRatingCount.setText(String.format(Locale.US, "%d", itemInfo.ratingCount));
            } else {
                mRatingBarProductRating.setVisibility(View.GONE);
                mTextRatingCount.setVisibility(View.GONE);
            }
        }

        private void notifyItemTrackClicked(final int position) {
            if (mOnItemTrackClickListener != null) {
                mOnItemTrackClickListener.onItemTrackClicked(position);
            }
        }
    }

    private static class LoadingViewHolder extends RecyclerView.ViewHolder {
        private LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }

    private static class AdvertiseViewHolder extends RecyclerView.ViewHolder {
        private final View mAdView;
        private AdvertiseViewHolder(final Context context, @NonNull View itemView, final @AdsManager.AdsType int adsType) {
            super(itemView);
            final AdsManager adsManager = AdsManager.getInstance();
            switch(adsType) {
                case AdsManager.ADSTYPE_FACEBOOK_AUDIENCE:
                    mAdView = adsManager.createFacebookAdView(context);
                    break;
                case AdsManager.ADSTYPE_GOOGLE_ADMOB:
                case AdsManager.ADSTYPE_NONE:
                default:
                    //mAdView = adsManager.createGoogleAdView(context);
                    mAdView = adsManager.createFacebookAdView(context);
                    break;
            }

            final RelativeLayout container = (RelativeLayout)itemView;
            container.addView(mAdView, new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        }

        private void resumeAdvertisement() {
            /*if (mAdView instanceof com.google.android.gms.ads.AdView) {
                final com.google.android.gms.ads.AdView googleAdView = (com.google.android.gms.ads.AdView)mAdView;
                googleAdView.resume();
            }*/
        }

        private void pauseAdvertisement() {
            /*if (mAdView instanceof com.google.android.gms.ads.AdView) {
                final com.google.android.gms.ads.AdView googleAdView = (com.google.android.gms.ads.AdView) mAdView;
                googleAdView.pause();
            }*/
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(mContext);
        if (viewType == VIEW_TYPE_PRODUCT) {
            final View view = inflater.inflate(R.layout.view_product_item, viewGroup, false);
            return new ProductViewHolder(mContext, view, mOnItemTrackClickListener);
        } else if (viewType == VIEW_TYPE_GOOGLE_ADS) {
            final View view = inflater.inflate(R.layout.view_product_advertisement_item, viewGroup, false);
            return new AdvertiseViewHolder(mContext, view, AdsManager.ADSTYPE_GOOGLE_ADMOB);
        } else if (viewType == VIEW_TYPE_FACEBOOK_ADS) {
            final View view = inflater.inflate(R.layout.view_product_advertisement_item, viewGroup, false);
            return new AdvertiseViewHolder(mContext, view, AdsManager.ADSTYPE_FACEBOOK_AUDIENCE);
        } else {
            final View view = inflater.inflate(R.layout.view_loading_item, viewGroup, false);
            return new LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof ProductViewHolder) {
            ProductItemInfo item = mProductsList.get(position);
            final int productIdx = getProductIdx(position);
            final ProductViewHolder productViewHolder = (ProductViewHolder) viewHolder;
            productViewHolder.updateItemView(productIdx, item);
            productViewHolder.getItemView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    notifyItemClicked(productIdx);
                }
            });
        } else if (viewHolder instanceof AdvertiseViewHolder) {
            final AdvertiseViewHolder advertiseViewHolder = (AdvertiseViewHolder) viewHolder;
            advertiseViewHolder.resumeAdvertisement();
        }
    }

    @Override
    public void onViewRecycled(@NonNull RecyclerView.ViewHolder viewHolder) {
        if (viewHolder instanceof AdvertiseViewHolder) {
            final AdvertiseViewHolder advertiseViewHolder = (AdvertiseViewHolder) viewHolder;
            advertiseViewHolder.pauseAdvertisement();
        }
        super.onViewRecycled(viewHolder);
    }

    @Override
    public int getItemCount() {
        return mProductsList.size();
    }

    private static final int VIEW_TYPE_LOADING = 0;
    private static final int VIEW_TYPE_PRODUCT = 1;
    private static final int VIEW_TYPE_GOOGLE_ADS = 2;
    private static final int VIEW_TYPE_FACEBOOK_ADS = 3;

    @Override
    public int getItemViewType(int position) {
        if (position < 0 || position >= mProductsList.size()) {
            return VIEW_TYPE_LOADING;
        }
        final ProductItemInfo itemInfo = mProductsList.get(position);
        if (itemInfo == null) {
            return VIEW_TYPE_LOADING;
        }
        if (itemInfo.id != null) {
            return VIEW_TYPE_PRODUCT;
        }

        final @AdsManager.AdsType int adsType = itemInfo.getAdsType();
        switch(adsType) {
            case AdsManager.ADSTYPE_FACEBOOK_AUDIENCE:
                return VIEW_TYPE_FACEBOOK_ADS;
            case AdsManager.ADSTYPE_GOOGLE_ADMOB:
            case AdsManager.ADSTYPE_NONE:
            default:
                return VIEW_TYPE_GOOGLE_ADS;
        }
    }
}
