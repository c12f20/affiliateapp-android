package com.jchain.affiliate.Product;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.jchain.affiliate.R;
import com.jchain.affiliate.UI.Fragments.BasicWebViewFragment;
import com.jchain.affiliate.UI.Widgets.OnlineImageViewUtils;

public final class CouponsDetailInfoFragment extends BasicWebViewFragment {

    private static final String ARG_PARAM_COUPON = "param.coupon";

    public static CouponsDetailInfoFragment newInstance(final String title, final CouponItemInfo coupon) {
        Bundle args = new Bundle();
        args.putString(ARG_PARAM_TITLE, title);
        args.putString(ARG_PARAM_WEBVIEW_URL, coupon.couponURL);
        args.putBoolean(ARG_PARAM_BACK_ENABLED, true);
        args.putBundle(ARG_PARAM_COUPON, coupon.buildBundleInfo());

        CouponsDetailInfoFragment fragment = new CouponsDetailInfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private OnlineImageViewUtils mOnlineImageViewUtils = null;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mOnlineImageViewUtils = new OnlineImageViewUtils(getContext());
        initData();
    }

    private CouponItemInfo mCoupon = null;
    private void initData() {
        Bundle args = getArguments();
        if (args != null && args.containsKey(ARG_PARAM_COUPON)) {
            final Bundle couponBundle = args.getBundle(ARG_PARAM_COUPON);
            if (couponBundle != null) {
                mCoupon = new CouponItemInfo(couponBundle);
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_coupon, container, false);
        initView(rootView);
        return rootView;
    }

    private ImageView mIconVendorLogo = null;
    private TextView mTextVendorName = null;
    private TextView mTextCouponCode = null;
    @Override
    protected void initView(View rootView) {
        super.initView(rootView);

        final View.OnClickListener codeClicklistener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String couponCode = mTextCouponCode.getText().toString();
                if (!couponCode.isEmpty()
                && !couponCode.equals(getString(R.string.ID_CAPTION_NO_COUPON_CODE))) {
                    copyCodeIntoClipboard(couponCode);
                }
            }
        };
        mIconVendorLogo = rootView.findViewById(R.id.iconVendorLogo);
        mIconVendorLogo.setOnClickListener(codeClicklistener);
        mTextVendorName = rootView.findViewById(R.id.textVendorName);
        mTextVendorName.setOnClickListener(codeClicklistener);
        mTextCouponCode = rootView.findViewById(R.id.textCouponCode);
        mTextCouponCode.setOnClickListener(codeClicklistener);

        if (mCoupon != null) {
            showCouponCode(mCoupon);
            copyCodeIntoClipboard(mCoupon.code);
        }
    }

    private void showCouponCode(final CouponItemInfo coupon) {
        mTextCouponCode.setVisibility(View.VISIBLE);
        if (coupon == null) { // Show no coupon code
            mTextCouponCode.setText(R.string.ID_CAPTION_NO_COUPON_CODE);
            return;
        }
        // Show coupon info
        mTextCouponCode.setText(String.format(getString(R.string.ID_CAPTION_COUPON_CODE_TEMPLATE), coupon.code));
        if (!TextUtils.isEmpty(coupon.vendorLogoURL)) {
            mOnlineImageViewUtils.loadImage(mIconVendorLogo, coupon.vendorLogoURL);
            mIconVendorLogo.setVisibility(View.VISIBLE);
        } else if (!TextUtils.isEmpty(coupon.vendorName)) {
            mTextVendorName.setVisibility(View.VISIBLE);
            mTextVendorName.setText(coupon.vendorName);
        }
    }

    private void copyCodeIntoClipboard(final String code) {
        final Context context = getContext();
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        if (clipboard != null) {
            ClipData clip = ClipData.newPlainText("Coupon Code", code);
            clipboard.setPrimaryClip(clip);
            // Show saved message
            final String msgSaved = String.format(getString(R.string.ID_MSG_COUPON_CODE_SAVED_TEMPLATE), code);
            Toast.makeText(getContext(), msgSaved, Toast.LENGTH_SHORT).show();
        }
    }
}
