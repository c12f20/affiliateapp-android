package com.jchain.affiliate.Account.OAuth;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public final class OAuthManagement {
    private static final String TAG = "OAuthManagement";

    private static OAuthManagement sInstance = null;

    public static OAuthManagement getInstance(final Context context) {
        if (sInstance == null) {
            sInstance = new OAuthManagement(context);
        }
        return sInstance;
    }

    private final FacebookOAuthHelper mFacebookOAuthHelper;
    private final GoogleOAuthHelper mGoogleOAuthHelper;
    private OAuthManagement(final Context context) {
        mFacebookOAuthHelper = new FacebookOAuthHelper(context);
        mFacebookOAuthHelper.init();
        mGoogleOAuthHelper = new GoogleOAuthHelper(context);
        mGoogleOAuthHelper.init();
    }

    @Override
    protected void finalize() throws Throwable {
        destroy();
        super.finalize();
    }

    public void destroy() {
        mFacebookOAuthHelper.destroy();
        mGoogleOAuthHelper.destroy();
    }

    public interface ResultListener {
        void onResult(final @BaseOAuthHelper.ResultValue int result, final OAuthInfo info);
    }

    private ResultListener mCurResultListener = null;
    private void notifyOAuthResult(final @BaseOAuthHelper.ResultValue int result, final OAuthInfo info) {
        if (mCurResultListener != null) {
            mCurResultListener.onResult(result, info);
        }
    }

    private BaseOAuthHelper mCurOAuthHelper = null;
    private final BaseOAuthHelper.OAuthInfoListener mOAuthInfoListener = new BaseOAuthHelper.OAuthInfoListener() {
        @Override
        public void onGotInfo(OAuthInfo info) {
            Log.d(TAG, "OAuthInfoListener, info: "+ (info == null ? null : info.id));
            notifyOAuthResult(BaseOAuthHelper.RESULT_SUCCESS, info);
        }
    };
    private final BaseOAuthHelper.OAuthResultListener mOAuthResultListener = new BaseOAuthHelper.OAuthResultListener() {
        @Override
        public void onLogin(final @BaseOAuthHelper.ResultValue int result) {
            Log.d(TAG, "OAuthResultListener, result: "+result);
            if (result != BaseOAuthHelper.RESULT_SUCCESS) {
                notifyOAuthResult(result, null);
                return;
            }
            mCurOAuthHelper.getOAuthInfo(mOAuthInfoListener);
        }

        @Override
        public void onLogout(int result) {
        }
    };

    public void loginFacebook(final Activity activity, final ResultListener listener) {
        // Ensure to logout previous login account
        logout();

        mCurOAuthHelper = mFacebookOAuthHelper;
        mCurResultListener = listener;
        mCurOAuthHelper.login(activity, mOAuthResultListener);
    }

    public void loginGoogle(final Activity activity, final ResultListener listener) {
        // Ensure to logout previous login account
        logout();

        mCurOAuthHelper = mGoogleOAuthHelper;
        mCurResultListener = listener;
        mCurOAuthHelper.login(activity, mOAuthResultListener);
    }

    public void logout() {
        if (mCurOAuthHelper != null) {
            mCurOAuthHelper.logout();
            mCurOAuthHelper = null;
        }
    }

    public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mCurOAuthHelper != null) {
            return mCurOAuthHelper.onActivityResult(requestCode, resultCode, data);
        }
        return false;
    }
}
