package com.jchain.affiliate.Account.OAuth;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.IntDef;
import androidx.annotation.Nullable;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public abstract class BaseOAuthHelper {

    @Retention(RetentionPolicy.SOURCE)

    @IntDef({RESULT_SUCCESS, RESULT_FAILURE_NETWORK, RESULT_FAILURE_REJECT, RESULT_FAILURE_GENERAL})
    public @interface ResultValue {}
    public static final int RESULT_SUCCESS = 0;
    public static final int RESULT_FAILURE_NETWORK = -1;
    public static final int RESULT_FAILURE_REJECT = -2;
    public static final int RESULT_FAILURE_GENERAL = -3;


    public interface OAuthResultListener {
        void onLogin(final @ResultValue int result);
        void onLogout(final @ResultValue int result);
    }

    protected OAuthResultListener mOAuthListener = null;
    protected void notifyLoginResult(final @ResultValue int result) {
        if (mOAuthListener != null) {
            mOAuthListener.onLogin(result);
        }
    }

    protected void notifyLogoutResult(final @ResultValue int result) {
        if (mOAuthListener != null) {
            mOAuthListener.onLogout(result);
        }
    }

    protected Context mContext;
    public BaseOAuthHelper(final Context context) {
        mContext = context;
    }

    public abstract void init();
    public abstract void destroy();

    public abstract boolean hasLogin();

    public void login(final Activity activity, final OAuthResultListener listener) {
        mOAuthListener = listener;
    }
    public void logout() {
        mOAuthListener = null;
    }

    public interface OAuthInfoListener {
        void onGotInfo(final OAuthInfo info);
    }

    protected OAuthInfoListener mOAuthInfoListener = null;
    protected void notifyGotInfo(final OAuthInfo info) {
        if (mOAuthInfoListener != null) {
            mOAuthInfoListener.onGotInfo(info);
        }
    }

    protected void getOAuthInfo(final OAuthInfoListener resultListener) {
        mOAuthInfoListener = resultListener;
    }

    public abstract boolean onActivityResult(int requestCode, int resultCode, @Nullable Intent data);
}
