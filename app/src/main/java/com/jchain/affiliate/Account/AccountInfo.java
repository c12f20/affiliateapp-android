package com.jchain.affiliate.Account;

import androidx.annotation.IntDef;
import android.util.Log;

import com.jchain.affiliate.Settings.VehicleProfile.VehicleInfo;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class AccountInfo {
    private static final String TAG = "AccountInfo";

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({ACCOUNT_TYPE_GENERAL, ACCOUNT_TYPE_FACEBOOK_OAUTH, ACCOUNT_TYPE_GOOGLE_OAUTH, ACCOUNT_TYPE_TWITTER_OAUTH})
    public @interface AccountType {}
    public static final int ACCOUNT_TYPE_GENERAL = 1;
    public static final int ACCOUNT_TYPE_FACEBOOK_OAUTH = 2;
    public static final int ACCOUNT_TYPE_GOOGLE_OAUTH = 3;
    public static final int ACCOUNT_TYPE_TWITTER_OAUTH = 4;

    @IntDef({ACCOUNT_GENDER_UNKNOWN, ACCOUNT_GENDER_MALE, ACCOUNT_GENDER_FEMALE, ACCOUNT_GENDER_OTHER})
    public @interface AccountGender {}
    public static final int ACCOUNT_GENDER_UNKNOWN = -1;
    public static final int ACCOUNT_GENDER_MALE = 1;
    public static final int ACCOUNT_GENDER_FEMALE = 2;
    public static final int ACCOUNT_GENDER_OTHER = 3;

    public static final String PATTERN_EMAIL = "[\\w.]+@.[\\w.]+\\.\\w+";
    public static final String PATTERN_PHONE = "[+]?\\d{10,13}";

    public @AccountType int type = ACCOUNT_TYPE_GENERAL;
    public String name = null;
    public String firstName = null;
    public String lastName = null;
    public @AccountGender int gender = ACCOUNT_GENDER_UNKNOWN;
    public String email = null;
    public String phone = null;
    public String avatarURL = null;
    public Date birthday = null;
    public String accessToken = null;
    private List<VehicleInfo> mVehiclesList = null;

    @Override
    public Object clone() throws CloneNotSupportedException {
        AccountInfo newAccount = (AccountInfo) super.clone();
        newAccount.type = this.type;
        newAccount.name = this.name;
        newAccount.firstName = this.firstName;
        newAccount.lastName = this.lastName;
        newAccount.gender = this.gender;
        newAccount.email = this.email;
        newAccount.phone = this.phone;
        newAccount.avatarURL = this.avatarURL;
        newAccount.birthday = this.birthday;
        newAccount.accessToken = this.accessToken;
        newAccount.mVehiclesList.addAll(this.mVehiclesList);
        return newAccount;
    }

    public String getDisplayName() {
        String displayName = "";
        if (firstName != null || lastName != null) {
            if (firstName != null) {
                displayName = firstName.trim();
            }
            if (lastName != null) {
                if (displayName.isEmpty()) {
                    displayName += lastName.trim();
                } else {
                    displayName += " " + lastName.trim();
                }
            }
        }
        if (displayName.isEmpty()) {
            displayName = name;
        }
        return displayName;
    }

    public void addVehicle(final VehicleInfo vehicle) {
        if (vehicle == null) {
            Log.w(TAG, "Failed to add vehicle, ignore null vehicle info");
            return;
        }
        if (mVehiclesList == null) {
            mVehiclesList = new ArrayList<VehicleInfo>();
        }
        mVehiclesList.add(vehicle);
    }

    public void removeVehicle(final VehicleInfo vehicle) {
        if (vehicle == null) {
            Log.w(TAG, "Failed to remove vehicle, ignore null vehicle info");
            return;
        }
        if (mVehiclesList != null) {
            mVehiclesList.remove(vehicle);
        }
    }

    public void setVehicles(final List<VehicleInfo> vehicles) {
        mVehiclesList = vehicles;
    }

    public List<VehicleInfo> getVehicles() {
        return mVehiclesList;
    }

    public boolean hasSyncedVehicles() {
        return mVehiclesList != null;
    }


    private int mCurVehicleIdx = -1;
    public void setVehicleIndex(final int index) {
        mCurVehicleIdx = index;
    }

    public int getVehicleIndex() {
        if (mVehiclesList == null
        || mCurVehicleIdx < 0 || mCurVehicleIdx > mVehiclesList.size()-1) {
            mCurVehicleIdx = -1;
        }
        return mCurVehicleIdx;
    }
}
