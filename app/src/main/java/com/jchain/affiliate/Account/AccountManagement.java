package com.jchain.affiliate.Account;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.jchain.affiliate.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

public final class AccountManagement {
    private static final String TAG = "AccountManagement";

    private static AccountManagement sInstance = null;

    public static AccountManagement getInstance(final Context context) {
        if (sInstance == null) {
            sInstance = new AccountManagement(context);
        }
        return sInstance;
    }

    private final SharedPreferences mSP;

    public final String mPrefKeyProfileToken;
    public final String mPrefKeyProfileName;
    public final String mPrefKeyProfileFirstName;
    public final String mPrefKeyProfileLastName;
    public final String mPrefKeyProfileBirthday;
    public final String mPrefKeyProfileGender;
    public final String mPrefKeyProfileAvatar;
    public final String mPrefKeyProfileEmail;
    public final String mPrefKeyProfilePhone;
    public final String mPrefKeyProfileVehicles;

    private final String mProfileBirthdayDateFormat;
    private final String mProfileBirthdayDefaultValue;

    private final String mProfileGenderMale;
    private final String mProfileGenderFemale;
    private final String mProfileGenderOther;

    private AccountManagement(final Context context) {
        mSP = PreferenceManager.getDefaultSharedPreferences(context);

        mPrefKeyProfileToken = context.getString(R.string.PREFKEY_PROFILE_TOKEN);
        mPrefKeyProfileName = context.getString(R.string.PREFKEY_PROFILE_NAME);
        mPrefKeyProfileFirstName = context.getString(R.string.PREFKEY_PROFILE_FIRSTNAME);
        mPrefKeyProfileLastName = context.getString(R.string.PREFKEY_PROFILE_LASTNAME);
        mPrefKeyProfileBirthday = context.getString(R.string.PREFKEY_PROFILE_BIRTHDAY);
        mPrefKeyProfileGender = context.getString(R.string.PREFKEY_PROFILE_GENDER);
        mPrefKeyProfileAvatar = context.getString(R.string.PREFKEY_PROFILE_AVATAR);
        mPrefKeyProfileEmail = context.getString(R.string.PREFKEY_PROFILE_EMAIL);
        mPrefKeyProfilePhone = context.getString(R.string.PREFKEY_PROFILE_PHONE);
        mPrefKeyProfileVehicles = context.getString(R.string.PREFKEY_PROFILE_VEHICLES);

        mProfileBirthdayDateFormat = context.getString(R.string.ID_DEFAULT_PROFILE_BIRTHDAY_DATE_FORMAT);
        mProfileBirthdayDefaultValue = context.getString(R.string.ID_DEFAULT_PROFILE_BIRTHDAY);

        mProfileGenderMale = context.getString(R.string.ID_GENDER_VALUE_MALE);
        mProfileGenderFemale = context.getString(R.string.ID_GENDER_VALUE_FEMALE);
        mProfileGenderOther = context.getString(R.string.ID_GENDER_VALUE_OTHER);
    }

    // Current Account Recorder methods
    private AccountInfo mCurAccount = null;
    public void setCurAccountInfo(final AccountInfo account) {
        mCurAccount = account;
        updateSPData();
    }

    public AccountInfo getCurAccountInfo() {
        return mCurAccount;
    }

    public boolean hasAccountLogin() {
        return mCurAccount != null;
    }

    public String loadCurAccountToken() {
        return mSP.getString(mPrefKeyProfileToken, "");
    }

    private void clearSPData() {
        final SharedPreferences.Editor editor = mSP.edit();
        editor.remove(mPrefKeyProfileToken);
        editor.remove(mPrefKeyProfileName);
        editor.remove(mPrefKeyProfileFirstName);
        editor.remove(mPrefKeyProfileLastName);
        editor.remove(mPrefKeyProfileBirthday);
        editor.remove(mPrefKeyProfileGender);
        editor.remove(mPrefKeyProfileAvatar);
        editor.remove(mPrefKeyProfileEmail);
        editor.remove(mPrefKeyProfilePhone);
        editor.apply();
    }

    private void updateSPData() {
        if (mCurAccount == null) {
            clearSPData();
            return;
        }
        final SharedPreferences.Editor editor = mSP.edit();
        editor.putString(mPrefKeyProfileToken, mCurAccount.accessToken);
        editor.putString(mPrefKeyProfileName, mCurAccount.name);
        editor.putString(mPrefKeyProfileFirstName, mCurAccount.firstName);
        editor.putString(mPrefKeyProfileLastName, mCurAccount.lastName);
        if (mCurAccount.birthday != null) {
            final SimpleDateFormat sdf = new SimpleDateFormat(mProfileBirthdayDateFormat, Locale.US);
            editor.putString(mPrefKeyProfileBirthday, sdf.format(mCurAccount.birthday));
        } else {
            editor.remove(mPrefKeyProfileBirthday);
        }
        editor.putString(mPrefKeyProfileGender, getPrefGenderValueFromAccountGender(mCurAccount.gender));
        editor.putString(mPrefKeyProfileAvatar, mCurAccount.avatarURL);
        editor.putString(mPrefKeyProfileEmail, mCurAccount.email);
        editor.putString(mPrefKeyProfilePhone, mCurAccount.phone);
        editor.apply();
    }

    public AccountInfo buildUpdateInfoWithPrefKey(final String key) {
        if (mCurAccount == null) { // always return null when there is no old account info
            return null;
        }
        boolean isChange = false;
        final AccountInfo newAccountInfo = new AccountInfo();
        newAccountInfo.accessToken = mCurAccount.accessToken;
        if (key.equals(mPrefKeyProfileFirstName)) {
            newAccountInfo.firstName = mSP.getString(key, "");
            isChange = (mCurAccount.firstName == null || !mCurAccount.firstName.equals(newAccountInfo.firstName));
        } else if (key.equals(mPrefKeyProfileLastName)) {
            newAccountInfo.lastName = mSP.getString(key, "");
            isChange = (mCurAccount.lastName == null || !mCurAccount.lastName.equals(newAccountInfo.lastName));
        } else if (key.equals(mPrefKeyProfileBirthday)) {
            final SimpleDateFormat sdf = new SimpleDateFormat(mProfileBirthdayDateFormat, Locale.US);
            String birthdayValue = mSP.getString(key, mProfileBirthdayDefaultValue);
            try {
                newAccountInfo.birthday = sdf.parse(birthdayValue);
                isChange = (mCurAccount.birthday == null || !mCurAccount.birthday.equals(newAccountInfo.birthday));
            } catch (ParseException e) {
                Log.w(TAG, "Failed to parse birthday value: "+birthdayValue, e);
                newAccountInfo.birthday = null;
            }
        } else if (key.equals(mPrefKeyProfileGender)) {
            String genderValue = mSP.getString(key, "");
            newAccountInfo.gender = getAccountGenderFromPrefGenderValue(genderValue);
            isChange = (mCurAccount.gender != newAccountInfo.gender);
        } else if (key.equals(mPrefKeyProfileAvatar)) {
            newAccountInfo.avatarURL = mSP.getString(key, "");
            isChange = (mCurAccount.avatarURL == null || !mCurAccount.avatarURL.equals(newAccountInfo.avatarURL));
        } else if (key.equals(mPrefKeyProfileEmail)) {
            newAccountInfo.email = mSP.getString(key, "");
            isChange = (mCurAccount.email == null || !mCurAccount.email.equals(newAccountInfo.email));
        } else if (key.equals(mPrefKeyProfilePhone)) {
            newAccountInfo.phone = mSP.getString(key, "");
            isChange = (mCurAccount.phone == null || !mCurAccount.phone.equals(newAccountInfo.phone));
        }
        if (!isChange) { // no change, return null to indicate no need do update
            return null;
        }
        return newAccountInfo;
    }

    public void updateCurAccountInfo(final AccountInfo newAccount) {
        if (newAccount.firstName != null) {
            mCurAccount.firstName = newAccount.firstName;
        }
        if (newAccount.lastName != null) {
            mCurAccount.lastName = newAccount.lastName;
        }
        if (newAccount.birthday != null) {
            mCurAccount.birthday = newAccount.birthday;
        }
        if (newAccount.gender >= 0) {
            mCurAccount.gender = newAccount.gender;
        }
        if (newAccount.avatarURL != null) {
            mCurAccount.avatarURL = newAccount.avatarURL;
        }
        if (newAccount.email != null) {
            mCurAccount.email = newAccount.email;
        }
        if (newAccount.phone != null) {
            mCurAccount.phone = newAccount.phone;
        }
    }

    // Value convert methods
    private String getPrefGenderValueFromAccountGender(final @AccountInfo.AccountGender int gender) {
        String genderValue = mProfileGenderOther;
        switch (gender) {
            case AccountInfo.ACCOUNT_GENDER_MALE:
                genderValue = mProfileGenderMale;
                break;
            case AccountInfo.ACCOUNT_GENDER_FEMALE:
                genderValue = mProfileGenderFemale;
                break;
            case AccountInfo.ACCOUNT_GENDER_OTHER:
            case AccountInfo.ACCOUNT_GENDER_UNKNOWN:
            default:
                break;
        }
        return genderValue;
    }

    private @AccountInfo.AccountGender int getAccountGenderFromPrefGenderValue(final String genderValue) {
        if (genderValue == null || genderValue.isEmpty()) {
            return AccountInfo.ACCOUNT_GENDER_UNKNOWN;
        }
        if (genderValue.equals(mProfileGenderMale)) {
            return AccountInfo.ACCOUNT_GENDER_MALE;
        } else if (genderValue.equals(mProfileGenderFemale)) {
            return AccountInfo.ACCOUNT_GENDER_FEMALE;
        } else {
            return AccountInfo.ACCOUNT_GENDER_OTHER;
        }
    }
}
