package com.jchain.affiliate.Account.OAuth;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.jchain.affiliate.R;

public final class GooglePlayServiceOAuthHelper extends BaseOAuthHelper {
    private static final String TAG = "GoogleOAuthHelper";

    public GooglePlayServiceOAuthHelper(final Context context) {
        super(context);
    }

    @Override
    public void init() {
    }

    @Override
    public void destroy() {
    }

    private static final int RC_SIGN_IN = 8888;
    private GoogleSignInClient mGoogleSignInClient = null;
    @Override
    public void login(final Activity activity, OAuthResultListener listener) {
        super.login(activity, listener);
        final GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(mContext.getString(R.string.google_plus_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(activity, gso);
        if (hasLogin()) {
            mGoogleSignInClient.silentSignIn().addOnCompleteListener(new OnCompleteListener<GoogleSignInAccount>() {
                @Override
                public void onComplete(@NonNull Task<GoogleSignInAccount> task) {
                    if (!handleSignInResult(task, false)) {
                        mGoogleSignInClient.signOut();
                        requestNewLogin(activity);
                    } else {
                        notifyLoginResult(RESULT_SUCCESS);
                    }
                }
            });
        } else {
            requestNewLogin(activity);
        }
    }

    private void requestNewLogin(final Activity activity) {
        final Intent intent = mGoogleSignInClient.getSignInIntent();
        activity.startActivityForResult(intent, RC_SIGN_IN);
    }

    @Override
    public boolean hasLogin() {
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(mContext);
        return account != null;
    }

    @Override
    public boolean onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task, true);
            return true;
        }
        return false;
    }

    private boolean handleSignInResult(final Task<GoogleSignInAccount> completedTask, final boolean needNotify) {
        int result = RESULT_SUCCESS;
        try {
            completedTask.getResult(ApiException.class);
            return true;
        } catch(ApiException e) {
            Log.e(TAG, "Failed to sign in Google+", e);
            final int statusCode = e.getStatusCode();
            if (statusCode == Status.RESULT_TIMEOUT.getStatusCode()) {
                result = RESULT_FAILURE_NETWORK;
            } else if (statusCode == Status.RESULT_CANCELED.getStatusCode()
                    || statusCode == Status.RESULT_INTERRUPTED.getStatusCode()) {
                result = RESULT_FAILURE_REJECT;
            } else {
                result = RESULT_FAILURE_GENERAL;
            }
            return false;
        } finally {
            if (needNotify) {
                notifyLoginResult(result);
            }
        }
    }

    @Override
    public void logout() {
        if (mGoogleSignInClient != null) {
            mGoogleSignInClient.revokeAccess();
            mGoogleSignInClient.signOut();
            mGoogleSignInClient = null;
        }
        notifyLogoutResult(RESULT_SUCCESS);
        super.logout();
    }

    @Override
    public void getOAuthInfo(OAuthInfoListener resultListener) {
        super.getOAuthInfo(resultListener);
        final GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(mContext);
        if (account != null) {
            final OAuthInfo info = new OAuthInfo();
            info.type = OAuthInfo.TYPE_GOOGLE;
            info.id = account.getId();
            info.name = account.getDisplayName();
            info.email = account.getEmail();
            notifyGotInfo(info);
        } else {
            notifyGotInfo(null);
        }
    }

}
