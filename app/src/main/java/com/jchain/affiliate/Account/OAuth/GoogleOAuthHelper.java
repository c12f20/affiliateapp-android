package com.jchain.affiliate.Account.OAuth;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

public final class GoogleOAuthHelper extends BaseOAuthHelper {
    private static final String TAG = "GoogleOAuthHelper";

    public GoogleOAuthHelper(final Context context) {
        super(context);
    }

    private BaseOAuthHelper mOAuthHelper = null;
    @Override
    public void init() {
        if (isGooglPlayServiceAvailable()) {
            Log.d(TAG, "Use Google PlayService for OAuth");
            mOAuthHelper = new GooglePlayServiceOAuthHelper(mContext);
        } else {
            Log.e(TAG, "Can't Auth Google+ without Google PlayService");
        }
    }

    private int mErrorCode = ConnectionResult.SUCCESS;
    private boolean isGooglPlayServiceAvailable() {
        final GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        mErrorCode = googleApiAvailability.isGooglePlayServicesAvailable(mContext);
        return mErrorCode == ConnectionResult.SUCCESS;
    }

    @Override
    public void destroy() {
        if (mOAuthHelper != null) {
            mOAuthHelper.destroy();
        }
    }

    @Override
    public void login(Activity activity, OAuthResultListener listener) {
        if (mOAuthHelper != null) {
            mOAuthHelper.login(activity, listener);
        } else {
            super.login(activity, listener);
            showErrorDialog(activity);
            notifyLoginResult(BaseOAuthHelper.RESULT_FAILURE_GENERAL);
        }
    }

    private static final int ERROR_DIALOG_REQEST_CODE = 2000;
    private void showErrorDialog(final Activity activity) {
        final GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        final Dialog errorDialog = googleApiAvailability.getErrorDialog(activity, mErrorCode, ERROR_DIALOG_REQEST_CODE);
        errorDialog.show();
    }

    @Override
    public void logout() {
        if (mOAuthHelper != null) {
            mOAuthHelper.logout();
        }
    }

    @Override
    public boolean hasLogin() {
        if (mOAuthHelper != null) {
            return mOAuthHelper.hasLogin();
        }
        return false;
    }

    @Override
    public void getOAuthInfo(OAuthInfoListener resultListener) {
        if (mOAuthHelper != null) {
            mOAuthHelper.getOAuthInfo(resultListener);
        }
    }

    @Override
    public boolean onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (mOAuthHelper != null) {
            return mOAuthHelper.onActivityResult(requestCode, resultCode, data);
        }
        return false;
    }
}
