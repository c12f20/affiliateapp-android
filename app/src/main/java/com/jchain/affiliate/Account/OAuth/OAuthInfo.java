package com.jchain.affiliate.Account.OAuth;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Date;

public final class OAuthInfo {
    @Retention(RetentionPolicy.SOURCE)
    @IntDef({TYPE_FACEBOOK, TYPE_GOOGLE, TYPE_TWITTER})
    public @interface InfoType{}
    public static final int TYPE_FACEBOOK = 1;
    public static final int TYPE_GOOGLE = 2;
    public static final int TYPE_TWITTER = 3;

    public @InfoType int type = TYPE_FACEBOOK;
    public String id = null;
    public String name = null;
    public String gender = null;
    public String email = null;
    public Date birthday = null;
}
