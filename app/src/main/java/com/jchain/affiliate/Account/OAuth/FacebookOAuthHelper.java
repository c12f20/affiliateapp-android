package com.jchain.affiliate.Account.OAuth;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;
import java.util.TimeZone;

public final class FacebookOAuthHelper extends BaseOAuthHelper {
    private static final String TAG = "FacebookOAuthHelper";

    private CallbackManager mCallbackManager = null;
    private LoginManager mLoginManager = null;

    public FacebookOAuthHelper(final Context context) {
        super(context);
    }

    @Override
    public void init() {
        mCallbackManager = CallbackManager.Factory.create();
        mLoginManager = LoginManager.getInstance();
    }

    @Override
    public void destroy() {
        if (mLoginManager != null && mCallbackManager != null) {
            mLoginManager.unregisterCallback(mCallbackManager);
        }
    }

    @Override
    public void login(final Activity activity, final OAuthResultListener listener) {
        super.login(activity, listener);
        if (hasLogin()) {
            notifyLoginResult(RESULT_SUCCESS);
            return;
        }

        mLoginManager.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "Login Successfully");
                notifyLoginResult(RESULT_SUCCESS);
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "Cancelled by user");
                notifyLoginResult(RESULT_FAILURE_REJECT);
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "Login failure", error);
                notifyLoginResult(RESULT_FAILURE_NETWORK);
            }
        });
        final Collection<String> permissions = new ArrayList<String>();
        permissions.add("public_profile");
        permissions.add("email");
        permissions.add("user_gender");
        permissions.add("user_birthday");
        mLoginManager.logInWithReadPermissions(activity, permissions);
    }

    @Override
    public boolean hasLogin() {
        final AccessToken token = AccessToken.getCurrentAccessToken();
        return token != null;
    }

    @Override
    public boolean onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        return mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void logout() {
        mLoginManager.logOut();
        notifyLogoutResult(RESULT_SUCCESS);
        super.logout();
    }

    @Override
    public void getOAuthInfo(final OAuthInfoListener resultListener) {
        super.getOAuthInfo(resultListener);
        final AccessToken token = AccessToken.getCurrentAccessToken();
        final GraphRequest meRequest = GraphRequest.newMeRequest(token, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                if (object == null) {
                    Log.e(TAG, "Failed to get MeRequest, response: "+response.getRawResponse());
                    notifyGotInfo(null);
                    return;
                }
                final OAuthInfo info = parseInfo(object);
                notifyGotInfo(info);
            }
        });
        final Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,gender,birthday");
        parameters.putString("locale", "en_US");
        meRequest.setParameters(parameters);
        meRequest.executeAsync();
    }

    private OAuthInfo parseInfo(JSONObject object) {
        OAuthInfo info = new OAuthInfo();
        try {
            info.type = OAuthInfo.TYPE_FACEBOOK;
            info.id = object.getString("id");
            info.name = object.getString("name");
            info.email = object.getString("email");
            info.gender = object.getString("gender");
            final String birthday = object.getString("birthday");
            final SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            info.birthday = sdf.parse(birthday);
            return info;
        } catch (JSONException e) {
            Log.e(TAG, "Failed to parse result", e);
            return null;
        } catch (ParseException e) {
            Log.e(TAG, "Failed to parse birthday in result", e);
            return info;
        }
    }
}
